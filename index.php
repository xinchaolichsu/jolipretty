<?php

/**

 * VietEsoft

 * ============================================================================

 * Copyright 2010.

 * Website: http://www.vietesfot.com, http://thietkewebsitetrongoi.net

 * ----------------------------------------------------------------------------

 * Phat trien boi VietEsoft

 * ============================================================================

 * $Id: index.php 

*/

	define('IN_VES', true);

	session_start();									/* Khởi tạo session */

	ob_start();	

	ini_set('error_reporting',1);						/* Hiển thị lỗi */

	// error_reporting(E_ALL);									

	include_once("core/config.class.php");				/* Load config */

	include_once("core/core.php");						/* Load core */

	include_once("core/fckeditor.php");					/* Load fckeditor dùng cho frontend */

	include_once("core/DBFrontEnd.class.php");

	include_once("mailtest/class.phpmailer.php");

	include_once("mailtest/class.smtp.php");		

	

	/**

	*	Load Module

	*	Các module được gọi với ajax ngược lại sẽ gọi module layout

	**/

	if(isset($_REQUEST['ajax'])) {		

		loadModule($_GET['mod'], $_GET['task']);

	} else {

		loadModule("layout");

	}

?>