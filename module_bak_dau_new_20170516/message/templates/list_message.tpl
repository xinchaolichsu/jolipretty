{literal}
<script type="text/javascript">
function deleteMsg(id)
{
    if (r==true)
    {
        $.ajax({
            type: "POST",
            url: "{/literal}{$smarty.const.SITE_URL}{literal}index.php?mod=message&task=deleteMsg&ajax",
            data: "msgid="+id,
            success: function(respond){
            	alert('Tin nhắn của bạn đã được xóa!');
                location.href = '/message/';
            }
        });
    }
}
    </script>
		<style type="text/css">
		.td-msg-header{
		border-right: 1px solid #bac9b6;
		border-left: 1px solid #bac9b6;
		border-top: 1px solid #bac9b6;
		border-bottom: 1px solid #bac9b6;
		padding: 10px;
		background: #f6f9f0;
		padding-left: 15px;
		font-weight: normal;
		font-size:11px;
	}
	.no-read{
		background: #FFFFFF;
	}
	.td-msg{
		border-right: 1px solid #bac9b6;
		border-left: 1px solid #bac9b6;
		border-bottom: 1px solid #bac9b6;
		padding-left: 15px;
		font-weight: normal;
		font-size:11px;
		padding: 10px;
	}
	.td-msg a{
		font-weight: bold;
		color: #000066;
		text-decoration: none;
	}
	</style>

{/literal}
<div class="box_center1" style="margin-left:0;">
	<div class="box_center_title1">{#inbox#}</div>
    <div class="box_center_content1">
        <table width="100%" cellspacing="0" cellpadding="5">
            <tr>
                <td class="td-msg-header" width="11%">{#from_name#}</td>
                <td class="td-msg-header">{#subject#}</td>
                <td class="td-msg-header">Voucher code</td>
                <td class="td-msg-header" width="11%">Sale off</td>
                <td class="td-msg-header" width="12%">Start date</td>
                <td class="td-msg-header" width="12%">End date</td>
                <td class="td-msg-header no-boder" width="11%">{#tool#}</td>
            </tr>
            {foreach from = $msg item = item name = item}
                <tr style="cursor: pointer;" {if !$item.IsRead}class="no-read"{/if}>
                    <td class="td-msg">{if $item.FromName}{$item.FromName}{else}Admin{/if}</td>
                    <td class="td-msg"><a href="/message/{$item.id}/{$item.Subject|remove_marks}.html">{$item.Subject}</a></td>
                    <td class="td-msg">{$item.Code}</td>
                    <td class="td-msg">{$item.Sale} %</td>
                    <td class="td-msg">{$item.BeginDate|date_format:"%d/%m/%Y %H:%M"}</td>
                    <td class="td-msg">{$item.EndDate|date_format:"%d/%m/%Y %H:%M"}</td>
                    <td class="td-msg no-boder" align="center">
                        {*<a href="/message/reply/{$item.id}/"><img src="/view/images/icon/reply.jpg" /></a>*}
                        <img src="/view/images/icon/delete.jpg" style="cursor: pointer;" onclick="deleteMsg({$item.id});" />
                    </td>
                </tr>
            {/foreach}
        </table>
    </div>
</div>