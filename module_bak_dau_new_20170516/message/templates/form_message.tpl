{literal}
<script type="text/javascript">
function sendMsg()
{
    var ToName = $("#ToName").val();
    var Subject = $("#Subject").val();
    var Content = $("#Content").val();
    //alert("Content="+Content+"&ToName="+ToName+"&Subject="+Subject);
    $.ajax({
        type: "POST",
        url: "{/literal}{$smarty.const.SITE_URL}{literal}index.php?mod=message&task=sendMsg&ajax",
        data: "Content="+Content+"&ToName="+ToName+"&Subject="+Subject,
        success: function(respond){
        	$("#containerMsg").html(respond);
        }
    });
}
</script>
{/literal}
<div class="box_center">
	<div class="box_center_title">Hộp tin nhắn</div>
    <div class="box_center_content" style="background: #FFFFFF;" id="containerMsg">
        <div style="padding-left: 10px; line-height: 30px;">
            <b>Người nhận</b> : <input type="text" id="ToName" name="ToName" style="width: 180px; background: #f6f9f0; border: 1px solid #d2cecf;" value="{$data.FromName}" {if $data.FromName}readonly="readonly"{/if} />
            <b>Tiêu đề</b> : <input type="text" id="Subject" name="Subject" style="width: 440px; background: #f6f9f0; border: 1px solid #d2cecf;"  value="Trả lời : {$data.Subject}" /><br />
            <b>Nội dung</b> : <br />
            <textarea name="Content" id="Content" style="height:100px; width:750px; background: #f6f9f0; font-family:Arial; font-size:12px; border: 1px solid #d2cecf;"></textarea>
            <div style="clear: both;"></div>
            <img onclick="sendMsg()" src="/view/images/datht/send-msg.jpg" style="float: right; cursor: pointer; margin-right: 20px; margin-top: 10px;" />
        </div>
    </div>
</div>