<?php
/**
 * VietEsoft
 * ===============================================================================================
 * Copyright 2010.
 * Website: http://www.vietesfot.com, http://thietkewebsitetrongoi.net
 * -----------------------------------------------------------------------------------------------
 * Phát triển bới VietEsoft
 * -----------------------------------------------------------------------------------------------
 * Chú ý: Tất cả các hàm, phương thức được viết mới do mình tự định nghĩa phải để lại comment gồm:
 	- Người viết
	- Ngày-tháng-năm
	- Chức năng
 * ===============================================================================================
 * Hiển thị tin tức
*/
if (!defined('IN_VES')) die('Hacking attempt');
class message extends VES_FrontEnd 
{	
	var $table; 		/* Khai báo bảng dữ liệu */
	var $mod;			/* Khai báo mod */
	var $id;			/* Khai báo id */
	var $LangID;		/* Khai báo id ngôn ngữ */
	var $field;			/* Khai báo trường dữ liệu */
	var $pre_table;		/* Khai báo bảng danh mục dữ liệu */	
	function __construct(){		
		$this -> table = 'tbl_message';
		$this -> id = mysql_real_escape_string(($_REQUEST['id']=='')?'0':$_REQUEST['id']);
		$this -> field = "*";
		$this -> LangID = $_SESSION['lang_id'];
		parent::__construct($this->table);
	}
	function run($task= "")
	{
		if($task==''){$task = $_GET['task'];}
	    switch($task)
			{
				case 'details':
					$this -> details();
				break;
                case 'create':
                    $this -> createMsg();
                break;
                case 'sendMsg':
                    $this -> sendMsg();
                break;
                case 'reply':
                    $this -> replyMsg();
                break;
                case 'deleteMsg':
                    $this -> deleteMsg();
                break;
				case 'toMsg':
                    $this -> defaultPage();
                break;
				case 'fromMsg':
                    $this -> defaultPage();
                break;
				default:
					$this->defaultPage();
				break;	
			}
	}
	/**
	 * Page info
	 *
	 * @param $task
	 */
	function getPageinfo($task= "")
	{
	 	global $oSmarty;	
		
		switch ($task)
		{
			case "toMsg":
				$aPageinfo=array('title'=>"Message", 'keyword'=>'', 'description'=>'');
				$aPath = array(array("link"=> '', "path"=>$oSmarty->get_config_vars("login")));
				break;
			case "details":
				$aPageinfo=array('title'=>"Message", 'keyword'=>'', 'description'=>'');
				$aPath = array(array("link"=> '', "path"=>$oSmarty->get_config_vars("login")));
				break;
			default:
				$aPageinfo=array('title'=>"Message", 'keyword'=>'', 'description'=>'');
				$aPath = array(array("link"=> '', "path"=>$oSmarty->get_config_vars("login")));
				break;
		}
		$oSmarty->assign('aPageinfo', $aPageinfo);
		$oSmarty->assign("aPath", $aPath);
	}
	
    function defaultPage()
    {
        global $oSmarty, $oDb;
		if ( $_SESSION["userid_cus"] == "")
        {
            $url = SITE_URL."index.php?mod=customer&task=login";
            echo "<script language = 'javascript'>location.href = '".$url."'</script>";
            exit();
        }
		$where = " ToID={$_SESSION['userid_cus']} OR FromID={$_SESSION['userid_cus']} ";
		if($_GET['task']=='toMsg')
			$where = "ToID={$_SESSION['userid_cus']}";
		if($_GET['task']=='fromMsg')
			$where = "FromID={$_SESSION['userid_cus']}";
        $sql = "SELECT *,(SELECT username FROM tbl_customer WHERE id = FromID) as FromName,(SELECT username FROM tbl_customer WHERE id = ToID) as ToName FROM tbl_message WHERE {$where} ORDER BY CreateDate DESC";
        $msg = $oDb->getAll($sql);
        $oSmarty->assign("msg", $msg);
        $oSmarty->display("list_message.tpl");
    }
    function details()
    {
        global $oSmarty, $oDb;
		if ( $_SESSION["userid_cus"] == "")
        {
            $url = SITE_URL."index.php?mod=customer&task=login";
            echo "<script language = 'javascript'>location.href = '".$url."'</script>";
            exit();
        }
        $sql = "SELECT *,(SELECT username FROM tbl_customer WHERE id = FromID) as FromName,(SELECT username FROM tbl_customer WHERE id = ToID) as ToName FROM tbl_message WHERE id={$_GET['id']}";
        $msg = $oDb->getRow($sql);
        $msg['Content'] = nl2br($msg['Content']);
        if($msg['ToID'] == $_SESSION["userid_cus"])
        {
            $aData  = array(
                "IsRead"     => 1,
                );
            $res_insert = $oDb -> autoExecute("tbl_message", $aData, DB_AUTOQUERY_UPDATE, "id='".$_GET["id"]."'");
            $oSmarty->assign("msg", $msg);
            $oSmarty->display("detail_message.tpl");
        }else
            echo $this->get_config_vars('access_denied');
    }
    function createMsg()
    {
        global $oSmarty, $oDb;
		if ( $_SESSION["userid_cus"] == "")
        {
            $url = SITE_URL."index.php?mod=customer&task=login";
            echo "<script language = 'javascript'>location.href = '".$url."'</script>";
            exit();
        }
		if(isset($_GET['id']) && $_GET['id']!=0)
		{
		$sql = "SELECT username FROM tbl_customer WHERE id={$_GET['id']}";
        $toname = $oDb->getOne($sql);
		$data = array("FromName"=>$toname);
		$oSmarty->assign("data", $data);
		}
        $oSmarty->display("form_message.tpl");
    }
    function sendMsg()
    {
        global $oSmarty, $oDb;
        if ( $_SESSION["userid_cus"] == "")
        {
            $url = SITE_URL."index.php?mod=customer&task=login";
            echo "<script language = 'javascript'>location.href = '".$url."'</script>";
            exit();
        }
        $sql = "SELECT id FROM tbl_customer WHERE username='{$_POST['ToName']}'";
        $toID = $oDb->getOne($sql);
		if(!$toID)
			$toID=0;
        $aData  = array(
                "IsRead"     => 0,
                "ToID"         => $toID,
                "FromID"         => $_SESSION["userid_cus"],
                "Subject"         => $_POST['Subject'],
                "CreateDate"         => date("Y-m-d H:i"),
                "Content"             => $_POST['Content'],
                );
        $res_insert = $oDb -> autoExecute("tbl_message", $aData, DB_AUTOQUERY_INSERT);
        $url = "/message/";
        echo "<script language = 'javascript'>
        alert('Tin nhắn của bạn đã được gửi đi!');
        location.href = '".$url."'</script>";
    }
    function replyMsg()
    {
        global $oSmarty, $oDb;
		if ( $_SESSION["userid_cus"] == "")
        {
            $url = SITE_URL."index.php?mod=customer&task=login";
            echo "<script language = 'javascript'>location.href = '".$url."'</script>";
            exit();
        }
        $sql = "SELECT *,(SELECT username FROM tbl_customer WHERE id = FromID) as FromName,(SELECT username FROM tbl_customer WHERE id = ToID) as ToName FROM tbl_message WHERE id={$_GET['id']}";
        $data = $oDb->getRow($sql);
        $oSmarty->assign("data", $data);
        $oSmarty->display("form_message.tpl");
    }
    function deleteMsg()
    {
        global $oSmarty, $oDb;
        if ( $_SESSION["userid_cus"] == "")
        {
            $url = SITE_URL."index.php?mod=customer&task=login";
            echo "<script language = 'javascript'>location.href = '".$url."'</script>";
            exit();
        }
        $id = $_POST['msgid'];
        $sql = "SELECT * FROM tbl_message WHERE id='$id'";
        $msg = $oDb->getRow($sql);
        if($msg['ToID'] == $_SESSION["userid_cus"])
        {
            $oDb->query("DELETE FROM tbl_message WHERE id='$id'");
        }else
            echo $this->get_config_vars('access_denied');
        
    }
}
?>