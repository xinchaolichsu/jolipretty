<?php 
/**
* Create garung - 30102016 class for homepage
*/
if (!defined('IN_VES')) die('Hacking attempt');
class homepage extends VES_FrontEnd 
{
	var $table;
	var $mod;
    var $url_mod;
	var $id;
	var $LangID;
	var $field;
	var $pre_table;
	function __construct(){		
		$this->table = 'tbl_product_item';
		$this->pre_table = "tbl_product_category";
		$this->mod = 'product';
        $this->url_mod = "product";
		$this->id = mysql_real_escape_string(($_REQUEST['id']=='')?'0':$_REQUEST['id']);
		$this->field = "*";
		$this->LangID = $_SESSION['lang_id'];
		parent::__construct($this->table);
	}
	public function run($task= "")
	{
		global $oSmarty, $oDb;
		// Load file ngôn ngữ
		$oSmarty->config_load($_SESSION['lang_file']);
		if (MULTI_LANGUAGE) {
			$where = " AND LangID={$_SESSION['lang_id']} ";
		} else {
			$where = '';
		}
		$sql = "SELECT * FROM {$this->table} WHERE Is_new=1 {$where} ORDER BY Ordering ASC"; 
		$new_items = $oDb->getAll($sql);
		$oSmarty->assign("new_items",$new_items);

		$sql = "SELECT * FROM {$this->table} WHERE Price > 0 AND Status = 1 {$where} ORDER BY ViewNumber DESC LIMIT 10"; 
		$views_more = $oDb->getAll($sql);
		$oSmarty->assign("views_more",$views_more);

		$oSmarty->display("homepage.tpl");
	}
}