{literal}
<style>
      html, body, #map {
        height: 100%;
        margin: 0px;
        padding: 0px
      }
      #panel {
        position: absolute;
        top: 5px;
        left: 50%;
        margin-left: -180px;
        z-index: 5;
        background-color: #fff;
        padding: 5px;
        border: 1px solid #999;
      }
    </style>
    <style>
      #directions-panel {
        height: 100%;
        width: 390px;
        overflow: auto;
		position:absolute;
		top:100px;
		left:650px;
      }
   
      #control {
        background: #fff;
        padding: 5px;
        font-size: 14px;
        font-family: Arial;
        border: 1px solid #ccc;
        box-shadow: 0 2px 2px rgba(33, 33, 33, 0.4);
        display: none;
      }

      @media print {
        #map {
          height: 500px;
          margin: 0;
        }
      }
    </style>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script type="text/javascript">
var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
var map;
function initialize() {
  directionsDisplay = new google.maps.DirectionsRenderer();
  var latitude_pos = document.getElementById('latitude').value;
  var latitude_pos2 = document.getElementById('latitude2').value;
  var addr_map = document.getElementById('name_cat').value;
  var chicago = new google.maps.LatLng(latitude_pos,latitude_pos2);
  var mapOptions = {
	zoom:12,
	mapTypeId: google.maps.MapTypeId.ROADMAP,
	center: chicago
  }
  map = new google.maps.Map(document.getElementById('map'), mapOptions);
  directionsDisplay.setMap(map);
  directionsDisplay.setPanel(document.getElementById('directions-panel'));

  var control = document.getElementById('control');
  control.style.display = 'block';
  map.controls[google.maps.ControlPosition.TOP_CENTER].push(control);

  var marker=new google.maps.Marker({
	  position:chicago,
	  });
	marker.setMap(map);
	var infowindow = new google.maps.InfoWindow({
  content: addr_map
  });

google.maps.event.addListener(marker, 'click', function() {
  infowindow.open(map,marker);
  });
}

function calcRoute() {
  var start = document.getElementById('txtAddress_begin').value;
  var end = document.getElementById('txtAddress_end').value;
  var request = {
	  origin:start,
	  destination:end,
	  travelMode: google.maps.DirectionsTravelMode.DRIVING
  };
  directionsService.route(request, function(response, status) {
	if (status == google.maps.DirectionsStatus.OK) {
	  directionsDisplay.setDirections(response);
	}
  });
}
google.maps.event.addDomListener(window, 'load', initialize);
</script>
{/literal}
<table align="left" width="100%" cellpadding="0" cellspacing="0" border="0" style="text-transform:none;">
	<tr>
		<td colspan="2"><div id="control"></td>
	</tr>
	<tr>
			<td align="center" colspan="2">Đi từ: <input type="text" size="80" id="txtAddress_begin" name="from" style="height:25px; margin-bottom:3px"/>
	</td></tr>
	<tr>
			<td align="center" colspan="2">Đến: &nbsp;<input type="text" size="80" id="txtAddress_end" name="from" value="{$place.Address}"  style="height:25px; margin-bottom:3px"/></td>
	</tr>
	<tr>
			<td align="center" colspan="2"><input  type="button" value="Tìm đường" onclick="calcRoute();" style="margin-bottom:3px;"/>
							   <input type="hidden" id="name_cat" value="{$place.Name}" />
							   <input type="hidden" id="latitude" value="{$place.Latitude}" />
							   <input type="hidden" id="latitude2" value="{$place.Latitude2}" />
			</td>
	</tr>
	<tr>
		<td align="center" colspan="2">
				<div style="background:#EEEEEE; height:20px;width:100%; float:left; font-weight:bold;padding-top:5px">Bản đồ</div></div>
		</td>
	</tr>
	<tr>
		<td colspan="2">
		<div id="map" style="width:650px;height:560px"></div>
		<div id="directions-panel" style="width:350px;height:560px"></div>
		</td>
	</tr>
</table>