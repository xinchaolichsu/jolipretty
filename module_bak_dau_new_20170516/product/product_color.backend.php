<?php
/**
 * VietEsoft
 * ===============================================================================================
 * Copyright 2010.
 * Website: http://www.vietesfot.com, http://thietkewebsitetrongoi.net
 * -----------------------------------------------------------------------------------------------
 * Phát triển bới VietEsoft
 * -----------------------------------------------------------------------------------------------
 * Chú ý: Tất cả các hàm, phương thức được viết mới do mình tự định nghĩa phải để lại comment gồm:
 	- Người viết
	- Ngày-tháng-năm
	- Chức năng
 * ===============================================================================================
 * Quản lý màu sắc
*/
if (!defined('IN_VES')) die('Hacking attempt');
class product_colorBackEnd extends Bsg_Module_Base
{
	var $smarty;			/* Khai báo Smarty */
	var $db;				/* Khai báo db */
    var $datagrid;			/* Khai báo datagrid */
	var $id;				/* Khai báo id */
	var $imgPath;			/* Khai báo đường dẫn ảnh */
	var $imgPathShort;		/* Khai báo đường dẫn ảnh tương đối */
	var $table='';			/* Khai báo bảng dữ liệu */
	
	public function __construct($oSmarty, $oDb, $oDatagrid)
	{	
		$this -> smarty = $oSmarty;
		$this -> db = $oDb;		
        $this -> datagrid = $oDatagrid;
		$this -> id=$_REQUEST[id];		
		$this -> table = "tbl_product_color";	 		
		parent::__construct($oDb);		
		$this -> bsgDb -> setTable($this->table);
		$this->imgPath = SITE_DIR."upload/product/";
		$this->imgPathShort = "/upload/product/";
	}
	
	function run($task)
	{	
		switch( $task ){
			/* Thêm mới 1 bản ghi */
            case 'add':
				$this -> addItem();
				break;
			/* Edit 1 bản ghi được chọn */
			case 'edit':
				$this -> editItem();
				break;
			/* Xóa 1 bản ghi được chọn */
			case 'delete':
				$this -> deleteItem();
				break;
			/* Xóa nhiều bản ghi được chọn */
			case 'delete_all':
				$this -> deleteItems();
				break;
			/* Hiển thị select box filter */
            case 'ajax_filter':
                $this -> ajax_filter();
                break;
			/* Hiển thị tất cả danh sách bản ghi theo datagrid */
			default:					
				$this -> listItem( $_GET['msg'] );		
				break;
		}
	}
	
	/**
	 * Page info
	 *
	 */
	function getPageInfo()
	{
		return true;
	}

	/**
	 * Thêm mới 1 bản ghi
	 * Form thêm mới
	 */
	function addItem()
	{
		$this -> getPath("Quản lý kích thước > Quản lý màu sắc > Thêm mới màu sắc");		
		$this -> buildForm();
	}
	
	/**
	 * Edit 1 bản ghi
	 * Form edit với dữ liệu một bản ghi được chọn
	 */
	function editItem()
	{
		$id = $_GET['id'];
		$this -> getPath("Quản lý kích thước > Quản lý màu sắc > Sửa màu sắc");	
		$row = $this -> bsgDb -> getRow( $id );
		$this -> buildForm( $row );
	}
	
	/**
	 * Kiểm tra màu sắc con hoặc bản ghi thuộc màu sắc
	 * Return bool
	 */
	function hasSubItem($catID){
		global $oDb;
		//$sub = $oDb->getOne("SELECT count(*) FROM {$this->table} WHERE ParentID = {$catID}");
		//$item = $oDb->getOne("SELECT count(*) FROM tbl_product_item WHERE CatID = {$catID}");
		
		if ($sub>0 || $item>0)
			return true;
		else
			return false;
	}
	
	/**
	 * Xóa 1 bản ghi được chọn
	 * Hiển thị danh sách bản ghi với bản ghi đã xóa	
	 */
	function deleteItem()
	{
		global  $oDb;
		$id = $_GET["id"];
		if (!$this->hasSubItem($id)) {
			$this -> bsgDb -> deleteWithPk( $id );
			$msg = "Xóa màu sắc thành công!";
		}
		else 	
			$msg = "Không thể xóa màu sắc này vì còn chứa màu sắc con";
		$this -> listItem( $msg );
	}
	
	
	/**
	 * Xóa nhiều bản ghi
	 * 
	 */
	function deleteItems()
	{
		$aItems	 = $_GET['arr_check'];
		$del = true;
		if(is_array( $aItems) && count( $aItems) > 0){
			$sItems = implode( ',', $aItems );
			if (!$this->hasSubItem($sItems)) {
				$this -> bsgDb -> deleteWithPk( $sItems );				
			}
			else 
				$del = false;
		}
		if ($del)
			$msg = "Xóa (các) màu sắc thành công!";
		else 
			$msg = "Không thể xóa toàn bộ màu sắc vì còn chứa màu sắc con hoặc tin bài";
		$this -> listItem( $msg );
	}
	
	/**
	 * Form
	 * $data
	 */
	function buildForm( $data=array() , $msg = ''){
		global $oDb;
                
		$form = new HTML_QuickForm('frmAdmin','post',$_COOKIE['re_dir']."&task=".$_GET['task'], '', "style='padding:10px 15px 0 20px;'");
		
		$form -> setDefaults($data);
		
		$form -> addElement('text', 'Name', 'Tên màu:', array('size' => 50, 'maxlength' => 255));
		
		$btn_group[] = $form -> createElement('submit',null,'Hoàn tất',array("style"=> "border:1px solid gray; padding:0 5px 0 5px;"));		
        $btn_group[] = $form -> createElement('button',null,'Quay lại',array('onclick'=>'window.location.href = \''.$_COOKIE['re_dir'].'\'', "style"=> "border:1px solid gray;"));      
        $form -> addGroup($btn_group);
      
		$form->addElement('hidden', 'id', $data['id']);		
		
        $form -> addRule('Name','Tên màu không được để trống','required',null,'client');
		
		if( $form -> validate())
		{			
			$aData  = array(
				"Name" 	=> $_POST['Name']
			);
			
			if( !$_POST['id'] ){
				
				 $id = $this -> bsgDb -> insert($aData);				 
				 $msg = "Thêm màu sắc kích thước thành công! ";
			}else {
				$id = $_POST['id'];				
				$this -> bsgDb -> updateWithPk($id, $aData);
				$msg = "Chỉnh sửa màu sắc kích thước dành công ";
			}
			
			$this -> redirect($_COOKIE['re_dir']. "&msg={$msg}");
		}
		
		$form->display();
	}
	
	/**
	 * Xóa hình ảnh của 1 record
	 * $str_id
	 */
	function deleteImage($str_id, $field, $path){
		if($str_id == '')
			return;
		$arr_id = explode(',', $str_id);
		foreach($arr_id as $id){
			$imgpath = $path.$this->db->getOne("SELECT {$field} FROM ".$this->table." WHERE id = {$id}");
			if(is_file($imgpath))			
				@unlink($imgpath);
		}
	}
	
	/**
	 * List tất cả các bản ghi
	 * 
	 */
	function listItem( $sMsg= '' )
	{		
		global $oDb;
		global $oDatagrid;				
		
		$root_path = "Quản lý sản phẩm > Danh sách sản phẩm";						
		$submit_url= "/index.php?".$_SERVER['QUERY_STRING'];
		
		$table = $this -> table;
		
		$arr_filter= array(
			array(
				'field' => 'Name',
				'display' => 'Tên màu sắc',
				'type' => 'text',                
				'name' => 'filter_title',
				'selected' => $_REQUEST['filter_title'],
				'filterable' => true
			)			
		); 
		$arr_cols= array(
            array(
                "field" => "Name",
                "display" => "Tên màu sắc",
                'link' => SITE_URL.'index.php?mod=admin&amod='.$_GET['amod'].'&atask='.$_GET['atask'].'&tab=1&frame&task=edit',
                "align"    => 'left',                
                "datatype" => "text",
                "sortable" => true
            ),
        );
		$arr_cols = array_merge($arr_cols);
		
		$arr_check = array(
			array(
				"task" => "delete_all",
				"confirm"	=> "Xác nhận xóa?",
				"display" => "Xóa"
			),
			array(
				"task" => "public_all",
				"confirm"	=> "Xác nhận thay đổi trạng thái?",
				"display" => "Kích hoạt"
			),
			array(
				"task" => "unpublic_all",
				"confirm"	=> "Xác nhận thay đổi trạng thái?",
				"display" => "Vô hiệu"
			)			
		);
		if( $sMsg )
			$oDatagrid -> setMessage( $sMsg );
		$oDatagrid->display_datagrid($table, $arr_cols, $arr_filter, $submit_url, $this -> getAct() ,120, $root_path, false ,$this->getActCheck());	
		
	}		
	
}

?>