<?php
class article_traloiBackEnd extends Bsg_Module_Base{
	
	var $smarty;
	var $db;
	var $url="";
    var $datagrid;
	var $id;
	var $imgPath;
	var $imgPathShort;
	var $table='';
	var $arrAction;
	
	public function __construct($oSmarty, $oDb, $oDatagrid) {	
		$this -> smarty = $oSmarty;
		$this -> db = $oDb;		
        $this -> datagrid = $oDatagrid;
		$this -> id=$_REQUEST[id];		
		$this -> table	="tbl_article_traloi";	 		
		parent::__construct($oDb);		
		$this->bsgDb->setTable($this->table);
		$this->imgPath = SITE_DIR."upload/article/";
		$this->imgPathShort = "/upload/article/";
	}
	
	function run($task)
	{	
		switch( $task ){
			case 'add':
				$this -> addItem();
				break;
			case 'edit':
				$this -> editItem();
				break;
			case 'delete':
				$this -> deleteItem();
				break;
			case 'delete_all':
				$this -> deleteItems();
				break;
			case 'change_status':				
				$this -> changeStatus($_GET['id'], $_GET['status']);
				break;
			case 'public_all':						
				$this -> changeStatusMultiple( 1 );
				break;
			case 'unpublic_all':						
				$this -> changeStatusMultiple( 0 );
				break;
			case 'save_order':
				$this -> saveOrder();
				break;
			default:					
				$this -> listItem( $_GET['msg'] );		
				break;
		}
	}
	
	function getPageInfo(){
		return true;
	}

	function addItem(){
		$this -> getPath("Quản lý comment > Thêm mới comment");		
		$this -> buildForm();
	}
	
	function editItem(){
		$id = $_GET['id'];
		$this -> getPath("Quản lý comment > Chỉnh sửa comment");	
		$row = $this -> bsgDb -> getRow( $id );
		$this -> buildForm( $row );
	}
	
	function deleteItem(){
		global  $oDb;
		$id = $_GET["id"];
		$this -> bsgDb -> deleteWithPk( $id );
		$msg = "Xóa comment thành công!";
		$this -> listItem( $msg );
	}
	
	function deleteItems()
	{
		$aItems	 = $_GET['arr_check'];
		if(is_array( $aItems) && count( $aItems) > 0){
			$sItems = implode( ',', $aItems );
			$this -> bsgDb -> deleteWithPk( $sItems );
		}
		$msg = "Xóa (các) comment thành công!";
		$this -> listItem( $msg );
	}
	
	function changeStatusMultiple( $status = 0 )
	{
		$aItems	 = $_GET['arr_check'];
		if(is_array( $aItems) && count( $aItems) > 0){
			$sItems = implode( ',', $aItems );
			$this -> bsgDb -> updateWithPk( $sItems, array("Status" => $status) );
		}
		$msg = "Sửa trạng thái thành công!";
		$this -> listItem( $msg );
	}
	
	function buildForm( $data=array() , $msg = ''){
		
		$form = new HTML_QuickForm('frmAdmin','post',$_COOKIE['re_dir']."&task=".$_GET['task'], '', "style='padding:10px 15px 0 20px;'");
		
		$form -> setDefaults($data);
	/*
		if (multiLang()) {
			$lang = parent::loadLang();
			$form -> addElement('select', 'LangID', 'Ngôn ngữ', $lang);		
		}	*/
		
		//$form -> addElement('text', 'Title', 'Tiêu đề', array('size' => 50, 'maxlength' => 255));
		$form -> addElement('text', 'Name', 'Tên', array('size' => 50, 'maxlength' => 255));
	//	$form -> addElement('text', 'Email', 'Email', array('size' => 50, 'maxlength' => 255));
	//	$form -> addElement('text', 'Phone', 'Phone', array('size' => 50, 'maxlength' => 255));
		
		/*$form->addElement('file', 'Photo', 'Ảnh');
		
		if($_GET['task']=='edit')
			$form->addElement('static', null, '',"<a href='".SITE_URL.$data['Photo']."' onclick='return hs.expand(this)' class='highslide'><img src='".SITE_URL.$data['Photo']."' width=100 hight=100 border=0></a>");*/
	//	$form->addElement('textarea','Summarise','Mô tả vắn tắt',array('style'=>'width:600px; height:200px;'));	
		$content_editor=parent::editor('Content',$data['Content'],array('width'=>'800', 'height'=>'400'));
        $form -> addElement('static',NULL,'Nội dung',$content_editor);
       // $form -> addElement('checkbox', 'ShowHome', 'Hiển thị trang chủ');
	   $date_time = parent::date_time('CreateDate',($data['CreateDate']) ? ($data['CreateDate']) : date("Y-m-d h:i"));			
		$form -> addElement('static',NULL,'Ngày tạo',$date_time);
		$form -> addElement('checkbox', 'Status', 'Kích hoạt');
		
		$btn_group[] = $form -> createElement('submit',null,'Hoàn tất',array("style"=> "border:1px solid gray; padding:0 5px 0 5px;"));		
        $btn_group[] = $form -> createElement('button',null,'Quay lại',array('onclick'=>'window.location.href = \''.$_COOKIE['re_dir'].'\'', "style"=> "border:1px solid gray;"));      
        $form -> addGroup($btn_group);
      
		$form->addElement('hidden', 'id', $data['id']);		
		
        $form -> addRule('Name','Tiêu đề không được để trống','required',null,'client');
		
		if( $form -> validate())
		{
			/*if($_FILES['Photo']['name']!='')
			{
				$this->deleteImage($this->id, "Photo", $this->imgPath);
				$FileName = rand().'_'.$_FILES['Photo']['name'];												
				parent::crop_image($_FILES['Photo']['tmp_name'],$FileName, $this->imgPath);				
				$_POST['Photo'] = $this->imgPathShort.$FileName;	
			}*/
			
			$aData  = array(
			//	"LangID"	=> $_POST['LangID'],
				"Name" 		=> $_POST['Name'],
				//"Title" 		=> $_POST['Title'],
				//"Email" 		=> $_POST['Email'],
			//	"Phone" 		=> $_POST['Phone'],
			//	"Summarise" => $_POST['Summarise'],
				"Content" 	=> $_POST['Content'],
				//"ShowHome" 	=> $_POST['ShowHome'],
			 	"CreateDate"  => date("d-m-Y"),
				//"CreateUser"  => 0,
				"Status" 	=> $_POST['Status'],
			);
			//if ($_POST['Photo']!='')
				///$aData['Photo'] = $_POST['Photo'];
				
			if( !$_POST['id'] ){
				
				 $id = $this -> bsgDb -> insert($aData);				 
				 $msg = "Thêm comment thành công! ";
			}else {
				$id = $_POST['id'];				
				$this -> bsgDb -> updateWithPk($id, $aData);
				$msg = "Chỉnh comment thành công ";
			}
			if ($_POST['ShowHome'])
				 $this->setShowHome($id);
			$this -> redirect($_COOKIE['re_dir']. "&msg={$msg}");
		}
		
		$form->display();
	}
	function setShowHome($id)
	{
		global $oDb;
		$lang=$_POST['LangID'];
		$oDb->query("UPDATE {$this->table} SET ShowHome = 0 WHERE LangID={$lang}");
		$oDb->query("UPDATE {$this->table} SET ShowHome = 1 WHERE id={$id}");
	}
	function deleteImage($id, $field, $path){
		if($id == '')
			return;
		$imgpath = $path.$this->db->getOne("SELECT $field FROM ".$this->table." WHERE id = $id");
		if(is_file($imgpath))			
			@unlink($imgpath);
	}
	
	function changeStatus( $itemId , $status ){
		$aData = array( 'Status' => $status );
		$this -> bsgDb -> updateWithPk( $itemId, $aData );
		//return true;
		$msg = "Sửa trạng thái thành công!";
		$this -> listItem( $msg );
	}
	
	function listItem( $sMsg= '' )
	{		
		global $oDb;
		global $oDatagrid;				
		
		$root_path = "Quản lý comment > Danh sách comment";						
		$submit_url= "/index.php?".$_SERVER['QUERY_STRING'];
		
		$table = $this -> table;
		
		$arr_filter= array(
			/*array(
				'field' => 'Name',
				'display' => 'Tên',
				'type' => 'text',
				'name' => 'filter_name',
				'selected' => $_REQUEST['filter_name'],
				'filterable' => true
			),*/
			/*array(
				'field' => 'Title',
				'display' => 'Tiêu đề',
				'type' => 'text',
				'name' => 'filter_title',
				'selected' => $_REQUEST['filter_title'],
				'filterable' => true
			),*/
			array(
				'field' => 'Status',
				'display' => 'Trạng thái',				
				'name' => 'filter_show',
				'selected' => $_REQUEST['filter_show'],
				'options' => array('Vô hiệu','Kích hoạt'),
				'filterable' => true
			)			
		);
		
		$arr_cols= array(			
			array(
				"field" 		=> "id",					
				"primary_key" 	=>true,
				"display" 		=> "Id",				
				"align" 		=> "center",
				"sortable" 		=> true
			),	
		
			array(
				"field" 	=> "Name",
				"display" 	=> "Họ Tên",
				"align"		=> 'left',				
				"datatype" 	=> "text",
				"sortable" 	=> true
			),
			array(
				"field" 	=> "FaqID",
				"display" 	=> "Tên SP",
				"sql" 	=> "SELECT Name FROM tbl_product_item WHERE id=FaqID",
				"align"		=> 'left',				
				"datatype" 	=> "text",
				"sortable" 	=> true
			),
			
			array(
				"field" 	=> "CreateDate",
				"display" 	=> "Ngày gửi",
				"align"		=> 'left',				
				"datatype" 	=> "text",
				"sortable" 	=> true
			),
		/*	array(
				"field" 	=> "LangID",
				"display" 	=> "Ngôn ngữ",
				"sql"		=> "SELECT name FROM tbl_sys_lang WHERE id = LangID",
				"align"		=> 'left',				
				"datatype" 	=> "text",
				"sortable" 	=> true
			),*/
			/*array(
				"field" 	=> "CreateUser",
				"display" 	=> "Tên thành viên",
				"sql"		=> "SELECT username FROM tbl_customer WHERE id = CreateUser",
				"align"		=> 'left',				
				"datatype" 	=> "text",
				"sortable" 	=> true
			),*/
		
			array(
				"field" 	=> "Status",
				"display" 	=> "Trạng thái",				
				"datatype" 	=> "publish",
				"sortable" 	=> true
			)
			
		); 
		
		
		$arr_check = array(
			array(
				"task" 		=> "delete_all",
				"confirm"	=> "Xác nhận xóa?",
				"display" 	=> "Xóa"
			),
			array(
				"task" 		=> "public_all",
				"confirm"	=> "Xác nhận thay đổi trạng thái?",
				"display" 	=> "Kích hoạt"
			),
			array(
				"task" 		=> "unpublic_all",
				"confirm"	=> "Xác nhận thay đổi trạng thái?",
				"display" 	=> "Vô hiệu"
			)
		);
		if( $sMsg )
			$oDatagrid -> setMessage( $sMsg );
		$oDatagrid->display_datagrid($table, $arr_cols, $arr_filter, $submit_url, $this -> getAct() ,120, $root_path, false ,$arr_check);		
		
	}		
	
}

?>