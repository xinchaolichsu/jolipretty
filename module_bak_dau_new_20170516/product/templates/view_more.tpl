<div class="box_viewmore">
<div class="viewmore_title">{#product_hot#}<a href="{$smarty.const.SITE_URL}product/san-pham-xem-nhieu.html">{#view_more#}</a></div>
	<div class="viewmore_content">
    <ul id="mycarousel4" class="jcarousel-skin-tango2">	
    	{foreach from=$view_more item=item name=item}
        <li>
            <div class="viewmore_item">
                   <a href="{$smarty.const.SITE_URL}product/{$item.id}/{$item.Name|remove_marks}.html" title=""><img src="{$smarty.const.SITE_URL}{$item.Photo}" width="148" height="224" style="border:1px solid #e3e3e3; margin:1px 15px 5px 1px;"  alt="{$item.Name}" /></a>
                	<a class="viewmore_name" href="{$smarty.const.SITE_URL}product/{$item.id}/{$item.Name|remove_marks}.html" title="">{$item.Name}</a>
                <div class="viewmore_price"> 
                     <a class="viewmore_detail" href="{$smarty.const.SITE_URL}cart/{$item.id}/{$item.Name|remove_marks}.html">{#add_cart#}</a>
                	{if $item.Sale_off}<del>{$item.OldPrice|number_format:0:".":"."} đ</del>{/if}<span>{$item.price|number_format:0:".":"."} đ</span> 
                </div>
            </div>
        </li>
        {foreachelse}
            <div style="font-size:11px; text-align:center;">{#updating#}</div>
        {/foreach}
    </ul> 
    </div>
</div>
