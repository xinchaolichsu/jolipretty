
<div class="language right-logo">
    {if $cus_id || $abc}
    <a href="#">
        {$customer.FullName} {$smarty.get.cookies}{$customer.FirstName}
    </a>
    (
    <a href="{$smarty.const.SITE_URL}customer/logout/">
        {#logout#}
    </a>
    ) |
    <a href="{$smarty.const.SITE_URL}customer/frm_login/">
        My account
    </a>
    |
    {else}
    <a href="{$smarty.const.SITE_URL}customer/login/">
        {#login#}
    </a>
    |
    <a href="{$smarty.const.SITE_URL}customer/register/">
        {#Sign_up#}
    </a>
    {/if}
</div>
<div class="form_seach search-mobile">
    <div class="frm_search">
        <form action="{$smarty.const.SITE_URL}" method="get">
            <input class="inova" id="search_text" name="keyword" onblur="setValue(this,'... Tìm kiếm sản phẩm ...');" onclick="this.value=''" onfocus="delValue(this, '... Tìm kiếm sản phẩm ...');" type="text" value=" "/>
            <input class="bt_search" onclick="showHide_search();" type="submit" value=""/>
            <input name="mod" type="hidden" value="product"/>
            <input name="task" type="hidden" value="search"/>
        </form>
    </div>
</div>
{php}loadModule("cart","show_count");{/php}
