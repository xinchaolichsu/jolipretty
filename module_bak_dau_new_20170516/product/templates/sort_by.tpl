{literal}
<script language="javascript" type="text/javascript">
	function gotoPagePrice(page)
	{		
		arr_url = page.split('#');
		var page = arr_url[1];
		$.ajax({
			   type:"GET",
			   url:"/?mod=product&task=sort_by&ajax&page="+page+"&id={/literal}{$smarty.get.id}{literal}",			   
			   success: function(response_text){			   	
				   $("#sort-product").html(response_text);
			   }
		   });
	}
	$(document).ready(function(){
		$("#sort-product span.span_a_class a").click(function () {
			var page = $(this).attr("href");
			gotoPagePrice(page);  
		});
	});
</script>
{/literal}
<div id="sort-product">
{config_load file=$smarty.session.lang_file}

{literal}
<script type="text/javascript">
$(document).ready(function(){
    $("#sort_price").change(function(){
        var sort_price = $("#sort_price").val();
		$.ajax({
			type: "GET",
			url: "/index.php?mod=product&task=sort_by&ajax",
			data: "&sort_price="+sort_price,
			success: function(response_text){
			$("#sort_pro").html(response_text);
			}
		});
    });    
})
</script>
{/literal}

{literal}
<script> 
	function pro_number_change(id)
		{	
			$("#changeNumber").children("div").each(function(i){
				$(this).removeClass("active");
				$(this).addClass("item");
			});
			$("#"+id).removeClass("item").addClass("active");
			$.ajax({
				type: "POST",
				url: "/index.php?mod=product&task=product_qty&ajax",
				data: "&id="+id,
				success: function(response_text){
					$("#pro_number_change").html(response_text);
				}
			});
		}
</script>
{/literal}

{config_load file=$smarty.session.lang_file}

<div id="sort_pro">
<div class="box_center">		
	<div class="product_center_title">   
    	<div class="ct_le">
            {php}
                loadModule("product","getLink");
            {/php}  
		</div>
        <div class="ct_ri">          
            {if $num_rows > $limit}
                {$num_rows|page:$limit:$smarty.get.page:$paging_path}
            {/if} 
		</div>
    </div>
    
    <div class="box_product_title">
    	<div class="pro_title_left">
    	 <select id="sort_price">
            <option value="0" selected="selected">{#Sort_by#}</option>
            <option value="1">{#sort_by_price_asc#}</option>
            <option value="2">{#sort_by_price_desc#}</option>
         </select> 
		</div>
        <div class="pro_title_cen">
        	<!-- AddThis Button BEGIN -->
            <a class="addthis_button" href="http://www.addthis.com/bookmark.php?v=300&amp;pubid=ra-4f83ed2107499a13"><img src="http://s7.addthis.com/static/btn/v2/lg-share-en.gif" width="125" height="16" alt="Bookmark and Share" style="border:0"/></a>
            {literal}
            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f83ed2107499a13"></script>
            {/literal}
            <!-- AddThis Button END -->
        </div>
        <div class="pro_title_right">
        	<div class="no_t1">{#view#}</div>
        	<div id="changeNumber">
                <div {if id==30} class="active" {else} class="item" {/if} id="30" onClick="pro_number_change(this.id)">30</div>
                <div {if id==60} class="active" {else} class="item" {/if} id="60" onClick="pro_number_change(this.id)">60</div>
                <div {if id==90} class="active" {else} class="item" {/if} id="90" onClick="pro_number_change(this.id)">90</div>
            </div>
            <div class="no_t2">{#PRODUCT#}</div>
        </div>
        
    </div>
    
    
    
    
		<div class="box_center_content" id="pro_number_change"> 
        <div class="div_general">
			{foreach from=$listItem item=item name=item}
            {if $smarty.foreach.item.index%3==0}<div class="div_general">{/if}
                <div class="listProduct">
                    <div class="listProductImg">
                        <a href="{$smarty.const.SITE_URL}product/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}"><img src="{$smarty.const.SITE_URL}{$item.Photo}" width="256" height="386" style="padding:3px;" alt="{$item.Name}" style="float:left;"/></a>
                    </div>
                    <div class="listProductName">
                         <a href="{$smarty.const.SITE_URL}product/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{$item.Name}</a>
                    </div>
                    <div class="listProductCode">
                        {#product_code#} : <span>{$item.Code}</span> {$item.OldPrice|number_format:0:".":"."} VNĐ
                    </div>
                    <div class="listProductCart">
                        <a href="{$smarty.const.SITE_URL}cart/{$item.id}/{$item.Name|remove_marks}.html">{#cart_title#} : </a> 
                    </div>
                </div>
            {if $smarty.foreach.item.iteration%3==0 || ($smarty.foreach.item.total%3!=0 && $smarty.foreach.item.last)}</div>{/if}                 
            {foreachelse}
                <div style="font-size:11px; text-align:center;">{#updating#}</div>
            {/foreach}
          
            {if $num_rows > $limit}
            <div class="listPage">
                {$num_rows|page:$limit:$smarty.get.page:$paging_path}
            </div>
            {/if} 
		</div>
	</div>
</div></div>

</div>
		
</div>