{if !empty($view)}
	<div class="show_view">
        <div class="showview_title">{#show_view#}</div>
        <div class="showview_content">
                {foreach from=$array_page  item=item name=item}
                {if $smarty.foreach.item.iteration == $count || $smarty.foreach.item.last}
            <div class="viewmore_item" style=" height:275px;">
                   <a href="{$smarty.const.SITE_URL}product/{$item.product.id}/{$item.product.Name|remove_marks}.html" title=""><img src="{$smarty.const.SITE_URL}{$item.product.Photo}" width="148" height="216" style="border:1px solid #e3e3e3; margin:1px 15px 5px 1px;"  alt="{$item.product.Name}" /></a>
                	<a class="viewmore_name" href="{$smarty.const.SITE_URL}product/{$item.product.id}/{$item.product.Name|remove_marks}.html" title="">{$item.product.Name}</a>
                <div class="viewmore_price"> 
                     <a class="viewmore_detail" href="{$smarty.const.SITE_URL}cart/{$item.product.id}/{$item.product.Name|remove_marks}.html">{#add_cart#}</a>
                	{if $item.Sale_off}<del>{$item.OldPrice|number_format:0:".":"."} đ</del>{/if}<span>{$item.price|number_format:0:".":"."} đ</span> 
                </div>
            </div>{/if}
            {/foreach}
        </div>
    </div>
{else}
	<div class="show_view">
        <div class="showview_title">{#product_new#}</div>
        <div class="showview_content">
                {foreach from=$products_new  item=item name=item}
            <div class="viewmore_item" style=" height:275px;">
                   <a href="{$smarty.const.SITE_URL}product/{$item.id}/{$item.Name|remove_marks}.html" title=""><img src="{$smarty.const.SITE_URL}{$item.Photo}" width="148" height="216" style="border:1px solid #e3e3e3; margin:1px 15px 5px 1px;"  alt="{$item.Name}" /></a>
                	<a class="viewmore_name" href="{$smarty.const.SITE_URL}product/{$item.id}/{$item.Name|remove_marks}.html" title="">{$item.Name}</a>
                <div class="viewmore_price"> 
                     <a class="viewmore_detail" href="{$smarty.const.SITE_URL}cart/{$item.id}/{$item.Name|remove_marks}.html">{#add_cart#}</a>
                	{if $item.Sale_off}<del>{$item.OldPrice|number_format:0:".":"."} đ</del>{/if}<span>{$item.price|number_format:0:".":"."} đ</span> 
                </div>
            </div>
            {/foreach}
        </div>
    </div>
{/if}