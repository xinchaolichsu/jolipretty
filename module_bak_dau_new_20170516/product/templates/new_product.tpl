<div id="sort-product">
{literal}
<script language="javascript" type="text/javascript">
	function gotoPagePrice(page)
	{		
		arr_url = page.split('#');
		var page = arr_url[1];
        var sort_price = $("#sort_price").val();
		var cat_id = $("#hdn_cat").val();	
		$.ajax({
			   type:"GET",
			   url:"/?mod=product&task=sort_by&ajax&sort_price="+sort_price+"&cat_id="+cat_id+"&page="+page+"&id={/literal}{$smarty.get.id}{literal}",	
			   success: function(response_text){			   	
				   $("#sort-product").html(response_text);
			   }
		   });
	}
	$(document).ready(function(){
		$("#sort-product span.span_a_class a").click(function () {
			var page = $(this).attr("href");
			gotoPagePrice(page);  
		});
	});
</script>
{/literal}
{config_load file=$smarty.session.lang_file}
{literal}
<script type="text/javascript">
$(document).ready(function(){
    $("#sort_price").change(function(){
        var sort_price = $("#sort_price").val();
		var cat_id = $("#hdn_cat").val();	
		$.ajax({
			type: "GET",
			url: "/index.php?mod=product&task=sort_by&ajax",
			data: "&sort_price="+sort_price+"&cat_id="+cat_id,
			success: function(response_text){
			$("#sort_pro").html(response_text);
			}
		});
    });  
	
    $("#sort_size").change(function(){
        var sort_size = $("#sort_size").val();
		var cat_id = $("#hdn_cat").val();	
		$.ajax({
			type: "GET",
			url: "/index.php?mod=product&task=sort_by&ajax",
			data: "&sort_size="+sort_size+"&cat_id="+cat_id,
			success: function(response_text){
			$("#sort_pro").html(response_text);
			}
		});
    });  
	  
})
</script>
{/literal}
<div id="sort_pro">
<input type="hidden" value="{$cat_id}" name="hdn_cat" id="hdn_cat" />
<div class="box_center1" style="width:1000px;">		
    {if $cat_photo}<img src="{$smarty.const.SITE_URL}{$cat_photo}" width="1000" height="280" style="margin:0 0 8px 0;" alt="image" />{/if}
	<div class="box_center_title1">{php}loadModule("product","getLink");{/php}
        <div class="pro_title_left" style="float:right; margin-right:10px;">  
    	<select id="sort_size" name="sort_size" style="color:#704b3a;">
            <option value="0" selected="selected">Size</option>
        {foreach from=$cat_size item=item name=item}
            <option value="{$item.id}" {if $smarty.get.sort_size==$item.id} selected="selected"{/if}>{$item.Name}</option>
        {/foreach}
         </select> 
		</div>
        	<div class="pro_title_left" style="float:right; margin-right:20px;">       
    	<select id="sort_price" name="sort_price" style="color:#704b3a;">
            <option value="0" selected="selected">{#Sort_by#}</option>
            <option value="1" {if $smarty.get.sort_price==1} selected="selected"{/if}>{#sort_by_price_asc#}</option>
            <option value="2" {if $smarty.get.sort_price==2} selected="selected"{/if}>{#sort_by_price_desc#}</option>
         </select> 
		</div>
    </div>
		<div class="box_center_content1"> 
			{foreach from=$listItem item=item name=item}
            <div class="home_item" {if $smarty.foreach.item.iteration%4==0} style="margin-right:0;"{/if}>
            	<div class="home_img">
                  <a href="{$smarty.const.SITE_URL}product/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}"><img src="{$smarty.const.SITE_URL}{$item.Photo}" width="213" height="310"  alt="{$item.Name}" /></a>	
                  {if $item.SoldOut}<div class="sold_out">{$item.SoldOut}</div>{/if}	  
                  </div>
                  <div class="sold_outT">{if $item.SoldOut}<font color="#019faf">{$item.SoldOut}</font>{elseif $item.Is_new}<font color="#1bca03">New arrival</font>{/if}</div>
                  <a class="home_name" href="{$smarty.const.SITE_URL}product/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{$item.Name|truncate:40}</a>
                  <div class="home_summarise"></div>
                  <div class="home_price">
                  	SGD <span>$ {if $item.Price}{$item.Price}{else}{$item.OldPrice|number_format:0:".":"."}{/if} </span>
                  	<del> {if $item.Price}$ {$item.OldPrice}{/if}</del>
                  </div>  
             </div>
            {foreachelse}
                <div style="font-size:11px; text-align:center;">{#updating#}</div>
            {/foreach}
          
            {if $num_rows2 > $limit}
            <div class="listPage">
                {$num_rows2|page:$limit:$smarty.get.page:$paging_path}
            </div>
            {/if} 
	</div>
</div>
</div>
</div>


