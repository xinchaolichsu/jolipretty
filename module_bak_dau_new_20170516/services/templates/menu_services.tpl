<div class="box_left">
	<div class="side_title" style=" font-size:14px;">{#SERVICES#}</div>
	<div class="side_content">
		{foreach from = $cat item=item name=item}
		<div class="product_cat">
<a {if $smarty.get.id==$item.id} style="color:#474747;"{/if} href="{$smarty.const.SITE_URL}services/c-{$item.id}/{$item.Name|remove_marks}.html">{$item.Name}</a></div>
                {if ($smarty.get.id == $item.id || $parent == $item.id)}
                {foreach from=$item.sub item=subitem name=subitem}
                        <div class="product_sub"><a {if $smarty.get.id==$subitem.id} style="color:#474747;"{/if} href="{$smarty.const.SITE_URL}services/c-{$subitem.id}/{$subitem.Name|remove_marks}.html">{$subitem.Name}</a></div>
                    {if $smarty.get.id == $subitem.id || $parent == $subitem.id || $parent1 == $subitem.id}
                    {foreach from=$subitem.sub1 item=sub1 name=sub1}
                        <div class="product_sub1"><a {if $smarty.get.id==$sub1.id} style="color:#474747;"{/if} href="{$smarty.const.SITE_URL}services/c-{$sub1.id}/{$sub1.Name|remove_marks}.html">{$sub1.Name}</a></div>
                    {/foreach}
                    {/if}
                {/foreach}
                {/if}
		{/foreach}
	</div>
</div>