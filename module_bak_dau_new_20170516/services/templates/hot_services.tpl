
<div class="box_center">
	<div class="box_center_title">{#news_home_title#}</div>
	<div class="box_center_content" style="padding-top:10px;">
    	<table width="95%" align="center" border="0" cellpadding="4" cellspacing="0">
		{foreach from=$news item=item name=item}
            <tr>
				<td  align="left">{if $item.Photo}<a href="{$smarty.const.SITE_URL}news/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}"><img src="{$smarty.const.SITE_URL}{$item.Photo}" width="62" height="48" border="0" align="left" style="margin:0 8px 5px 0;"/></a>{/if}</td>
				<td valign="top"><a href="{$smarty.const.SITE_URL}news/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{$item.Name}</a></td>
			</tr>
           
		{/foreach}
        </table>
	</div>
	<div class="box_left_bottom_1"></div>
</div>