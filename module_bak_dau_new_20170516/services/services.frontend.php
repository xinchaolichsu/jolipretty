<?php
/**
 * VietEsoft
 * ===============================================================================================
 * Copyright 2010.
 * Website: http://www.vietesfot.com, http://thietkewebsitetrongoi.net
 * -----------------------------------------------------------------------------------------------
 * Phát triển bới VietEsoft
 * -----------------------------------------------------------------------------------------------
 * Chú ý: Tất cả các hàm, phương thức được viết mới do mình tự định nghĩa phải để lại comment gồm:
 	- Người viết
	- Ngày-tháng-năm
	- Chức năng
 * ===============================================================================================
 * Hiển thị tin tức
*/
if (!defined('IN_VES')) die('Hacking attempt');
class services extends VES_FrontEnd 
{	
	var $table; 		/* Khai báo bảng dữ liệu */
	var $mod;			/* Khai báo mod */
	var $id;			/* Khai báo id */
	var $LangID;		/* Khai báo id ngôn ngữ */
	var $field;			/* Khai báo trường dữ liệu */
	var $pre_table;		/* Khai báo bảng danh mục dữ liệu */	
	function __construct(){		
		$this->table = 'tbl_services_item';
		$this->pre_table = "tbl_services_category";
		$this->mod = 'services';
        $this->url_mod = "services";
		$this->id = mysql_real_escape_string(($_REQUEST['id']=='')?'0':$_REQUEST['id']);
		$this->field = "*";
		$this->LangID = $_SESSION['lang_id'];
		parent::__construct($this->table);
	}
	function run($task= "")
	{
		if($task==''){$task = $_GET['task'];}
	    switch($task)
			{
				/* Hiển thị trang chủ */
				case 'home':
					$this -> home();
				break;
				/* Hiển thị tin hot */
				case 'hot':
					$this -> hot();
				break;
				/* Hiển thị chi tiết một bản ghi */
				case 'details':
					$this -> details();
				break;
				case 'menu':
					$this -> menu();
				break;
				/* Hiển thị dach sách bản ghi theo danh mục đã chọn */
				case 'category':
					$this->listItem($_GET['id']);
				break;
				default:
                    /* List toàn bộ bản ghi */
					$this->listItem();
                    
                    /*List toàn bộ bản ghi theo từng danh mục*/
                    //$this->listbyCategory();
				break;	
			}
	}
	/**
	 * Page info
	 *
	 * @param $task
	 */
	function getPageinfo($task= "")
	{
	 	global $oSmarty, $oDb;
		switch ($task) {
			case 'category':
				$id = $_GET['id'];
				if ($id)
				{
					$sql = "SELECT Name FROM {$this->pre_table} WHERE id=$id";	
					$row = $oDb->getOne($sql);	
				}
				$aPageinfo=array('title'=> $row." | " . parent::get_config_vars("Term & condition","Term & condition").' | '. PAGE_TITLE, 'keyword'=>PAGE_KEYWORD.",".$row, 'description'=>PAGE_DESCRIPTION);
				$aPath[] = array('link' => '', 'path' => 'services');
				break;
            case 'details':
                $id = $_GET['id'];
                if ($id)
                {
                    $sql = "SELECT Name FROM {$this->table} WHERE id=$id";    
                    $row = $oDb->getOne($sql);    
                }
                $aPageinfo=array('title'=> $row." | " . parent::get_config_vars("Term & condition","Term & condition").' | '. PAGE_TITLE, 'keyword'=>PAGE_KEYWORD.",".$row, 'description'=>PAGE_DESCRIPTION);
                $aPath[] = array('link' => '', 'path' => 'services');
                break;
			default:			
				$aPageinfo=array('title'=> parent::get_config_vars("Term & condition","Term & condition").' | ' . PAGE_TITLE, 'keyword'=>PAGE_KEYWORD.",".$row, 'description'=>PAGE_DESCRIPTION);
				$aPath[] = array('link' => '', 'path' => 'services');
				break;
		}
		
		$oSmarty->assign('aPageinfo', $aPageinfo);
		$oSmarty->assign("aPath", $aPath);
	}
	
	/**
	 * Display in home
	 *
	 */
	function home()
	{
		global $oSmarty, $oDb;
		$where = "Status=1";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		$sql = "SELECT * FROM {$this->pre_table} WHERE {$where} ORDER BY Ordering asc Limit 20";
		$services = $oDb->getAll($sql);
		$oSmarty->assign("services",$services);		
		$oSmarty->display('home_services.tpl');
	}
	function menu()
	{
		global $oSmarty, $oDb;            
        $where = "Status=1";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		$sql = "SELECT * FROM {$this->pre_table} WHERE {$where} AND ParentID= 0 ORDER BY Ordering";
		$cat = $oDb->getALL($sql);
		foreach($cat as $k=>$v)
		{
			$sql = "SELECT * FROM {$this->pre_table} WHERE {$where} AND ParentID= {$v['id']} ORDER BY Ordering";
			$sub = $oDb->getALL($sql);
			foreach($sub as $k1=>$v1)
			{
				$sql = "SELECT * FROM {$this->pre_table} WHERE {$where} AND ParentID= {$v1['id']} ORDER BY Ordering";
				$sub1 = $oDb->getALL($sql);//pre($sub1);
				$sub[$k1]['sub1']=$sub1;
			}
			$cat[$k]['sub']=$sub;//pre($sub);
		}
		if ($_GET['task']=='category' && $_GET['id'] || $_GET['task']=='details')
		{
			if ($_GET['task']=='category')
			{
				$parent_id = $oDb->getOne("SELECT ParentID FROM {$this->pre_table} WHERE id = {$_GET['id']}");
				if ($parent_id==0)
					$oSmarty->assign("parent",$_GET['id']);
				else
				{
					$parent_id1 = $oDb->getOne("SELECT ParentID FROM {$this->pre_table} WHERE id = {$parent_id}");//pre($parent_id1);
					if($parent_id1==0)
						$oSmarty->assign("parent",$parent_id);
					else
					{
						$oSmarty->assign("parent",$parent_id1);
						$oSmarty->assign("parent1",$parent_id);
					}
				}
			}	
			else
			{
				$parent_id1 = $oDb->getOne("SELECT CatID FROM {$this->table} WHERE id = {$_GET['id']}");
				$oSmarty->assign("parent1",$parent_id1);
					$parent_id = $oDb->getOne("SELECT ParentID FROM {$this->pre_table} WHERE id = {$parent_id1}");
				$oSmarty->assign("parent",$parent_id);
			}
		}
		$oSmarty->assign("cat",$cat);
		$oSmarty->display('menu_services.tpl');
	}
	/**
	 * Hiển thị tin hot
	 *
	 */
    function hot()
	{
		global $oSmarty, $oDb;
		$sql = "SELECT * FROM {$this->pre_table} WHERE Status=1 AND ShowHome=1 ORDER BY Ordering ASC ";
		$cat = $oDb->getCol($sql);
		$cat_promot = implode(",",$cat);
		//pre($cat_promot);die();
		$sql2 = "SELECT * FROM {$this->table} WHERE Status=1 AND IsHot=1 AND CatID IN ({$cat_promot}) AND IsHome=1 ORDER BY CreateDate DESC";
		$services = $oDb->getAll($sql2);
		$oSmarty->assign("services",$services);
		//pre($services);die();
		$oSmarty->display('hot_services.tpl');
		
	}
	
	/**
	 * Hiển thị chi tiết 1 bản ghi
	 *
	 */
	function details()
	{
		global $oDb,$oSmarty;
		$id = $_GET['id'];
        $where = "Status=1";
		$sql = "SELECT * FROM {$this->table} WHERE {$where} AND id={$id}";
		$detail_item = $oDb->getRow($sql);		
		$oSmarty->assign('detail_item',$detail_item);
		
		if( $detail_item['CatID'] ){
			$full_cat_name = $this->getFullCatName($detail_item['CatID']);
			$oSmarty->assign("full_cat_name",$full_cat_name);
			$str_cat_id = $this->getFullCatId($detail_item['CatID']);
			$sql = "SELECT * FROM {$this->table} WHERE {$where} AND id<>{$id} AND CatID IN ({$str_cat_id}) ORDER BY CreateDate DESC LIMIT 10";
		} else {
			$sql = "SELECT * FROM {$this->table} WHERE {$where} AND id<>{$id} ORDER BY CreateDate DESC LIMIT 10";
		}
		$other_item = $oDb->getAll($sql);		
		$oSmarty->assign('other_item',$other_item);
		
		$template = 'detail_services.tpl';
		
		$oSmarty->display($template);
	}
	
	/**
	 * Lấy id danh mục
	 * Trả về: chuỗi id danh  mục
	 */
	function getFullCatId($cat_id,$first = true)
	{
		global $oSmarty, $oDb;
        $where = "Status=1";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		$str_cat = "";
		$sql = "SELECT id FROM {$this->pre_table} WHERE {$where} AND ParentID={$cat_id} ORDER BY Ordering";
		$cat = $oDb->getCol($sql);
		if ($cat)
		{
			$str_cat = implode(',',$cat);
			$temp_str_cat = "";
			foreach ($cat as $key=>$value)
			{
				$temp_str_cat .= $this->getFullCatId($value,false);
			}
			$str_cat .=",".$temp_str_cat;			
		}
		if ($first)  
			$str_cat .=$cat_id;
		return $str_cat;
	}
	
	/**
	 * Lấy tên danh mục
	 * Trả về: chuỗi tên danh mục
	 */
	function getFullCatName($cat_id)
	{
		global $oSmarty, $oDb;
        $where = "Status=1";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		$sql = "SELECT Name FROM {$this->pre_table} WHERE {$where} AND id={$cat_id} ORDER BY Ordering";
		$cat_name = $oDb->getOne($sql);
		$sql = "SELECT ParentID FROM {$this->pre_table} WHERE {$where} AND id={$cat_id} ORDER BY Ordering";
		$parent = $oDb->getOne($sql);
		if ($parent)
		{
			$result = $this->getFullCatName($parent)."&nbsp; / &nbsp;".$cat_name;
		}else 
			$result = "&nbsp; / &nbsp;".$cat_name;
		return $result;
	}
	
	/**
	 * Hiển thị toàn bộ danh sách bản ghi
	 *
	 */
	function listItem($cat_id=0)
	{
		global $oDb,$oSmarty;
		//$id = $_GET['id'];
        $where = "Status=1";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		$url = $_SERVER['REQUEST_URI'];
		if ($_GET['page'])
			$url = str_replace("/".$_GET['page']."/","",$url);
		$page_link = "$url/i++/";
		
		$template = 'list_services.tpl';
		
		$this->getListItem($cat_id,$page_link,"",5,$template);
	}
	function getListItem($cat_id=0,$page_link,$cond="",$limit=5,$template="list_services.tpl",$order="ORDER BY CreateDate DESC")
	{
		global $oDb,$oSmarty;
		$where = " Status=1 ";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		if ($cat_id!=0)
		{
			$str_cat_id = $this->getFullCatId($cat_id);
			$where .=" AND CatID IN ({$str_cat_id})";
			$full_cat_name = $this->getFullCatName($cat_id);
			$oSmarty->assign("full_cat_name",$full_cat_name);
		}
		if ($cond != "")
			$where .= " AND ".$cond;
		$listItem = parent::Paging($limit, $this->field, $where,$page_link, $order);
		$oSmarty->assign("listItem",$listItem);
		$oSmarty->display($template);
	}
}
?>