
{literal}
<script>
function validate_form(){
	var frm = document.frmContact;
	var email = frm.Email.value;
	var apos=email.indexOf("@");
	var dotpos=email.lastIndexOf(".");
	if( email == "" )
	{
		alert ( "Your e-mail should not be empty" );
		$('#Email').focus();
		return false;
	}
	if (apos<1||dotpos-apos<2){
		alert("Invalid email address!");
		frm.Email.select();
		return false;
	}
	if(frm.Subject.value == '')
	{
		alert("Please enter your message title!");
		frm.Subject.focus();
		return false;
	}
	if(frm.Content.value == '')
	{
		alert("Please enter your message!");
		frm.Content.focus();
		return false;
	}
	
	
	return true;
}

</script>
{/literal}
<div class="box_center1 container-fluid">		
      <div class="box_center_title1">{#CONTACT#}</div>
		<div class="box_center_content1">
		<div class="col-md-12 col-sm-12 col-xs-12">	
        	<div style="width:100%; float:left;">{$regulation.Content1}</div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
			<table class="table" align="left" width="98%" style="padding-left:20px; text-transform:none;" cellpadding="3" cellspacing="3" border="0"> 
				<form action="" method="post" name="frmContact" onsubmit="return validate_form()">
					<tr>
						<td colspan="2" style="color:#FF0000; font-weight:bold ; text-align:left">{$msg_error}</td>
					</tr>
					<tr>
						<td align="left" style="text-align:left; "> Email <span style="color:#ff0000">*</span>:</td>
						<td align="left" style="text-align:left; "><input type="text" id="Email" name="Email"  class="bg_input form-control" value="{$smarty.post.Email}" /></td>
					</tr>
					<tr>
						<td align="left" style="text-align:left;"> {#ct_title#} <span style="color:#ff0000">*</span>:</td>
						<td align="left" style="text-align:left;"><input type="text"  name="Subject" class="bg_input form-control" value="{$smarty.post.Subject}" /></td>
					</tr>
					<tr>
						<td align="left" valign="top" style="text-align:left; "> Message <span style="color:#ff0000">*</span>:</td>
						<td align="left" style="text-align:left"><textarea  name="Content" class="contact_text form-control" style="height:100px; width:100%; font-family:Arial; font-size:12px">{$smarty.post.Content}</textarea></td>
					</tr>
					
					<tr>
						<td align="left">&nbsp;</td>
						<td align="left" style="text-align:left"><input type="submit" class="bg_submit" value="{#ct_submit#}" />&nbsp;</td>
					</tr>
				</form>
			</table>
        </div>
	</div>
</div>




