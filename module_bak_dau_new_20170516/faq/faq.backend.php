<?php
class faqBackEnd extends Bsg_Module_Base{
	
	var $smarty;
	var $db;
	var $url="";
    var $datagrid;
	var $id;
	var $imgPath;
	var $imgPathShort;
	var $table='';
	var $arrAction;
	
	public function __construct($oSmarty, $oDb, $oDatagrid) {	
		$this -> smarty = $oSmarty;
		$this -> db = $oDb;		
        $this -> datagrid = $oDatagrid;
		$this -> id=$_REQUEST[id];		
		$this -> table	="tbl_faq";	 
		$this -> pre_table	="tbl_faq_category";	 	
		parent::__construct($oDb);		
		$this->bsgDb->setTable($this->table);
		$this->imgPath = SITE_DIR."upload/faq/";
		$this->imgPathShort = "/upload/faq/";
	}
	
	function run($task)
	{	
		switch( $task ){
			case 'add':
				$this -> addItem();
				break;
			case 'edit':
				$this -> editItem();
				break;
			case 'delete':
				$this -> deleteItem();
				break;
			case 'delete_all':
				$this -> deleteItems();
				break;
			case 'change_status':				
				$this -> changeStatus($_GET['id'], $_GET['status']);
				break;
			case 'change_showhome':				
				$this -> changeShowHome($_GET['id'], $_GET['ShowHome']);
				break;
			case 'public_all':						
				$this -> changeStatusMultiple( 1 );
				break;
			case 'unpublic_all':						
				$this -> changeStatusMultiple( 0 );
				break;
			case 'save_order':
				$this -> saveOrder();
				break;
			default:					
				$this -> listItem( $_GET['msg'] );		
				break;
		}
	}
	
	function getPageInfo(){
		return true;
	}

	function addItem(){
		$this -> getPath("Quản lý tâm sự > Thêm mới bài tâm sự");		
		$this -> buildForm();
	}
	
	function editItem(){
		$id = $_GET['id'];
		$this -> getPath("Quản lý tâm sự > Chỉnh sửa bài tâm sự");	
		$row = $this -> bsgDb -> getRow( $id );
		$this -> buildForm( $row );
	}
	
	function deleteItem(){
		global  $oDb;
		$id = $_GET["id"];
		$this -> bsgDb -> deleteWithPk( $id );
		$msg = "Xóa bài tâm sự thành công!";
		$this -> listItem( $msg );
	}
	
	function deleteItems()
	{
		$aItems	 = $_GET['arr_check'];
		if(is_array( $aItems) && count( $aItems) > 0){
			$sItems = implode( ',', $aItems );
			$this -> bsgDb -> deleteWithPk( $sItems );
		}
		$msg = "Xóa (các) bài tâm sự thành công!";
		$this -> listItem( $msg );
	}
	
	function changeStatusMultiple( $status = 0 )
	{
		$aItems	 = $_GET['arr_check'];
		if(is_array( $aItems) && count( $aItems) > 0){
			$sItems = implode( ',', $aItems );
			$this -> bsgDb -> updateWithPk( $sItems, array("Status" => $status) );
		}
		$msg = "Sửa trạng thái bài tâm sự thành công!";
		$this -> listItem( $msg );
	}
	
	function buildForm( $data=array() , $msg = ''){
		
		$form = new HTML_QuickForm('frmAdmin','post',$_COOKIE['re_dir']."&task=".$_GET['task'], '', "style='padding:10px 15px 0 20px;'");
		
		$form -> setDefaults($data);

		if (MULTI_LANGUAGE) {
			$lang = parent::loadLang();
			$form -> addElement('select', 'LangID', 'Ngôn ngữ', $lang);		
		}	
		$category = array(0=>'--- Chọn danh mục ---') + $this->getCategory($selected_langid);
		$form -> addElement('select', 'CatID', 'Danh mục', $category, array('id'=>'CatID'));
		$form -> addElement('text', 'Title', 'Tiêu đề', array('size' => 50, 'maxlength' => 255));
		
		/*$form->addElement('file', 'Photo', 'Ảnh');
		
		if($_GET['task']=='edit')
			$form->addElement('static', null, '',"<a href='".SITE_URL.$data['Photo']."' onclick='return hs.expand(this)' class='highslide'><img src='".SITE_URL.$data['Photo']."' width=100 hight=100 border=0></a>");*/
		//$form->addElement('textarea','Summarise','Mô tả vắn tắt',array('style'=>'width:600px; height:200px;'));	
		$content_editor=parent::editor('Content',$data['Content'],array('width'=>'800', 'height'=>'400'));
        $form -> addElement('static',NULL,'Nội dung',$content_editor);
		$form -> addElement('text', 'Ordering', 'Thứ tự', array('size' => 10, 'maxlength' => 50));
       // $form -> addElement('checkbox', 'ShowHome', 'Hiển thị trang chủ');
		$form -> addElement('checkbox', 'Status', 'Kích hoạt');
		
		$btn_group[] = $form -> createElement('submit',null,'Hoàn tất',array("style"=> "border:1px solid gray; padding:0 5px 0 5px;"));		
        $btn_group[] = $form -> createElement('button',null,'Quay lại',array('onclick'=>'window.location.href = \''.$_COOKIE['re_dir'].'\'', "style"=> "border:1px solid gray;"));      
        $form -> addGroup($btn_group);
      
		$form->addElement('hidden', 'id', $data['id']);		
		
        $form -> addRule('Name','Tiêu đề không được để trống','required',null,'client');
		
		if( $form -> validate())
		{
			if($_FILES['Photo']['name']!='')
			{
				$this->deleteImage($this->id, "Photo", $this->imgPath);
				$FileName = rand().'_'.$_FILES['Photo']['name'];												
				parent::crop_image($_FILES['Photo']['tmp_name'],$FileName, $this->imgPath);				
				$_POST['Photo'] = $this->imgPathShort.$FileName;	
			}
			
			$aData  = array(
				"LangID"	=> $_POST['LangID'],
				"Title" 		=> $_POST['Title'],
				"CatID" => $_POST['CatID'],
				"Ordering" 	=> $_POST['Ordering'],
				"Content" 	=> $_POST['Content'],
				"CreateDate"  => date("d-m-Y"),
				"Status" 	=> $_POST['Status'],
			);
			if ($_POST['Photo']!='')
				$aData['Photo'] = $_POST['Photo'];
				
			if( !$_POST['id'] ){
				
				 $id = $this -> bsgDb -> insert($aData);				 
				 $msg = "Thêm  tâm sự thành công! ";
			}else {
				$id = $_POST['id'];				
				$this -> bsgDb -> updateWithPk($id, $aData);
				$msg = "Chỉnh sửa tâm sự thành công ";
			}
			//if ($_POST['ShowHome'])
				 //$this->setShowHome($id);
			$this -> redirect($_COOKIE['re_dir']. "&msg={$msg}");
		}
		
		$form->display();
	}
	
	function getCategory($lang_id=0){
		$table = $this -> pre_table;
        $cond = '';
        if ($lang_id!=0)
            $cond .= " LangID = {$lang_id} ";
		$result = parent::multiLevel( $table, "id", "ParentID", "*", $cond, "Ordering ASC");
		
		$category = array();
		foreach ($result as $value => $key)
		{
			if( $key['level'] > 0){				
				$name = $this -> getPrefix( $key['level']).$key['Name'];
			}
			else 
				$name = $key['Name'];
			$category[$key['id']] = $name;
		}
		
		return $category;
	}
	
	function setShowHome($id)
	{
		global $oDb;
		$lang=$_POST['LangID'];
		$oDb->query("UPDATE {$this->table} SET ShowHome = 0 WHERE LangID={$lang}");
		$oDb->query("UPDATE {$this->table} SET ShowHome = 1 WHERE id={$id}");
	}
	function deleteImage($id, $field, $path){
		if($id == '')
			return;
		$imgpath = $path.$this->db->getOne("SELECT $field FROM ".$this->table." WHERE id = $id");
		if(is_file($imgpath))			
			@unlink($imgpath);
	}
	
	function changeStatus( $itemId , $status ){
		$aData = array( 'Status' => $status );
		$this -> bsgDb -> updateWithPk( $itemId, $aData );
		$msg = "Thay đổi trạng thái thành công";
		$this->listItem($msg);
	}
	function changeShowHome( $itemId , $ShowHome ){
		$aData = array( 'ShowHome' => $ShowHome );
		$this -> bsgDb -> updateWithPk( $itemId, $aData );
		$msg = "Thay đổi trạng thái thành công";
		$this->listItem($msg);
		//return true;
	}
	
	function saveOrder(){	
		$aItem = $_GET['Ordering'];
		if(is_array($aItem) && count( $aItem ) > 0){
			// save order for item.
			foreach( $aItem as $key => $value){
				if( !is_numeric($value)) $value = 0;				
				$this -> bsgDb -> updateWithPk( $key, array('Ordering' => $value ));
			}
		}	
		$msg = "Lưu thứ tự danh mục thành công!";
		$this -> listItem( $msg );
	}
	
	function listItem( $sMsg= '' )
	{		
		global $oDb;
		global $oDatagrid;				
		
		$root_path = "Quản lý tâm sự > Danh sách bài tâm sự";						
		$submit_url= "/index.php?".$_SERVER['QUERY_STRING'];
		
		$table = $this -> table;
		
		$arr_filter= array(
			
			array(
				'field' => 'Title',
				'display' => 'Tiêu đề',
				'type' => 'text',
				'name' => 'filter_title',
				'selected' => $_REQUEST['filter_title'],
				'filterable' => true
			),
			array(
				'field' => 'Status',
				'display' => 'Trạng thái',				
				'name' => 'filter_show',
				'selected' => $_REQUEST['filter_show'],
				'options' => array('Vô hiệu','Kích hoạt'),
				'filterable' => true
			)			
		);
		if (MULTI_LANGUAGE)
		{
		$arr_filter[]= array(
			
                   'field' => 'LangID',
                   'display' => 'Ngôn ngữ',                
                   'name' => 'filter_lang',
                   'selected' => $_REQUEST['filter_lang'],
                   'options' => parent::loadLang(),
                    'filterable' => true
            );
		}
		
		$arr_cols= array(			
			array(
				"field" 	=> "Title",
				"display" 	=> "Tiêu đề",
				"align"		=> 'left',				
				"datatype" 	=> "text",
				"sortable" 	=> true
			),
		array(
				"field" => "CatID",
				"display" => "Danh mục",
				"sql"    => "SELECT name FROM tbl_faq_category WHERE id = CatID",
				"align"    => 'left',                
				"datatype" => "text",
				"sortable" => true
			),
            
		/*array(
				"field" 	=> "LangID",
				"display" 	=> "Ngôn ngữ",
				"sql"		=> "SELECT name FROM tbl_sys_lang WHERE id = LangID",
				"align"		=> 'left',				
				"datatype" 	=> "text",
				"sortable" 	=> true
			),*/
            array(
				"field" => "Ordering",
				"display" => "Thứ tự",
				"datatype" 		=> "order",
				"sortable" => true,
				"order_default" => "asc"
			),
			array(
				"field" 	=> "Status",
				"display" 	=> "Trạng thái",				
				"datatype" 	=> "publish",
				"sortable" 	=> true
			)
			
		); 
		
		
		$arr_check = array(
			array(
				"task" 		=> "delete_all",
				"confirm"	=> "Xác nhận xóa?",
				"display" 	=> "Xóa"
			),
			array(
				"task" 		=> "public_all",
				"confirm"	=> "Xác nhận thay đổi trạng thái?",
				"display" 	=> "Kích hoạt"
			),
			array(
				"task" 		=> "unpublic_all",
				"confirm"	=> "Xác nhận thay đổi trạng thái?",
				"display" 	=> "Vô hiệu"
			)
		);
		if( $sMsg )
			$oDatagrid -> setMessage( $sMsg );
		$oDatagrid->display_datagrid($table, $arr_cols, $arr_filter, $submit_url, $this -> getAct() ,120, $root_path, false ,$arr_check);		
		
	}		
	
}

?>