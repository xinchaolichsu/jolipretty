{literal}
<script>
function abc(){
	alert(user);
	var frm = document.frmadd;
	var user = frm.user.value;
	if(frm.user.value == '')
	{
		alert("Mời bạn đăng nhập !");
		return false;
	}
	return true;
}
function validate_form(x){
	//alert(x);
	var frm = document.frmadd;
	var email1 = frm.email.value;
	var apos=email1.indexOf("@");
	var dotpos=email1.lastIndexOf(".");
	if(frm.name.value == '')
	{
		alert("{/literal}{#Please_enter_your_name#}{literal}!");
		frm.name.focus();
		return false;
	}
	if(email1 == '')
	{
		alert("{/literal}{#Please_enter_your_email_address#}{literal}!");
		frm.email1.focus();
		return false;
	}
	else if (apos<1||dotpos-apos<2){
		alert("Địa chỉ Email không đúng định dạng!");
		frm.email.select();
		return false;
	}
	if(frm.title.value == '')
	{
		alert("{/literal}{#Please_enter_the_article_title_to_send#}{literal}!");
		$('#title').focus();
		return false;
	}
	if(frm.content.value == '')
	{
		alert("{/literal}{#Please_enter_a_brief_content#}{literal}!");
		frm.content.focus();
		return false;
	}
	
	if(frm.c.value == '')
	{
		alert("{/literal}{#You_did_not_answer_the_question_certified#}{literal}!");
		frm.c.focus();
		return false;
	}
	if(frm.c.value!=x)
	{
		alert("{/literal}{#Answer_incorrect_code#}{literal}!");
		frm.c.focus();
		return false;
	}
	frm.submit();
}
</script>
{/literal}
<div class="box_center">
	<div class="box_center_title">{#faq#} {$full_cat_name}</div>
	<div class="box_center_content">
			<form style="margin-left:15px; float:left;" action="{$smarty.const.SITE_URL}faq/add/" method="post" name="frmadd" onsubmit="return abc()">
			  <table width="550" border="0" cellpadding="5" cellspacing="5" style="color:#1882C6;">
                <tr>
                  <td>{#FullName#}</td>
                  <td><input name="name" type="text" id="name" style="width:260px;height:22px; border:1px solid #bcbcbc;" /></td>
                </tr>
                <tr>
                  <td>{#email#}</td>
                  <td><input name="email" type="text" id="email" style="width:260px;height:22px; border:1px solid #bcbcbc;" /></td>
                </tr>
                <tr>
                  <td>{#title#}</td>
                  <td><input name="title" type="text" id="title" style="width:260px;height:22px; border:1px solid #bcbcbc;" /></td>
                </tr>
                <tr>
                  <td>{#Summarise#}</td>
                  <td><textarea name="summarise" cols="50" rows="3" id="summarise" style="border:1px solid #bcbcbc;" ></textarea></td>
                </tr>
                <tr>
                  <td>{#Content#}</td>
                  <td><textarea name="content" cols="50" rows="8" id="content" style="border:1px solid #bcbcbc;" ></textarea></td>
                </tr>
				<tr>
                  <td>{#confirmation#}</td>
                  <td>{$a} + {$b} = 
                  <input name="c" type="text" id="c" size="4" maxlength="2" style="width:50px;height:22px; border:1px solid #bcbcbc;" /></td>
                </tr>
                <tr>
                  <td>&nbsp;<input type="hidden" name="user"  id="user" value="{$user}"  /></td>
                  <td><input name="cmdok" class="submit"  type="button" onclick="return validate_form('{$c}');" id="cmdok" value="{#accepted#}" />
                  <input type="reset" class="submit" name="Submit2" value="{#Cancel#}" /> {$user}
                  </td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
              </table>
			</form>
	</div>
</div>