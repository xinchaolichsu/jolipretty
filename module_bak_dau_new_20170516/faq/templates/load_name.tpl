  {literal}
  <script type="text/javascript">
      	function load_content(id)
		{	
				{/literal}{foreach from=$name item=item name=item}{literal}
                if(id!={/literal}{$item.id}{literal})
				{
					document.getElementById("name_a"+{/literal}{$item.id}{literal}).style.color="#666";
					document.getElementById("faq_name"+{/literal}{$item.id}{literal}).style.background="url(/view/images/background/faq_content.png) left 5px no-repeat";
				}
				{/literal}{/foreach}{literal}
				document.getElementById("name_a"+id).style.color="#376fed";
				document.getElementById("faq_name"+id).style.background="url(/view/images/background/b_prev.png) left 5px no-repeat";
				$.ajax({
					type: "POST",
					url: "/index.php?mod=faq&task=load_content&ajax",
					data: "id="+id,
						success: function(response_text){
						$("#ContentP").html(response_text);						
					}
				});
		}	

  </script>
  {/literal} 
  
	<div style="width:100%; float:left; margin-bottom:30px; font-weight:700;font-size: 14px;">{$catname}</div>
    {foreach from =$name name=cat item=cat}
        <div class="faq_content" id="faq_name{$cat.id}">
            <a onclick="load_content({$cat.id})" style="cursor:pointer;" id="name_a{$cat.id}" title="{$cat.Title}">{$cat.Title}</a>
        </div>
    {/foreach}
