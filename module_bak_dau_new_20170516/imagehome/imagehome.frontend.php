<?php
/**
 * VietEsoft
 * ===============================================================================================
 * Copyright 2010.
 * Website: http://www.vietesfot.com, http://thietkewebsitetrongoi.net
 * -----------------------------------------------------------------------------------------------
 * Phát triển bới VietEsoft
 * -----------------------------------------------------------------------------------------------
 * Chú ý: Tất cả các hàm, phương thức được viết mới do mình tự định nghĩa phải để lại comment gồm:
 	- Người viết
	- Ngày-tháng-năm
	- Chức năng
 * ===============================================================================================
 * Hiển thị slideshow
*/
if (!defined('IN_VES')) die('Hacking attempt');
class imagehome extends VES_FrontEnd 
{	
	var $table; 		/* Khai báo bảng dữ liệu */
	var $mod;			/* Khai báo mod */
	var $id;			/* Khai báo id */
	var $LangID;		/* Khai báo id ngôn ngữ */
	var $field;			/* Khai báo trường dữ liệu */
	var $pre_table;		/* Khai báo bảng danh mục dữ liệu */	
	
	function __construct(){		
		$this -> table = 'tbl_imagehome_item';
		$this -> pre_table = "";
		$this -> id = mysql_real_escape_string(($_REQUEST['id']=='')?'0':$_REQUEST['id']);
		$this -> field = "*";
		$this -> LangID = $_SESSION['lang_id'];
		parent::__construct($this->table);
	}
	function run($task= "")
	{
		if($task==''){$task = $_GET['task'];}
	    switch($task)
			{
				case 'thuvien':
					$this->thuvien();
				break;	
				default:
					$this->listItem();
				break;	
			}
	}
	/**
	 * Page info
	 *
	 * @param $task
	 */
	function getPageinfo($task= "")
	{
	 	global $oSmarty, $oDb;
		switch ($task) {
			case 'category':
				$id = $_GET['id'];
				if ($id)
				{
					$sql = "SELECT Name FROM {$this->pre_table} WHERE id=$id";	
					$row = $oDb->getOne($sql);	
				}
				$aPageinfo=array('title'=> $row." | " . parent::get_config_vars("slideshow","").' | '. PAGE_TITLE, 'keyword'=>PAGE_KEYWORD.",".$row, 'description'=>PAGE_DESCRIPTION);
				$aPath[] = array('link' => '', 'path' => 'slideshow');
				break;
            case 'details':
                $id = $_GET['id'];
                if ($id)
                {
                    $sql = "SELECT Name FROM {$this->table} WHERE id=$id";    
                    $row = $oDb->getOne($sql);    
                }
                $aPageinfo=array('title'=> $row." | " . parent::get_config_vars("slideshow","").' | '. PAGE_TITLE, 'keyword'=>PAGE_KEYWORD.",".$row, 'description'=>PAGE_DESCRIPTION);
                $aPath[] = array('link' => '', 'path' => 'slideshow');
                break;
			default:			
				$aPageinfo=array('title'=> parent::get_config_vars("slideshow","").' | ' . PAGE_TITLE, 'keyword'=>PAGE_KEYWORD.",".$row, 'description'=>PAGE_DESCRIPTION);
				$aPath[] = array('link' => '', 'path' => 'slideshow');
				break;
		}
		
		$oSmarty->assign('aPageinfo', $aPageinfo);
		$oSmarty->assign("aPath", $aPath);
	}
	
	function thuvien()
	{
		
		global $oDb,$oSmarty;
		$where = " Status=1 ";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		$sql = "SELECT * FROM tbl_thuvien_item WHERE {$where} Order by Ordering"; 
		$thuvien = $oDb->getAll($sql);
		$oSmarty->assign("thuvien",$thuvien);
		$template = 'thuvien.tpl';
		$oSmarty->display($template);
	}	
	function listItem($cat_id=0)
	{
		
		global $oDb,$oSmarty;
		$where = " Status=1 ";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		$sql = "SELECT * FROM {$this->table} WHERE {$where} Order by Ordering"; 
		$listItem = $oDb->getAll($sql);
		$oSmarty->assign("listItem",$listItem);
		$sql = "SELECT * FROM tbl_thuvien_item WHERE {$where} Order by Ordering Limit 1"; 
		$thuvien = $oDb->getAll($sql);
		$oSmarty->assign("thuvien",$thuvien);
		$template = 'imagehome.tpl';
		$oSmarty->display($template);
	}	
}
?>