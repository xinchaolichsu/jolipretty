<hr>
<div class="row">
	<div class="col-md-9 col-md-offset-3 col-sm-12 col-xs-12">
		<!-- <div class="box_center_title1">{#register#}</div> -->
		<div class="center-content-padding" style="padding-top:30px; float:left; ">
					<div style="font-weight:bold; color:black">THANK YOU FOR REGISTERING WITH JOLIPRETTY, YOUR ACCOUNT HAS BEEN CREATED</div>
					<p><a href="/" style="font-weight:bold; color:#0033CC">Back to home</a></p>
					<p><a href="{$smarty.const.SITE_URL}customer/frm_login/" style="font-weight:bold; color:#0033CC">Click here to manage your account</a></p>
		</div>
	</div>
</div>