
<div class="box_center1">
	<div class="box_center_title1"><a href="{$smarty.const.SITE_URL}">Home / </a>{#login#}</div>
	<div class="box_center_content1">
		<div class="row">
			<div class="col-md-4 col-md-offset-2 col-sm-4 col-xs-12 left">
		    	<div style="width:100%; height:300px; margin:25px 150px 0 0;">
		        	<span style=" font-size:26px; float:left; width:100%; text-transform:uppercase;">Guest login</span>
		            <a class="bg_submit" style="color:#fff; padding:0 8px;" href="{$smarty.const.SITE_URL}cart/checkinfo/">Continue as guest</a>
		        </div>
		    </div>
		    <div class="col-md-6 col-sm-8 col-xs-12 left">
		        <div style="width:100%; margin-top:25px;">
						{if $error!=""}
						<div style="color: #FF0000; padding: 10px 0 40px 0;">
							{$error}
						</div>
						{/if}
						
						{if $error!="success" && $error!="logout_success"}
		                <span style=" font-size:26px; float:left; width:100%;text-transform:uppercase;">Login</span>
						<div style="margin:10px 10px 10px 0; float:left;">				
							<form action="" method="post" style="margin: 0px; padding: 0px; color:#686868;">
									{*<img style="float: right; border: 1px solid #25261e;" src="{$smarty.const.SITE_URL}themes/{$smarty.session.theme}/images/user.jpg" />*}
			
								Email<br />
								<input type="text" class="form-control" style="width: 200px; margin-top:10px;" name="tex_username" value=""/><br /><br />
								Password<br />
								<input type="password" class="form-control" style="width: 200px; margin-top:10px;" name="tex_password" value=""/><br /><br />
								<input type="submit" class="payment_cart" value="{#login#}" style=""/>
							</form>				
						</div>
						<div style=" width:100%; float:left;">
								<a href="{$smarty.const.SITE_URL}customer/forgot_password/" style="color:#686868">Forgot your password</a>
						</div>				
						{/if}
		        </div>
		    </div>
		</div>
	</div>
</div>