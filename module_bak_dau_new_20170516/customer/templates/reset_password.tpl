<div class="product">
	<div class="product-corner">
		<span class="top-left"></span>
		<span class="top-center"></span>
		<span class="top-right"></span>
	</div>
	<div class="clearboth" ></div>
	<div  class="content-block">
		<div class="title-block"></div>	
		<div style="font-weight:bold; color:#0033CC">{$msg}</div>
		<div style="font-weight:bold; color:#0033CC">{$continue}</div>
	</div>
	<div class="product-corner">
		<span class="bottom-left"></span>
		<span class="bottom-center"></span>
		<span class="bottom-right"></span>
	</div>
</div>
<div class="clearboth" ></div>