        <table width="1000"  cellpadding="0" cellspacing="0" style=" color:#454a50; font-size:11px" >
        {foreach from=$orderid item=item name=item}
         	<tr height="20px"><td colspan="3"></td></tr>
          <tr>
            <td colspan="3" style="margin-bottom:10px; float:left; border:0; color:#6D86DB; padding-left:10px; font-weight:700;" bo>Order ID: {$item.OrderID} </td>
          </tr>
          <tr bgcolor="#fcfbfb" style="border:1px solid #d0d0d0;" align="center" height="30px">
            <td style="border:1px solid #d0d0d0;border-right:none;">Product</td>
            <td style="border:1px solid #d0d0d0;border-right:none;">Size</td>
            <td style="border:1px solid #d0d0d0;border-right:none;">Date</td>
            <td style="border:1px solid #d0d0d0;border-right:none;">Payment</td>
            <td style="border:1px solid #d0d0d0;border-right:none;">Shipping</td>
            <td style="border:1px solid #d0d0d0;border-right:none;">Quantity</td>
            <td style="border:1px solid #d0d0d0;">Price</td>
          </tr>
          {foreach from=$item.history item=item1 name=item1}
          <tr>
            <td align="left" style="padding:8px 0 8px 8px;border:1px solid #d0d0d0; border-right:none;">{$item1.Name} </td>
            <td align="center" style="padding:8px 0;border:1px solid #d0d0d0; border-right:none;">{$item1.size_name} </td>
            <td  align="center" style="border:1px solid #d0d0d0;border-right:none;">{$item1.CreateDate}</td>
            <td  align="center" style="border:1px solid #d0d0d0;border-right:none;">{if $item1.IsPay==1} completed {else} pending{/if}</td>
            <td  align="center" style="border:1px solid #d0d0d0;border-right:none;">{if $item1.IsShip==1} completed {else} pending{/if}</td>
            <td  align="center" style="border:1px solid #d0d0d0;border-right:none;">{$item1.quantity}</td>
            <td  align="center" style="border:1px solid #d0d0d0;">$ {$item1.price}</td>
          </tr>
          {/foreach}
          <tr style="font-weight:700;" height="30px">
          	<td colspan="5" style="border-bottom:1px dotted #d0d0d0;"></td>
           	<td align="center" style="border-bottom:1px dotted #d0d0d0;">Total</td>
           	<td align="center" style="border-bottom:1px dotted #d0d0d0;"><span style="color:#FF0000">$ {$item.total}</span></td>
          </tr>
        {foreachelse}
          <tr style="font-weight:700;" height="30px">
          	<td colspan="7" style="color:#FF0000;">You have not placed any orders</td>
          </tr>
        {/foreach}
        </table>
