<br><br>
<div class="box_center1">
	<!-- <div class="box_center_title1"><a href="{$smarty.const.SITE_URL}">Home / </a>{#login#}</div> -->
	<div class="box_center_content1">
		{if $error!=""}
		<div style="color: #FF0000; padding: 10px 0 40px 0;">
			{$error}
		</div>
		{/if}
		<div class="row vertical-offset-100">
	    	<div class="col-md-4 col-md-offset-4">
	    		<div class="panel panel-default">
				  	<div class="panel-heading">
				    	<h3 class="panel-title">Please sign in</h3>
				 	</div>
				  	<div class="panel-body">
				  	{if $error!="success" && $error!="logout_success"}
				    	<form accept-charset="UTF-8" role="form" method="post" action="">
		                    <fieldset>
					    	  	<div class="form-group">
					    		    <input class="form-control" placeholder="E-mail" name="tex_username" type="text">
					    		</div>
					    		<div class="form-group">
					    			<input class="form-control" placeholder="Password" name="tex_password" type="password" value="">
					    		</div>
					    		<input class="btn btn-lg btn-success btn-block" type="submit" value="{#login#}">
					    	</fieldset>
					    	<div style=" width:100%; float:left;">
									<a href="{$smarty.const.SITE_URL}customer/forgot_password/" style="color:#686868">Forgot your password</a> <br />
									<a href="{$smarty.const.SITE_URL}customer/register/" style="color:#686868">Don't have a JoliPretty account? Sign up now </a>
							</div>
				      	</form>
				    {/if}
				    </div>
				</div>
			</div>
		</div>
        <!-- <div style="width:400px; float:left; margin-top:25px;">
				{if $error!=""}
				<div style="color: #FF0000; padding: 10px 0 40px 0;">
					{$error}
				</div>
				{/if}
				
				{if $error!="success" && $error!="logout_success"}
                <span style=" font-size:26px; float:left; width:100%;text-transform:uppercase;">Login</span>
				<div style="margin:10px 10px 10px 0; float:left;">				
					<form action="" method="post" style="margin: 0px; padding: 0px; color:#686868;">
							{*<img style="float: right; border: 1px solid #25261e;" src="{$smarty.const.SITE_URL}themes/{$smarty.session.theme}/images/user.jpg" />*}
						Email<br />
						<input type="text" style="width: 200px; height:18px; margin-top:10px;" name="tex_username" value=""/><br /><br />
						Password<br />
						<input type="password" style="width: 200px; height:18px; margin-top:10px;" name="tex_password" value=""/><br /><br />
						<input type="submit" class="payment_cart" value="{#login#}" style=""/>
					</form>				
				</div>
				<div style=" width:100%; float:left;">
						<a href="{$smarty.const.SITE_URL}customer/forgot_password/" style="color:#686868">Forgot your password</a> <br />
						<a href="{$smarty.const.SITE_URL}customer/register/" style="color:#686868">Don't have a JoliPretty account? Sign up now </a>
				</div>				
				{/if}
         </div> -->
	</div>
</div>