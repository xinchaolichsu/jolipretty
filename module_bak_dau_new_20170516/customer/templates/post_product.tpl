{literal}
<style type="text/css">
#form_contact b{
	float:left;
	width:170px;
	margin-top:5px;
	text-align:left;
}

#form_contact span{
	float:left;
	width: 65%;
	margin-top:5px;
	text-align:left;
}
#form_contact .line{
	float:left;
	width:100%;
	padding:3px 0px;
	font-size:11px;
}
.member_title{background:#0B53D3; height:20px; float:left; width:95%; margin:8px 0 5px 0; line-height:20px; color:#fff; text-indent:8px; font-weight:700;}
</style>
<script type="text/javascript">	
	
	function change_cat1()
		{
			var make = $("#make").val();
			$.ajax({
				type: "GET",
				url: "/index.php?mod=customer&task=customer_cat1&ajax",
				data: "make="+make,
				success: function(response_text){
					$("#car").html(response_text);
				}
			}); 
		}
		
	
	function checkSubmit()
	{	
		var re = /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/;
		var email = $('#tex_email').val();
	   
		if($("#tex_username").val()=="")
		{
			alert("Tên đăng nhập không được để trống");
			$("#tex_username").focus();
			return false;
		}
		if($("#tex_password").val()=="")
		{
			alert("Mật khẩu không được để trống");
			$("#tex_password").focus();
			return false;
		}
		if($("#tex_password").val()!=$("#tex_password_confirm").val())
		{
			alert("Xác nhận mật khẩu không đúng");
			$("#tex_password_confirm").focus();
			return false;
		}
		if( email == "" )
		{
			alert ( "Địa chỉ e-mail không được để trống" );
			$('#tex_email').focus();
			return false;
		}
		if ( !email.match(re) )
		{
			alert ("Địa chỉ e-mail không đúng định dạng");
			$('#tex_email').focus();
			return false;
		} 
		/*if($("#tex_email").val()!=$("#tex_email_confirm").val())
		{
			alert("Xác nhận e-mail không đúng");
			$("#tex_email_confirm").focus();
			return false;
		}*/
		
		if($("#tex_fullname").val()=="")
		{
			alert("Họ tên không được để trống");
			$("#tex_fullname").focus();
			return false;
		}
	
		if($("#tex_phone").val()=="")
		{
			alert("Điện thoại không được để trống");
			$("#tex_phone").focus();
			return false;
		}
	
		if($("#tex_phone").val()!="" && isNaN($("#tex_phone").val()))
		{
			alert("Trường Điện thoại vui lòng nhập số");
			$("#tex_phone").val('');
			$("#tex_phone").focus();
			return false;
		}
		
		if($("#tex_c").val()=="")
		{
			alert("Mời bạn trả lời câu hỏi bảo mật");
			$("#tex_c").focus();
			return false;
		} else if( $("#confirm_sercurity").val() != $("#tex_c").val() ) {
			alert("Trả lời câu hỏi bảo mật không đúng!");
			$("#tex_c").focus();
			return false;
		}
	
		
		return true;
	}
</script>
{/literal}
{config_load file=$smarty.session.lang_file}
<div class="box_center">
<div class="box_center_title">{#add_member#}</div>
<div class="box_center_content" style="padding-left:20px; width:780px;">
		{if $error!=""}
			<div style="color: #FF0000; padding-left: 10px; padding-top: 10px;">
				{$error}
			</div>
		{/if}		
        <div class="member_title">{#info_post#}</div>			
		<div id="form_contact">
			<form action="" method="post" enctype="multipart/form-data" style="margin: 0px; padding: 0px;" onsubmit="return checkSubmit();">

				<div class="line"><b>{#product_category#}(*):</b><span>
						<select name="sgd_category" id="sgd_category" style="height:25px;width:200px;">
                                    <option value="">--- {#select#} ---</option>
                                         {foreach from=$cat item=item name=item}
                                    		<option value="{$item.id}" style="color:#0f056e;font-weight:normal;" {if $smarty.get.sgd_category == $item.id} selected="selected" {/if}>{$item.Name}</option>
                                          {foreach from=$item.sub item=item1 name=item1}
                                          <option value="{$item1.id}" style="color:#034d77;font-weight:normal;" {if $smarty.post.sgd_category == $item1.id} selected="selected" {/if}>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{$item1.Name}</option>
                                          {/foreach}
                                    	{/foreach}
                         </select>
				</span></div>
                
				<div class="line"><b>{#city#}(*):</b><span>
						<select name="sgd_city" id="sgd_city" style="height:25px;width:200px;">
                                    <option value="">--- {#select#} ---</option>
                                         {foreach from=$city item=item name=item}
                                    		<option value="{$item.id}" style="color:#0f056e;font-weight:normal;" {if $smarty.get.sgd_city == $item.id} selected="selected" {/if}>{$item.Name}</option>
                                    	{/foreach}
                         </select>
				</span></div>
                               						
				<div class="line"><b>{#youis#}</b><span>
				  <input id="youis" type="radio" checked="checked" value="0" name="youis" > {#personal#} 
				  <input id="youis" type="radio" value="1" name="youis" > {#company#}
				</span></div>
                
				<div class="line"><b>{#you_post#}</b><span>
				  <input id="you_post" type="radio" checked="checked" value="0" name="you_post" > {#sell#} 
				  <input id="you_post" type="radio" value="1" name="you_post" > {#buy#}
				</span></div>
                
				<div class="line"><b>{#Status#}</b><span>
				  <input id="IsStock" type="radio" checked="checked" value="0" name="IsStock" > {#Outstock#} 
				  <input id="IsStock" type="radio" value="1" name="IsStock" > {#offstock#}
				</span></div>
                
				<div class="line"><b>{#title#} (*):</b><span>
				  <input type="text" style="width: 400px;" id="tex_fullname" name="tex_fullname" value="{$smarty.request.tex_fullname}"/>
				</span></div>
                
				<div class="line"><b>{#content#} (*):</b><span>
				  <textarea name="Content" style="height:200px; width:400px; font-family:Arial; font-size:12px">{$smarty.post.Content}</textarea>
				</span></div>
                
				<div class="line"><b>{#price#}(*):</b><span>
				  <input type="text" style="width: 300px;" id="price" name="price" value="{$smarty.request.price}"/> VND
				</span></div>
                
				<div class="line"><b>Ảnh :</b><span>
                    <input name="Photo" type="file" id="imgFile" size="29"/>
                    <br />
                    <br />
                    {if $data.Photo}<img src="{$data.Photo}" width="200" />{/if}
				</span></div>
				
<!--				<div class="line"><b>Địa chỉ e-mail(*):</b><span>
				  <input type="text" style="width: 300px;" id="tex_email" name="tex_email" value="{$smarty.request.tex_email}"/>
				</span></div>
				
				<div class="line"><b>Điện thoại (*) :</b><span>
				  <input type="text" name="tex_phone" style="width:300px;" id="tex_phone" value="{$smarty.request.tex_phone}" />
				</span></div>
				
				<div class="line"><b>Nick Yahoo :</b><span>
				  <input type="text" name="tex_yahoo" style="width:300px;" id="tex_yahoo" value="{$smarty.request.tex_yahoo}" />
				</span></div>-->
                
                <div class="line"><b><font>Mã bảo mật </font><strong>:</strong></b> <img src="{$smarty.const.SITE_URL}lib/captcha/captcha.class.php" align="left" border="1"  style="margin-right:5px;" id="imgCaptcha"><input type="text" name="txt_captcha" id="txt_captcha"  style="height:25px; line-height:25px; width:100px;"/><font style="color:#1b05c1">(*)</font></div>
				

				<div align="center" style="margin-top:10px;">
					<span style="text-align:left; float:right">
						<input type="submit" class="button_1" value="{#add#}" />
						<input type="reset" class="button_1" name="bt_reset" value="Làm lại" />
					</span>
				</div>
				<br><br>
			</form>
		</div>
	</div>
    <div class="bg_bottom"></div>
</div>