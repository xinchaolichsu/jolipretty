<div class="new_home">
	<div class="news_home_title">{#commu_info#}</div>
	<div class="news_home_content">
    	{foreach from = $info item = item name = item}
        	<div class="line_info">
                    <div class="date">
                    	{$item.CreateDate|date_format:"%d/%m/%Y"}
                    </div>
                	<div class="name">
                    	<a href="{$smarty.const.SITE_URL}info/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{$item.Name}</a>
                    </div>
                    <div class="img">
                    	<img src="{$item.Photo}" width="196" height="135" style="margin:5px 0 0 6px;" />
                    </div>
                    <div class="sum">
                    	{$item.Summarise|truncate:360}
                    </div>
                    <div class="view">
                    	<a href="{$smarty.const.SITE_URL}info/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{#details#}</a>
                    </div>
            </div>
    	{/foreach}
        {if $num_rows > $limit}
        <div class="listPage">
            {$num_rows|page:$limit:$smarty.get.page:$paging_path}
        </div>
        {/if} 
	</div>
</div>

