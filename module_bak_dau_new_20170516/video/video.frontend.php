<?php

class video extends VES_FrontEnd
{	
	var $table;
	var $mod;
	var $id;
	var $LangID;
	var $field;
	var $pre_table;	
	function __construct(){
		$this->table = 'tbl_video';
		$this->id = mysql_real_escape_string(($_REQUEST['id']=='')?'0':$_REQUEST['id']);
		parent::__construct($this->table);
        $this->LangID = $_SESSION['lang_id'];
	}
	
	function run($task="")
	{
		switch($task)
         {
		 case "getPageinfo":
		  		$this->getPageinfo($task);
		  		break;
		case 'all':
		        $this->all_video();
				break;	
		 case 'ajax_video':
		        $this->ajax_video();
				break;							
		 case 'home':
		        $this->home_video();
		        break;
		case 'video_all_ajax':
		        $this->video_all_ajax();
		        break;
		 case 'video_name_ajax':
		        $this->video_name_ajax();
		        break;
		 case 'video_content_ajax':
		        $this->video_content_ajax();
		        break;
		 case 'video_code_ajax':
		        $this->video_code_ajax();
		        break;
		case 'video_name_ajax_center':
		        $this->video_name_ajax_center();
		        break;
		 case 'video_content_ajax_center':
		        $this->video_content_ajax_center();
		        break;
		 case 'video_code_ajax_center':
		        $this->video_code_ajax_center();
		        break;
		 case 'page':
		        $this->page_video();
		        break;
		 case 'details':
		 		$this->viewVideo();
				break;
         default:
                    $this->listItem();
                break;
		 }
	}
	
	function getPageinfo($task= "")
	{
		global $oSmarty, $oDb;				
		
		switch ($task)
		{
			case "details":
				$id = $_GET['id'];		
				$sql = "SELECT * FROM ".$this->table." WHERE id = $id";
				$result = $oDb->getRow($sql);				
				$aPageinfo=array('title'=>"Media - ".$result['Name'], 
					'keyword'=>PAGE_KEYWORD, 
					'description'=>PAGE_TITLE.$result['Summarise']);
				break;						 
			default:
				$aPageinfo=array('title'=>"Media ", 'keyword'=>PAGE_KEYWORD
						, 'description'=>PAGE_DESCRIPTION);
				break;	
		}
		$oSmarty->assign('aPageinfo', $aPageinfo);		
	}
	function all_video(){
		global $oSmarty, $oDb ;	
		
		$sql = "SELECT * FROM {$this->table} WHERE Status = 1 ORDER BY id DESC LIMIT 1";
		$video = $oDb->getRow($sql);
		//print_r($video);
		if($video)
		{
			$video["Content"] = str_replace('420','560',$video["Content"]);
			$video["Content"] = str_replace('560','560',$video["Content"]);
			$video["Content"] = str_replace('315','315',$video["Content"]);
		}
		$oSmarty->assign('video',$video);
		//$sql = "SELECT * FROM {$this->table} WHERE Status = 1 AND id<>{$video['id']} ORDER BY id DESC";
		$sql = "SELECT * FROM {$this->table} WHERE Status = 1 ORDER BY id DESC";
		$video_all_ajax = $oDb->getAll($sql);
		$oSmarty->assign('video_all_ajax',$video_all_ajax);
		
		$oSmarty->display('all_video.tpl');	
	}
	
	function video_all_ajax(){
		global $oSmarty, $oDb ;	
		
		$sql = "SELECT * FROM {$this->table} WHERE Status = 1 ORDER BY id DESC LIMIT 1";
		$video = $oDb->getRow($sql);
		//print_r($video);

		$oSmarty->assign('video',$video);
		$sql = "SELECT * FROM {$this->table} WHERE Status = 1 AND id<>{$video['id']} ORDER BY id DESC";
		$video_all_ajax = $oDb->getAll($sql);
		$oSmarty->assign('video_all_ajax',$video_all_ajax);
		
		$oSmarty->display('video_all_ajax.tpl');	
	}
	function home_video(){
		global $oSmarty, $oDb ;	
		
		$sql = "SELECT * FROM {$this->table} WHERE Status = 1 ORDER BY id DESC LIMIT 1";
		$video = $oDb->getRow($sql);
		//print_r($video);
		if($video)
		{
			$video["Content"] = str_replace('420','210',$video["Content"]);
			$video["Content"] = str_replace('560','210',$video["Content"]);
			$video["Content"] = str_replace('315','200',$video["Content"]);
		}
		$oSmarty->assign('video',$video);
		//$sql = "SELECT * FROM {$this->table} WHERE Status = 1 AND id<>{$video['id']} ORDER BY id DESC LIMIT 6";
		$sql = "SELECT * FROM {$this->table} WHERE Status = 1 ORDER BY id DESC LIMIT 3";
		$other_video = $oDb->getAll($sql);
		$oSmarty->assign('other_video',$other_video);
		
		$oSmarty->display('video_home.tpl');	
	}
	function video_name_ajax(){
		global $oSmarty, $oDb ;	
		$video_id = $_GET['video_id'];
		$sql = "SELECT Name FROM {$this->table} WHERE id={$video_id}";
		$video_name = $oDb->getOne($sql);
		//print_r($video);
		$oSmarty->assign('video_name',$video_name);
		$oSmarty->display('video_name_ajax.tpl');	
	}	
	function video_content_ajax(){
		global $oSmarty, $oDb ;	
		$video_id = $_GET['video_id'];
		$sql = "SELECT Content FROM {$this->table} WHERE id={$video_id}";
		$video_content = $oDb->getOne($sql);
		if($video_content)
		{
			$video_content = str_replace('420','210',$video_content);
			$video_content = str_replace('560','210',$video_content);
			$video_content = str_replace('315','200',$video_content);
		}
		//print_r($video);
		$oSmarty->assign('video_content',$video_content);
		$oSmarty->display('video_content_ajax.tpl');	
	}
	function video_code_ajax(){
		global $oSmarty, $oDb ;	
		$video_id = $_GET['video_id'];
		$sql = "SELECT * FROM {$this->table} WHERE id={$video_id}";
		$video_code = $oDb->getRow($sql);
		//print_r($video);
		$oSmarty->assign('video',$video_code);
		$oSmarty->display('video_code_ajax.tpl');	
	}
	
	function video_name_ajax_center(){
		global $oSmarty, $oDb ;	
		$video_id = $_GET['video_id'];
		$sql = "SELECT Name FROM {$this->table} WHERE id={$video_id}";
		$video_name = $oDb->getOne($sql);
		//print_r($video);
		$oSmarty->assign('video_name',$video_name);
		$oSmarty->display('video_name_ajax_center.tpl');	
	}	
	function video_content_ajax_center(){
		global $oSmarty, $oDb ;	
		$video_id = $_GET['video_id'];
		$sql = "SELECT Content FROM {$this->table} WHERE id={$video_id}";
		$video_content = $oDb->getOne($sql);
		if($video_content)
		{
			$video_content = str_replace('420','560',$video_content);
			$video_content = str_replace('560','560',$video_content);
			$video_content = str_replace('315','315',$video_content);
		}
		//print_r($video);
		$oSmarty->assign('video_content',$video_content);
		$oSmarty->display('video_content_ajax_center.tpl');	
	}
	function video_code_ajax_center(){
		global $oSmarty, $oDb ;	
		$video_id = $_GET['video_id'];
		$sql = "SELECT * FROM {$this->table} WHERE id={$video_id}";
		$video_code = $oDb->getRow($sql);
		//print_r($video);
		$oSmarty->assign('video',$video_code);
		$oSmarty->display('video_code_ajax_center.tpl');	
	}
	
	function ajax_video()
	{
		global $oDb;
		$video = $oDb->getOne("SELECT Code FROM tbl_video WHERE id = ".$_POST['id']);
		$content = '<object id="player" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" name="player" width="300" height="270">
                <param name="movie" value="'.SITE_URL.'view/js/player.swf" />
                <param name="allowfullscreen" value="true" />
                <param name="allowscriptaccess" value="always" />
                <param name="flashvars" value="file='.SITE_URL.'upload/video/'.$video.'" />
                <object type="application/x-shockwave-flash" data="'.SITE_URL.'view/js/player.swf" width="300" height="270">
                    <param name="movie" value="'.SITE_URL.'view/js/player.swf" />
                    <param name="allowfullscreen" value="true" />
                    <param name="allowscriptaccess" value="always" />
                    <param name="flashvars" value="file='.SITE_URL.'upload/video/'.$video.'" />
                </object>
            </object>';
		echo $content;
	}
	function page_video()
	{
		global $oSmarty, $oDb ;
		if (!intval($_GET['page']))
			$_GET['page'] = 1;
		$where = "Status = 1";
		$page_link = "#i++";
		$other_video = parent::Paging(6, "*", $where,$page_link, "ORDER BY id DESC ");
		$oSmarty->assign('other_video',$other_video);
		$oSmarty->display('video_page.tpl');
	}
	function viewVideo()
	{
		global $oSmarty, $oDb ;	
		$id = $_GET["id"];
		$sql = "SELECT * FROM {$this->table} WHERE Status = 1 ORDER BY id DESC";
		$video = $oDb->getRow($sql);
		//print_r($video);
		$sql = "SELECT * FROM {$this->table} WHERE Status = 1 AND id<>{$video['id']} ORDER BY id DESC";
		$other_video = $oDb->getAll($sql);
		$oSmarty->assign('video',$video);
		$oSmarty->assign('other_video',$other_video);
		$oSmarty->display('video_view.tpl');
	}
    function listItem()
    {
        global $oDb,$oSmarty;        
        $sql = "SELECT * FROM {$this->table} WHERE Status=1 ORDER BY id DESC";
        $video = $oDb->getAll($sql);
        $oSmarty->assign("video",$video);
        $oSmarty->display("list_video.tpl");
    }
}
?>
