<script src="/lib/flowplayer/js/flashembed.min.js" type="text/javascript"></script>
{literal}
<script> 
	function playVideo(video_name)
	{
			flashembed("mvmain_center", 
		  	{
				src:'/lib/flowplayer/player/FlowPlayerLight.swf',
				width: 560,
				height: 315
		  	},
		  	{config: {   
				autoPlay: false,
				autoBuffering: true,
				showVolumeSlider : true,
				showFullScreenButton : true,
				wmode : 'transparent',
				showMenu: true,
				showScrubber : true,
		      	/*controlBarBackgroundColor:'0xc1eab4',*/
				initialScale: 'fit',
				videoFile: '/upload/video/'+video_name
		  	}}
			);	
	}
</script>

{/literal}

<script type="text/javascript">	
	var s_fi = '{$video.Code}';
</script>   
{literal}	
<script type="text/javascript">
	playVideo(s_fi);
</script>	
{/literal}
<div id="mvmain_center" align="center"></div>