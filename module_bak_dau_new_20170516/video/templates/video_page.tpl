{literal}
<script>
	$(document).ready(function(){ 
  				$("#other-video span.span_a_class a").click(function () {  
      			var page = $(this).attr("href");      			
      			gotoPage(page);  
    			});
			});
</script>
{/literal}
{foreach from = $other_video item = item name = item}
<div style="float: left; width: 62px; margin-left: 5px; {if $smarty.foreach.item.index % 3 != 0}_margin-left: -3px;{else}_margin-left: 0px;{/if} margin-top: 5px;">
	<a href="javascript:playVideo('{$item.id}','{$item.Code}')"><img src="{$item.Photo}" width="60" height="55" style="border: 1px solid #949593; float: left;" /></a>
</div>
{/foreach}
{if $num_rows > $limit}
<div style="text-align:right; float: left; width: 200px; margin-top: 10px; padding-right: 10px;">
	{$num_rows|page:$limit:$smarty.get.page:$paging_path}
</div>
{/if}