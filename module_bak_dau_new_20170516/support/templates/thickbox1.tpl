{literal}
<style type="text/css">
body{font-size:12px !important;font-family:Arial;}
</style>
<script>
function validate_form(){
	var frm = document.frmContact;
	var email = frm.Email.value;
	var apos=email.indexOf("@");
	var dotpos=email.lastIndexOf(".");
	if(frm.FullName.value == '')
	{
		alert("Mời bạn nhập họ tên!");
		frm.FullName.focus();
		return false;
	}
	if(email == '')
	{
		alert("Mời bạn nhập địa chỉ Email!");
		frm.Email.focus();
		return false;
	}
	else if (apos<1||dotpos-apos<2){
		alert("Địa chỉ Email không đúng định dạng!");
		frm.Email.select();
		return false;
	}
	if(frm.Content.value == '')
	{
		alert("Mời bạn nhập nội dung bài gửi!");
		frm.Content.focus();
		return false;
	}
	
	if ( frm.txt_captcha.value == '')
		{
			alert ("Bạn chưa nhập mã bảo vệ!");
			frm.txt_captcha.focus();
			return false;
		}
	return true;
}
</script>
{/literal}
{config_load file=$smarty.session.lang_file}	
                <table align="left" width="98%" style=" border-bottom: 1px solid #ccc; margin-bottom:10px;">
                    <tr>
                    <td width="30" align="left" valign="middle"><img src="../../../view/images/form-mail.png" width="32" height="32"/></td>
                    <td style="color:#FF0000;" valign="middle">Gửi Email cho chúng tôi </td>
                    </tr>
                </table>              
                <div style="clear:both"></div>    
            <table align="left" width="98%" style="padding-left:20px;font-size:12px; text-transform:none;padding-bottom:20px;" cellpadding="3" cellspacing="3" border="0"> 
                    <form action="{$smarty.const.SITE_URL}support/thickbox/" method="post" name="frmContact" onsubmit="return validate_form()">
                        <tr>
                            <td align="left"  style="text-align:left;">{#ct_fullname#}<span style="color:#CC3300">*</span>:
                            <td  align="left" style="text-align:left" ><input type="text" style="width:200px" name="FullName" class="contact_text" value="{$smarty.post.FullName}" /></td>
                        </tr>
                        <tr>
                            <td align="left" style="text-align:left; "> Email<span style="color:#CC3300">*</span>:</td>
                            <td align="left" style="text-align:left; "><input type="text" name="Email" style="width:200px" class="contact_text" value="{$smarty.post.Email}" /></td>
                        </tr>
                        <tr>
                            <td align="left" valign="top" style="text-align:left; "> {#ct_content#}<span style="color:#CC3300">*</span>:</td>
                            <td align="left" style="text-align:left"><textarea name="Content" class="contact_text" style="height:100px; width:300px; font-family:Arial; font-size:12px">{$smarty.post.Content}</textarea></td>
                        </tr>
                        <tr>
                            <td align="left">&nbsp;</td>
                            <td align="left" style="text-align:left"><input type="submit" value="HOÀN THÀNH" style="background-color: #0088D5;background-image: -moz-linear-gradient(center top , rgba(0, 0, 0, 0.05) 0%, rgba(0, 0, 0, 0.45) 100%);
    border: 1px solid #004487;border-radius: 3px 3px 3px 3px;cursor: pointer;display: inline-block;font-size: 12px;font-weight: bold;
    height: 40px;line-height: 40px;margin: 0 2px;overflow: visible;padding: 0 10px;vertical-align: middle;white-space: nowrap;color:white;"/>&nbsp;</td>
                        </tr>
                    </form>
         </table>
         <div class="clr"></div>
        <div style="font-size:15px; padding:0 20px; border-bottom: 1px solid #ccc;">
            		<span style="color:#FF0000;text-transform:uppercase">Hỗ trợ trực tuyến </span>   
       </div>
       <table align="left" width="98%" style="padding-left:20px; text-transform:none;padding-bottom:20px;" cellpadding="3" cellspacing="3" border="0">
               <tr>
                        <td align="right"><b>{$support.Phone}</b></td>
                        <td align="center">
                        	{if $support.Skype}
                                <a href="skype:{$support.Skype}">
                                        <img alt="Skype Me™!" style="border: none;" src="/view/images/skype.png">
                                        </a>
                            {/if}
                         </td>
                        <td><a href="ymsgr:sendIM?{$support.Nick}">
                                <img style="border: none;" src="/view/images/yahoo.png">
                                </a>
                        </td>              
               </tr>          
       </table> 
                                