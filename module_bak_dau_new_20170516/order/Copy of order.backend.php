<?php
/**
 * VietEsoft
 * ===============================================================================================
 * Copyright 2010.
 * Website: http://www.vietesfot.com, http://thietkewebsitetrongoi.net
 * -----------------------------------------------------------------------------------------------
 * Phát triển bới VietEsoft
 * -----------------------------------------------------------------------------------------------
 * Chú ý: Tất cả các hàm, phương thức được viết mới do mình tự định nghĩa phải để lại comment gồm:
 	- Người viết
	- Ngày-tháng-năm
	- Chức năng
 * ===============================================================================================
 * Hóa đơn mua hàng
*/
class orderBackEnd extends Bsg_Module_Base{
	var $smarty;
	var $db;
	var $table; 		/* Khai báo bảng dữ liệu */
	var $mod;			/* Khai báo mod */
	var $id;			/* Khai báo id */
	var $LangID;		/* Khai báo id ngôn ngữ */
	var $field;			/* Khai báo trường dữ liệu */
	var $pre_table;		/* Khai báo bảng danh mục dữ liệu */	
	public function __construct($oSmarty, $oDb, $oDatagrid)
	{	
		$this -> smarty = $oSmarty;
		$this -> db = $oDb;		
        $this -> datagrid = $oDatagrid;
		$this -> id=$_REQUEST[id];		
		$this -> table	="tbl_transaction";	 		
		parent::__construct($oDb);		
		$this->bsgDb->setTable($this->table);
        parent::loadJs('order.js');
	}
	
	function run($task)
	{	
		switch( $task ){
            case 'add':
				$this -> addItem();
				break;
			case 'edit':
				$this -> editItem();
				break;
			case 'delete':
				$this -> deleteItem();
				break;
			case 'delete_all':
				$this -> deleteItems();
				break;
			case 'change_status':				
				$this -> changeStatus($_GET['id'], $_GET['status']);
				break;
			case 'public_all':						
				$this -> changeStatusMultiple( 1 );
				break;
			case 'unpublic_all':						
				$this -> changeStatusMultiple( 0 );
				break;
			case 'save_order':
				$this -> saveOrder();
				break;
            case 'ajax_filter':
                $this -> ajax_filter();
                break;
            case 'ajax_form':
                $this->ajax_form();
                break;
			default:					
				$this -> listItem( $_GET['msg'] );		
				break;
		}
	}
	
	function getPageInfo()
	{
		return true;
	}
    
    function ajax_form()
    {
        global $oDb;
        $lang_id = $_REQUEST['lang_id'];
        $category = $this->getCategory($lang_id);
        $option_str = parent::changeOption($category);
        echo $option_str;
    }
    
    function ajax_filter()
    {
        global $oDb;
        $lang_id = $_REQUEST['lang_id'];
        $category = array(0 => "---Tất cả---" ) + $this->getCategory($lang_id);
        $option_str = parent::changeOption($category);
        echo $option_str;
    }

	function addItem()
	{
		$this -> getPath("Quản lý đơn hàng > Thêm mới đơn hàng");		
		$this -> buildForm();
	}
	
	function editItem()
	{
		$id = $_GET['id'];
		$this -> getPath("Quản lý đơn hàng > Chỉnh sửa đơn hàng");	
		$row = $this -> bsgDb -> getRow( $id );
		$this -> buildForm( $row );
	}

	function deleteItem()
	{
		global  $oDb;
		$id = $_GET["id"];
        //$this -> deleteImage($id, "Photo", $this->imgPath);
		$this -> bsgDb -> deleteWithPk( $id );
		$msg = "Xóa đơn hàng thành công!";
		$this -> listItem( $msg );
	}
	
	function deleteItems()
	{
		$aItems	 = $_GET['arr_check'];
		if(is_array( $aItems) && count( $aItems) > 0){
			$sItems = implode( ',', $aItems );
            //$this->deleteImage($sItems, "Photo", $this->imgPath);
			$this -> bsgDb -> deleteWithPk( $sItems );
		}
		$msg = "Xóa (các) đơn hàng thành công!";
		$this -> listItem( $msg );
	}
	
	function changeStatusMultiple( $status = 0 )
	{
		$aItems	 = $_GET['arr_check'];
		if(is_array( $aItems) && count( $aItems) > 0){
			$sItems = implode( ',', $aItems );
			$this -> bsgDb -> updateWithPk( $sItems, array("Status" => $status) );
		}
		$msg = "Sửa trạng thái đơn hàng thành công!";
		$this -> listItem( $msg );
	}
	
	function getCart($id)
	{
		global $oDb, $oDatagrid;
		$sql = "SELECT * FROM tbl_shopping_cart WHERE OrderID='".$id."'";
		$result = $oDb->getAll($sql);
		$total = 0;		
		foreach ($result as $key => $val) {
			//pre($result);
			$products = $oDb->getRow("SELECT * FROM tbl_product_item WHERE id = ".$val['product_id']);	
			if($val['color'])
				{
					$sql = "SELECT Name FROM tbl_color WHERE id='".$val['color']."' ";
					$color = $oDb->getOne( $sql );
					$products['color'] = $color;
				}
			if($val['size'])
				{
					$sql = "SELECT Name FROM tbl_size WHERE id='".$val['size']."' ";
					$size = $oDb->getOne( $sql );
					$products['size'] = $size;
				}
				
			$subtotal = ($products['OldPrice'] - $products['OldPrice']*$products['Sale_off']/100)*$val['quantity'];
			
			$result[$key]['subtotal'] = $subtotal;
			
			$result[$key]['Name'] = $products['Name'];
			$result[$key]['Code'] = $products['Code'];
			$result[$key]['Photo'] = $products['Photo'];
			$result[$key]['Price'] = $products['Price'];
			$result[$key]['tt'] = $key+1;
			$result[$key]['size'] = $products['size'];
			$result[$key]['color'] = $products['color'];
			
			$total = $total + $subtotal;
		}
		
		$cart = '<table width="600" cellpadding="4" cellspacing="2" bgcolor="#CCCCCC">
				   <tr align="center">
				   		<th bgcolor="#FFFFFF">STT</th>
					   	<th bgcolor="#FFFFFF">Sản phẩm</th>
						<th bgcolor="#FFFFFF">Mã</th>
						<th bgcolor="#FFFFFF">Size</th>
						<th bgcolor="#FFFFFF">Color</th>
						<th bgcolor="#FFFFFF">Số lượng</th>
						<th bgcolor="#FFFFFF">Thành tiền</th>
				   </tr>';
		foreach ($result as $key => $val) {
			$cart .= '	<tr align="center">';
			$cart .= '		<td bgcolor="#FFFFFF">'.$val['tt'].'</td>';	
			$cart .= '		<td bgcolor="#FFFFFF"><img src="'.$val['Photo'].'" width="200" /> <br /><strong>'.$val['Name'].'</strong></td>';
			$cart .= '		<td bgcolor="#FFFFFF">'.$val['Code'].'</td>';	
			$cart .= '		<td bgcolor="#FFFFFF">'.$val['size'].'</td>';
			$cart .= '		<td bgcolor="#FFFFFF">'.$val['color'].'</td>';
			$cart .= '		<td bgcolor="#FFFFFF">'.$val['quantity'].'</td>';
			$cart .= '		<td bgcolor="#FFFFFF">'.number_format($val['subtotal'], 0, ':', '.').'</td>';
			$cart .= '	</tr>';
		}
		$cart .= '<tr align="center">';
		$cart .= '	<td colspan="6" bgcolor="#FFFFFF"><strong>Tổng cộng</strong></td>';
		$cart .= '	<td bgcolor="#FFFFFF"><strong style="color:#F00;">'.number_format($total, 0, ':', '.').'</strong></td>';
		$cart .= '</tr>';
		
		$cart .= '</table>';
		//pre($result);
		
		echo $cart;
	}
	
	function buildForm( $data=array() , $msg = ''){
		global $oDb;
        
		$form = new HTML_QuickForm('frmAdmin','post',$_COOKIE['re_dir']."&task=".$_GET['task'], '', "style='padding:10px 15px 0 20px;'");
		
		$form -> setDefaults($data);
			
		$cart_content = $this->getCart($data['OrderID']);	
			
		$form -> addElement('text', 'Name', 'Họ tên', array('size' => 50, 'maxlength' => 255));
		
		$form -> addElement('textarea','Address','Địa chỉ',array('style'=>'width:400px; height:100px;'));
		
		$form -> addElement('text', 'Email', 'Email', array('size' => 50, 'maxlength' => 255));
		
		$form -> addElement('text', 'Phone', 'Điện thoại', array('size' => 50, 'maxlength' => 255));
		
		$payment = $this->getPayment();
		$form -> addElement('select', 'PayID', 'Hình thức thanh toán', $payment);
			
		//$form -> addElement('textarea','AdditionInfo','Yêu cầu thêm',array('style'=>'width:400px; height:150px;'));
        
        $date_time = parent::date_time('CreateDate',($data['CreateDate']) ? ($data['CreateDate']) : date("Y-m-d h:i"));			
		$form -> addElement('static',NULL,'Ngày tạo',$date_time);
		
		$form -> addElement('checkbox', 'Status', 'Kích hoạt');
		
		$btn_group[] = $form -> createElement('submit',null,'Hoàn tất',array("style"=> "border:1px solid gray; padding:0 5px 0 5px;"));		
        $btn_group[] = $form -> createElement('button',null,'Quay lại',array('onclick'=>'window.location.href = \''.$_COOKIE['re_dir'].'\'', "style"=> "border:1px solid gray;"));      
        $form -> addGroup($btn_group);
      
		$form->addElement('hidden', 'id', $data['id']);		
		
        $form -> addRule('Name','Tiêu đề không được để trống','required',null,'client');
		
		if( $form -> validate())
		{
			
			$aData  = array(
				"Name" 		=> $_POST['Name'],
                "Address" 	=> $_POST['Address'],
				"Email" 	=> $_POST['Email'],
				"Phone" 	=> $_POST['Phone'],
				"PayID" 	=> $_POST['PayID'],
				"AdditionInfo" 	=> $_POST['AdditionInfo'],
				"CreateDate" 	=> $_POST['CreateDate'],
				"Status" 	=> $_POST['Status'],
			);
				
			if( !$_POST['id'] ){
				
				 $id = $this -> bsgDb -> insert($aData);				 
				 $msg = "Thêm đơn hàng thành công! ";
			}else {
				$id = $_POST['id'];				
				$this -> bsgDb -> updateWithPk($id, $aData);
				$msg = "Chỉnh sửa đơn hàng thành công ";
			}
			
			$this -> redirect($_COOKIE['re_dir']. "&msg={$msg}");
		}
		
		$form->display();
	}
	
	function deleteImage($id, $field, $path){
		if($id == '')
			return;
		$imgpath = $path.$this->db->getOne("SELECT $field FROM ".$this->table." WHERE id = $id");
		if(is_file($imgpath))			
			@unlink($imgpath);
	}
	
	function getPayment()
    {
        global $oDb;
		$where = " Status=1 ";
		//if (MULTI_LANGUAGE)
		//$where .= " AND LangID={$_SESSION['lang_id']} ";
        	$sql = "SELECT id,Name FROM tbl_payment_item WHERE {$where}";
        $rs = $oDb->getAssoc($sql);
        return $rs;
	}
	
	function getCategory($lang_id=0){
		$table = 'tbl_order_category';
        $cond = '';
        if ($lang_id!=0)
            $cond .= " LangID = {$lang_id} ";
		$result = parent::multiLevel( $table, "id", "ParentID", "*", $cond, "Ordering ASC");
		
		$category = array();
		foreach ($result as $value => $key)
		{
			if( $key['level'] > 0){				
				$name = $this -> getPrefix( $key['level']).$key['Name'];
			}
			else 
				$name = $key['Name'];
			$category[$key['id']] = $name;
		}
		
		return $category;
	}
	
	function changeStatus( $itemId , $status ){
		$aData = array( 'Status' => $status );
		$this -> bsgDb -> updateWithPk( $itemId, $aData );
		return true;
	}
	
	function listItem( $sMsg= '' )
	{		
		global $oDb;
		global $oDatagrid;				
		
		$root_path = "Quản lý đơn hàng > Danh sách đơn hàng";						
		$submit_url= "/index.php?".$_SERVER['QUERY_STRING'];
		
		$table = $this -> table;
		
		$arr_filter= array(
			array(
				'field' => 'Name',
				'display' => 'Tên',
				'type' => 'text',
				'name' => 'filter_title',
				'selected' => $_REQUEST['filter_title'],
				'filterable' => true
			),
			array(
				'field' => 'Phone',
				'display' => 'Điện thoại',
				'type' => 'text',
				'name' => 'filter_phone',
				'selected' => $_REQUEST['filter_phone'],
				'filterable' => true
			)
        );
		
		$arr_filter[] = array(
				'field' => 'Status',
				'display' => 'Trạng thái',				
				'name' => 'filter_show',
				'selected' => $_REQUEST['filter_show'],
				'options' => array('Vô hiệu','Kích hoạt'),
				'filterable' => true
			);
		
		$arr_cols= array(		
			
			array(
				"field" => "id",					
				"primary_key" =>true,
				"display" => "Id",				
				"align" => "center",
				"sortable" => true
			),	
			array(
				"field" => "Name",
				"display" => "Tiêu đề",
				"align"	=> 'left',		
                'link' => SITE_URL.'index.php?mod=admin&amod='.$_GET['amod'].'&atask='.$_GET['atask'].'&tab=1&frame&task=edit',		
				"datatype" => "text",
				"sortable" => true
			)
        );
            
		$arr_cols_more = array(	
			array(
				"field" => "Phone",
				"display" => "Điện thoại",
				"align"	=> 'left',				
				"datatype" => "text",
				"sortable" => true
			),
			array(
				"field" => "CreateDate",
				"display" => "Ngày tạo",
				"align"	=> 'left',				
				"datatype" => "date",
				"sortable" => true
			),
			array(
				"field" => "Status",
				"display" => "Trạng thái",				
				"datatype" => "publish",
				"sortable" => true
			),		
		);		
        
        $arr_cols = array_merge($arr_cols,$arr_cols_more);
		
		$arr_check = array(
			array(
				"task" => "delete_all",
				"confirm"	=> "Xác nhận xóa?",
				"display" => "Xóa"
			),
			array(
				"task" => "public_all",
				"confirm"	=> "Xác nhận thay đổi trạng thái?",
				"display" => "Kích hoạt"
			),
			array(
				"task" => "unpublic_all",
				"confirm"	=> "Xác nhận thay đổi trạng thái?",
				"display" => "Vô hiệu"
			)			
		);
        
		if( $sMsg )
			$oDatagrid -> setMessage( $sMsg );
		$oDatagrid->display_datagrid($table, $arr_cols, $arr_filter, $submit_url, $this -> getAct() ,120, $root_path, false ,$this->getActCheck());		
		
	}		
	
}

?>