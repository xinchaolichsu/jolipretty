<?php
/**
 * VietEsoft
 * ===============================================================================================
 * Copyright 2010.
 * Website: http://www.vietesfot.com, http://thietkewebsitetrongoi.net
 * -----------------------------------------------------------------------------------------------
 * Phát triển bới VietEsoft
 * -----------------------------------------------------------------------------------------------
 * Chú ý: Tất cả các hàm, phương thức được viết mới do mình tự định nghĩa phải để lại comment gồm:
 	- Người viết
	- Ngày-tháng-năm
	- Chức năng
 * ===============================================================================================
 * Hóa đơn mua hàng
*/
class orderBackEnd extends Bsg_Module_Base{
	var $smarty;
	var $db;
	var $table; 		/* Khai báo bảng dữ liệu */
	var $mod;			/* Khai báo mod */
	var $id;			/* Khai báo id */
	var $LangID;		/* Khai báo id ngôn ngữ */
	var $field;			/* Khai báo trường dữ liệu */
	var $pre_table;		/* Khai báo bảng danh mục dữ liệu */	
	public function __construct($oSmarty, $oDb, $oDatagrid)
	{	
		$this -> smarty = $oSmarty;
		$this -> db = $oDb;		
        $this -> datagrid = $oDatagrid;
		$this -> id=$_REQUEST[id];		
		$this -> table	="tbl_transaction";	 		
		parent::__construct($oDb);		
		$this->bsgDb->setTable($this->table);
        parent::loadJs('order.js');
	}
	
	function run($task)
	{	
		switch( $task ){ 
            case 'add':
				$this -> addItem();
				break;
			case 'edit':
				$this -> editItem();
				break;
			case 'delete':
				$this -> deleteItem();
				break;
			case 'delete_all':
				$this -> deleteItems();
				break;
			case 'change_status':				
				$this -> changeStatus($_GET['id'], $_GET['status']);
				break;
			case 'public_all':						
				$this -> changeStatusMultiple( 1 );
				break;
			case 'unpublic_all':						
				$this -> changeStatusMultiple( 0 );
				break;
			case 'save_order':
				$this -> saveOrder();
				break;
            case 'ajax_filter':
                $this -> ajax_filter();
                break;
            case 'ajax_form':
                $this->ajax_form();
                break;
			case 'export_order':
				$this -> export_error();
				break;
			default:					
				$this -> listItem( $_GET['msg'] );		
				break;
		}
	}
	
	function getPageInfo()
	{
		return true;
	}
    function export_error()
	{
		$this -> getPath("Xuất đanh sách thành viên");	
		$this -> exportErr();
	}
	function exportErr()
	{
		global $oDb;
		$aItems	 = $_GET['arr_check'];
		if(is_array( $aItems) && count( $aItems) > 0){
			$sItems = implode( ',', $aItems );
			require_once 'lib/PHPExcel/Classes/PHPExcel.php';
			require_once 'lib/PHPExcel/Classes/PHPExcel/Writer/Excel2007.php';
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial')->setSize(12);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
			$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(40);
			$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(40);
			$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(40);
			$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->getActiveSheet()->getStyle("A1:O3")->getFont()->setBold(true);	
			$objPHPExcel->getActiveSheet()->getStyle('A1:O3')->getAlignment()->setWrapText(true); 
			$objPHPExcel->getActiveSheet()->mergeCells('A1:N1');
			$objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setSize(14);
			$objPHPExcel->getActiveSheet()->SetCellValue('A1', "BÁO CÁO GIỎ HÀNG");
			$styleArray = array('borders' => array('left' => array('style' => PHPExcel_Style_Border::BORDER_THIN,),'top' => array( 'style' => PHPExcel_Style_Border::BORDER_THIN,),'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN,),'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN,),'vertical' => array('style' => PHPExcel_Style_Border::BORDER_THIN,),),);
			$objPHPExcel->getActiveSheet()->getStyle('A3:O3')->applyFromArray($styleArray);
			$objPHPExcel->getActiveSheet()->getRowDimension(1)->setRowHeight(40);
			$objPHPExcel->getActiveSheet()->getRowDimension(3)->setRowHeight(40);
			$objPHPExcel->getActiveSheet()->SetCellValue('A3', "Index");
			$objPHPExcel->getActiveSheet()->SetCellValue('B3', "OrderID");
			$objPHPExcel->getActiveSheet()->SetCellValue('C3', "Ngày tạo");
			$objPHPExcel->getActiveSheet()->SetCellValue('D3', "Họ tên");
			$objPHPExcel->getActiveSheet()->SetCellValue('E3', "Email");
			$objPHPExcel->getActiveSheet()->SetCellValue('F3', "Địa chỉ");
			$objPHPExcel->getActiveSheet()->SetCellValue('G3', "Mã bưu điện");
			$objPHPExcel->getActiveSheet()->SetCellValue('H3', "Điện thoại");
			$objPHPExcel->getActiveSheet()->SetCellValue('I3', "Sản phẩm");
			$objPHPExcel->getActiveSheet()->SetCellValue('J3', "Size");
			$objPHPExcel->getActiveSheet()->SetCellValue('K3', "Số lượng");
			$objPHPExcel->getActiveSheet()->SetCellValue('L3', "Thành tiền");
			$objPHPExcel->getActiveSheet()->SetCellValue('M3', "Hình thức giao hàng");
			$objPHPExcel->getActiveSheet()->SetCellValue('N3', "Hình thức thanh toán");
			$objPHPExcel->getActiveSheet()->SetCellValue('O3', "Memo to Seller");
			$i = 4;
			$j = 0;	
			$list_order = $aItems;
			for($l=0;$l<count($list_order);$l++)
			{
				$j++;
				$i_goc = $i;
				$sql = "Select CreateUser,CreateDate from tbl_shopping_cart where OrderID = ".$list_order[$l];
				$createuser = $oDb->getRow($sql);
				$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':O'.$i)->applyFromArray($styleArray);	
				$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':O'.$i)->getAlignment()->setWrapText(true); 
				$objPHPExcel->getActiveSheet()->SetCellValue('A'.$i, $j);
				$objPHPExcel->getActiveSheet()->SetCellValue('B'.$i, $list_order[$l]);
				$objPHPExcel->getActiveSheet()->SetCellValue('C'.$i, $createuser['CreateDate']);
				$sql = "Select *,(Select Name from tbl_shipping_item where id = ShipID) as sname,(Select Name from tbl_payment_item where id = PayID) as pname from tbl_transaction where OrderID = ".$list_order[$l];
				$dulieu = $oDb->getRow($sql);
				if($createuser['CreateUser']!=0)
				{
					$sql = "SELECT * FROM tbl_customer WHERE id='".$createuser['CreateUser']."'";
					$result = $oDb->getRow($sql);
					$objPHPExcel->getActiveSheet()->SetCellValue('D'.$i, $result['FullName']." ".$result['LastName']);
					$objPHPExcel->getActiveSheet()->SetCellValue('E'.$i, $result['Email']);
					$objPHPExcel->getActiveSheet()->SetCellValue('F'.$i, $result['Address']);
					$objPHPExcel->getActiveSheet()->SetCellValue('H'.$i, $result['Phone']);
					$objPHPExcel->getActiveSheet()->SetCellValue('G'.$i, $result['Postal']);
				}
				else
				{
					$objPHPExcel->getActiveSheet()->SetCellValue('D'.$i, $dulieu['FullName']." ".$dulieu['LastName']);
					$objPHPExcel->getActiveSheet()->SetCellValue('E'.$i, $dulieu['Email']);
					$objPHPExcel->getActiveSheet()->SetCellValue('F'.$i, $dulieu['Address']);
					$objPHPExcel->getActiveSheet()->SetCellValue('H'.$i, $dulieu['Phone']);
					$objPHPExcel->getActiveSheet()->SetCellValue('G'.$i, $dulieu['Postal']);
				}
				$objPHPExcel->getActiveSheet()->SetCellValue('M'.$i, $dulieu['sname']);
				$objPHPExcel->getActiveSheet()->SetCellValue('N'.$i, $dulieu['pname']);
				$objPHPExcel->getActiveSheet()->SetCellValue('O'.$i, $dulieu['memotoseller']);
				$sql = "SELECT t1.OrderID,t1.product_id,t1.quantity,t2.Name,t1.size,t1.CreateDate,t2.OldPrice,(Select Name from tbl_product_category where id = t2.CatID) as namep FROM tbl_shopping_cart as t1 inner join tbl_product_item as t2 on t1.product_id = t2.id WHERE t1.OrderID='".$list_order[$l]."'";
				$result2 = $oDb->getAll($sql);
				$count_sp = count($result2);
				$i_merge = $i_goc + $count_sp - 1;
				// MERGE DU LIEU GOC
				if($i_merge > $i_goc)
				{
					$objPHPExcel->getActiveSheet()->mergeCells('A'.$i_goc.':A'.$i_merge);
					$objPHPExcel->getActiveSheet()->mergeCells('B'.$i_goc.':B'.$i_merge);
					$objPHPExcel->getActiveSheet()->mergeCells('C'.$i_goc.':C'.$i_merge);
					$objPHPExcel->getActiveSheet()->mergeCells('D'.$i_goc.':D'.$i_merge);
					$objPHPExcel->getActiveSheet()->mergeCells('E'.$i_goc.':E'.$i_merge);
					$objPHPExcel->getActiveSheet()->mergeCells('F'.$i_goc.':F'.$i_merge);
					$objPHPExcel->getActiveSheet()->mergeCells('G'.$i_goc.':G'.$i_merge);
					$objPHPExcel->getActiveSheet()->mergeCells('H'.$i_goc.':H'.$i_merge);
					$objPHPExcel->getActiveSheet()->mergeCells('M'.$i_goc.':M'.$i_merge);
					$objPHPExcel->getActiveSheet()->mergeCells('N'.$i_goc.':N'.$i_merge);
					$objPHPExcel->getActiveSheet()->mergeCells('O'.$i_goc.':O'.$i_merge);
				}
				if($result2 && isset($result2))
				{	
					$z =0;	
					foreach ($result2 as $key => $val) 
					{
						$z++;
						$objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight(25);
						$objPHPExcel->getActiveSheet()->SetCellValue('I'.$i, $val['Name']);
						if($val['size'])
						{
							$sql = "Select Name from tbl_size where id = ".$val['size'];
							$name_size = $oDb->getOne($sql);
						}
						else
							$name_size = "";
						$objPHPExcel->getActiveSheet()->SetCellValue('J'.$i, $name_size);
						$objPHPExcel->getActiveSheet()->SetCellValue('K'.$i, $val['quantity']);
						$objPHPExcel->getActiveSheet()->SetCellValue('L'.$i, $val['OldPrice']);
						
						$objPHPExcel->getActiveSheet()->getStyle("I".$i)->getFont()->setBold(true);
						$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':O'.$i)->applyFromArray($styleArray);
						$objPHPExcel->getActiveSheet()->getStyle('A'.$i.':O'.$i)->getAlignment()->setWrapText(true);
						if($count_sp != 1 && $z != $count_sp)
							$i++;
						
						
					}
				}
				$i++;
			}
			$objPHPExcel->getActiveSheet()->setTitle("THONG KE KT");
			$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
			$objWriter->save("upload/order/thongke1_".date("m")."_".date("Y").".xls");
			echo '<script language="javascript">
						window.location ="'.SITE_URL.'upload/order/thongke1_'.date("m").'_'.date("Y").'.xls";</script>';
		}
	}
    function ajax_form()
    {
        global $oDb;
        $lang_id = $_REQUEST['lang_id'];
        $category = $this->getCategory($lang_id);
        $option_str = parent::changeOption($category);
        echo $option_str;
    }
    
    function ajax_filter()
    {
        global $oDb;
        $lang_id = $_REQUEST['lang_id'];
        $category = array(0 => "---Tất cả---" ) + $this->getCategory($lang_id);
        $option_str = parent::changeOption($category);
        echo $option_str;
    }

	function addItem()
	{
		$this -> getPath("Quản lý đơn hàng > Thêm mới đơn hàng");		
		$this -> buildForm();
	}
	
	function editItem()
	{
		global  $oDb;
		$id = $_GET['id'];
		if(!$id)
			$id=$_POST['id'];
		$this -> getPath("Quản lý đơn hàng > Chỉnh sửa đơn hàng");	
		$row = $oDb->getRow("SELECT * FROM tbl_transaction WHERE OrderID=$id");
		//$row = $this -> bsgDb -> getRow( $id );
		$this -> buildForm( $row );
	}

	function deleteItem()
	{
		global  $oDb;
		$id = $_GET["id"];
		$oDb->query("DELETE FROM tbl_shopping_cart WHERE OrderID=$id");
		$oDb->query("DELETE FROM tbl_transaction WHERE OrderID=$id");
		$msg = "Xóa đơn hàng thành công!";
		$this -> listItem( $msg );
	}
	
	function deleteItems()
	{
		global  $oDb;
		$aItems	 = $_GET['arr_check'];
		if(is_array( $aItems) && count( $aItems) > 0){
			$sItems = implode( ',', $aItems );
			$oDb->query("DELETE FROM tbl_shopping_cart WHERE OrderID IN ($sItems)");
			$oDb->query("DELETE FROM tbl_transaction WHERE OrderID IN ($sItems)");
			//$this -> bsgDb -> deleteWithPk( $sItems );
		}
		$msg = "Xóa (các) đơn hàng thành công!";
		$this -> listItem( $msg );
	}
	
	function changeStatusMultiple( $status = 0 )
	{
		$aItems	 = $_GET['arr_check'];
		if(is_array( $aItems) && count( $aItems) > 0){
			$sItems = implode( ',', $aItems );
			$this -> bsgDb -> updateWithPk( $sItems, array("Status" => $status) );
		}
		$msg = "Sửa trạng thái đơn hàng thành công!";
		$this -> listItem( $msg );
	}
	
	function getCart($id)
	{
		global $oDb, $oDatagrid;
		if($id)
		{
			$ship = $oDb->getOne("SELECT ShipID FROM tbl_transaction WHERE OrderID='".$id."'");
			$price_ship = $oDb->getOne("SELECT Price FROM tbl_shipping_item WHERE id = {$ship}");		
			$sql = "SELECT id,OrderID,product_id,quantity,size,CreateDate,CreateUser FROM tbl_shopping_cart WHERE OrderID='".$id."'";
			$result = $oDb->getAll($sql);
			if($result)
			{
			$total = 0;		
			foreach ($result as $key => $val) {
				$products = $oDb->getRow("SELECT * FROM tbl_product_item WHERE id = ".$val['product_id']);
				if($val['size'])
					{
						$sql = "SELECT Name FROM tbl_size WHERE id='".$val['size']."' ";
						$size = $oDb->getOne( $sql );
						$products['size'] = $size;
					}
				if($products['Price']=='')	
					$products['Price']=$products['OldPrice'];
				$subtotal = $products['Price']*$val['quantity'];
				$result[$key]['subtotal'] = $subtotal;
				
				$result[$key]['Name'] = $products['Name'];
				$result[$key]['Code'] = $products['Code'];
				$result[$key]['Photo'] = $products['Photo'];
				$result[$key]['Price'] = $products['Price'];
				$result[$key]['tt'] = $key+1;
				$result[$key]['size'] = $products['size'];
				$result[$key]['color'] = $products['color'];
				$total = $total + $subtotal;
				$discount =$val['Code']*$total/100;
			}
			$total1=$total-$discount+$price_ship;
			$cart = '<table width="600" cellpadding="4" cellspacing="2" bgcolor="#CCCCCC">
					   <tr align="center">
							<th bgcolor="#FFFFFF">STT</th>
							<th bgcolor="#FFFFFF">Sản phẩm</th>
							<th bgcolor="#FFFFFF">Size</th>
							<th bgcolor="#FFFFFF">Số lượng</th>
							<th bgcolor="#FFFFFF">Thành tiền</th>
					   </tr>';
			foreach ($result as $key => $val) {
				$cart .= '	<tr align="center">';
				$cart .= '		<td bgcolor="#FFFFFF">'.$val['tt'].'</td>';	
				$cart .= '		<td bgcolor="#FFFFFF"><img src="'.$val['Photo'].'" width="200" /> <br /><strong>'.$val['Name'].'</strong></td>';
				$cart .= '		<td bgcolor="#FFFFFF">'.$val['size'].'</td>';
				$cart .= '		<td bgcolor="#FFFFFF">'.$val['quantity'].'</td>';
				$cart .= '		<td bgcolor="#FFFFFF">'.number_format($val['subtotal'], 0, ':', '.').'</td>';
				$cart .= '	</tr>';
			}
			$cart .= '<tr align="center">';
			$cart .= '	<td colspan="4" bgcolor="#FFFFFF"><strong>Subtotal</strong></td>';
			$cart .= '	<td align="left" bgcolor="#FFFFFF"><strong>SGD $ '.number_format($total, 0, ':', '.').'</strong></td>';
			$cart .= '</tr>';
			$cart .= '<tr align="center">';
			$cart .= '	<td colspan="4" bgcolor="#FFFFFF"><strong>Discounts %</strong></td>';
			$cart .= '	<td align="left" bgcolor="#FFFFFF"><strong>SGD $ '.$discount.'</strong></td>';
			$cart .= '</tr>';
			$cart .= '<tr align="center">';
			$cart .= '	<td colspan="4" bgcolor="#FFFFFF"><strong>Total shipping & Handling Fees:</strong></td>';
			$cart .= '	<td align="left" bgcolor="#FFFFFF"><strong>SGD $ '.$price_ship.'</strong></td>';
			$cart .= '</tr>';
			$cart .= '<tr align="center">';
			$cart .= '	<td colspan="4" bgcolor="#FFFFFF"><strong>Total (Tax incl)</strong></td>';
			$cart .= '	<td align="left" bgcolor="#FFFFFF"><strong style="color:#F00;">SGD $ '.$total1.'</strong></td>';
			$cart .= '</tr>';
			$cart .= '</table>';
			//pre($result);
			}
		}
		echo $cart;
	}
	
	function buildForm( $data=array() , $msg = ''){
		global $oDb;
        
		$form = new HTML_QuickForm('frmAdmin','post',$_COOKIE['re_dir']."&task=".$_GET['task'], '', "style='padding:10px 15px 0 20px;'");
		
		$form -> setDefaults($data);
		$cart_content = $this->getCart($data['OrderID']);	
		if($data['CreateUser']!=0)
		{	
			$sql = "SELECT * FROM tbl_customer WHERE id='".$data['CreateUser']."'";
			$result = $oDb->getRow($sql);
			$form -> addElement('static',NULL,'Họ tên',$result['FullName'].$result['LastName']);
			$form -> addElement('static',NULL,'Địa chỉ',$result['Address']);
			if($result['Address1'])
				$form -> addElement('static',NULL,'Địa chỉ nhận hàng',$result['Address1']);
			$form -> addElement('static',NULL,'Email',$result['Email']);
			$form -> addElement('static',NULL,'Điện thoại',$result['Phone'].$result['LastName']);
			$form -> addElement('static',NULL,'Mã bưu điện',$result['Postal']);
		}
		else
		{
			$form -> addElement('static',NULL,'Họ tên',$data['FullName'],array('size' => 50, 'maxlength' => 255));
			$form -> addElement('text', 'LastName', 'LastName', array('size' => 50, 'maxlength' => 255));
			$form -> addElement('text', 'Email', 'Email', array('size' => 50, 'maxlength' => 255));
			$form -> addElement('textarea','Address','Địa chỉ',array('style'=>'width:400px; height:100px;'));
			if($result['Address1'])
				$form -> addElement('textarea','Address1','Địa chỉ nhận hàng',array('style'=>'width:400px; height:100px;'));
			$form -> addElement('text', 'City', 'Thành phố', array('size' => 50, 'maxlength' => 255));
			$country = array(0=>'--- Đất nước---') + $this->getCountry($selected_langid);
			$form -> addElement('select', 'CityID', 'Đất nước', $country, array('id'=>'CatID'));
			$form -> addElement('text', 'Phone', 'Điện thoại', array('size' => 50, 'maxlength' => 255));
			$form -> addElement('text', 'Postal', 'Mã bưu điện', array('size' => 50, 'maxlength' => 255));
		}
		$shipping = $this->getShipping();
		$form -> addElement('select', 'ShipID', 'Hình thức giao hàng', $shipping);
		$payment = $this->getPayment();
		$form -> addElement('select', 'PayID', 'Hình thức thanh toán', $payment);
			
		//$form -> addElement('textarea','AdditionInfo','Yêu cầu thêm',array('style'=>'width:400px; height:150px;'));
        
			$date_time = parent::date_time('CreateDate',($data['CreateDate']) ? ($data['CreateDate']) : date("Y-m-d h:i"));			
			$form -> addElement('static',NULL,'Ngày tạo',$date_time);
			$form -> addElement('checkbox', 'IsShip', 'Chuyển hàng');
			$form -> addElement('checkbox', 'IsPay', 'Thanh toán');
			$form -> addElement('checkbox', 'Status', 'Kích hoạt');
		$btn_group[] = $form -> createElement('submit',null,'Hoàn tất',array("style"=> "border:1px solid gray; padding:0 5px 0 5px;"));		
        $btn_group[] = $form -> createElement('button',null,'Quay lại',array('onclick'=>'window.location.href = \''.$_COOKIE['re_dir'].'\'', "style"=> "border:1px solid gray;")); 
        $form -> addGroup($btn_group);
		$form->addElement('hidden', 'id', $data['id']);		
		
        $form -> addRule('Name','Tiêu đề không được để trống','required',null,'client');
		
		if( $form -> validate())
		{
			
			$aData  = array(
				"ShipID" 	=> $_POST['ShipID'],
				"PayID" 	=> $_POST['PayID'],
				"IsShip" 	=> $_POST['IsShip'],
				"IsPay" 	=> $_POST['IsPay'],
				"CreateDate" 	=> $_POST['CreateDate'],
				"Status" 	=> $_POST['Status'],
			);
				
			if( !$_POST['id'] ){
				
				 $id = $this -> bsgDb -> insert($aData);				 
				 $msg = "Thêm đơn hàng thành công! ";
			}else {
				$id = $_POST['id'];				
				$this -> bsgDb -> updateWithPk($id, $aData);
				$msg = "Chỉnh sửa đơn hàng thành công ";
			}
			
			$this -> redirect($_COOKIE['re_dir']. "&msg={$msg}");
		}
		
		$form->display();
	}
	
	function deleteImage($id, $field, $path){
		if($id == '')
			return;
		$imgpath = $path.$this->db->getOne("SELECT $field FROM ".$this->table." WHERE id = $id");
		if(is_file($imgpath))			
			@unlink($imgpath);
	}
	
	function getCountry($lang_id=0){
		global $oDb;
		$sql="Select id,Name from tbl_country Order by Ordering";
		$country=$oDb->getAssoc($sql);
		return $country;
	}
	
	function getPayment()
    {
        global $oDb;
		$where = " Status=1 ";
		//if (MULTI_LANGUAGE)
		//$where .= " AND LangID={$_SESSION['lang_id']} ";
        	$sql = "SELECT id,Name FROM tbl_payment_item WHERE {$where}";
        $rs = $oDb->getAssoc($sql);
        return $rs;
	}
	
	function getShipping()
    {
        global $oDb;
		$where = " Status=1 ";
		//if (MULTI_LANGUAGE)
		//$where .= " AND LangID={$_SESSION['lang_id']} ";
        	$sql = "SELECT id,Name FROM tbl_shipping_item WHERE {$where} Order by Ordering DESC";
        $rs = $oDb->getAssoc($sql);
        return $rs;
	}
	
	function getCategory($lang_id=0){
		$table = 'tbl_order_category';
        $cond = '';
        if ($lang_id!=0)
            $cond .= " LangID = {$lang_id} ";
		$result = parent::multiLevel( $table, "id", "ParentID", "*", $cond, "Ordering ASC");
		
		$category = array();
		foreach ($result as $value => $key)
		{
			if( $key['level'] > 0){				
				$name = $this -> getPrefix( $key['level']).$key['Name'];
			}
			else 
				$name = $key['Name'];
			$category[$key['id']] = $name;
		}
		
		return $category;
	}
	
	function changeStatus( $itemId , $status ){
		global $oDb;
		$aData = array( 'Status' => $status );
		$this -> bsgDb -> updateWithPk( $itemId, $aData );
/*		$OrderID = $oDb->getOne("SELECT OrderID FROM tbl_transaction WHERE id='".$itemId."'");
		$product = $oDb->getAll("SELECT product_id,size,quantity FROM tbl_shopping_cart WHERE OrderID='".$OrderID."'");
		foreach ($product as $k => $v) {
			$sql = "update tbl_product_size set number=number-{$v['quantity']} WHERE pro_id='".$v['product_id']."' AND size_id='".$v['size']."'";
			$oDb->query($sql);
		}
*/		$msg = "Thay đổi trạng thái thành công";
		$this->listItem($msg);
	}
	
/*	function changeIsPay( $itemId , $ispay ){
		global $oDb;
		$aData = array( 'IsPay' => $ispay );
		$this -> bsgDb -> updateWithPk( $itemId, $aData );
		$msg = "Thay đổi trạng thái thành công";
		$this->listItem($msg);
	}
*/	
	function listItem( $sMsg= '' )
	{		
		global $oDb;
		global $oDatagrid;				
		
		$root_path = "Quản lý đơn hàng > Danh sách đơn hàng";						
		$submit_url= "/index.php?".$_SERVER['QUERY_STRING'];
		
		$table = "(select * from tbl_transaction where PayID <> '') as". $this -> table;
		
		$arr_filter= array(
		
        );
		
		$arr_filter[] = array(
				'field' => 'Status',
				'display' => 'Trạng thái',				
				'name' => 'filter_show',
				'selected' => $_REQUEST['filter_show'],
				'options' => array('Vô hiệu','Kích hoạt'),
				'filterable' => true
			);
		
		$arr_cols= array(	
			array(
				"field" => "OrderID",					
				"primary_key" =>true,
				"display" => "OrderID",				
				"align" => "center",
				"sortable" => true
			),	
			
			array(
				"field" => "FullName",
				"display" => "FullName(Khách)",
				"align"	=> 'left',		
				"datatype" => "text",
				"sortable" => true
			),
			array(
				"field" => "CreateUser",
				"display" => "FullName(Thành viên)",
				"align"	=> 'left',		
				"sql"    => "SELECT FullName FROM tbl_customer WHERE id = CreateUser",
				"datatype" => "text",
				"sortable" => true
			),
			array(
				"field" => "memotoseller",
				"display" => "Lời nhắn",
				"align"	=> 'left',		
				"datatype" => "text",
				"sortable" => true
			)
        );
            
		$arr_cols_more = array(	
			array(
				"field" => "CreateDate",
				"display" => "Ngày tạo",
				"align"	=> 'left',				
				"datatype" => "date",
				"sortable" => true
			),
			array(
				"field" => "IsPay",
				"display" => "Thanh toán",				
				"datatype" => "publish",
				"sortable" => true
			),	
			array(
				"field" => "IsShip",
				"display" => "Chuyển hàng",				
				"datatype" => "publish",
				"sortable" => true
			),	
			array(
				"field" => "Status",
				"display" => "Trạng thái",				
				"datatype" => "publish",
				"sortable" => true
			),		
		);		
        
        $arr_cols = array_merge($arr_cols,$arr_cols_more);
		
		$arr_check = array(
			array(
				"task" => "delete_all",
				"confirm"	=> "Xác nhận xóa?",
				"display" => "Xóa"
			),
			array(
				"task" => "public_all",
				"confirm"	=> "Xác nhận thay đổi trạng thái?",
				"display" => "Kích hoạt"
			),
			array(
				"task" => "unpublic_all",
				"confirm"	=> "Xác nhận thay đổi trạng thái?",
				"display" => "Vô hiệu"
			)			
		);
        
		if( $sMsg )
			$oDatagrid -> setMessage( $sMsg );
		$oDatagrid->display_datagrid($table, $arr_cols, $arr_filter, $submit_url, $this -> getAct() ,120, $root_path, false ,$this->getActCheck());		
		
	}		
	
}

?>