{literal}
<script type="text/javascript">
		$(window).load(function() {
			$('#slider').nivoSlider();
		});
</script>
{/literal}
<div class="container-fluid pd0">
<div class="slide-lis" id="fix-height">
    <div class="owl-carousel lis-slide">
        
            {foreach from=$listItem item=item name=item}
            <div>
               <a target="_blank" href="{$item.Link}"><img src='{$smarty.const.SITE_URL}{$item.Photo}' /></a>
            </div>
            {/foreach}
    </div>
</div>
</div>
{literal}
    <script>
        $('.lis-slide').owlCarousel({
            loop:true,
            nav:true,
            dots:true,
            dotsClass: 'carousel-indicators',
            dotClass: 'dot',
            autoplayTimeout:10000,
            autoplay:true,
            responsiveClass:true,
            items:1,
            smartSpeed:450,
            navText : ['<i class="pe-7s-angle-left"></i>','<i class="pe-7s-angle-right"></i>'],
        });
    </script>
{/literal}
