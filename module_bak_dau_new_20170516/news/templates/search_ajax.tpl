{literal}
<script language="javascript" type="text/javascript">
	function page_news(page)
	{		
		arr_url = page.split('#page=');
		var page = arr_url[1];
		var keyword = $("#key_search").val();
		$.ajax({
			   type:"GET",
			   url:"/?mod=news&task=search&ajax&page="+page+"&keyword="+keyword+"&mod_search=news",			   
			   success: function(response_text){			   	
				   $("#page_news").html(response_text);
			   }
		   });
	}
	$(document).ready(function(){
		$("#page_news span.span_a_class a").click(function () {
			var page = $(this).attr("href");
			page_news(page);  
		});
	});
	
	function page_services(page)
	{		
		arr_url = page.split('#page=');
		var page = arr_url[1];
		var keyword = $("#key_search").val();
		$.ajax({
			   type:"GET",
			   url:"/?mod=news&task=search&ajax&page="+page+"&keyword="+keyword+"&mod_search=services",			   
			   success: function(response_text){			   	
				   $("#page_services").html(response_text);
			   }
		   });
	}
	$(document).ready(function(){
		$("#page_services span.span_a_class a").click(function () {
			var page = $(this).attr("href");
			page_services(page);  
		});
	});
	
	function page_product(page)
	{		
		arr_url = page.split('#page=');
		var page = arr_url[1];
		var keyword = $("#key_search").val();
		$.ajax({
			   type:"GET",
			   url:"/?mod=news&task=search&ajax&page="+page+"&keyword="+keyword+"&mod_search=product",			   
			   success: function(response_text){			   	
				   $("#page_product").html(response_text);
			   }
		   });
	}
	$(document).ready(function(){
		$("#page_product span.span_a_class a").click(function () {
			var page = $(this).attr("href");
			page_product(page);  
		});
	});
	
	function page_blog(page)
	{		
		arr_url = page.split('#page=');
		var page = arr_url[1];
		var keyword = $("#key_search").val();
		$.ajax({
			   type:"GET",
			   url:"/?mod=news&task=search&ajax&page="+page+"&keyword="+keyword+"&mod_search=blog",			   
			   success: function(response_text){			   	
				   $("#page_blog").html(response_text);
			   }
		   });
	}
	$(document).ready(function(){
		$("#page_blog span.span_a_class a").click(function () {
			var page = $(this).attr("href");
			page_blog(page);  
		});
	});
	function page_about(page)
	{		
		arr_url = page.split('#page=');
		var page = arr_url[1];
		var keyword = $("#key_search").val();
		$.ajax({
			   type:"GET",
			   url:"/?mod=news&task=search&ajax&page="+page+"&keyword="+keyword+"&mod_search=about",			   
			   success: function(response_text){			   	
				   $("#page_about").html(response_text);
			   }
		   });
	}
	$(document).ready(function(){
		$("#page_about span.span_a_class a").click(function () {
			var page = $(this).attr("href");
			page_blog(page);  
		});
	});
</script>
{/literal}
{config_load file=$smarty.session.lang_file}
<div id="page_{$mod_search}">
    	{foreach from = $ajax item = item name = item}
        	{if $mod_search=='product'}
            <div class="home_item" {if $smarty.foreach.item.iteration%2==0} style="margin-right:0;"{/if}>
            	<div class="home_img1">
                  <a href="{$smarty.const.SITE_URL}product/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}"><img src="{$smarty.const.SITE_URL}{$item.Photo}" width="88" height="100"  alt="{$item.Name}" /></a>		  
                  </div>
                  <a class="home_name1" href="{$smarty.const.SITE_URL}product/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{$item.Name|truncate:40}</a>
                  <div class="home_summarise">{$item.Summarise|truncate:120}</div>
                  <div class="home_price">{#price#}:<span> {$item.OldPrice|number_format:0:".":"."} d<span></div>  
             </div>
            {else}
        	<div class="listMod" {if $smarty.foreach.item.last} style="border:none;"{/if}>
                	<div class="name">
                    <div class="date">
                    	{$item.CreateDate|date_format:"%d/%m/%Y"}
                    </div>
                    	<a href="{$smarty.const.SITE_URL}{$mod_search}/{if $mod_search=='services'}c-{/if}{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{$item.Name}</a>
                    </div>
            	<div class="img">
                	{if $item.Photo}
                    	<a href="{$smarty.const.SITE_URL}{$mod_search}/{if $mod_search=='services'}c-{/if}{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}"><img src="{$item.Photo}" class="photo" alt="{$item.Name}"/></a>
					{/if}
                </div>
                <div class="info">
                    <div class="sum">
                    	{$item.Summarise|truncate:260}
                    </div>
                    <div class="view">
                    	<a href="{$smarty.const.SITE_URL}{$mod_search}/{if $mod_search=='services'}c-{/if}{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{#details#}</a>
                    </div>
                </div>
            </div>
            {/if}
    	{/foreach}
        {if $mod_search=='services'}   
            {if $num_rows1 > $limit}
            <div class="listPage">
                {$num_rows1|page:$limit:$smarty.get.page:$paging_path}
            </div>
            {/if}
        {elseif $mod_search=='product'}   
            {if $num_rows2 > $limit}
            <div class="listPage">
                {$num_rows2|page:$limit:$smarty.get.page:$paging_path}
            </div>
            {/if}
        {elseif $mod_search=='blog'}   
            {if $num_rows3 > $limit}
            <div class="listPage">
                {$num_rows3|page:$limit:$smarty.get.page:$paging_path}
            </div>
            {/if}
        {elseif $mod_search=='about'}   
            {if $num_rows4 > $limit}
            <div class="listPage">
                {$num_rows4|page:$limit:$smarty.get.page:$paging_path}
            </div>
            {/if}
        {else} 
            {if $num_rows > $limit}
            <div class="listPage">
                {$num_rows|page:$limit:$smarty.get.page:$paging_path}
            </div>
            {/if}
        {/if}
</div>
