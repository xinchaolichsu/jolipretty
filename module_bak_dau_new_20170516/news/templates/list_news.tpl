<div class="box_center">
	<div class="box_center_title">{#NEWS#} {$full_cat_name}</div>
    <div class="box_center_content">
    	{foreach from = $listItem item = item name = item}
        {if $smarty.foreach.item.total <= 1}
            <div class="detailMod 11111">
                <div class="info">
                    <div class="name" style="height:50px;">
                        {$item.Name}
                    <div class="date">
                    	{$item.CreateDate|date_format:"%d/%m/%Y"}
                    </div>
                   <div class="share">
                        <div class="addthis_toolbox addthis_default_style addthis_25x25_style" style="padding-left:25px;">
                       <a class="addthis_button_google_plusone_share"></a>
                        <a class="addthis_button_facebook"></a>
                        <a class="addthis_button_zingme"></a>
                        <a class="addthis_button_govn"></a>
                        </div>
                        <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4f2a921d35198164"></script>
                   </div>
                    </div>
                    <div class="sum">
                        {$item.Summarise}
                    </div>
                </div>
                <div class="content">
                    {$item.Content}
                </div>            
            </div>
        {else}
        	<div class="listMod" {if $smarty.foreach.item.last} style="border:none;"{/if}>
                	<div class="name">
                    <div class="date">
                    	{$item.CreateDate|date_format:"%d/%m/%Y"}
                    </div>
                    	<a href="{$smarty.const.SITE_URL}news/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{$item.Name}</a>
                    </div>
            	<div class="img">
                	{if $item.Photo}
                    	<a href="{$smarty.const.SITE_URL}news/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}"><img src="{$item.Photo}" class="photo" alt="{$item.Name}"/></a>
					{/if}
                </div>
                <div class="info">
                    <div class="sum">
                    	{$item.Summarise|truncate:260}
                    </div>
                    <div class="view">
                    	<a href="{$smarty.const.SITE_URL}news/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{#details#}</a>
                    </div>
                </div>
            </div>
        {/if}
    	{/foreach}
        {if $num_rows > $limit}
        <div class="listPage">
            {$num_rows|page:$limit:$smarty.get.page:$paging_path}
        </div>
        {/if} 
	</div>
</div>

