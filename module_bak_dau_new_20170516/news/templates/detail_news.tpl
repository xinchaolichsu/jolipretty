<div class="box_center">
	<div class="box_center_title">{#NEWS#} {$full_cat_name}</div>
    <div class="box_center_content">
    	<div class="detailMod">
            <div class="info">
                <div class="name">
                {if $detail_item.CreateDate}
                <div class="date">
                    {$detail_item.CreateDate|date_format:"%d/%m/%Y"}
                </div>
                {/if}
                    {$detail_item.Name}
                </div>
                <div class="sum">
                    {$detail_item.Summarise}
                </div>
            </div>
            <div class="content">
            	{$detail_item.Content}
            </div>
            
            {if $otherlist}
            <div class="other_item">
                <div class="title">{#other_news#}</div>
                {foreach item=item from=$otherlist}
                    <div class="name">
                    	<a href="{$smarty.const.SITE_URL}news/c-{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{$item.Name}</a>
                        <span>{$item.CreateDate|date_format:"%d/%m/%Y"}</span>
					</div>
                {/foreach}
            </div>
            {/if}
        
        </div>
    </div>
</div>
