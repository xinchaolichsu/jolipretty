<div class="box_left">
	<div class="side_title">{#NEWS#}</div>
	<div class="side_content" style="width:200px;">
		{foreach from = $cat item=item name=item}
		<div class="product_cat">
<a {if $smarty.get.id==$item.id} style="color:#474747;"{/if} href="{$smarty.const.SITE_URL}news/c-{$item.id}/{$item.Name|remove_marks}.html">{$item.Name}</a></div>
                {if ($smarty.get.id == $item.id || $parent == $item.id)}
                {foreach from=$item.sub item=subitem name=subitem}
                        <div class="product_sub"><a {if $smarty.get.id==$subitem.id} style="color:#474747;"{/if} href="{$smarty.const.SITE_URL}news/c-{$subitem.id}/{$subitem.Name|remove_marks}.html">{$subitem.Name}</a></div>
                {/foreach}
                {/if}
		{/foreach}
	</div>
</div>