<div class="box_center">
	<div class="box_center_title">{#NEWS#} {$full_cat_name}</div>
    <div class="box_center_content">
    	<div class="detailMod">
            <div class="info">
                <div class="name" style="height:50px;">
                    {$cat_news.Name}
                </div>
                   <div class="share">
                        <div class="addthis_toolbox addthis_default_style addthis_25x25_style" style="padding-left:25px;">
                       <a class="addthis_button_google_plusone_share"></a>
                        <a class="addthis_button_facebook"></a>
                        <a class="addthis_button_zingme"></a>
                        <a class="addthis_button_govn"></a>
                        </div>
                        <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4f2a921d35198164"></script>
                   </div>
                <div class="sum">
                    {$cat_news.Summarise}
                </div>
            </div>
            <div class="content">
            	{$cat_news.Content}
            </div>            
        </div>
    </div>
</div>
