<?php
/**
 * VietEsoft
 * ===============================================================================================
 * Copyright 2010.
 * Website: http://www.vietesfot.com, http://thietkewebsitetrongoi.net
 * -----------------------------------------------------------------------------------------------
 * Phát triển bới VietEsoft
 * -----------------------------------------------------------------------------------------------
 * Chú ý: Tất cả các hàm, phương thức được viết mới do mình tự định nghĩa phải để lại comment gồm:
 	- Người viết
	- Ngày-tháng-năm
	- Chức năng
 * ===============================================================================================
 * Quản lý nhóm người dùng
*/
if (!defined('IN_VES')) die('Hacking attempt');
class groups extends Bsg_Module_Base 
{
	var $smarty;
	var $db;
	var $url="";
    var $datagrid;
	var $id;
	var $table='';
	var $arrAction;
	var $imgPath;
	var $refTable;
	var $refId;
	
	public function __construct($oSmarty, $oDb, $oDatagrid)
	{
		$this -> smarty = $oSmarty;
		$this -> db = $oDb;		
        $this -> datagrid = $oDatagrid;
		$this -> id=$_REQUEST[id];		
		$this -> table = "tbl_sys_groups";	 		
		parent::__construct($oDb);		
		$this->bsgDb->setTable($this->table);	
	    parent::getRootPath();
		
	}
	
	function run($task)
	{
		global $oDb;
		if( isset($_GET['arr_check']) )$str_id=implode(',',$_GET['arr_check']);	
		switch ($task)
		{						
			case 'delete':
				$this->bsgDb->deleteWithPk($this->id);
				$oDb->query("DELETE FROM tbl_sys_groups_module WHERE GroupID = '".$this->id."'");
				parent::redirect($_COOKIE['re_dir']); 
				break;
			case 'delete_all':				
				$this->bsgDb->deleteWithPk($str_id);
				$oDb->query("DELETE FROM tbl_sys_groups_module WHERE GroupID IN ($str_id)");
				parent::redirect($_COOKIE['re_dir']); 
				break;			
			case 'unpublic_all':
				$this->bsgDb->updateWithPk($str_id,  array("Status"=>"0"));
				parent::redirect($_COOKIE['re_dir']);
				break;
			case 'public_all':
				$this->bsgDb->updateWithPk($str_id,  array("Status"=>"1"));
				parent::redirect($_COOKIE['re_dir']);
				break;	
			case 'list':
				$this->list();
				break;
			case 'add':	
			case 'edit':				
				$this->insertUpdate($task);
				break;					
			default:								
					$this->listRecordContentGuild();				
				break;
		}
	}
	
	function deleteImage($id, $field, $path){
		if($id == '')
			return;
		$imgpath = $path.$this->db->getOne("SELECT $field FROM ".$this->table." WHERE id = $id");
		if(is_file($imgpath))			
			@unlink($imgpath);
	}
	
	function getPageInfo($task){
		return true;
	}
		
	function insertUpdate($task)
	{	
		global $oDb;
		if($this->isPost())
		{
			$data = array (
				"Name" => $_POST['Name'],
				"Summarise" => $_POST['Summarise'],
                "Status" => $_POST['Status'],
                "CreateDate" => $_POST['CreateDate']
			);				
			//Prosess Insert Update		
			if($task=='add'){
				$oDb->autoExecute($this->table, $data, DB_AUTOQUERY_INSERT);
				$this->id = $oDb->getOne("SELECT Max(id) as id FROM ".$this->table);
			}elseif($task=='edit')
				$this->bsgDb->updateWithPk($this->id, $data);
			
			// Delete tbl_sys_users_module info 
			$oDb->query("DELETE FROM tbl_sys_groups_module WHERE GroupID = '".$this->id."'");

			foreach($_POST['roll'] as $key=>$value){
                $arr_roll = $this->splitRoll($value);
                $data = array(
                    "GroupID" => $this->id,
                    "ModuleID" => $arr_roll[0],
                    "RollID"    => $arr_roll[1],
                );
                $oDb->autoExecute('tbl_sys_groups_module', $data, DB_AUTOQUERY_INSERT);
            }
			parent::redirect($_COOKIE['re_dir']);		
			//End Prosess Insert Update		
		}			
		
		$this->makeFormContentGuild($task);	
	}	
	
	function listRecordContentGuild()
	{
		$startdate = date('Y-m-d',strtotime("-1 month",strtotime(date("Y-m-d"))));
		$enddate = date('Y-m-d', time());	
		
		$arr_cols= array(
			array(
				"field" => "id",
				"datatype" => "text",
				"primary_key" => true,
				"visible" => "hidden",
				"sortable" => true,
				"searchable" => true
			),		
			array(
				"field" => "Name",
				"display" => "Tên nhóm",
				"datatype" => "text",
				"sortable" => true,
				"searchable" => true
			),
			array(
				"field" => "CreateDate",
				"display" => "Ngày tạo",
				"datatype" => "date",
				"sortable" => true,
				"searchable" => true
			),
			array(
				"field" => "Status",
				"display" => 'Trạng thái',
				"datatype" => "boolean",
				"sortable" => true,
				"width" 		=> "100",				
			)
		);
		// 
		$arr_filter= array(	
			array(
				"field" 	=> "Name",		
				"display" 	=> 'Tên nhóm',
				"type" 		=> "text",
				"style"		=> "width:200px",
				"selected" 	=> isset($_REQUEST["username"])?$_REQUEST["username"]:"",
				"filterable"=>true
			)
		);		
		$arr_check = array(
			array(
			'task' => 'delete_all',
			'confirm'	=> 'Are you sure?',
			'display' => 'Xóa tất cả'
			),
			array(
				'task' 	=> 'public_all',
				'confirm'	=> 'Are you sure?',
				'display' 	=> 'Kích hoạt',
			),
			array(
				'task' 	=> 'unpublic_all',
				'confirm'	=> 'Are you sure?',
				'display' 	=> 'Vô hiệu',
			)
		);
		$arr_action = parent::getAct();	
		$this -> datagrid -> display_datagrid($this->table, $arr_cols, $arr_filter, "?".$_SERVER['QUERY_STRING'], $arr_action, null, $root_path, false,$arr_check);			

	}
	
	function makeFormContentGuild ($task, $id=0)
	{		
		global $oDb;
		$form = new HTML_QuickForm('frmGuild','post',$_COOKIE['re_dir']."&task=".$_GET['task']);		
		if($task == 'edit'){			
			$row = $this->bsgDb->getRow($this->id);
			
			$result = $oDb->getAssoc("SELECT id, ModuleID FROM tbl_sys_users_module WHERE UserID = ".$this->id);
			foreach($result as $k=>$v){
				$row['module'][$v] = "1";
			}
			
			$form->setDefaults($row);		
		}
		
		$form->addElement('text', 'Name', 'Tên nhóm', array('size' => 50, 'maxlength' => 255));
         $form->addElement('textarea','Summarise','Mô tả vắn tắt',array('style'=>'width:600px; height:200px;'));
		
		$assign_roll = $this->getRoll();
        $form -> addElement('static',NULL,'Phân quyền',$assign_roll);
		$date_time = parent::date_time('CreateDate',($data['CreateDate']) ? ($data['CreateDate']) : date("Y-m-d h:i"));            
        $form -> addElement('static',NULL,'Ngày tạo',$date_time);
		$cbActive = array("1"=>"Kích hoạt","0"=>"Vô hiệu");
		$form -> addElement("select","Status",'Trạng thái',$cbActive,array("style" => "width:100px;"));
		
		$btn_group[] = &HTML_QuickForm::createElement('submit',null,'Hoàn tất');
		
        $btn_group[] = &HTML_QuickForm::createElement('button',null,'Quay lại',array('onclick'=>'window.location.href = \''.$_COOKIE['re_dir'].'\''));
      	
        $form -> addGroup($btn_group);
      
		$form->addElement('hidden', 'id', $row['id']);
		
        $form -> addRule('username','Nhập lại tên đăng nhập','required',null,'client');
		$form -> addRule('newpassword','Nhập lại mật khẩu','required',null,'client');
		$form -> addRule(array('newpassword','cfpassword'),'Hai mật khẩu không trùng nhau','compare',null,'client');
		
		$form->display();
	}
    
    function getRoll()
    {
        global $oSmarty,$oDb;                
        $module = $oDb->getAll("SELECT id, Name FROM tbl_sys_menu WHERE ParentID = 0 ORDER BY Zindex");
        foreach ($module as $key=>$value)
        {
            $sub_module = $oDb->getAll("SELECT id, Name FROM tbl_sys_menu WHERE Editable = 1 AND ParentID = {$value["id"]}");
            if ($sub_module)
            {
                $module[$key]["sub_module"] = $sub_module;
                $check_full_roll = TRUE;
                foreach ($sub_module as $k=>$val)
                {
                    $sql = "SELECT RollID FROM tbl_sys_groups_module WHERE GroupID='{$this->id}' AND ModuleID='{$val["id"]}'";
                    $current_roll = $oDb->getCol($sql);
                    $full_roll = $oDb->getCol("SELECT roll_id FROM tbl_sys_module_roll WHERE module_id = '{$val["id"]}'");
                    $result = array_diff($full_roll, $current_roll);
                    if (empty($result))
                        $module[$key]["sub_module"][$k]["is_full_roll"] = TRUE;
                    else 
                        $check_full_roll = FALSE;    
                    $module[$key]["sub_module"][$k]["current_roll"] = $current_roll;
                    $full_id_roll = implode(",",$full_roll);
                    $display_full_roll = $oDb->getAll("SELECT id, name FROM tbl_sys_roll WHERE id IN ($full_id_roll) ORDER BY ordered");
                    $module[$key]["sub_module"][$k]["full_roll"] = $display_full_roll;
                }
                $module[$key]["is_full_roll"] = $check_full_roll;
            }
            else 
            {
                $sql = "SELECT RollID FROM tbl_sys_groups_module WHERE GroupID='{$this->id}' AND ModuleID='{$value["id"]}'";
                $current_roll = $oDb->getCol($sql);
                $full_roll = $oDb->getCol("SELECT roll_id FROM tbl_sys_module_roll WHERE module_id = '{$value["id"]}'");
                $result = array_diff($full_roll, $current_roll);
                if (empty($result))
                    $module[$key]["is_full_roll"] = TRUE;
                $module[$key]["current_roll"] = $current_roll;
                $full_id_roll = implode(",",$full_roll);
                $display_full_roll = $oDb->getAll("SELECT id, name FROM tbl_sys_roll WHERE id IN ($full_id_roll) ORDER BY ordered");
                $module[$key]["full_roll"] = $display_full_roll;
            }
        }
        $oSmarty->assign("module",$module);
        return $oSmarty->fetch('assign_roll.tpl');
    }
    function splitRoll($str)
    {
        return $arr = explode("-",$str);
    }
}
?>
