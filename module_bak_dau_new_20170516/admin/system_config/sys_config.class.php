<?php
/**
 * VietEsoft
 * ===============================================================================================
 * Copyright 2010.
 * Website: http://www.vietesfot.com, http://thietkewebsitetrongoi.net
 * -----------------------------------------------------------------------------------------------
 * Ph�t tri?n b?i VietEsoft
 * -----------------------------------------------------------------------------------------------
 * Ch� �: T?t c? c�c h�m, phuong th?c du?c vi?t m?i do m�nh t? d?nh nghia ph?i d? l?i comment g?m:
 	- Ngu?i vi?t
	- Ng�y-th�ng-nam
	- Ch?c nang
 * ===============================================================================================
 * Qu?n l� th�ng thin trang web
*/
if (!defined('IN_VES')) die('Hacking attempt');
class sys_config extends Bsg_Module_Base 
{
	var $smarty;
	var $db;
	var $url="";
    var $datagrid;
	var $id;
	var $table='';
	var $arrAction;
	
	public function __construct($oSmarty, $oDb, $oDatagrid)
	{
	
		$this -> smarty = $oSmarty;
		$this -> db = $oDb;		
        $this -> datagrid = $oDatagrid;
		$this -> id=$_REQUEST[id];
		
		$this -> table	="tbl_sys_config";
		
		parent::__construct($oDb);
		$this->bsgDb->setTable($this->table);	
		parent::getRootPath();
	}
	
	function run($task)
	{
		if( isset($_GET['arr_check']) )$str_id=implode(',',$_GET['arr_check']);
	
		switch ($task)
		{						
			case 'delete':	
				$this->bsgDb->deleteWithPk($this->id);
				parent::redirect($_COOKIE['re_dir']); 
				break;
			case 'delete_all':				
				$this->bsgDb->deleteWithPk($str_id);
				parent::redirect($_COOKIE['re_dir']); 
				break;			
			case 'unpublic_all':
				$this->bsgDb->updateWithPk($str_id,  array("status"=>"0"));
				parent::redirect($_COOKIE['re_dir']);
				break;
			case 'public_all':
				$this->bsgDb->updateWithPk($str_id,  array("status"=>"1"));
				parent::redirect($_COOKIE['re_dir']);
				break;	
			case 'list':
				$this->list();
				break;
			case 'add':
				$this->insert($_GET['sub']);
				break;
			case 'edit':
				$this->update($_GET['sub']);
				break;					
			default:								
					$this->listRecordModul();				
				break;
		}
		

	}
	
	function getPageInfo($task)
	{
		return true;
	}
	
	function insert($sub)
	{	
		if($this->isPost($_POST)){				
			$this->bsgDb->insert($_POST);
			parent::redirect($_COOKIE['re_dir']);
		}
		$this->makeFormProduct('add');
	}
	
	
	function update($sub)
	{				
		if($this->isPost()){			
			$this->bsgDb->updateWithPk($this->id, $_POST);
		}
		
		if($_POST['btnSubmit'] == '')
			$this->makeFormProduct('edit');
		else
			$this->listRecordModul();
	}
	
		
	function listRecordModul()
	{					
		$arr_cols= array(
			array(
				"field" => "id",
				"datatype" => "text",
				"primary_key" => true,
				"visible" => "hidden",
				"sortable" => true,
				"searchable" => true
			),		
			array(
				"field" => "Name",
				"display" => "Name",
				"datatype" => "text",
				"sortable" => true,
				"searchable" => true
			),
			array(
				"field" => "Description",
				"display" => 'Description',
				"datatype" => "text",
				"sortable" => true,
				"searchable" => true
			),
			array(
				"field" => "Value",
				"display" => 'Value',
				"datatype" => "text",
				"sortable" => true,
				"searchable" => true
			)		
		);
		
		
		$arr_check = array(
			array(
			'task' => 'delete_all',
			'confirm'	=> 'Are you sure?',
			'display' => 'Delete all'
			)
		);
		
		$arr_action = parent::getAct();
		$this -> datagrid -> display_datagrid($this->table, $arr_cols, $arr_filter, "?".$_SERVER['QUERY_STRING'], $arr_action, null, $root_path, false,$arr_check);			

	}
    
	
	
	
	function makeFormProduct ($task, $id=0)
	{		
		$form = new HTML_QuickForm('frmCategory','post',$_COOKIE['re_dir']."&task=".$_GET['task']);
		
        $att_arr = array('size' => 50, 'maxlength' => 255);  
        
		if($task == 'edit'){
			$row = $this->bsgDb->getRow($this->id);
			$form->setDefaults($row);	
            $att_arr['readonly'] = 'readonly';
		}              
		
		$form->addElement('text', 'Name', "Name", $att_arr);
		$form->addElement('text', 'Description', "Description", $att_arr);
		$form->addElement('text', 'Value', "Value", array('size' => 50, 'maxlength' => 255));
        
        $btn_group[] = &HTML_QuickForm::createElement('submit',"btnSubmit",'Submit');
		
        $btn_group[] = &HTML_QuickForm::createElement('button',null,'Back',array('onclick'=>'window.location.href = \''.$_COOKIE['re_dir'].'\''));
      
        $form -> addGroup($btn_group);
      
		$form->addElement('hidden', 'id', $row['category_id']);
		
        $form -> addRule('Name','Name cannot be blank','required',null,'client');
		
		$form->display();

	}
}
?>
