<?php
/**
 * VietEsoft
 * ===============================================================================================
 * Copyright 2010.
 * Website: http://www.vietesfot.com, http://thietkewebsitetrongoi.net
 * -----------------------------------------------------------------------------------------------
 * Ph�t tri?n b?i VietEsoft
 * -----------------------------------------------------------------------------------------------
 * Ch� �: T?t c? c�c h�m, phuong th?c du?c vi?t m?i do m�nh t? d?nh nghia ph?i d? l?i comment g?m:
 	- Ngu?i vi?t
	- Ng�y-th�ng-nam
	- Ch?c nang
 * ===============================================================================================
 * Qu?n l� module
*/
if (!defined('IN_VES')) die('Hacking attempt');
class manage_module extends Bsg_Module_Base{
	var $smarty;
	var $db;
	var $url="";
    var $datagrid;
	var $id;
	var $imgPath;
	var $imgPathShort;
	var $table='';
	var $arrAction;
	public function __construct($oSmarty, $oDb, $oDatagrid)
	{	
		$this -> smarty = $oSmarty;
		$this -> db = $oDb;		
        $this -> datagrid = $oDatagrid;
		$this -> id=$_REQUEST[id];		
		$this -> table	="tbl_sys_menu";	 		
		parent::__construct($oDb);		
		$this->bsgDb->setTable($this->table);	
		//parent::getRootPath();
		$this->imgPath = SITE_DIR."upload/admin/";
		$this->imgPathShort = "/upload/admin/";
	}
	
	function run($task)
	{	
		switch( $task ){
			case 'add':
				$this -> addItem();
				break;
			case 'edit':
				$this -> editItem();
				break;
			case 'delete':
				$this -> deleteItem();
				break;
			case 'delete_all':
				$this -> deleteItems();
				break;
			case 'change_status':				
				$this -> changeStatus($_GET['id'], $_GET['status']);
				break;
			case 'public_all':						
				$this -> changeStatusMultiple( 1 );
				break;
			case 'unpublic_all':						
				$this -> changeStatusMultiple( 0 );
				break;
			case 'save_order':
				$this -> saveOrder();
				break;
			default:					
				$this -> listItem( $_GET['msg'] );		
				break;
		}
	}
	
	function getPageInfo()
	{
		return true;
	}

	function addItem()
	{
		$this -> getPath("System Config >> Manage Module >> Add Module ");		
		$this -> buildForm();
	}
	
	function editItem()
	{
		$id = $_GET['id'];
		$this -> getPath("System Config >> Manage Module >> Edit Module with id: {$id}");	
		$row = $this -> bsgDb -> getRow( $id );
		$this -> buildForm( $row );
	}

	function deleteItem()
	{
		global  $oDb;
		$id = $_GET["id"];
		/*$sql = "delete from tbl_usertype_moduleroll where module_roll_id in (select id from tbl_sys_module_roll where module_id ='{$id}')";
		$res = $oDb -> query( $sql );
		$sql = "delete from tbl_sys_module_roll where module_id ='{$id}'";
		$res = $oDb -> query( $sql );*/
        $sql = "SELECT id FROM {$this->table} WHERE ParentID=$id";
        $arr_id_to_del = $oDb->getCol($sql);
        $id_to_del = implode(",",$arr_id_to_del);
        if($id_to_del != "")
            $id_to_del .= ",".$id;
        else
            $id_to_del = $id;
        $oDb->query("DELETE FROM tbl_sys_users_module WHERE ModuleID IN ($id_to_del)");
        $oDb->query("DELETE FROM tbl_sys_module_roll WHERE module_id IN ($id_to_del)");
		$this -> bsgDb -> deleteWithPk( $id_to_del );
		$msg = "Item has been deleted at ". date('Y-m-d h:i:s');
		$this -> redirect($_COOKIE['re_dir']. "&msg={$msg}");
	}
	
	function deleteItems()
	{
		$aItems	 = $_GET['arr_check'];
		if(is_array( $aItems) && count( $aItems) > 0){
			$sItems = implode( ',', $aItems );
            $sql = "SELECT id FROM {$this->table} WHERE ParentID IN ($sItems)";
            $arr_id_to_del = $oDb->getCol($sql);
            $id_to_del = implode(",",$arr_id_to_del);
            $id_to_del .= ",".$sItems;
            $oDb->query("DELETE FROM tbl_sys_users_module WHERE ModuleID IN ($id_to_del)");
            $oDb->query("DELETE FROM tbl_sys_module_roll WHERE module_id IN ($id_to_del)");
			$this -> bsgDb -> deleteWithPk( $id_to_del );
		}
		$msg = "Item(s) has been deleted successfull!";
		$this -> redirect($_COOKIE['re_dir']. "&msg={$msg}");
	}
	
	function changeStatusMultiple( $status = 0 )
	{
		$aItems	 = $_GET['arr_check'];
		if(is_array( $aItems) && count( $aItems) > 0){
			$sItems = implode( ',', $aItems );
			$this -> bsgDb -> updateWithPk( $sItems, array("Status" => $status) );
		}
		$msg = "Item(s) has been change status successfull!";
		$this -> redirect($_COOKIE['re_dir']. "&msg={$msg}");
	}
	
	function saveOrder(){	
		$aItem = $_GET['Zindex'];
		if(is_array($aItem) && count( $aItem ) > 0){
			// save order for item.
			foreach( $aItem as $key => $value){
				if( !is_numeric($value)) $value = 0;				
				$this -> bsgDb -> updateWithPk( $key, array('Zindex' => $value ));
			}
		}	
		$msg = "Item(s) has been save order successfull!";
		$this -> listItem( $msg );
	}
	
	function getRoll($moduleId){
		global $oDb;
		$sResult = "";
		$stbl1 = 'tbl_sys_roll';
		$aChecked = array();
		if( $moduleId ){
			$stbl2 = 'tbl_sys_module_roll';
			if($moduleId ) $where = " and module_id = '{$moduleId}'";
			$sql = "SELECT t1.id FROM {$stbl1} t1 join (SELECT * FROM {$stbl2} WHERE 1 {$where}) t2 on(t1.id = t2.roll_id) WHERE 1";
			$aChecked = $oDb -> getCol( $sql );
		}
		
		$sql = "SELECT  id, name FROM {$stbl1} WHERE 1 ORDER BY ordered";
		$result = $oDb -> getAssoc( $sql );
		if(count($result) > 0){
			foreach ( $result as $key => $val){
				if( in_array( $key, $aChecked )) $sChecked = "checked=\"checked\"";
				else $sChecked = "";
				$sResult .= "<input type=\"checkbox\" name=\"module_roll[]\" value=\"{$key}\" {$sChecked}>{$val} &nbsp;&nbsp;&nbsp;";
			}
		}
		
		return $sResult;
	}
	
	function removeRoll( $moduleId ){
		global $oDb ;
		$stbl ="tbl_sys_module_roll";
		if( $moduleId ){			
			$sql = "DELETE FROM {$stbl} WHERE module_id = '{$moduleId}'";
			$oDb -> query ( $sql );
		}
	}
	
	function addRoll( $moduleId, $aRollId ){
		global $oDb ;
		$stbl = "tbl_sys_module_roll";
		foreach( $aRollId as $key => $val ){
			$sql = "INSERT INTO {$stbl}(module_id, roll_id) VALUES ('{$moduleId}', '{$val}')";
			$oDb -> query ( $sql );
		}
	}
	
	function buildForm( $data=array() , $msg = ''){
		
		$form = new HTML_QuickForm('frmAdmin','post',$_COOKIE['re_dir']."&task=".$_GET['task'], '', "style='padding:10px 15px 0 20px;'");
		
		$form -> setDefaults($data);		
		$form -> addElement('text', 'Name', 'Name', array('size' => 50, 'maxlength' => 255));
		$form -> addElement('text', 'Link', 'Link', array('size' => 50, 'maxlength' => 255));
		$aParent = array(0 => "- - - Root Module - - -" ) + $this->getParentModule();
		
		$form -> addElement('select', 'ParentID', 'Parent', $aParent);
		$form->addElement('file', 'Photo', 'Photo');
		
		if($_GET['task']=='edit')
			$form->addElement('static', null, '',"<a href='".SITE_URL.$data['Photo']."' onclick='return hs.expand(this)' class='highslide'><img src='".SITE_URL.$data['Photo']."' width=70 hight=70 border=0></a>");
		$form -> addElement('text', 'Zindex', 'Order', array('size' => 10, 'maxlength' => 50));
		$form -> addElement("static", null, "Select Roll", $this -> getRoll( $data['id']));
		$form -> addElement('checkbox', 'Status', 'Status');
		
		$btn_group[] = $form -> createElement('submit',null,'Save',array("style"=> "border:1px solid gray; padding:0 5px 0 5px;"));		
        $btn_group[] = $form -> createElement('button',null,'Go Back',array('onclick'=>'window.location.href = \''.$_COOKIE['re_dir'].'\'', "style"=> "border:1px solid gray;"));      
        $form -> addGroup($btn_group);
      
		$form->addElement('hidden', 'id', $data['id']);		
		
        $form -> addRule('title','Title cannot be blank','required',null,'client');
		$reg = "/[a-zA-Z0-9\_]+$/";
		$form -> addRule('title', "Title can not contain special character or space", 'regex', $reg);
		$form -> addRule('order', 'Order must be a number', 'numeric');
		
		if( $form -> validate())
		{	
			if($_FILES['Photo']['name']!='')
			{
				$this->deleteImage($this->id, "Photo", SITE_DIR);
				$FileName = rand().'_'.$_FILES['Photo']['name'];												
				parent::crop_image($_FILES['Photo']['tmp_name'],$FileName, $this->imgPath,"70");				
				$_POST['Photo'] = $this->imgPathShort.$FileName;				
			}
			
			$aData  = array(
				"Name" => $_POST['Name'],
				"Link" => $_POST['Link'],
				"ParentID" => $_POST['ParentID'],
				"Zindex" 	=> $_POST['Zindex'],
				"Status" 	=> $_POST['Status'],
				"Photo"		=> $_POST['Photo']
			);
			if( !$_POST['id'] ){
				
				 $id = $this -> bsgDb -> insert($aData);
				 if( is_array($_POST['module_roll']) && count( $_POST['module_roll']) > 0){
				 	$this -> addRoll( $id, $_POST['module_roll']);
				 }
				 $msg = "Item has been inserted at ". date('Y-m-d h:i:s');
			}else {
				$id = $_POST['id'];				
				$this -> bsgDb -> updateWithPk($id, $aData);
				$this -> removeRoll( $id );
				if( is_array($_POST['module_roll']) && count( $_POST['module_roll']) > 0){
				 	$this -> addRoll( $id, $_POST['module_roll']);
				}
				$msg = "Item has been updated at ". date('Y-m-d h:i:s');
			}
			
			$this -> redirect($_COOKIE['re_dir']. "&msg={$msg}");
		}
		
		$form->display();
	}
	
	function getParentModule(){
		//@eval( getGlobalVars());
		global $oDb;
		$sTbl = $this -> table;
		
		$query = "SELECT id, CONCAT('&nbsp;&nbsp;&nbsp;',Name) FROM {$sTbl} WHERE ParentID=0";
		$result = $oDb -> getAssoc( $query );
		
		return $result;
	}
	
	function changeStatus( $itemId , $status ){
		$aData = array( 'Status' => $status );
		$this -> bsgDb -> updateWithPk( $itemId, $aData );
		return true;
	}
	
	function listItem( $sMsg= '' )
	{		
		global $oDb;
		global $oDatagrid;				
		
		$root_path = "System config > Manage Module > List Module";						
		$submit_url= "/index.php?".$_SERVER['QUERY_STRING'];
		$table = $this -> table;
		$order = ($_GET['sort_by'])?($_GET['sort_by']):'Zindex';
		$orderType = $_GET['sort_value'];
		if( $_GET['filter_title']!= '')
			$where[] = " Name like '{$_GET['filter_title']}'";
		if( $_GET['filter_show']!= '')
			$where[] = " Status = '{$_GET['filter_show']}'";
		
		//$where[] = " Editable = '1'";	
		if( is_array( $where) && count( $where )> 0)
			$condition = implode( " and ", $where );
		
		$aData = $this -> multiLevel( $table, "id", "ParentID", "*", "{$condition}", "{$order} {$orderType}");
		
		foreach ( $aData as $key => $row){
			if( $row['level'] > 0){				
				$aData[$key]['Name'] = $this -> getPrefix( $row['level']).$row['Name'];
			}
		}
		
		$arr_filter= array(
			array(
				'field' => 'Name',
				'display' => 'Name',
				'type' => 'text',
				'name' => 'filter_title',
				'selected' => $_REQUEST['filter_title'],
				'filterable' => true
			),
			array(
				'field' => 'Status',
				'display' => 'Status',				
				'name' => 'filter_show',
				'selected' => $_REQUEST['filter_show'],
				'options' => array('No','Yes'),
				'filterable' => true
			)
			
		); 
		
		$arr_cols= array(		
			
			array(
				"field" => "id",					
				"primary_key" =>true,
				"display" => "Id",				
				"align" => "center",
				"sortable" => true
			),	
			array(
				"field" => "Name",
				"display" => "Name",
				"align"	=> 'left',				
				"datatype" => "text",
				"sortable" => true
			),
			array(
				"field" 		=> "Photo",
				"display" 		=> 'Photo',
				"datatype" 		=> "img",
				"img_path"		=> SITE_URL,
                "width"			=> "70",		
			),		
			array(
				"field" => "Zindex",
				"display" => "Order",
				"datatype" 		=> "order",
				"sortable" => true,
				"order_default" => "asc"
			),	
			array(
				"field" => "Status",
				"display" => "Status",				
				"datatype" => "publish",
				"sortable" => true
			),		
			array(
				"field" => "CreateDate",
				"display" => "Create Date",				
				"datatype" => "date",
				"sortable" => true
			)
		);		
		
		$arr_check = array(
			array(
				"task" => "delete_all",
				"display" => "Delete all"
			),
			array(
				"task" => "public_all",
				"display" => "Public all"
			),
			array(
				"task" => "unpublic_all",
				"display" => "UnPublic all"
			)			
		);
		if( $sMsg )
			$oDatagrid -> setMessage( $sMsg );
		$oDatagrid->display_datagrid($aData, $arr_cols, $arr_filter, $submit_url, $this -> getAct() ,120, $root_path, false ,$arr_check);		
		
	}		
	
}

?>