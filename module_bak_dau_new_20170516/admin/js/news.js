﻿// JavaScript Document
$(document).ready(function(){
	$("#filter_cat").change(function(){
		var cat_id = $("#filter_cat").val();
		$.ajax({
			type: "POST",
			url: "index.php?mod=admin&amod=news&atask=news_comment&task=ajax_filter&ajax",
			data: "cat_id="+cat_id,
			success: function(response_text){
				$("#filter_article").html(response_text);	
			}
		});
	});	
	$("#cat_form_lang").change(function(){
		var lang_id = $("#cat_form_lang").val();
		$.ajax({
			type: "POST",
			url: "index.php?mod=admin&amod=news&atask=news_category&task=ajax_form&ajax",
			data: "lang_id="+lang_id,
			success: function(response_text){
				$("#cat_form_parent").html(response_text);	
			}
		});
	});
    
    $("#filter_lang").change(function(){
        var lang_id = $("#filter_lang").val();
        $.ajax({
            type: "POST",
            url: "index.php?mod=admin&amod=news&atask=news&task=ajax_filter&ajax",
            data: "lang_id="+lang_id,
            success: function(response_text){
                $("#filter_cat").html(response_text);    
            }
        });
    });
    
    $("#news_form_lang").change(function(){
        var lang_id = $("#news_form_lang").val();
        $.ajax({
            type: "POST",
            url: "index.php?mod=admin&amod=news&atask=news&task=ajax_form&ajax",
            data: "lang_id="+lang_id,
            success: function(response_text){
                $("#news_form_cat").html(response_text);    
            }
        });
    });
})
