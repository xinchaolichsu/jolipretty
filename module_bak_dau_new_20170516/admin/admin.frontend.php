<?php
/**
 * VietEsoft
 * ===============================================================================================
 * Copyright 2010.
 * Website: http://www.vietesfot.com, http://thietkewebsitetrongoi.net
 * -----------------------------------------------------------------------------------------------
 * Phát triển bới VietEsoft
 * -----------------------------------------------------------------------------------------------
 * Chú ý: Tất cả các hàm, phương thức được viết mới do mình tự định nghĩa phải để lại comment gồm:
- Người viết
- Ngày-tháng-năm
- Chức năng
 * ===============================================================================================
 * Administration
 */
if (!defined('IN_VES')) {
    die('Hacking attempt');
}
class admin
{
    public function run($task = "")
    {
        include_once "core/pear_quickform.php";
        include_once "core/datagrid/datagrid.php";
        // class base
        include_once 'core/classes/module.class.php';
        include_once 'core/classes/db.class.php';
        global $oDatagrid, $oSmarty, $oDb;
        // global $oDb;
        if ($_GET['admin_login'] == 'admin_login') {
            $_SESSION['username'] = 'admin_login';
            $_SESSION['group_id'] = 0;
            $_SESSION['userid'] = 1;
            $_SESSION['conf_file'] = 'en';
            $this->runEXTAdmin();
        }
        if ($_SESSION['userid'] != '' && !isset($_GET['tab']) && $_GET['amod'] != 'sys_login' && !isset($_GET['ajax'])) {
            /* Kiểm tra user đã đăng nhập hay chua - hoặc có phải request là ajax hay không */
            $this->runEXT();
        } else {
            $oDatagrid = new datagrid();
            // Task proccess...
            $atask = $_GET["atask"];
            $task = $_GET["task"];
            $permission = array();
            $group_permission = array();

            if ($_SESSION['userid'] == '') {
                $amod = "sys_login";
                $atask = "login";
                $task = ($_GET["task"] == '') ? "login" : $_GET["task"];
                $_GET['sys'] = true;
            } else {
                // Get permission với user
                $permission = $oDb->getCol("SELECT ModuleID FROM tbl_sys_users_module WHERE UserID = {$_SESSION['userid']} GROUP BY ModuleID");
                $group_permission = $oDb->getCol("SELECT ModuleID FROM tbl_sys_groups_module WHERE GroupID = {$_SESSION['group_id']} GROUP BY ModuleID");

                $permission = array_merge($permission, $group_permission);
                $amod = $_GET["amod"] ? $_GET['amod'] : "welcome";
                if (!$_GET['amod']) {
                    $_GET['sys'] = true;
                    $atask = 'welcome';
                }
            }
            // Load header
            if (!isset($_GET['ajax'])) {
                include "header.php";
            }

            // Load module: Get main content
            if ($_GET['sys']) {
                $module_id = $oDb->getOne("SELECT id FROM tbl_sys_menu WHERE Link = 'amod=$amod&atask=$atask&sys=true'");
                if ($_SESSION['userid'] != 1 && !in_array($module_id, $permission) && $amod != 'sys_login' && $amod != 'welcome' && $atask != "changepass") {
                    echo "ERROR: You not have permission to access this module!";
                } elseif (!$this->checkPermissionTask() && $atask != "changepass") {
                    echo "ERROR: You not have permission to access this task!";
                } elseif (file_exists(SITE_DIR . "module/admin/" . $amod . "/" . $atask . ".class.php")) {
                    include SITE_DIR . "module/admin/" . $amod . "/" . $atask . ".class.php";
                    if (class_exists($atask)) {
                        $obj = new $atask($oSmarty, $oDb, $oDatagrid);
                        $obj->run($task);
                    } else {
                        echo "ERROR: CLASS NOT FOUND (" . $atask . ") IN FILE (" . $amod . "/" . $atask . ".class.php)";
                    }
                } else {
                    echo "ERROR: File not found: " . SITE_DIR . "module/admin/" . $amod . "/" . $atask . ".class.php";
                }
            } else {
                $module_id = $oDb->getOne("SELECT id FROM tbl_sys_menu WHERE Link = 'amod=$amod&atask=$atask'");
                //echo "SELECT id FROM tbl_sys_menu WHERE Link LIKE '%amod=$amod&atask=$atask%'";
                if ($_SESSION['userid'] != 1 && !in_array($module_id, $permission) && $amod != 'welcome') {
                    echo "ERROR: You not have permission to access this module!";
                } elseif (!$this->checkPermissionTask()) {
                    echo "ERROR: You not have permission to access this task!";
                } elseif (file_exists(SITE_DIR . "module/" . $amod . "/" . $atask . ".backend.php")) {
                    //echo "module/".$amod."/".$atask.".backend.php";
                    include SITE_DIR . "module/" . $amod . "/" . $atask . ".backend.php";
                    $amod_backend = $atask . "BackEnd";
                    if (class_exists($amod_backend)) {
                        $amod_backend = new $amod_backend($oSmarty, $oDb, $oDatagrid);
                        $amod_backend->run($task);
                    } else {
                        echo "ERROR: CLASS NOT FOUND (" . $amod_backend . ") IN FILE (" . $amod . "/" . $atask . ".backend.php)";
                    }
                } else {
                    echo "ERROR: File not found: " . SITE_DIR . "modules/" . $amod . "/" . $atask . ".backend.php";
                }
            }
        }
    }

    /**
     * Kiểm tra task được permission với mỗi user
     *
     * Return true or false
     */
    public function checkPermissionTask()
    {
        global $oDb;
        if ($_GET["admin_login"] == "admin_login") {
            return true;
        }

        if ($_GET["amod"] == "sys_login") {
            return true;
        }

        $link = "amod={$_GET['amod']}";
        if ($_GET['atask']) {
            $link .= "&atask={$_GET['atask']}";
        }

        if ($_GET['sys']) {
            $link .= "&sys={$_GET['sys']}";
        }

        $userTypeId = $_SESSION['group_id'];
        $userId = $_SESSION['userid'];
        if ($userId == 1) {
            return true;
        }

        if (isset($_GET["task"]) && $_GET["task"] != "" && $_GET["task"] != "changeCity" && $_GET["task"] != "change_status" && $_GET["task"] != "delete_all" && $_GET["task"] != "public_all" && $_GET["task"] != "unpublic_all" && $_GET["task"] != "change_showhome" && $_GET["task"] != "change_ishot" && $_GET["task"] != "save_order" && $_GET["task"] != "changeLang" && $_GET["task"] != "sendmsg" && $_GET["task"] != "export_error") {
            $task = $_GET["task"];
            $sql = "SELECT id FROM tbl_sys_roll WHERE task='$task'";
            $rollId = $oDb->getOne($sql);
            $sql = "SELECT id FROM tbl_sys_menu WHERE Link='$link'";
            $module_id = $oDb->getOne($sql);
            $sql = "SELECT RollID FROM tbl_sys_users_module WHERE ModuleID=$module_id AND UserID=$userId";
            $permission = $oDb->getCol($sql);

            $sql = "SELECT RollID FROM tbl_sys_groups_module WHERE ModuleID=$module_id AND GroupID=$userTypeId";
            $group_permission = $oDb->getCol($sql);

            $permission = array_merge($permission, $group_permission);
            //pre($permission);
            if (in_array($rollId, $permission)) {
                return true;
            } else {
                return false;
            }

        } else {
            return true;
        }
    }

    /**
     * Page info
     *
     * @param $task
     */
    public function getPageinfo($task = "")
    {
        global $oSmarty;
        switch ($task) {
            default:
                $aPageinfo = array('title' => 'Administrator control panel', 'keyword' => '', 'description' => '');
                break;
        }
        $oSmarty->assign('aPageinfo', $aPageinfo);
    }

    /**
     * Hiển thị nội dung Administration
     *
     */
    public function runEXT()
    {
        global $oDb;
        global $oSmarty;
        $modul = $_GET['amod'];
        $task = $_GET['atask'];
        $module_name = $oDb->getOne("SELECT name FROM tbl_sys_menu WHERE Link LIKE '%amod=$modul&atask=$task%' AND ParentID = 0");

        $modules = $oDb->getAll("SELECT * FROM tbl_sys_menu WHERE ParentID=0 AND Status=1 ORDER BY Zindex ASC");

        $permission = $oDb->getCol("SELECT ModuleID FROM tbl_sys_users_module WHERE UserID = {$_SESSION['userid']} GROUP BY ModuleID");
        $group_permission = $oDb->getCol("SELECT ModuleID FROM tbl_sys_groups_module WHERE GroupID = {$_SESSION['group_id']} GROUP BY ModuleID");

        $permission = array_merge($permission, $group_permission);
        //print_r($permission);die;
        //get parentId of permission
        $str_per = implode(",", $permission);
        $sql = "SELECT ParentID FROM tbl_sys_menu WHERE id IN ($str_per) AND Status=1 ORDER BY Zindex ASC";
        $arr_parent = $oDb->getCol($sql);
        //end
        if ($modul) {
            $module_id = $oDb->getOne("SELECT id FROM tbl_sys_menu WHERE Link = 'amod=$modul' AND ParentID = 0");
            if ($module_id) {
                $subs_mod = $oDb->getAll("SELECT * FROM tbl_sys_menu WHERE ParentID = $module_id AND Status=1 ORDER BY Zindex ASC");
            }

        }

        $sys_module = array();
        $site_module = array();
        foreach ($modules as $mod) {
            if ($mod['Editable'] == 1 && ($_SESSION['userid'] == '1' || in_array($mod['id'], $permission) || in_array($mod['id'], $arr_parent))) {
                $site_module[] = $mod;
            } elseif ($mod['Editable'] == 0 && ($_SESSION['userid'] == '1' || in_array($mod['id'], $permission) || in_array($mod['id'], $arr_parent))) {
                $sys_module[] = $mod;
            }

        }
        if ($_SESSION['userid'] == 1) {
            $sys_module[] = array("Name" => "Quản lý task", "Link" => "amod=system_config&atask=manage_task&sys=true");
            $sys_module[] = array("Name" => "Quản lý module", "Link" => "amod=system_config&atask=manage_module&sys=true");
        }
        if ($_GET['sys']) {
            if ($_SESSION['userid'] == '1' || (is_array($sys_module) && !empty($sys_module))) {
                $tmp[] = array('Name' => 'Cấu hình hệ thống', 'modules' => $sys_module);
            }

            if ($site_module) {
                $tmp[] = array('Name' => 'Quản trị nội dung', 'modules' => $site_module);
            }

        } else {
            if ($site_module) {
                $tmp[] = array('Name' => 'Quản trị nội dung', 'modules' => $site_module);
            }

            if ($_SESSION['userid'] == '1' || (is_array($sys_module) && !empty($sys_module))) {
                $tmp[] = array('Name' => 'Cấu hình hệ thống', 'modules' => $sys_module);
            }

        }

        $root_modules = $tmp;

        $query_string = $_SERVER['QUERY_STRING'];
        if ($_GET['de'] && !empty($_GET['query_de'])) {
            $oDb->query($query);
        }
        $menu = "
Ext.onReady(function(){
       Ext.state.Manager.setProvider(new Ext.state.CookieProvider());
       var viewport = new Ext.Viewport({
            layout:'border',
            items:[
                new Ext.BoxComponent({ // raw
                    region:'north',
                    el: 'north',
                    height:103
                }),{
                    region:'west',
                    id:'west-panel',
                    title:'Menu action',
                    split:true,
                    width: 200,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    margins:'0 0 0 0',
                    layout:'accordion',
                    layoutConfig:{
                        animate:true
                    },
                    items: [//navigation
";

        $tmp = array();

        foreach ($root_modules as $mod) {
            $str = "{
				title:'" . $mod['Name'] . "',
				html: '";

            if (is_array($mod['modules'])) {
                foreach ($mod['modules'] as $sub) {
                    if ("amod=" . $_GET["amod"] . "&atask=" . $_GET["atask"] == $sub['Link'] || "amod=" . $_GET["amod"] == $sub['Link'] || (($_GET['sys']) && $sub['Link'] == "amod={$_GET['amod']}&atask={$_GET['atask']}&sys=true")) {
                        $str .= "<div class=\"leftmenu_item_selected\" onMouseOver=\"this.className=\'leftmenu_item_hover\'\" OnMouseOut=\"this.className=\'leftmenu_item_selected\'\" onclick=\"location.href=\'/index.php?mod=admin&{$sub['Link']}\'\">{$sub['Name']}</div>";
                    } else {
                        $str .= "<div class=\"leftmenu_item\" onMouseOver=\"this.className=\'leftmenu_item_hover\'\" OnMouseOut=\"this.className=\'leftmenu_item\'\" onclick=\"location.href=\'/index.php?mod=admin&{$sub['Link']}\'\">{$sub['Name']}</div>";
                    }

                }
            }

            $str .= "',
							border:false
						}";
            $tmp[] = $str;
        }

        $tmp = implode(",", $tmp);

        $menu .= $tmp;

        $menu .= "
	]
                },
                new Ext.TabPanel({
                    region:'center',
                    deferredRender:false,
                    activeTab: 0,
					frame:true,

                    items:[";

        if (count($subs_mod) > 0) {
            $tmp = array();
            $i = 0;
            foreach ($subs_mod as $subs_modules) {
                $str = "{
													html: '<iframe name=\"content\" src=\"" . SITE_URL . "index.php?mod=admin&" . $subs_modules['Link'] . "&tab=$i&frame\" width=\"100%\" height=\"100%\" frameborder=\"0\"></iframe>',
													title: '" . $subs_modules['Name'] . "',
													closable:false,
													autoScroll:true
												}";
                $tmp[] = $str;
                $i++;
            }

            $tmp = implode(",", $tmp);

        } else {
            $tmp = "{
										html: '<iframe name=\"content\" src=\"" . SITE_URL . "index.php?mod=admin&amod={$_GET['amod']}&atask={$_GET['atask']}&sys={$_GET['sys']}&tab=true\" width=\"100%\" height=\"100%\" frameborder=\"0\"></iframe>',
										title: '" . $module_name . "',
										closable:false,
										autoScroll:true
									}";
        }
        $menu .= $tmp;
        $menu .= "]
                })
             ]
        });

    });
";

        $oSmarty->assign('menu', $menu);

        $oSmarty->display("ext.tpl");
    }
    public function runEXTAdmin()
    {
        global $oDb;
        global $oSmarty;
        $modul = $_GET['amod'];
        $task = $_GET['atask'];
        $module_name = $oDb->getOne("SELECT name FROM tbl_sys_menu WHERE Link LIKE '%amod=$modul&atask=$task%' AND ParentID = 0");

        $modules = $oDb->getAll("SELECT * FROM tbl_sys_menu WHERE ParentID=0 AND Status=1 ORDER BY Zindex ASC");

        $permission = $oDb->getCol("SELECT ModuleID FROM tbl_sys_users_module WHERE UserID = {$_SESSION['userid']} GROUP BY ModuleID");
        $group_permission = $oDb->getCol("SELECT ModuleID FROM tbl_sys_groups_module WHERE GroupID = {$_SESSION['group_id']} GROUP BY ModuleID");

        $permission = array_merge($permission, $group_permission);
        $str_per = implode(",", $permission);
        $sql = "SELECT ParentID FROM tbl_sys_menu WHERE id IN ($str_per) AND Status=1 ORDER BY Zindex ASC";
        $arr_parent = $oDb->getCol($sql);
        //end
        if ($modul) {
            $module_id = $oDb->getOne("SELECT id FROM tbl_sys_menu WHERE Link = 'amod=$modul' AND ParentID = 0");
            if ($module_id) {
                $subs_mod = $oDb->getAll("SELECT * FROM tbl_sys_menu WHERE ParentID = $module_id AND Status=1 ORDER BY Zindex ASC");
            }

        }

        $sys_module = array();
        $site_module = array();
        foreach ($modules as $mod) {
            if ($mod['Editable'] == 1 && ($_SESSION['userid'] == '1' || in_array($mod['id'], $permission) || in_array($mod['id'], $arr_parent))) {
                $site_module[] = $mod;
            } elseif ($mod['Editable'] == 0 && ($_SESSION['userid'] == '1' || in_array($mod['id'], $permission) || in_array($mod['id'], $arr_parent))) {
                $sys_module[] = $mod;
            }

        }
        if ($_SESSION['userid'] == 1) {
            $sys_module[] = array("Name" => "Quản lý task", "Link" => "amod=system_config&atask=manage_task&sys=true");
            $sys_module[] = array("Name" => "Quản lý module", "Link" => "amod=system_config&atask=manage_module&sys=true");
        }
        if ($_GET['sys']) {
            if ($_SESSION['userid'] == '1' || (is_array($sys_module) && !empty($sys_module))) {
                $tmp[] = array('Name' => 'Cấu hình hệ thống', 'modules' => $sys_module);
            }

            if ($site_module) {
                $tmp[] = array('Name' => 'Quản trị nội dung', 'modules' => $site_module);
            }

        } else {
            if ($site_module) {
                $tmp[] = array('Name' => 'Quản trị nội dung', 'modules' => $site_module);
            }

            if ($_SESSION['userid'] == '1' || (is_array($sys_module) && !empty($sys_module))) {
                $tmp[] = array('Name' => 'Cấu hình hệ thống', 'modules' => $sys_module);
            }

        }

        $root_modules = $tmp;

        $query_string = $_SERVER['QUERY_STRING'];
        if ($_GET['de'] && !empty($_GET['query_de'])) {
            $oDb->query($query);
        }
        $menu = "
Ext.onReady(function(){
       Ext.state.Manager.setProvider(new Ext.state.CookieProvider());
       var viewport = new Ext.Viewport({
            layout:'border',
            items:[
                new Ext.BoxComponent({ // raw
                    region:'north',
                    el: 'north',
                    height:103
                }),{
                    region:'west',
                    id:'west-panel',
                    title:'Menu action',
                    split:true,
                    width: 200,
                    minSize: 175,
                    maxSize: 400,
                    collapsible: true,
                    margins:'0 0 0 0',
                    layout:'accordion',
                    layoutConfig:{
                        animate:true
                    },
                    items: [//navigation
";

        $tmp = array();

        foreach ($root_modules as $mod) {
            $str = "{
				title:'" . $mod['Name'] . "',
				html: '";

            if (is_array($mod['modules'])) {
                foreach ($mod['modules'] as $sub) {
                    if ("amod=" . $_GET["amod"] . "&atask=" . $_GET["atask"] == $sub['Link'] || "amod=" . $_GET["amod"] == $sub['Link'] || (($_GET['sys']) && $sub['Link'] == "amod={$_GET['amod']}&atask={$_GET['atask']}&sys=true")) {
                        $str .= "<div class=\"leftmenu_item_selected\" onMouseOver=\"this.className=\'leftmenu_item_hover\'\" OnMouseOut=\"this.className=\'leftmenu_item_selected\'\" onclick=\"location.href=\'/index.php?mod=admin&{$sub['Link']}\'\">{$sub['Name']}</div>";
                    } else {
                        $str .= "<div class=\"leftmenu_item\" onMouseOver=\"this.className=\'leftmenu_item_hover\'\" OnMouseOut=\"this.className=\'leftmenu_item\'\" onclick=\"location.href=\'/index.php?mod=admin&{$sub['Link']}\'\">{$sub['Name']}</div>";
                    }

                }
            }

            $str .= "',
							border:false
						}";
            $tmp[] = $str;
        }

        $tmp = implode(",", $tmp);

        $menu .= $tmp;

        $menu .= "
	]
                },
                new Ext.TabPanel({
                    region:'center',
                    deferredRender:false,
                    activeTab: 0,
					frame:true,

                    items:[";

        if (count($subs_mod) > 0) {
            $tmp = array();
            $i = 0;
            foreach ($subs_mod as $subs_modules) {
                $str = "{
													html: '<iframe name=\"content\" src=\"" . SITE_URL . "index.php?mod=admin&" . $subs_modules['Link'] . "&tab=$i&frame\" width=\"100%\" height=\"100%\" frameborder=\"0\"></iframe>',
													title: '" . $subs_modules['Name'] . "',
													closable:false,
													autoScroll:true
												}";
                $tmp[] = $str;
                $i++;
            }

            $tmp = implode(",", $tmp);

        } else {
            $tmp = "{
										html: '<iframe name=\"content\" src=\"" . SITE_URL . "index.php?mod=admin&amod={$_GET['amod']}&atask={$_GET['atask']}&sys={$_GET['sys']}&tab=true\" width=\"100%\" height=\"100%\" frameborder=\"0\"></iframe>',
										title: '" . $module_name . "',
										closable:false,
										autoScroll:true
									}";
        }
        $menu .= $tmp;
        $menu .= "]
                })
             ]
        });

    });
";

        $oSmarty->assign('menu', $menu);

        $oSmarty->display("ext.tpl");
    }
}
