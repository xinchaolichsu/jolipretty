<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Administrator Control Panel</title>
<head>
<script type="text/javascript" src="{$smarty.const.SITE_URL}core/jquery/jquery.js"></script> 
<link href="{$smarty.const.SITE_URL}core/highslide/highslide.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{$smarty.const.SITE_URL}core/highslide/highslide-with-gallery.js"></script>
<link rel="stylesheet" type="text/css" href="{$smarty.const.SITE_URL}core/ext/resources/css/ext-all.css"/>
<script type="text/javascript" src="{$smarty.const.SITE_URL}core/ext/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="{$smarty.const.SITE_URL}core/ext/ext-all.js"></script>
<script type="text/javascript">
	{$menu}
</script><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

{*literal}
	
	<script type="text/javascript">
    Ext.onReady(function(){

        // NOTE: This is an example showing simple state management. During development,
        // it is generally best to disable state management as dynamically-generated ids
        // can change across page loads, leading to unpredictable results.  The developer
        // should ensure that stable state ids are set for stateful components in real apps.
        Ext.state.Manager.setProvider(new Ext.state.CookieProvider());
        
       var viewport = new Ext.Viewport({
            layout:'border',
            items:[
                new Ext.BoxComponent({ // raw
                    region:'north',
                    el: 'north'
                }),{
                    region:'west',
                    id:'west-panel',
                    title:'Action menu',
                    split:true,
                    width: 200,
                    minSize: 200,
                    maxSize: 200,
                    collapsible: true,
                    margins:'0 0 0 5',
                    layout:'accordion',
					autoScroll:true,
                    layoutConfig:{
                        animate:true
                    },
                    items: [{
                        title:'System config',
                        html:'google',
						border:false
                    },{
                        title:'System config',
                        html:'<p>Some settings in here.</p>',
						border:false                    
                    },{
                        title:'System config',
                        html:'<p>Some settings in here.</p>',
						border:false
                    },{
                        title:'Manage user',
                        html:'<p>Some settings in here.</p>',
                        border:false
                    }]
                },
                new Ext.TabPanel({
                    region:'center',
                    deferredRender:true,
                    activeTab:0,
                    items:[{
                        title: 'Google',
						html:'<iframe name=\"content\" src=\"http://www.google.com" width=\"100%\" height=\"100%\" frameborder=\"0\"></iframe>',
                        closable:false,
                        autoScroll:true
                    }]
                })
             ]
        });
    });
	</script>
{/literal*}

{literal}     
	<style type="text/css">
body
{
	text-align: left;
	padding: 0px;
	height: 100%;
	margin: 0px;
	font-size:12px;
	font-family:Tahoma, Helvetica, sans-serif;
}
#div_root
{
	position: relative;
	background-image: url(images/bg_div_root.gif);
	background-repeat: repeat-x;
	width: 100%;
	height: 25px;	
	text-align: left;
	font-size: 14px;
	font-weight: bold;
	color: #FFFFFF;	
}

#div_search_control
{
	width: 100%;	
	background-image: url(images/bg_div_search_control.jpg);
	background-repeat: repeat;	
}

#div_content
{
	width: 100%;
	height: 100%;
	min-height: 400px;
	background: url(images/bg_div_search_control.jpg) top left repeat-x;
	text-align:left;	
}
#div_add
{
	width:100%;
	border:1px solid #000000;	
	text-align:right;
	margin-right:5px;
}
#div_title
{
   border:1px solid #000000;	
   with:100%;
   height:30px;
   	
}
.span_title{
	
	font-weight:bold;
}
.span_title_stt{
	
	width:100px;
}
.span_title_title{
	
	width:500px;	
	border:1px solid #100100100;
}
.td_border
{
	border-bottom:1px solid #999999;
	border-left:1px solid #E1E1E1;
}
.tr_title
{
	 background:#3C619D;
	 color:#FFFFFF;
	 height:25px;
	 font-weight:bold;
}

.td_active
{
	width:50px;
	text-align:center;
}
.td_add_edit
{
	width:80px;
	text-align:center;
}
.td_date
{
	width:100px;
	text-align:center;
}
.td_address
{
	width:300px;
	text-align:center;
}
.td_content_100,td_content_120,td_content_150,td_content_200
{
	width:10%;
	text-align:left;
}
.td_content_120
{
	width:12%;
}
.td_content_150
{
	width:15%;
}
.td_content_200
{
	width:20%;
}
.image_link
{
	border:0px;
	cursor:pointer;
}
.paging
{
	color:#FFFFFF; font-size:12px;
	font-weight:bold;
}
.pointer{
	cursor: pointer;
}
/*------------------------------------------------------------*/
/* Style cho the link                                         */
a:link, a:visited, a:hover 
{
	color: #18397E;
	text-decoration: none;
}

a:hover
{
	text-decoration: underline;
}

/*------------------------------------------------------------*/
/* ?ặt style cho Text 										  */
.text_format_normal /* Dung cho kiểu chữ thuong */
{
	font-style: normal;
}
.text_format_italic /* Dung cho kiểu chữ nghiêng */
{
	font-style: italic;
}
.text_format_bold /* Dung cho kiểu chữ đậm*/
{
	font-weight: bold;
}
.text_size_11 /* Dung cho kiểu chữ cỡ 11*/
{
	font-size: 11px;
}
.text_size_12 /* Dung cho kiểu chữ cỡ 12*/
{
	font-size: 12px;
}
.text_size_13 /* Dung cho kiểu chữ cỡ 13*/
{
	font-size: 13px;
}
.text_size_14 /* Dung cho kiểu chữ cỡ 14*/
{
	font-size: 14px;
}
.text_size_18 /* Dung cho kiểu chữ cỡ 18*/
{
	font-size: 18px;
}

/*-----------------------------------------------------------------------*/
/* CSS su dung cho the Button                                            */

button,reset,submit{
	border:1px solid gray;
}
.button_60, .button_100
 { 
	border:1px solid #FFCC00;	
	font-weight:bold;
	font-size:12px;
	color:#333333;
	height:21px;
	cursor:pointer;
	text-align:center;
	background:url(../images/bg_button.gif) repeat-x; 
}

.button_60 /* CSS su dung cho button dac biet - EX: Button "Dang ki" va "Tim kiem "o trang chu*/
{
	width: 60px;	
}
.button_80 /* CSS su dung cho button dac biet - EX: Button "Dang ki" va "Tim kiem "o trang chu*/
{ 
	border:1px solid gray
}
.button_100 /* CSS su dung cho button dac biet - EX: Button "Dang ki" va "Tim kiem "o trang chu*/
{ 
	width: 100px;
}

/*-------------------------------------------------------------------*/
/*CSS su dung chung cho cac the  Textbox                             */

.textbox_80, .textbox_100, .textbox_150, .textbox_200, .textbox_250  
{
	border:1px solid #C2CBCD;		
	padding:1px;	
	padding-bottom:1px;
	background:#FFFFFF;
	font-size:12px;
	color:#333333;
	text-align:left;
}

.textbox_80 /* CSS su dung cho Textbox binh thuong */
{ 
	width: 80px;
}
.textbox_100 /* CSS su dung cho Textbox binh thuong */
{ 
	width: 100px;
}
.textbox_150 /* CSS su dung cho Textbox binh thuong */
{ 
	width: 150px;
}
.textbox_200 /* CSS su dung cho Textbox binh thuong */
{ 
	width: 200px;
}
.textbox_250 /* CSS su dung cho Textbox dac biet*/
{ 
	width: 250px;
}

/*-----------------------------------------------------------*/
/* CSS su dung cho cac the select                            */
.combobox_80, .combobox_100, .combobox_150,.combobox_200,.combobox_250 
{ 
	border:1px solid #7d7d7d;
	padding-left:3px;
	height:18px;		
	font-size:12px;
	color:#333333;
	text-align:left;
}
.combobox_80 /* CSS su dung cho Combobox binh thuong*/
{ 
	width: 80px;
}
.combobox_100 /* CSS su dung cho Combobox binh thuong*/
{ 
	width: 100px;
}
.combobox_150 /* CSS su dung cho Combobox binh thuong*/
{ 
	width: 150px;
}
.combobox_200 /* CSS su dung cho Combobox binh thuong*/
{ 
	width: 200px;
}
.combobox_250 /* CSS su dung cho cac Combobox dac biet*/
{ 
	width: 250px;
}		

/*****************************************************/
/* Xay dung he thong datagrid quan ly                    */

#table
{
	border:1px #7c8494 solid;
	color:#747c8c;
}
.tr_mau1
{
	background:#FFFFFF;	
	height:23px;
}
.tr_mau2
{
	background:#EFEFEF;
	height:23px;
}
.tr_hover
{
	background:#E1FDF5;
	height:23px;
}


.paging_div {	
	color: #2238be;
	font-family:Tahoma, Helvetica, sans-serif;
	font-size:11px; 	
}
.paging_div span A:link, .paging_div span A:visited {
	text-decoration: none;
	color:#666666;
	font-size:11px;
}
.paging_div span A:hover{
	text-decoration: underline;
	font-weight: bold;
}
.paging_current_class{
	font-weight: bold;
	color:#2238be;	
	text-decoration:underline;
}
.paging_link_class{
	font-weight: normal;
	color: #2238be;	
	border: none;
}

.span_a_class
{
	background-color:#D9D9D9; 
	border:1px solid gray;
	font-weight:bold;
}

.span_select_class
{
	font-weight:bold;
	background-color:#FD8105;
	color:#FFFFFF;
	border:1px solid gray;
}
.td_border
{
	border-bottom:1px solid #999999;
	border-left:1px solid #E1E1E1;
}
.tr_title
{
	 background:#3C619D;
	 color:#FFFFFF;
	 height:25px;
	 font-weight:bold;
}
.td_index
{
	width:20px;
	text-align:center;	
}

.td_add_edit
{
	width:80px;
	text-align:center;
}
.td_date
{
	width:100px;
	text-align:center;
}
</style>
	<style type="text/css">
		p {
			margin:5px;
		}
		.settings {
			background-image:url(images/folder_wrench.png);
		}
		.nav {
			background-image:url(images/folder_go.png);
		}
		.leftmenu_item, .leftmenu_item_selected, .leftmenu_item_hover{
			width: 100%;
			height: 25px;
			text-align: left;
			text-decoration: none;
			font-size: 12px;
			border-bottom: 1px solid #99BBE8;
			background-color:#DAE9FB;
			cursor: pointer;
			padding-top: 5px;
			padding-left: 20px;
		}
		.leftmenu_item_selected{		
			background-color:#F0F6FD;
		}
		.leftmenu_item_hover{	
			background-color:#F0F6FD;
		}
		</style>
{/literal}
</head>
<body>
<div id="north" style="height: 87px; margin-bottom: 10px; padding: 3px 10px 3px 10px; background:url('{$smarty.const.SITE_URL}images/bg_header.jpg') repeat-x">
	<div style="float: left; font-weight: bold; font-size:25px; margin-top:30px; color:#FFFFFF">Administrator control panel</div>
	
	<div style="float: right; margin-top:30px;margin-right:15px;;text-align:center">
    <a style="cursor:pointer" onclick="location.href='{$smarty.const.SITE_URL}index.php?mod=admin&amod=sys_login&atask=login&task=logout&sys=true'"><img src="{$smarty.const.SITE_URL}images/logout.png"/><br><span style="color:#FFFFFF">
	Log out </span>
	</a>
	</div>
	
	<div style="float: right; margin-top:30px;margin-right:15px;text-align:center">
    <a style="cursor:pointer" onclick="location.href='{$smarty.const.SITE_URL}index.php?mod=admin&amod=system_config&atask=changepass&sys=true'"><img src="{$smarty.const.SITE_URL}images/key.png"/><br><span style="color:#FFFFFF">
	Change Password </span>
	</a>
	</div>
	
	<div style="float: right; margin-top:30px;margin-right:15px;;text-align:center">
    <a style="cursor:pointer" onclick="location.href='{$smarty.const.SITE_URL}index.php?mod=admin&amod=welcome&atask=welcome&sys=true'"><img src="{$smarty.const.SITE_URL}images/home.png"/><br><span style="color:#FFFFFF">
	Home </span>
	</a>
	</div>
	
	
</div>
<div id="west">
</div>
</body>
</html>