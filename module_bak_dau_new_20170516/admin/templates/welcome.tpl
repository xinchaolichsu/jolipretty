<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Admin panel</title>
{literal}
<style type="text/css" >
  /* header block */
   table.adminform {
		background-color: #f9f9f9;
		border: solid 1px #d5d5d5;
		width: 100%;
		padding: 10px;
		padding-left:0px;
		border-collapse: collapse;
   }
   
   table.adminform td{
        padding-bottom:5px;
		vertical-align:top;
		text-align:center;
   }
    
  /* header block */
    table.adminheading {
		background-color: #FFF;
		font-family : Arial, Helvetica, sans-serif;
		margin: 0px;
		padding: 0px;
		border: 0px;
		width: 100%;
		border-collapse: collapse;
		color: #C64934;
		font-size : 18px;
		font-weight: bold;
		text-align: left;
	}
	table.adminheading th {
		background: url(images/cpanel.png) no-repeat left;
		text-align: left;
		height: 50px;
		width: 99%;
		padding-left: 50px;
		border-bottom: 5px solid #fff;
	}
	
	#ilog{
	  	text-align: center;
		display: block;
		margin:5px;
		float: left;
	    height: 227px !important;
		height: 250px; 
		width: 250px !important;
		width: 300px; 
		vertical-align: bottom; 
		text-decoration : none;
		border: 1px solid #DDD;
		padding: 2px 5px 1px 5px;
		background-color:#FFF;
	}
	
	#ilog span.username{
	 	font-size:18px; 
	}
	
	#ipanel
	{
	    text-align: center;
		vertical-align: top;
		width:100%;
		float:left;
		padding-top:20px;
	}
	
	#ipanel div div.icon{ margin: 3px; }
	
	#ipanel div div.icon a
	{
		display: block; float: left;
	    height: 130px !important;
		height: 120px; 
		width: 108px !important;
		width: 110px; 
		vertical-align: middle; 
		text-decoration : none;
		border: 1px solid #DDD;
		padding: 2px 5px 1px 5px;
   	}
	
	#ipanel div div.icon a:hover 

    {
		color : #333; 
		background-color: #f1e8e6;  
		border: 1px solid #c24733;
		padding: 3px 4px 0px 6px; 
    }
	
	#ipanel div div.icon a:active  {  color : #808080;  }
    #ipanel div  div.icon a:visited {  color : #808080;  }

    #ipanel div div.icon img { margin-top: 13px; }
    #ipanel div div.icon span { display: block; padding-top: 3px;}

		
</style>
{/literal}
</head>
<body><br />
      <table class="adminheading">
	        <tr>
			    <th class="cpanel"> &nbsp;&nbsp;Quản trị hệ thống </th>
			</tr>  
	  </table><br />
	   <table class="adminform" border="1" cellpadding="0" cellspacing="0">
	        <tr>
			   <td width="75%" >
			    	<div id="ipanel">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr><td><font style="color:#C64934; font-weight:bold; font-size:16px; float:left; padding-left:30px;">Cấu hình hệ thống</font></td></tr>
							<tr><td>
					   {foreach from=$sys_module item=i_wel key=k_wel}
							<div style="float:left;">
			                    <div class="icon">
								    <a style="cursor:pointer" onclick="window.parent.location.href='{$smarty.const.SITE_URL}index.php?mod=admin&{$i_wel.Link}'">
								       <img src="{$i_wel.Photo}" border="0" style="max-width:100px; min-height:70px"> <br>												
					                   <span>{$i_wel.Name}</span>
									</a> 
								</div>
							</div>							
					   {/foreach}
							</td></tr>
					   </table>
					</div>
                    <div id="ipanel" style="margin-top:20px;">
						<table width="100%" cellpadding="0" cellspacing="0" border="0">
							<tr><td><font style="color:#C64934; font-weight:bold; font-size:16px; float:left; padding-left:30px;">Quản trị nội dung</font></td></tr>
							<tr><td>
					   {foreach from=$site_module item=i_site key=k_site}
							<div style="float:left;">
			                    <div class="icon">
								    <a style="cursor:pointer" onclick="window.parent.location.href='{$smarty.const.SITE_URL}index.php?mod=admin&{$i_site.Link}'">
								       <img src="{$i_site.Photo}" border="0" style="max-width:100px; min-height:70px"> <br>												
					                   <span>{$i_site.Name}</span>
									</a> 
								</div>
							</div>							
					   {/foreach}
					   		</td></tr>
						</table>
					</div>
			   </td>
			   <td>
			        <div id="ilog">
					   <span class="username">
					    Xin chào: <font color="#FF0000"> {$smarty.session.username} </font>
					    </span> <br><br><br>
						Lần đăng nhập cuối: {$smarty.session.logtime} <br>
						Tại địa chỉ ip:   {$smarty.session.logip}   	
					    <br><br>
		<a style="text-decoration:none; color:#0033CC; cursor:pointer" onclick="window.parent.location.href='{$smarty.const.SITE_URL}index.php?mod=admin&amod=system_config&atask=changepass&sys=true'">Đổi mật khẩu</a>
					</div>
			   </td>
			</tr>  
	  </table>
</body>
</html>
