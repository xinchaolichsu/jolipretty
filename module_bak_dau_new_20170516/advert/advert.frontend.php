<?php
/**
 * VietEsoft
 * ===============================================================================================
 * Copyright 2010.
 * Website: http://www.vietesfot.com, http://thietkewebsitetrongoi.net
 * -----------------------------------------------------------------------------------------------
 * Phát triển bới VietEsoft
 * -----------------------------------------------------------------------------------------------
 * Chú ý: Tất cả các hàm, phương thức được viết mới do mình tự định nghĩa phải để lại comment gồm:
 	- Người viết
	- Ngày-tháng-năm
	- Chức năng
 * ===============================================================================================
 * Quản lý quảng cáo
*/
if (!defined('IN_VES')) die('Hacking attempt');
class advert
{	
	function run($task= "")
	{
		switch ($task) {
		
			case 'left':	
			global $oSmarty, $oDb;
			$where = " Status=1 ";
       		 if (MULTI_LANGUAGE)
            	$where .= " AND LangID = {$_SESSION['lang_id']} ";
				$sql = "SELECT * FROM tbl_advert where {$where} AND trai =1";	
				$arr=$oDb->getAll($sql);
				$oSmarty->assign("scroll_left",$arr);
				$oSmarty->display("scroll_left.tpl");
		break;
		
		case 'right':
		global $oSmarty, $oDb;
		$where = " Status=1 ";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$_SESSION['lang_id']} ";
				$sql = "SELECT * FROM tbl_advert where {$where} AND phai=1";	
				$arr=$oDb->getAll($sql);
				$oSmarty->assign("scroll_right",$arr);
				$oSmarty->display("scroll_right.tpl");
		break;
		case 'popup':
		global $oSmarty, $oDb;
		$where = " Status=1 ";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$_SESSION['lang_id']} ";
				$sql = "SELECT * FROM tbl_advert where {$where} AND popup=1 Limit 1";
				$popup=$oDb->getAll($sql);
				$oSmarty->assign("popup",$popup);
				$oSmarty->display("popup.tpl");
		break;
		case 'giua2':
		global $oSmarty, $oDb;
		$where = " Status=1 ";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$_SESSION['lang_id']} ";
				$sql = "SELECT * FROM tbl_advert where {$where} AND giua2=1";	
				$giua2=$oDb->getAll($sql);
				$oSmarty->assign("giua2",$giua2);
				$oSmarty->display("giua2.tpl");
		break;
		case 'giua3':
		global $oSmarty, $oDb;
		$where = " Status=1 ";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$_SESSION['lang_id']} ";
				$sql = "SELECT * FROM tbl_advert where {$where} Limit 1";	
				$giua3=$oDb->getAll($sql);
				$oSmarty->assign("giua3",$giua3);
				$oSmarty->display("giua3.tpl");
		break;
		
		case 'top':
		global $oSmarty,$oDb;
		$where = " Status=1 ";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$_SESSION['lang_id']} ";
		$sql = "SELECT * FROM tbl_advert where {$where} AND tren=1";
		$advert_top = $oDb->getAll($sql);
		$oSmarty->assign("advert_top",$advert_top);
		$oSmarty->display("advert_top.tpl");
		break;
		
		
		case 'partner_left':
		global $oSmarty,$oDb;
		$where = " Status=1 AND Is_left=1";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$_SESSION['lang_id']} ";
		$sql = "SELECT * FROM tbl_advert WHERE {$where} ORDER BY Ordering ASC";
		$advert_left = $oDb->getAll($sql);
		//pre($advert_left);
		$oSmarty->assign("advert_left",$advert_left);
		$oSmarty->display("partner_left.tpl");
		break;
		
		case 'partner_right':
		global $oSmarty,$oDb;
		$where = " Status=1  AND Is_right=1";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$_SESSION['lang_id']} ";
		$sql = "SELECT * FROM tbl_advert WHERE {$where} ORDER BY Ordering ASC";
		$advert_right = $oDb->getAll($sql);
		$oSmarty->assign("advert_right",$advert_right);
		$oSmarty->display("partner_right.tpl");
		break;
		
		case 'center':
		global $oSmarty,$oDb;
		$where = " Status=1  AND center=1";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$_SESSION['lang_id']} ";
		$sql = "SELECT * FROM tbl_advert WHERE {$where} ORDER BY Ordering ASC";
		$advert_center = $oDb->getAll($sql);
		$oSmarty->assign("advert_center",$advert_center);
		$oSmarty->display("partner_center.tpl");
		break;
		}
	}
}
?>