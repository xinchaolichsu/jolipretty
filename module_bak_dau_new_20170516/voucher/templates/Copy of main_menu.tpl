<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type='text/javascript' src="/lib/drop_menu/js/jquery.hoverIntent.minified.js"></script>
<script type='text/javascript' src='/lib/drop_menu/js/jquery.dcmegamenu.1.3.3.js'></script>
<link href="/lib/drop_menu/css/skins/blue.css" rel="stylesheet" type="text/css" />

{literal}
<script type="text/javascript">
$(document).ready(function($){
	$('#mega-menu-4').dcMegaMenu({
		//event: 'click',
		//fullWidth: true, /* true, false */
		rowItems: '3', /*1,2,3,4,5 */
		speed: 'medium', /* fast, slow */
		effect: 'slide' /* fade, slide */
	});
});
</script>
{/literal}
<div class="ground_menu">
<div class="main_menu">
<div class="blue">  
	<ul id="mega-menu-4" class="mega-menu">
    
    <li>
    	<a href="{$smarty.const.SITE_URL}" ><span {if $smarty.get.mod == ''} class="menu_active" {else} class="menu_item" onmouseover="$(this).removeClass('menu_item'); $(this).addClass('menu_active');" onmouseout="$(this).removeClass('menu_active'); $(this).addClass('menu_item');"  {/if}> 
        <span class="menu_bt_left"></span>
        <span class="menu_bt_center">{#HOME#}</span>
        <span class="menu_bt_right"></span>
        </span></a>
    </li>
    
    <li>
    	<a href="{$smarty.const.SITE_URL}product/" ><span {if $smarty.get.mod == 'product'} class="menu_active" {else} class="menu_item" onmouseover="$(this).removeClass('menu_item'); $(this).addClass('menu_active');" onmouseout="$(this).removeClass('menu_active'); $(this).addClass('menu_item');"  {/if}> 
        <span class="menu_bt_left"></span>
        <span class="menu_bt_center">{#PRODUCT#}</span>
        <span class="menu_bt_right"></span>
        </span></a>
        {if $cap1_pro}
        	<ul>
            	{foreach from=$cap1_pro item=cap1_pro name=cap1_pro}
            	<li>
                	<a href="{$smarty.const.SITE_URL}product/c-{$cap1_pro.id}/{$cap1_pro.Name|remove_marks}.html" title="{$cap1_pro.Name}">{$cap1_pro.Name}</a>
                    {if $cap1_pro.cap2_pro}
                    	<ul>
                        	{foreach from=$cap1_pro.cap2_pro item=cap2_pro name=cap2_pro}
                            	 <li>
                                 	 <a href="{$smarty.const.SITE_URL}product/c-{$cap2_pro.id}/{$cap2_pro.Name|remove_marks}.html" title="{$cap2_pro.Name}">{$cap2_pro.Name}</a>
                                 </li>
                            {/foreach}
                        </ul>
                    {/if}
				</li>
                {/foreach}
            </ul>
        {/if}
    </li>
    
    <li>
    	<a href="{$smarty.const.SITE_URL}about/" ><span {if $smarty.get.mod == 'about'} class="menu_active" {else} class="menu_item" onmouseover="$(this).removeClass('menu_item'); $(this).addClass('menu_active');" onmouseout="$(this).removeClass('menu_active'); $(this).addClass('menu_item');"  {/if}> 
        <span class="menu_bt_left"></span>
        <span class="menu_bt_center">{#ABOUT#}</span>
        <span class="menu_bt_right"></span>
        </span></a>
    </li>           
    
    <li>
    	<a href="{$smarty.const.SITE_URL}video/" ><span {if $smarty.get.mod == 'video'} class="menu_active" {else} class="menu_item" onmouseover="$(this).removeClass('menu_item'); $(this).addClass('menu_active');" onmouseout="$(this).removeClass('menu_active'); $(this).addClass('menu_item');"  {/if}> 
        <span class="menu_bt_left"></span>
        <span class="menu_bt_center">{#VIDEO#}</span>
        <span class="menu_bt_right"></span>
        </span></a>
    </li>
    
    <!--BEGIN Menu tự sinh -->
    {foreach from=$cap1 item=cap1 name=cap1}
    <li>
    	<a href="{$smarty.const.SITE_URL}info/c-{$cap1.id}/{$cap1.Name|remove_marks}.html" title="{$cap1.Name}"><span {if $smarty.get.mod == 'info' && $smarty.get.id == $item.id} class="menu_active" {else} class="menu_item" onmouseover="$(this).removeClass('menu_item'); $(this).addClass('menu_active');" onmouseout="$(this).removeClass('menu_active'); $(this).addClass('menu_item');"  {/if}> 
        <span class="menu_bt_left"></span>
        <span class="menu_bt_center">{$cap1.Name}</span>
        <span class="menu_bt_right"></span>
        </span></a>
        {if $cap1.cap2}
		<ul>
        	{foreach from=$cap1.cap2 item=cap2 name=cap2}
			<li>
            	<a href="{$smarty.const.SITE_URL}info/c-{$cap2.id}/{$cap2.Name|remove_marks}.html" title="{$cap2.Name}">{$cap2.Name}</a>	
                {if $cap2.cap3}	
                <ul>
                	{foreach from=$cap2.cap3 item=cap3 name=cap3}
                    	<li>
                        	<a href="{$smarty.const.SITE_URL}info/c-{$cap3.id}/{$cap3.Name|remove_marks}.html" title="{$cap3.Name}">{$cap3.Name}</a>	
						</li>
                    {/foreach}
                </ul>		
                {/if}
			</li>
			{/foreach}
		</ul>
        {/if}
	</li>
    {/foreach}
    <!--END Menu tự sinh -->
    
    </ul>
</div>
                <div class="cart_header">
                    {php}
                        loadModule("cart","show_count");
                    {/php}
                </div>
</div>
</div>

 
