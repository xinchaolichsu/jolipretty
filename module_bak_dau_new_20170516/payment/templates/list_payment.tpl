<div class="box_center">		
	<div class="box_center_title">
    	{#payment#} {$full_cat_name}
	</div>
	<div class="box_center_content">   
		{foreach from = $listItem item = item name = item}
        	<div class="listMod" {if $smarty.foreach.item.last} style="border:none;"{/if}>
            	<div class="img">
                	{if $item.Photo}
                    	<a href="{$smarty.const.SITE_URL}news/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}"><img src="{$item.Photo}" class="photo" alt="{$item.Name}"/></a>
					{/if}
                </div>
                <div class="info">
                	<div class="name">
                    	<a href="{$smarty.const.SITE_URL}news/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{$item.Name}</a>
                    </div>
                    <div class="date">
                    	{$item.CreateDate|date_format:"%H:%M - %d/%m/%Y"}
                    </div>
                    <div class="sum">
                    	{$item.Summarise|truncate:200}
                    </div>
                    <div class="view">
                    	<a href="{$smarty.const.SITE_URL}news/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{#details#}</a>
                    </div>
                </div>
            </div>
    	{/foreach}
        {if $num_rows > $limit}
        <div class="listPage">
            {$num_rows|page:$limit:$smarty.get.page:$paging_path}
        </div>
        {/if} 
	</div>
</div>


