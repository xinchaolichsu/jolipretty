<div class="box_center">		
	<div class="box_center_title">
		{#payment#} {$full_cat_name}
	</div>
	<div class="box_center_content"> 
    	<div class="detailMod">
        	<div class="img">
                {if $detail_item.Photo}
                    <img src="{$detail_item.Photo}" class="photo" alt="{$detail_item.Name}"/>
                {/if}
            </div>
            <div class="info">
                <div class="name">
                    {$detail_item.Name}
                </div>
                <div class="date">
                    {$detail_item.CreateDate|date_format:"%H:%M - %d/%m/%Y"}
                </div>
                <div class="sum">
                    {$detail_item.Summarise}
                </div>
            </div>
            <div class="content">
            	{$detail_item.Content}
            </div>
            
            {if $other_item}
            <div class="other_item">
                <div class="title">{#other_news#}</div>
                {foreach item=item from=$other_item}
                    <div class="name">
                    	<a href="{$smarty.const.SITE_URL}news/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{$item.Name}</a>
                        <span>{$item.CreateDate|date_format:"%d/%m/%Y"}</span>
					</div>
                {/foreach}
            </div>
            {/if}
        
        </div>
	</div>
</div>