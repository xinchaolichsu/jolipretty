<div class="box_center">
	<div class="gallery_new" style="color:#0a6699; font-weight:700; text-transform:uppercase;">{#letter#} <div onclick="showHide()" style="float:right; cursor:pointer; color:#ff0000;font-weight:700; margin:-5px 10px 5px 0;"><img  src="/view/images/background/shoowhide.png" style="margin:5px 8px 0 0;" /><div id="showhide_abc" style="float:right;font-weight:700;">Hide</div></div></div>
    <div id="showHideDiv" style="display:block; border:1px solid #e7e7e7; margin:0 0 10px 0;  padding:10px 0 10px 50px; border-radius:12px;">
		{$xmlstr}
    </div>
    <div class="box_center_content">
    	{foreach from = $listItem item = item name = item}
        	<div class="listMod" {if $smarty.foreach.item.last} style="border:none;"{/if}>
                	<div class="name">
                    <div class="date">
                    	{$item.CreateDate|date_format:"%d/%m/%Y"}
                    </div>
                    	<a href="{$smarty.const.SITE_URL}letter/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{$item.Name}</a>
                    </div>
            	<div class="img">
                	{if $item.Photo}
                    	<a href="{$smarty.const.SITE_URL}letter/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}"><img src="{$item.Photo}" class="photo" alt="{$item.Name}"/></a>
					{/if}
                </div>
                <div class="info">
                    <div class="sum">
                    	{$item.Summarise|truncate:260}
                    </div>
                    <div class="view">
                    	<a href="{$smarty.const.SITE_URL}letter/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{#details#}</a>
                    </div>
                </div>
            </div>
    	{foreachelse}
        	<div style="width:100%; float:left; color:#FF0000; text-align:center;">{#updating#}</div>
    	{/foreach}
        {if $num_rows > $limit}
        <div class="listPage">
            {$num_rows|page:$limit:$smarty.get.page:$paging_path}
        </div>
        {/if} 
	</div>
</div>

