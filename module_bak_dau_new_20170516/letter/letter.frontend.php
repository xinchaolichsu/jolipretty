<?php
/**
 * VietEsoft
 * ===============================================================================================
 * Copyright 2010.
 * Website: http://www.vietesfot.com, http://thietkewebsitetrongoi.net
 * -----------------------------------------------------------------------------------------------
 * Phát triển bới VietEsoft
 * -----------------------------------------------------------------------------------------------
 * Chú ý: Tất cả các hàm, phương thức được viết mới do mình tự định nghĩa phải để lại comment gồm:
 	- Người viết
	- Ngày-tháng-năm
	- Chức năng
 * ===============================================================================================
 * Hiển thị tin tức
*/
if (!defined('IN_VES')) die('Hacking attempt');
class letter extends VES_FrontEnd 
{	
	var $table; 		/* Khai báo bảng dữ liệu */
	var $mod;			/* Khai báo mod */
	var $id;			/* Khai báo id */
	var $LangID;		/* Khai báo id ngôn ngữ */
	var $field;			/* Khai báo trường dữ liệu */
	var $pre_table;		/* Khai báo bảng danh mục dữ liệu */	
	function __construct(){		
		$this->table = 'tbl_letter_item';
		$this->pre_table = "tbl_letter_category";
		$this->mod = 'letter';
        $this->url_mod = "letter";
		$this->id = mysql_real_escape_string(($_REQUEST['id']=='')?'0':$_REQUEST['id']);
		$this->field = "*";
		$this->LangID = $_SESSION['lang_id'];
		parent::__construct($this->table);
	}
	function run($task= "")
	{
		if($task==''){$task = $_GET['task'];}
	    switch($task)
			{
				/* Hiển thị trang chủ */
				case 'home':
					$this -> home();
				break;
				case 'getyear':
					$this -> getyear();
				break;
				case 'home_ajax':
					$this -> home_ajax();
				break;
				/* Hiển thị tin hot */
				case 'hot':
					$this -> hot();
				break;
				/* Hiển thị chi tiết một bản ghi */
				case 'details':
					$this -> details();
				break;
				case 'menu':
					$this -> menu();
				break;
				/* Hiển thị dach sách bản ghi theo danh mục đã chọn */
				case 'category':
					$this->listItem($_GET['id']);
				break;
				default:
                    /* List toàn bộ bản ghi */
					$this->listItem();
                    
                    /*List toàn bộ bản ghi theo từng danh mục*/
                    //$this->listbyCategory();
				break;	
			}
	}
	/**
	 * Page info
	 *
	 * @param $task
	 */
	function getPageinfo($task= "")
	{
	 	global $oSmarty, $oDb;
		switch ($task) {
			case 'category':
				$id = $_GET['id'];
				if ($id)
				{
					$sql = "SELECT Name FROM {$this->pre_table} WHERE id=$id";	
					$row = $oDb->getOne($sql);	
				}
				$aPageinfo=array('title'=> $row." | " . parent::get_config_vars("letter","").' | '. PAGE_TITLE, 'keyword'=>PAGE_KEYWORD.",".$row, 'description'=>PAGE_DESCRIPTION);
				$aPath[] = array('link' => '', 'path' => 'letter');
				break;
            case 'details':
                $id = $_GET['id'];
                if ($id)
                {
                    $sql = "SELECT Name FROM {$this->table} WHERE id=$id";    
                    $row = $oDb->getOne($sql);    
                }
                $aPageinfo=array('title'=> $row." | " . parent::get_config_vars("letter","").' | '. PAGE_TITLE, 'keyword'=>PAGE_KEYWORD.",".$row, 'description'=>PAGE_DESCRIPTION);
                $aPath[] = array('link' => '', 'path' => 'letter');
                break;
			default:			
				$aPageinfo=array('title'=> parent::get_config_vars("letter","").' | ' . PAGE_TITLE, 'keyword'=>PAGE_KEYWORD.",".$row, 'description'=>PAGE_DESCRIPTION);
				$aPath[] = array('link' => '', 'path' => 'letter');
				break;
		}
		
		$oSmarty->assign('aPageinfo', $aPageinfo);
		$oSmarty->assign("aPath", $aPath);
	}
	
	/**
	 * Display in home
	 *
	 */
	function home()
	{
		global $oSmarty, $oDb;
		$where = "Status=1";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		$sql = "SELECT * FROM {$this->table} WHERE {$where} AND ShowHome=1  ORDER BY CreateDate DESC Limit 5";
		$letter = $oDb->getAll($sql);
		$oSmarty->assign("letter",$letter);		
		$oSmarty->display('home_letter.tpl');
	}
	
	function getyear()
	{
		global $oSmarty, $oDb;
		$now = date("Y");
		for($i = "2013"; $i<= $now; $i++)
		{
			$xmlstr .='<a onclick="letter1('.$i.')" style=" color:#4b4b4b; cursor:pointer; margin-right:10px; font-weight:700;">'.$i." &nbsp;&nbsp;&nbsp;".'</a>';
			for($j=1; $j<=12;$j++)
				$xmlstr .='<a onclick="letter1('.$i.','.$j.')" style=" color:#7e7f7f;margin-right:10px;cursor:pointer;">'. $j." &nbsp;&nbsp;&nbsp;".'</a>';
				$xmlstr .="<br/>";
		} 
		$oSmarty->assign('xmlstr',$xmlstr);
		$year=$_POST['year'];
		$month=$_POST['month'];
		if($month!='undefined' &&  $month < 10)
			$yearmonth= $year."-0".$month;
		elseif($month!='undefined' && $month >= 10)
			$yearmonth= $year."-".$month;
		else
			$yearmonth= $year;
		
		$url = $_SERVER['REQUEST_URI'];
		if ($_GET['page'])
			$url = str_replace("/".$_GET['page']."/","",$url);
		$page_link = "#i++/";
		$cond = "CreateDate like '%".$yearmonth."%'"; 
		if($year)
		{		
			$_SESSION['concon'] = $cond;
			$_SESSION['yearmonth'] = $yearmonth;
		}
		if($_GET['page'])
		{
			$cond = $_SESSION['concon'];
			$yearmonth=$_SESSION['yearmonth'];
		}
		$oSmarty->assign("yearmonth",$yearmonth);
		$template = 'getyear1.tpl';
		$this->getListItem(0,$page_link,$cond,6,$template);	
	}
	
	function home_ajax()
		{
			global $oSmarty, $oDb;
			if (MULTI_LANGUAGE)
				$where .= " AND LangID = {$this->LangID} ";
			$parentid=$_POST['parentid'];
			//$id=$_POST['id'];
			$where = " Status=1 ";
			$sql = "SELECT id FROM {$this->pre_table} WHERE id = $parentid AND {$where}  ORDER BY Ordering ASC"; 
			$cat_name = $oDb->getOne($sql);
			if ($parentid==$cat_name)
			{
				$sql = "SELECT * FROM {$this->table} WHERE CatID = $parentid AND {$where}  Limit 5";
				$sub = $oDb->getALL($sql);
				$oSmarty->assign('sub',$sub);
				$oSmarty->display("home_letter_ajax.tpl");
			}
	}
	
	function menu()
	{
		global $oSmarty, $oDb;            
        $where = "Status=1";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		 echo "<script type=\"text/javascript\" src=\"/view/js/lang.js\"></script>";
			$now = date("Y");
			for($i = $now; $i>= "2013"; $i--)
			{
				$xmlstr .='<div class="product_sub"><a onclick="letter1('.$i.')" style=" color:#0082c8; cursor:pointer; margin-right:10px; font-weight:400;">'.$i." &nbsp;&nbsp;&nbsp;".'</a></div>';
					$xmlstr .="<br/>";
			} 
			$oSmarty->assign('xmlstr',$xmlstr);
		$template = 'menu_letter.tpl';		
		$oSmarty->display($template);
	}
	/**
	 * Hiển thị tin hot
	 *
	 */
    function hot()
	{
		global $oSmarty, $oDb;
		$sql = "SELECT * FROM {$this->pre_table} WHERE Status=1 AND ShowHome=1 ORDER BY Ordering ASC ";
		$cat = $oDb->getCol($sql);
		$cat_promot = implode(",",$cat);
		//pre($cat_promot);die();
		$sql2 = "SELECT * FROM {$this->table} WHERE Status=1 AND IsHot=1 AND CatID IN ({$cat_promot}) AND IsHome=1 ORDER BY CreateDate DESC";
		$letter = $oDb->getAll($sql2);
		$oSmarty->assign("letter",$letter);
		//pre($letter);die();
		$oSmarty->display('hot_letter.tpl');
		
	}
	
	/**
	 * Hiển thị chi tiết 1 bản ghi
	 *
	 */
	function details()
	{
		global $oDb,$oSmarty;
		$id = $_GET['id'];
        $where = "Status=1";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		$sql = "SELECT * FROM {$this->table} WHERE {$where} AND id={$id}";
		$detail_item = $oDb->getRow($sql);		
		$oSmarty->assign('detail_item',$detail_item);
		
/*		if( $detail_item['CatID'] ){
			//$full_cat_name = $this->getFullCatName($detail_item['CatID']);
			//$oSmarty->assign("full_cat_name",$full_cat_name);
			//$str_cat_id = $this->getFullCatId($detail_item['CatID']);
			$sql = "SELECT * FROM {$this->table} WHERE {$where} AND id<>{$id} AND CatID IN ({$str_cat_id}) ORDER BY CreateDate DESC LIMIT 10";
		} else {
*/			$sql = "SELECT * FROM {$this->table} WHERE {$where} AND id<>{$id} ORDER BY CreateDate DESC LIMIT 10";
		//}
		$other_item = $oDb->getAll($sql);		
		$oSmarty->assign('other_item',$other_item);
		
		$template = 'detail_letter.tpl';
		
		$oSmarty->display($template);
	}
	
	/**
	 * Lấy id danh mục
	 * Trả về: chuỗi id danh  mục
	 */
	function getFullCatId($cat_id,$first = true)
	{
		global $oSmarty, $oDb;
        $where = "Status=1";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		$str_cat = "";
		$sql = "SELECT id FROM {$this->pre_table} WHERE {$where} AND ParentID={$cat_id} ORDER BY Ordering";
		$cat = $oDb->getCol($sql);
		if ($cat)
		{
			$str_cat = implode(',',$cat);
			$temp_str_cat = "";
			foreach ($cat as $key=>$value)
			{
				$temp_str_cat .= $this->getFullCatId($value,false);
			}
			$str_cat .=",".$temp_str_cat;			
		}
		if ($first)  
			$str_cat .=$cat_id;
		return $str_cat;
	}
	
	/**
	 * Lấy tên danh mục
	 * Trả về: chuỗi tên danh mục
	 */
	function getFullCatName($cat_id)
	{
		global $oSmarty, $oDb;
        $where = "Status=1";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		$sql = "SELECT Name FROM {$this->pre_table} WHERE {$where} AND id={$cat_id} ORDER BY Ordering";
		$cat_name = $oDb->getOne($sql);
		$sql = "SELECT ParentID FROM {$this->pre_table} WHERE {$where} AND id={$cat_id} ORDER BY Ordering";
		$parent = $oDb->getOne($sql);
		if ($parent)
		{
			$result = $this->getFullCatName($parent)."&nbsp; / &nbsp;".$cat_name;
		}else 
			$result = "&nbsp; / &nbsp;".$cat_name;
		return $result;
	}
	
	/**
	 * Hiển thị toàn bộ danh sách bản ghi
	 *
	 */
	function listItem($cat_id=0)
	{
		
		global $oDb,$oSmarty;
		 echo "<script type=\"text/javascript\" src=\"/view/js/lang.js\"></script>";
		$now = date("Y");
		for($i = "2013"; $i<= $now; $i++)
		{
			$xmlstr .='<a onclick="letter1('.$i.')" style=" color:#4b4b4b; cursor:pointer; margin-right:10px; font-weight:700;">'.$i." &nbsp;&nbsp;&nbsp;".'</a>';
			for($j=1; $j<=12;$j++)
				$xmlstr .='<a onclick="letter1('.$i.','.$j.')" style=" color:#7e7f7f;margin-right:10px;cursor:pointer;">'. $j." &nbsp;&nbsp;&nbsp;".'</a>';
				$xmlstr .="<br/>";
		} 
		$oSmarty->assign('xmlstr',$xmlstr);
		$url = $_SERVER['REQUEST_URI'];
		if ($_GET['page'])
			$url = str_replace("/".$_GET['page']."/","",$url);
		$page_link = "$url/i++/";
		
		$template = 'list_letter.tpl';
		
		$this->getListItem($cat_id,$page_link,"",5,$template);
	}
	function getListItem($cat_id=0,$page_link,$cond="",$limit=5,$template="list_letter.tpl",$order="ORDER BY CreateDate DESC")
	{
		global $oDb,$oSmarty;
		$where = " Status=1 ";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		if ($cat_id!=0)
		{
			$str_cat_id = $this->getFullCatId($cat_id);
			$where .=" AND CatID IN ({$str_cat_id})";
			//$full_cat_name = $this->getFullCatName($cat_id);
			//$oSmarty->assign("full_cat_name",$full_cat_name);
		}
		if ($cond != "")
			$where .= " AND ".$cond;
		$listItem = parent::Paging($limit, $this->field, $where,$page_link, $order);
		$oSmarty->assign("listItem",$listItem);
		$oSmarty->display($template);
	}
}
?>