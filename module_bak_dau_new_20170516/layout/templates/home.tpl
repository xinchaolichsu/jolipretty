{php}
	loadModule("header");
{/php}
<div id="middle">
    <div class="main_home">
           <div id="advance">
            {php}
                 loadModule("slideshow");
             {/php}
          </div>
    </div>
    <hr>
    {php}
        loadModule("homepage");
    {/php}
</div>

{literal}
<script>
    $('.product-lis').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
})
</script>
{/literal}
<div class="clr"></div>
{php}
	loadModule("footer");
{/php}

