<?php
class transactionBackEnd extends Bsg_Module_Base{
	var $smarty;
	var $db;
	var $url="";
    var $datagrid;
	var $id;
	var $imgPath;
	var $imgPathShort;
	var $table='';
	var $arrAction;
	public function __construct($oSmarty, $oDb, $oDatagrid)
	{	
		$this -> smarty = $oSmarty;
		$this -> db = $oDb;		
        $this -> datagrid = $oDatagrid;
		$this -> id=$_REQUEST[id];		
		$this -> table	="tbl_transaction";	 		
		parent::__construct($oDb);		
		$this->bsgDb->setTable($this->table);
	}
	
	function run($task)
	{	
		
		switch( $task ){
			case 'edit':
				$this -> editItem();
				break;
			default:					
				$this -> listItem( $_GET['msg'] );		
				break;
		}
	}
	
	function getPageInfo()
	{
		return true;
	}

	
	function editItem()
	{
		$id = $_GET['id'];
		$this -> getPath("Quản lý hóa đơn > Thông tin hóa đơn");	
		$row = $this -> bsgDb -> getRow( $id );		
		$cart = $this -> get_cart($row['CartSessionID']);
		$row['cart'] = $cart;
		
		$this -> buildForm( $row );
	}
	
	function get_cart($session)
	{
		global $oDb, $oSmarty;
		
		$result = $oDb->getAll("SELECT * FROM tbl_shopping_cart WHERE sessionid = '{$session}'");
		$total = 0;
		
		foreach ($result as $key => $value) {
			
			$products = $oDb->getRow("SELECT * FROM tbl_product_item WHERE id = ".$result[$key]['product_id']);
			
			$subtotal = $products['Price']*$result[$key]['quantity'];
			
			$result[$key]['subtotal'] = $subtotal;
			$result[$key]['product'] = $products;
			$total = $total + $subtotal;
		}
		
		$oSmarty->assign('result',$result);
		$oSmarty->assign('total',$total);
		$cart = $oSmarty->fetch('cart_content.tpl');
		return $cart;
	}

	
	function buildForm( $data=array() , $msg = ''){
		
		$form = new HTML_QuickForm('frmAdmin','post',$_COOKIE['re_dir']."&task=".$_GET['task'], '', "style='padding:10px 15px 0 20px;'");
		
		$form -> setDefaults($data);
		
		$form -> addElement('static','','Thông tin đơn hàng',$data['cart']);
		
		$form -> addElement('text', 'Name', 'Họ tên khách hàng', array('size' => 50, 'maxlength' => 255,'readonly'=>'readonly'));
		
		$form -> addElement('text', 'Email', 'Email', array('size' => 50, 'maxlength' => 255,'readonly'=>'readonly'));
		
		$form -> addElement('text', 'Address', 'Địa chỉ', array('size' => 50, 'maxlength' => 255,'readonly'=>'readonly'));
		
		$form -> addElement('text', 'Phone', 'Điện thoại', array('size' => 50, 'maxlength' => 255,'readonly'=>'readonly'));
		
		$form -> addElement('textarea', 'AdditionInfo', 'Yêu cầu thêm', array('style' => 'width:200px; height:100px;','readonly'=>'readonly'));
        
		$form -> addElement('text', 'CreateDate', 'Ngày tạo', array('size' => 50, 'maxlength' => 255,'readonly'=>'readonly'));

		$form -> addElement('select', 'Status', 'Trạng thái', array(1=>'Đang chờ',2=>'Đang giao hàng',3=>'Hoàn tất',4=>'Hủy bỏ'));
		
		$btn_group[] = $form -> createElement('submit',null,'Hoàn tất',array("style"=> "border:1px solid gray; padding:0 5px 0 5px;"));		
        $btn_group[] = $form -> createElement('button',null,'Quay lại',array('onclick'=>'window.location.href = \''.$_COOKIE['re_dir'].'\'', "style"=> "border:1px solid gray;"));      
        $form -> addGroup($btn_group);
      
		$form->addElement('hidden', 'id', $data['id']);		
		
		
		if( $form -> validate())
		{
			
			$aData  = array(
				"Status" 	=> $_POST['Status'],
			);

			if( !$_POST['id'] ){
				
				 $id = $this -> bsgDb -> insert($aData);				 
				 $msg = "Thêm hóa đơn thành công! ";
			}else {
				$id = $_POST['id'];				
				$this -> bsgDb -> updateWithPk($id, $aData);
				$msg = "Chỉnh sửa hóa đơn thành công ";
			}
			
			$this -> redirect($_COOKIE['re_dir']. "&msg={$msg}");
		}
		
		$form->display();
	}
	
	function listItem( $sMsg= '' )
	{		
		global $oDb;
		global $oDatagrid;				
		
		$root_path = "Quản lý hóa đơn > Danh sách hóa đơn";						
		$submit_url= "/index.php?".$_SERVER['QUERY_STRING'];
		
		$table = $this -> table;
		
		$arr_filter= array(
			array(
				'field' => 'Name',
				'display' => 'Họ tên khách hàng',
				'type' => 'text',
				'name' => 'filter_title',
				'selected' => $_REQUEST['filter_title'],
				'filterable' => true
			),
			array(
				'field' => 'Status',
				'display' => 'Trạng thái',				
				'name' => 'filter_show',
				'selected' => $_REQUEST['filter_show'],
				'options' => array(1=>'Đang chờ',2=>'Đang giao hàng',3=>'Hoàn tất',4=>'Hủy bỏ'),
				'filterable' => true
			)
			
		); 
		
		$arr_cols= array(		
			
			array(
				"field" => "id",					
				"primary_key" =>true,
				"display" => "Id",				
				"align" => "center",
				"sortable" => true
			),	
			array(
				"field" => "Name",
				"display" => "Họ tên khách hàng",
				"align"	=> 'left',				
				"datatype" => "text",
				"sortable" => true
			),
			array(
				"field" => "Email",
				"display" => "Email",
				"align"	=> 'left',				
				"datatype" => "text",
				"sortable" => true
			),
			array(
				"field" => "Address",
				"display" => "Địa chỉ",
				"align"	=> 'left',				
				"datatype" => "text",
				"sortable" => true
			),				
			array(
				"field" => "Phone",
				"display" => "Điện thoại",
				"align"	=> 'left',				
				"datatype" => "text",
				"sortable" => true
			),
			array(
				"field" => "CreateDate",
				"display" => "Ngày tạo",
				"align"	=> 'left',				
				"datatype" => "date",
				"sortable" => true
			),
			array(
				"field" => "Status",
				"display" => "Trạng thái",				
				"datatype" => "value_set",
				"case"	=> array('1'=>'Đang chờ','2'=>'Đang giao hàng','3'=>'Hoàn tất','4'=>'Hủy bỏ'),
				"sortable" => true
			),		
		);

		$arr_act = array(
				array(
					"task" => "edit",
					"icon" => "view.gif",
					"tooltip" => "Xem chi tiết"		
				)
		);
		
		$arr_check = array();
		if( $sMsg )
			$oDatagrid -> setMessage( $sMsg );
		$oDatagrid->display_datagrid($table, $arr_cols, $arr_filter, $submit_url, $arr_act ,120, $root_path, false ,$arr_check);		
		
	}		
	
}

?>