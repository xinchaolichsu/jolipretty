<?php
/**
 * VietEsoft
 * ===============================================================================================
 * Copyright 2010.
 * Website: http://www.vietesfot.com, http://thietkewebsitetrongoi.net
 * -----------------------------------------------------------------------------------------------
 * Phát triển bới VietEsoft
 * -----------------------------------------------------------------------------------------------
 * Chú ý: Tất cả các hàm, phương thức được viết mới do mình tự định nghĩa phải để lại comment gồm:
 	- Người viết
	- Ngày-tháng-năm
	- Chức năng
 * ===============================================================================================
 * Giỏ hàng online
*/
if (!defined('IN_VES')) die('Hacking attempt');
class cart extends VES_FrontEnd
{
	var $table;			/* Khai báo bảng dữ liệu */
	var $mod;			/* Khai báo mod */
	var $id;			/* Khai báo id sản phẩm */
	var $quantity;		/* Khai báo số lượng sản phẩm */
	var $color;		/* Khai báo màu sắc sản phẩm */
	var $LangID;		/* Khai báo id ngôn ngữ */
	var $field;			/* Khai báo trường dữ liệu */
	var $pre_table;		/* Khai báo bảng danh mục dữ liệu */

	function __construct()
	{
		$this -> table = 'tbl_shopping_cart';
		$this -> pre_table = "";
		$this -> id = mysql_real_escape_string(($_REQUEST['pid']=='')?'0':$_REQUEST['pid']);
		$this -> quantity = mysql_real_escape_string(($_REQUEST['qty']=='')?'1':$_REQUEST['qty']);
		$this -> color = mysql_real_escape_string(($_REQUEST['color']=='')?'0':$_REQUEST['color']);
		$this -> size = mysql_real_escape_string(($_REQUEST['size']=='')?'0':$_REQUEST['size']);
		$this -> field = "*";
		$this -> LangID = $_SESSION['lang_id'];
		parent::__construct($this->table);
	}
	
	function run($task= "")
	{
		if($task==''){$task = $_GET['task'];}
		switch($task)
	      {
	      	case 'add':
	      		$this->add_item($this->id,$this->quantity,$this->size, $this->color);
	      		$this->redirect();
	      		break;
			case 'show_payment':
				$this -> show_payment();
				break;
	      	case 'add1':
	      		$this->add_item1($this->id,$this->quantity,$this->color,$this->size);
	      		$this->redirect1();
	      		break;
		  	case 'display_contents1':
		       $this->display_contents1(); 
				break;		
			case 'check_item123':
	      		$this->check_item123($this->id,$this->color);
	      		$this->redirect();
	      		break;
	      	case 'delete':
	      		$this->delete_item($this->id,$this->size);
	      		$this->redirect();
	      		break;
	      	case 'update_quantity':
	      		$this->modify_quantity($this->id,$this->quantity,$this->size);
	      		$this->redirect();
	      		break;
	      	case 'clear':
	      		$this->clear_cart();
	      		$this->redirect();
	      		break;
	      	case 'checkinfo':
	      		$this->checkinfo();
	      		break;
	      	case 'shipping':
	      		$this->shipping();
	      		break;
	      	case 'checkout':
	      		$this->checkout();
	      		break;
	      	case 'checkout_success':
	      		$this->checkout_success();
	      		break;
	      	case 'paypal_ipn':
	      		$this->paypal_ipn();
	      		break;
			case 'show_count':
				$this->show_count();
				break;
			case 'show_cart':
				$this->show_cart();
				break;
			case 'getcaptcha':
				echo $_SESSION['key_captcha'];
				break;
			case 'newcaptcha':
				include("lib/captcha/captcha.class.php");
				break;
			case 'kiemtra_conhang':
				$this->kiemtra_conhang();
				break;
		  	default:
		       $this->display_contents(); 
				break;
		  }

    }
    /**
	 * Page info
	 *
	 * @param $task
	 */
    function getPageinfo($task= "")
    {
        global $oSmarty, $oDb;
        switch ($task) {
            default:            
                $aPageinfo=array('title'=> parent::get_config_vars("SHOPPING CART","Shopping cart").' | ' . PAGE_TITLE, 'keyword'=>PAGE_KEYWORD.",".$row, 'description'=>PAGE_DESCRIPTION);
                $aPath[] = array('link' => '', 'path' => 'shopping cart');
                break;
        }
        
        $oSmarty->assign('aPageinfo', $aPageinfo);
        $oSmarty->assign("aPath", $aPath);
    }
	function show_payment()
	{
		global  $oDb;
		$pay_id = $_GET['pay_id'];
		$sql = "SELECT Content FROM tbl_payment_item WHERE id={$pay_id}";
		$pay_info = $oDb->getOne($sql);
		echo $pay_info;
	}
	/**
	 * Redirect theo url
	 *
	 */
	function redirect()
	{
		$cart_url = SITE_URL."cart/";
	    echo '<script>location.href="'.$cart_url.'"</script>';
	}
	function redirect1()
	{			//die();
			$url = SITE_URL."cart/display_contents1/";
			echo "<script language = 'javascript'>location.href = '".$url."'</script>";
	}
	/**
	 * Kiểm tra sản phẩm trong giỏ hàng
	 * Trả về: 0 nếu không có, ngược lại trả về số lượng sản phẩm
	 */
	function check_item($product_id, $quantity, $size, $color) {
		global $oDb;
		$cart = $_SESSION['cart'];
		$flag = false;
		foreach($cart as $k=>$v){
			if($v['product_id'] == $product_id && $v['size'] == $size){
				$cart[$k]['quantity'] = intval($v['quantity']+$quantity);
				$cart[$k]['size'] = $size;
				$cart[$k]['color'] = $color;
				$flag = true;
			}
		}
		// pre($cart);die('quan'.$flag);
		$_SESSION['cart'] = $cart;
		return $flag;
	}
	
	/**
	 * Thêm sản phẩm vào giỏ hàng
	 * 
	 */
	function add_item($product_id, $quantity=1, $size, $color) {
		global $oDb;
		if( $product_id < 1 ) return;
		

		// tru san pham trong gio hang
		//$oDb->query("Update tbl_product_size Set number = number - $quantity WHERE pro_id = $product_id AND size_id = $size AND number >= $quantity");		
		
		//$_SESSION['curr_size_numb']=$_GET['curr_size_numb'];
		// Nếu giỏ hàng đã tồn tại
		//pre($_SESSION['cart']);die();
		if( $_SESSION['cart'] ){
			// Nếu sản phẩm đã tồn tại thì tăng số lượng lên
			$check = $this->check_item($product_id, $quantity, $size, $color);
			if(!$check){
				$cart = array(
							'product_id' 	=> $product_id,
							'quantity' 	=> $quantity,
							'size' 		=> $size,
							'color'     => $color
						);
				$_SESSION['cart'][] = $cart;
			}
		}else{
		// Nếu giỏ hàng chưa tồn tại
			$cart = array(
							'product_id' => $product_id,
							'quantity'	=> $quantity,
							'size' 	=> $size,
							'color' => $color
						);
			$_SESSION['cart'][] = $cart;
		}
	}
	
	function add_item1($product_id, $quantity=1, $color, $size) {
		global $oDb;
		if( $product_id < 1 ) return;
		
		// Nếu giỏ hàng đã tồn tại
		if( $_SESSION['cart'] ){
			// Nếu sản phẩm đã tồn tại thì tăng số lượng lên
			$check = $this->check_item($product_id, $quantity, $color, $size);
			if(!$check){
				$cart = array(
							'product_id' 	=> $product_id,
							'quantity' 	=> $quantity,
							'color' 	=> $color,
							'size' 		=> $size,
						);
				$_SESSION['cart'][] = $cart;
			}
		}else{
		// Nếu giỏ hàng chưa tồn tại
			$cart = array(
							'product_id' => $product_id,
							'quantity'	=> $quantity,
							'color' 	=> $color,
							'size' 	=> $size,
						);
			$_SESSION['cart'][] = $cart;
		} 
	}
	/**
	 * Xóa 1 sản phẩm khỏi giỏ hàng
	 *
	 */
	function delete_item($product_id,$size) {
		global $oDb;
		$cart = $_SESSION['cart'];
		foreach($cart as $k=>$v){
			// update lai soluong san pham
			//$oDb->query("Update tbl_product_size Set number = number + {$v['quantity']} WHERE pro_id = {$v['product_id']} AND size_id = {$v['size']}");	

			if(!empty($_SESSION['code_id'])) {
				$oDb->query("Update tbl_message Set IsRead = IsRead - 1 WHERE id = {$_SESSION['code_id']}");
				unset($_SESSION['code_id']);
			}
			if(!empty($_SESSION['code'])) {
				unset($_SESSION['code']);
			}
			if(!empty($_SESSION['code_text'])) {
				unset($_SESSION['code_text']);
			}
			if($product_id == $v['product_id'] && $size==$v['size'])
			unset($_SESSION['cart'][$k]);
		}
	}
	function check_item123($product_id,$color) {
		global $oDb;
		$cart = $_SESSION['cart'];
		$product_id = $_GET["id"];
		foreach($cart as $k=>$v){
			if($v['product_id'] == $product_id){
			
				$cart[$k]['color'] = $color;
				$flag = true;
			}
		}
		$_SESSION['cart'] = $cart;
	}
	/**
	 * Cập nhật số lượng của 1 sản phẩm vào giỏ hàng
	 *
	 */
	function modify_quantity($product_id, $quantity,$size) {
		global $oDb;
		// update lai soluong san pham
		/*if($_GET['nvu'] == 'sub')
			$oDb->query("Update tbl_product_size Set number = number + 1 WHERE pro_id = $product_id AND size_id = $size");		
		if($_GET['nvu'] == 'add')
			$oDb->query("Update tbl_product_size Set number = number - 1 WHERE pro_id = $product_id AND size_id = $size AND number > 0");*/		

		$cart = $_SESSION['cart'];
		foreach($cart as $k=>$v){
			if($v['product_id'] == $product_id && $v['size'] == $size)
				$cart[$k]['quantity'] = intval($quantity);
		}
		$_SESSION['cart'] = $cart;
		$price = $oDb->getOne("SELECT OldPrice From tbl_product_item where id =".$product_id);
		echo '$'.number_format($price*$quantity);
	}

	/**
	 * Xóa toàn bộ giỏ hàng
	 *
	 */
	function clear_cart() {
		global $oDb;
		unset($_SESSION['cart']);
	}
	
	/**
	 * 	Lấy thông tin sản phẩm
	 *	Return array sản phẩm
	 */
	function getProducts($array)
	{
		global $oDb, $oSmarty;	
		
		foreach ($array as $key => $value)
		{
			$sql = "SELECT * FROM tbl_product_item WHERE id='".$value['product_id']."' AND Status=1";
			$products = $oDb->getRow( $sql );

			// Kiển tra sản phẩm có tồn tại hoặc có được active hay không?
			if($products){
				if($value['color']) {
					$sql_color = "SELECT Name FROM tbl_product_color WHERE id='{$value['color']}'";
					$color_name = $oDb->getOne( $sql_color );
					$products['color_name'] = $color_name;
				}
				if($value['size'])
				{
					$sql = "SELECT Name FROM tbl_size WHERE id='".$value['size']."' ";
					$size = $oDb->getOne( $sql );
					$products['size'] = $size;
					$products['sizeid'] = $value['size'];
					$sql = "SELECT number FROM tbl_product_size WHERE size_id=".$value['size']." AND pro_id=".$value['product_id'];
					$curr_size_numb = $oDb->getOne( $sql );
					$products['curr_size_numb'] = $curr_size_numb;
					$sql_price = "SELECT price_size FROM tbl_product_size WHERE size_id='".$value['size']."' AND pro_id='".$value['product_id']."' ";
					$price_size = $oDb->getOne( $sql_price );
					$products['price_size'] = $price_size;
				}
				
				$array[$key]['tt'] = $key+1;

				/* Nếu có VAT, 10%VAT*/
				//$vat = ($subtotal*10)/100;
				//$array[$key]['vat'] = $vat;
				$array[$key]['subtotal'] = $subtotal;
				$array[$key]['product'] = $products;
			}
		}
		//pre($array);
		return $array;
	}
	
	
	/**
	 * 	Lấy tổng giá trị đơn hàng
	 *	Return giá trị đơn hàng
	 */
	function getTotal($array)
	{
		global $oDb, $oSmarty;	
		$total = 0;
		foreach ($array as $key => $value)
		{
			$sql = "SELECT * FROM tbl_product_item WHERE id='".$value['product_id']."'  AND Status=1";//pre($sql);
			$products_old = $oDb->getRow( $sql );
			$sql2 = "SELECT * FROM tbl_product_size WHERE pro_id='".$value['product_id']."' AND size_id='".$value['size']."'";
			$products = $oDb->getRow( $sql2 );
			// Kiển tra sản phẩm có tồn tại hoặc có được active hay không?
			if($products)
			{
				if($products['price_size']=='' || $products['price_size'] == 0) {
					if($products_old['Price'] != 0) {
						$products['price_size'] = $products_old['Price'];
					} else {
						$products['price_size'] = $products_old['OldPrice'];
					}
				}
				$subtotal = intval($products['price_size']*$value['quantity']);
				//$subtotal = intval(($products['OldPrice']-($products['OldPrice']*$products['Sale_off']/100))*$value['quantity']);
				$total = $total + $subtotal ;
			}
		}

		return $total;
	}

//Đếm số lượng sản phẩm
	function getTotal123($array)
	{
		global $oDb, $oSmarty;	
		$total123 = 0;
		foreach ($array as $key => $value)
		{
			if($value["color"] == 0){
				$sql = "SELECT * FROM tbl_product_item WHERE id='".$value['product_id']."'  AND Status=1";//pre($sql);
				$products = $oDb->getRow( $sql );
				// Kiển tra sản phẩm có tồn tại hoặc có được active hay không?
				if($products)
				{
					if($products['Price']=='')
						$products['Price'] = $products['OldPrice'];
					$subtotal = intval($value['quantity']);
					$total123 = $total123 + $subtotal ;
				}
			}
		}
		return $total123;
	}			
	
	/**
	 * Hiển thị nội dung giỏ hàng
	 *
	 */
	function display_contents()
	{
		global $oDb, $oSmarty;
		$useid=$_SESSION["userid_cus"];
		$oSmarty->assign('useid',$useid);
		
		//pre($_SESSION['cart']); // SESSION CHUA DU LIEU PRODUCT
		if($_SESSION['cart']){
			// Giỏ hàng
			$cart = $_SESSION['cart'];
			// Thông tin các sản phẩm có trong giỏ hàng
			$product = $this->getProducts($cart);
			// Tổng giá trị đơn hàng
			$total = $this->getTotal($cart);
// Tổng sản phẩm
			$total123 = $this->getTotal123($cart);
			//pre($cart);
			$oSmarty->assign('result',$product);
			$oSmarty->assign('total',$total);
            $oSmarty->assign('total123',$total123);
		}
		if($_POST['voucher']!='')
		{
			if(!empty($_SESSION['code_text'])) {
				$code = $_SESSION['code_text'];
			} else {
				$code=trim($_POST['voucher']);
			}
			//garung
			$message = $oDb->getRow("SELECT id, Code,Sale,BeginDate,EndDate FROM tbl_message WHERE Code = '{$code}' AND IsRead < LimitCode Order by id DESC Limit 1");
			if($message['Code']==$code && $message['BeginDate']<=date("Y-m-d H:i") && $message['EndDate']>=date("Y-m-d H:i") && ($message['IsRead'] <= $message['LimitCode']))
			{
				$_SESSION['code']=$message['Sale'];
				$_SESSION['code_id'] = $message['id'];
				$_SESSION['code_text'] = $message['code'];
				$msg_voucher = "<div class='alert alert-success' style='text-align: left;'><span style='font-size: 18px;'>Complete: voucher is valid!</span></div>";
				$oDb->query("Update tbl_message Set IsRead = IsRead + 1 WHERE id = {$message['id']}");
				var_dump($_SESSION['code_id']);
			}
			else
			{
				$_SESSION['code']=0;
				$msg_voucher = "<div class='alert alert-danger' style='text-align: left;'><span style='font-size: 18px;'>Error: voucher is invalid!</span></div>";
			}
			$oSmarty -> assign('msg_voucher',$msg_voucher);
		}
		$oSmarty->display('view_cart.tpl');
	}
	
	function display_contents1()
	{
		global $oDb, $oSmarty;
		if($_SESSION['cart']){
			// Giỏ hàng
			$cart = $_SESSION['cart'];
			// Thông tin các sản phẩm có trong giỏ hàng
			$product = $this->getProducts($cart);
			// Tổng giá trị đơn hàng
			$total = $this->getTotal($cart);
			//pre($cart);
			$oSmarty->assign('result',$product);
			$oSmarty->assign('total',$total);
		}
			$url = SITE_URL."/cart/checkout/";
			echo "<script language = 'javascript'>location.href = '".$url."'</script>";
		//pre($cart);
		$oSmarty->display('view_cart.tpl');
	}
	
	function show_cart()
	{
		global $oDb, $oSmarty;
		if($_SESSION['cart']){
			// Giỏ hàng
			$cart = $_SESSION['cart'];
			// Thông tin các sản phẩm có trong giỏ hàng
			$product = $this->getProducts($cart);
			// Tổng giá trị đơn hàng
			$total = $this->getTotal($cart);
			//pre($cart);
			$oSmarty->assign('result',$product);
			$oSmarty->assign('total',$total);
		}
		//pre($cart);
		$oSmarty->display('show_cart.tpl');
	}

	/**
	 * Checkout!
	 *
	 */
	 
	function checkinfo()
	{
		global $oDb, $oSmarty;
		if ($_SERVER['REQUEST_METHOD']=='POST')
		{			
			$cart_content = $_SESSION['cart'];
			$oDb->autoExecute("tbl_order_item",array('Status'=>1),DB_AUTOQUERY_INSERT);
			$order_id = mysql_insert_id();
			$arr_data = array(
				"FullName"		=> $_POST['txt_name'],
				"LastName"		=> $_POST['LastName'],
				"Address"		=> $_POST['txt_address'],
				"Yahoo"			=> $_POST['txt_city'],
				"Email"			=> $_POST['txt_email'],
				"Phone"			=> $_POST['txt_phone'],
				"CityID"		=> $_POST['country'],
				"Postal"		=> $_POST['txt_postal'],
			);
			if($_POST['use_same']=='on')
				$_POST['use_same']=1;
			else 
				$_POST['use_same']=0;
			//pre($order_id);pre($_SESSION["userid_cus"]);pre($_POST['use_same']);die();
			if($_SESSION['memotoseller']) {
				$memotoseller = $_SESSION['memotoseller'];
			} else {
				$memotoseller = '';
			}
			if($_SESSION["userid_cus"])
			{
				$oDb->autoExecute("tbl_transaction",array('OrderID'=>$order_id,"CreateUser"	=> $_SESSION['userid_cus'],'Use_same'=>$_POST['use_same'], 'memotoseller' => $memotoseller),DB_AUTOQUERY_INSERT);
				$oDb->autoExecute("tbl_customer", $arr_data, DB_AUTOQUERY_UPDATE,"id='".$_SESSION["userid_cus"]."'");
			}
			else
			{
				$oDb->autoExecute("tbl_transaction",array('OrderID'=>$order_id,'FullName'=> $_POST['txt_name'],'LastName'=> $_POST['LastName'],"Address"=> $_POST['txt_address'],"Email"=> $_POST['txt_email'],"City"=> $_POST['txt_city'],"Phone"=> $_POST['txt_phone'],"Postal"=> $_POST['txt_postal'],"CityID"=> $_POST['country'],'Use_same'=>$_POST['use_same'], 'memotoseller' => $memotoseller),DB_AUTOQUERY_INSERT);
			}
			//=================garung ===================
			$cus_id = $_SESSION['userid_cus'];
			$oSmarty->assign('cus_id',$cus_id);
			if($cus_id)
			{
				$sql = "SELECT * FROM tbl_customer WHERE id = '$cus_id'";
				$cus1 = $oDb->getRow("Select Address1,Use_same from tbl_transaction order by id DESC Limit 1");
				$oSmarty->assign('cus1',$cus1);
			}
			else
				$sql="Select * from tbl_transaction order by id DESC Limit 1";
			$cus = $oDb->getRow($sql);//pre($cus);
			$oSmarty->assign('cus',$cus);
			$country_name1 = $oDb->getOne("SELECT Name FROM tbl_country WHERE id={$cus['CityID1']}");
			$country_name = $oDb->getOne("SELECT Name FROM tbl_country WHERE id={$cus['CityID']}");
			$oSmarty->assign("country_name",$country_name);
			$oSmarty->assign("country_name1",$country_name1);
			$country = $oDb->getAll("SELECT id,Name FROM tbl_country WHERE Status=1 ORDER BY Ordering ASC");
			$oSmarty->assign("country",$country);
			$sql = "SELECT id FROM tbl_transaction WHERE CreateUser = '$cus_id' Order by id DESC Limit 1";//pre($sql);
			$tran_id = $oDb->getOne($sql);
			$sql = "SELECT * FROM tbl_shipping_item where Status=1 Order by Ordering  DESC";
			$shipping = $oDb->getAll($sql);
			$oSmarty->assign('shipping',$shipping);
			if($_SESSION['memotoseller']) {
				$memotoseller = $_SESSION['memotoseller'];
			} else {
				$memotoseller = '';
			}
			if($_POST['shipping']!='')
			{
				$oDb->autoExecute("tbl_transaction",array('ShipID'=>$_POST['shipping'], 'memotoseller' => $memotoseller),DB_AUTOQUERY_UPDATE,"id='".$tran_id."'");
				$url = SITE_URL."cart/checkout/";
				echo "<script type='text/javascript'>window.location.href = '".$url."';</script>";
			}
			//============================================
			// $url = SITE_URL."cart/shipping/";
			// echo "<script type='text/javascript'>window.location.href = '".$url."';</script>";
		}
		else 
		{
			//=================garung ===================
			$sql = "SELECT * FROM tbl_shipping_item where Status=1 Order by Ordering DESC";
			$shipping = $oDb->getAll($sql);
			$oSmarty->assign('shipping',$shipping);
			//============================================
			$cus_id = $_SESSION['userid_cus'];
			$sql = "SELECT * FROM tbl_customer WHERE id = '$cus_id'";
			$country = $oDb->getAll("SELECT id,Name FROM tbl_country WHERE Status=1 ORDER BY Ordering ASC");
			$oSmarty->assign("country",$country);
			$cus = $oDb->getRow($sql);
			$oSmarty->assign('cus_id',$cus_id);
			$oSmarty->assign('cus',$cus);
			$oSmarty->display('checkinfo.tpl');
		}
	}
	
	function shipping()
	{
		global $oDb, $oSmarty;
		$cus_id = $_SESSION['userid_cus'];
		$oSmarty->assign('cus_id',$cus_id);
		if($cus_id)
		{
			$sql = "SELECT * FROM tbl_customer WHERE id = '$cus_id'";
			$cus1 = $oDb->getRow("Select Address1,Use_same from tbl_transaction order by id DESC Limit 1");
			$oSmarty->assign('cus1',$cus1);
		}
		else
			$sql="Select * from tbl_transaction order by id DESC Limit 1";
		$cus = $oDb->getRow($sql);//pre($cus);
		$oSmarty->assign('cus',$cus);
		$country_name1 = $oDb->getOne("SELECT Name FROM tbl_country WHERE id={$cus['CityID1']}");
		$country_name = $oDb->getOne("SELECT Name FROM tbl_country WHERE id={$cus['CityID']}");
		$oSmarty->assign("country_name",$country_name);
		$oSmarty->assign("country_name1",$country_name1);
		$country = $oDb->getAll("SELECT id,Name FROM tbl_country WHERE Status=1 ORDER BY Ordering ASC");
		$oSmarty->assign("country",$country);
		$sql = "SELECT * FROM tbl_shipping_item where Status=1 Order by CreateDate DESC";
		$shipping = $oDb->getAll($sql);
		$oSmarty->assign('shipping',$shipping);
		$sql = "SELECT id FROM tbl_transaction WHERE CreateUser = '$cus_id' Order by id DESC Limit 1";//pre($sql);
		$tran_id = $oDb->getOne($sql);
		if($_POST['address']!='')
		{
			if($_POST['use_same']=='on')
				$_POST['use_same']=1;
			else 
				$_POST['use_same']=0;
			if($cus_id)
				$oDb->autoExecute("tbl_customer",array('FullName1'=>$_POST['txt_name'],'LastName1'=>$_POST['LastName'],'Address1'=>$_POST['address'],'City1'=>$_POST['city'],'Postal1'=>$_POST['postal'],'CityID1'=>$_POST['country'],'Phone1'=>$_POST['txt_phone']),DB_AUTOQUERY_UPDATE,"id='".$cus_id."'");
			else
			
				$oDb->autoExecute("tbl_transaction",array('FullName1'=>$_POST['txt_name'],'LastName1'=>$_POST['LastName'],'Address1'=>$_POST['address'],'City1'=>$_POST['city'],'Postal1'=>$_POST['postal'],'CityID1'=>$_POST['country'],'Phone1'=>$_POST['txt_phone'],'AddID'=>$_POST['place'],'Use_same'=>$_POST['use_same']),DB_AUTOQUERY_UPDATE,"id='".$tran_id."'");
			echo "<script language = 'javascript'> alert('Add address complete!'); location.href = '".$_SERVER['HTTP_REFERER']."'</script>";
		}
		if($_POST['shipping']!='')
		{
			$oDb->autoExecute("tbl_transaction",array('ShipID'=>$_POST['shipping']),DB_AUTOQUERY_UPDATE,"id='".$tran_id."'");
			$url = SITE_URL."cart/checkout/";
			echo "<script type='text/javascript'>window.location.href = '".$url."';</script>";
		}
		$oSmarty->display("shipping.tpl");
	}
	 
	function checkout()
	{
		global $oDb, $oSmarty;
		// Check customer login, nếu site yều cầu đăng nhập để thanh toán
		//$this->checkCustomerLogin();
		
		if($_POST["payment"]) {
			$_SESSION["payment"] = $_POST["payment"];
		}
		$sql = "SELECT * FROM tbl_payment_item WHERE Status=1 Order by CreateDate DESC";
		$pay = $oDb->getAll($sql);
		$oSmarty->assign('pay',$pay);  
		
		$cus_id = $_SESSION['userid_cus'];
		$oSmarty->assign('cus_id',$cus_id);
		if($cus_id)
		{
			$sql = "SELECT * FROM tbl_customer WHERE id = '$cus_id'";
			$cus1 = $oDb->getRow("Select Address1,Use_same from tbl_transaction order by id DESC Limit 1");
			$oSmarty->assign('cus1',$cus1);
		}
		else
			$sql="Select * from tbl_transaction order by id DESC Limit 1";
		$cus = $oDb->getRow($sql);
		$oSmarty->assign('cus',$cus);
		$country_name1 = $oDb->getOne("SELECT Name FROM tbl_country WHERE id={$cus['CityID1']}");
		$country_name = $oDb->getOne("SELECT Name FROM tbl_country WHERE id={$cus['CityID']}");
		$oSmarty->assign("country_name",$country_name);
		$oSmarty->assign("country_name1",$country_name1);
		// Giỏ hàng
		$cart = $_SESSION['cart'];
		// Thông tin các sản phẩm có trong giỏ hàng
		$product = $this->getProducts($cart);
		// Tổng giá trị đơn hàng
		$total = $this->getTotal($cart);
		$discount=$_SESSION['code']*$total/100;
		$oSmarty->assign('discount',$discount);
		$oSmarty->assign('result',$product);
		$oSmarty->assign('total',$total);
		$cart_mail = $cart;
		$cart_mail =$oSmarty->fetch('cart_mail.tpl');
		$cart = $oSmarty->fetch('cart_content.tpl');
		$oSmarty->assign('cart_content',$cart);
		$sql = "SELECT id,OrderID,ShipID FROM tbl_transaction WHERE CreateUser = '$cus_id' Order by id DESC Limit 1";
		$tran_id = $oDb->getRow($sql);
		$price_ship = $oDb->getOne("SELECT Price FROM tbl_shipping_item WHERE id = {$tran_id['ShipID']}");
		$oSmarty->assign('price_ship',$price_ship);
		$total1=$total - $discount + $price_ship;
		$oSmarty->assign('total1',$total1);
		$sql = "SELECT Content FROM tbl_payment_item WHERE Status=1 Order by CreateDate DESC Limit 1";
		$pay_info = $oDb->getOne($sql);
		$oSmarty->assign('pay_info',$pay_info);
		if ($_SERVER['REQUEST_METHOD']=='POST')
		{			
			if($_POST['memotoseller']){
				$_SESSION["memotoseller"] = $_POST['memotoseller'];
			}
			else
			{
				$_SESSION["memotoseller"] = '';
			}
			//$cart_content = $_SESSION['cart'];
			// kiem tra xem con hang hay ko
			//pre("Aaaaaaa");die();
			$cart_content = $_SESSION['cart'];
			$array_name = array();
			$dem = 0;
			if($cart_content)
			{
				// CHECK SAN PHAM CON TON TAI KHONG
				$array_conlai = array(); // LUU TRU LAI 
				foreach($cart_content as $k=>$v)
				{
					$sql = "Select t2.number from tbl_product_item as t1 inner join tbl_product_size as t2 on t1.id = t2.pro_id where t2.pro_id = ".$v['product_id']." and t2.size_id = ".$v['size'];
					//pre($sql);
					$soluong = $oDb->getOne($sql);
					$sotru = $soluong - (int)$v['quantity'];
					//pre($sotru);
					if($sotru < 0)
					{
						$sql = "Select Name from tbl_product_item where id = ".$v['product_id'];
						$p_name = $oDb->getOne($sql);
						$array_name[$dem] = $p_name;
						$dem++;
						unset($cart_content[$k]);
						
					}
					else
					{
						if($v['quantity'] != 0)
						{
							$sql = "insert into tbl_shopping_cart (OrderID,product_id,quantity,Code,size,CreateUser,CreateDate) Values ('" .$tran_id['OrderID']. "','" .$v['product_id']. "','" .$v['quantity']. "','" .$_SESSION['code']. "','" .$v['size']. "','" .$_SESSION['userid_cus']. "','".date("Y-m-d H:i")."')";
							$oDb->query($sql);
							$oDb->query("Update tbl_product_size Set number = number - ".$v['quantity']." WHERE pro_id = ".$v['product_id']." AND size_id = ".$v['size']);
						}
					} // END ELSE
				} // END FOREACH
				$_SESSION['cart'] = $cart_content;
				if(count($array_name) >0)
				{
					$str_ds_sp_loi = implode(",",$array_name);
					$str_loi = "alert('These product are not enough stock for your selection'); : ".$str_ds_sp_loi;
					echo '<script type="text/javascript">alert("'.$str_loi.'")</script>';
					echo '<script type="text/javascript">location.href="'.SITE_URL.'cart/checkout/";</script>';
				} // END IF KIEM TRA CO HAY KHONG
				else
				{
					$this->clear_cart();
					if($_POST['agree']=='on'){
						$_POST['agree']=1;
					}
					else{ 
						$_POST['agree']=0;
					}
					if($_POST['memotoseller']) {
						$_SESSION['memotoseller'] = $_POST['memotoseller'];
					}
					$oDb->autoExecute("tbl_transaction",array('AgeID'=>$_POST['agree'],'PayID'=>$_POST['payment'],'memotoseller'=>$_POST['memotoseller'] ,'CreateDate'=>date("Y-m-d H:i")),DB_AUTOQUERY_UPDATE,"id='".$tran_id['id']."'");
					$oSmarty->assign('OrderID',$tran_id['OrderID']);
					$sql = "SELECT Content FROM tbl_infocheck_item WHERE Status=1";
					$infocheck = $oDb->getOne($sql);
					$oSmarty->assign('infocheck',$infocheck);
					$to 		= $cus['Email'];
					$subject 	= "Order confirmation!";

					if(!$cus_id)
					{
						if($cus["Use_same"]==1)
						{
							$eName = $cus["FullName"]." ".$cus["LastName"];
							$eAddress = $cus["Address"];
							$eCity = $cus["City"];
							$ePostal = $cus["Postal"];
						}
						else
						{
							$eName = $cus["FullName1"]." ".$cus["LastName1"];
							$eAddress = $cus["Address1"];
							$eCity = $cus["City1"];
							$ePostal = $country_name1;
						}
					}
			    	else
			    	{
			    		if($cus1["Use_same"]==1)
						{
							$eName = $cus["FullName"]." ".$cus["LastName"];
							$eAddress = $cus["Address"];
							$eCity = $cus["Yahoo"];
							$ePostal = $cus["Postal"];
							$eCountry = $country_name;
						}
						else
						{
							$eName = $cus["FullName1"]." ".$cus["LastName1"];
							$eAddress = $cus["Address1"];
							$eCity = $cus["City1"];
							$ePostal = $cus["Postal1"];
							$eCountry = $country_name1;
						}
			    	}

					$message = "Thank you for shopping with us. <br />
								Below is the confirmation of your order with us, please find below information about your order. <br />
		
		Kindly note that bank transfer must be made within 24 hours after placing your order. <br /> 
								<b>1. Order info:</b> <br /> 
								Order #".$tran_id['OrderID']."<br />". $cart_mail.
								" <table class='table table-bordered' width='100%' cellpadding='3' cellspacing='0'  style='font-size:12px; font-weight:700;'>
									<tr>
										<td width='505px'></td>
										<td style='color:#000;font-weight:bold; '>Discount %:</td>
										<td > SGD $ {$discount}</td>
									</tr>
									<tr>
										<td width='500px'></td>
										<td style='color:#000;font-weight:bold; '>Total shipping & Handling Fees:</td>
										<td > SGD $ {$price_ship}</td>
									</tr>
									<tr>
										<td width='500px'></td>
										<td style='color:#000;font-weight:bold; '>Total (Tax incl):</td>
										<td  style='color:#ff0000;'> SGD $ {$total1}</td>
									</tr>
								 </table>  <br /> 
								 <b>2. Bank transfer instructions:</b> <br />
								 <table width='100%' cellpadding='3' cellspacing='0'  style='font-size:12px; font-weight:700;'>
									<tr>
										<td style='color:#cd232c;font-weight:bold; background: #B9BABE; ' >DELIVERY ADDRESS</td>
										<td style='color:#cd232c;font-weight:bold; background: #B9BABE; ' >BILLING ADDRESS</td>
									</tr>
									<tr>
										<td style='color:#cd232c; background: #EBECEE; '>
											{$eName}<br>
											{$eAddress}<br>
											{$eCity}<br>
											{$ePostal}<br>
											{$eCountry}<br>
										</td>
										<td style='color:#cd232c; background: #EBECEE; '>
											{$eName}<br>
											{$eAddress}<br>
											{$eCity}<br>
											{$ePostal}<br>
											{$eCountry}<br>
										</td>
									</tr>
								 </table><br>
								With love, <br />
								Jolipretty Team" ;	
					/*$message = "Thank you for shopping with us. <br />
								Below is the confirmation of your order with us, please find below information about your order. <br />
		
		Kindly note that bank transfer must be made within 24 hours after placing your order. <br /> 
								<b>1. Order info:</b> <br /> 
								Order #".$tran_id['OrderID']."<br />". $cart_mail.
								" <table width='100%' cellpadding='3' cellspacing='0'  style='font-size:12px; font-weight:700;'>
									<tr>
										<td width='505px'></td>
										<td style='color:#000;font-weight:bold; '>Discount %:</td>
										<td > SGD $ {$discount}</td>
									</tr>
									<tr>
										<td width='500px'></td>
										<td style='color:#000;font-weight:bold; '>Total shipping & Handling Fees:</td>
										<td > SGD $ {$price_ship}</td>
									</tr>
									<tr>
										<td width='500px'></td>
										<td style='color:#000;font-weight:bold; '>Total (Tax incl):</td>
										<td  style='color:#ff0000;'> SGD $ {$total1}</td>
									</tr>
								 </table>  <br /> 
								 <b>2. Bank transfer instructions:</b> <br />
								 
								 We currently accept local bank transfers via Internet Banking. If you transfer using ATM, <br />
		
								please let us know and provide us receipt for verification. Kindly note that Interbank <br />
								
								Transfers* take 3 working days to be credited in our <br />
								
								account. Payment verification will only take place after the transaction is successful. <br />
								*All other local banks apart from DBS or POSB   <br /><br /><br />
								
								Please make all internet banking transfers to: <br />
								
								POSB eSavings Account number: 133-29028-1 <br />
								
								Account type: POSB eSavings Account <br /><br /><br />
								
								After making payment, please send an email to <a href="."mailto:info.jolipretty@gmail.com".">info.jolipretty@gmail.com</a> with the following  <br />
								
								information: <br />
								
								- Date & time of transfer: <br />
								
								- Transaction reference number: <br />
								
								- IB Nick: <br />
								With love, <br />
								Jolipretty Team" ;*/
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
					$headers .= "From: info.jolipretty@gmail.com <info.jolipretty@gmail.com>" . "\r\n";
					$result=@mail($to,$subject,$message,$headers);
				}
			} // END IF
			// ket thuc kiem tra
			//die();
			$oSmarty->display("checkout_success.tpl");
		} //END IF POST
		else 
		{
			//pre($_SESSION['cart']); // SESSION CHUA DU LIEU PRODUCT
			if($_SESSION['cart']){
				// Giỏ hàng
				$cart = $_SESSION['cart'];
				// Thông tin các sản phẩm có trong giỏ hàng
				$product = $this->getProducts($cart);
				// Tổng giá trị đơn hàng
				$total = $this->getTotal($cart);
	// Tổng sản phẩm
				$total123 = $this->getTotal123($cart);
				//pre($cart);
				$oSmarty->assign('result',$product);
				$oSmarty->assign('total',$total);
	            $oSmarty->assign('total123',$total123);
			}
			$oSmarty->display('frm_checkout.tpl');
		}
	}
	function kiemtra_conhang()
	{
		global $oDb, $oSmarty;
		if($_POST['memo']) {
			$_SESSION['memotoseller'] = $_POST['memo'];
		} else {
			$_SESSION['memotoseller'] = '';
		}
		if($_SESSION['cart']){
			// Giỏ hàng
			$cart = $_SESSION['cart'];
			// Thông tin các sản phẩm có trong giỏ hàng
			$product = $this->getProducts($cart);
			// Tổng giá trị đơn hàng
			$total = $this->getTotal($cart);
			// Tổng sản phẩm
			$total123 = $this->getTotal123($cart);
			//pre($cart);
			$oSmarty->assign('result',$product);
			$oSmarty->assign('total',$total);
            $oSmarty->assign('total123',$total123);

            $reloadp = 0;
            foreach ($product as $key => $value) {
            	$proid_vz = $value["product_id"];
            	$prosize_vz = $value["size"];
            	$buynumber = $value["quantity"];
            	$conlai = $oDb->getOne("SELECT number FROM tbl_product_size WHERE pro_id = $proid_vz AND size_id = $prosize_vz");
            	if($conlai < $buynumber)
            	{
            		$reloadp = 1;
            		unset($_SESSION["cart"][$key]);
            	}       		

            }
            if($reloadp == 0)
            {
            	// neu van con hang thi tien hanh mua hang luon: tru sp + gui email
            	$this->trusp_guimail();
            }
		}
		echo $reloadp;
	}
	function trusp_guimail()
	{
		global $oDb, $oSmarty;
		$sql = "SELECT * FROM tbl_payment_item WHERE Status=1 Order by CreateDate DESC";
		$pay = $oDb->getAll($sql);
		$oSmarty->assign('pay',$pay);  
		
		$cus_id = $_SESSION['userid_cus'];
		$oSmarty->assign('cus_id',$cus_id);
		if($cus_id)
		{
			$sql = "SELECT * FROM tbl_customer WHERE id = '$cus_id'";
			$cus1 = $oDb->getRow("Select Address1,Use_same from tbl_transaction order by id DESC Limit 1");
			$oSmarty->assign('cus1',$cus1);
		}
		else {
			$sql="Select * from tbl_transaction order by id DESC Limit 1";
		}
		$cus = $oDb->getRow($sql);
		$oSmarty->assign('cus',$cus);
		$country_name1 = $oDb->getOne("SELECT Name FROM tbl_country WHERE id={$cus['CityID1']}");
		$country_name = $oDb->getOne("SELECT Name FROM tbl_country WHERE id={$cus['CityID']}");
		$oSmarty->assign("country_name",$country_name);
		$oSmarty->assign("country_name1",$country_name1);
		// Giỏ hàng
		$cart = $_SESSION['cart'];
		// Thông tin các sản phẩm có trong giỏ hàng
		$product = $this->getProducts($cart);
		// Tổng giá trị đơn hàng
		$total = $this->getTotal($cart);
		$discount=$_SESSION['code']*$total/100;
		$oSmarty->assign('discount',$discount);
		$oSmarty->assign('result',$product);
		$oSmarty->assign('total',$total);
		$cart_mail = $cart;
		$cart_mail =$oSmarty->fetch('cart_mail.tpl');
		$cart = $oSmarty->fetch('cart_content.tpl');
		$oSmarty->assign('cart_content',$cart);
		// ==== them CreateUser
		$sql = "SELECT id,OrderID,ShipID, CreateUser FROM tbl_transaction WHERE CreateUser = '$cus_id' Order by id DESC Limit 1";
		$tran_id = $oDb->getRow($sql);
		$price_ship = $oDb->getOne("SELECT Price FROM tbl_shipping_item WHERE id = {$tran_id['ShipID']}");
		$oSmarty->assign('price_ship',$price_ship);

		$name_ship = $oDb->getOne("SELECT Name FROM tbl_shipping_item WHERE id = {$tran_id['ShipID']}");
		$oSmarty->assign('name_ship',$name_ship);

		$total1=$total - $discount + $price_ship;
		$oSmarty->assign('total1',$total1);
		$sql = "SELECT Content FROM tbl_payment_item WHERE Status=1 Order by CreateDate DESC Limit 1";
		$pay_info = $oDb->getOne($sql);
		$oSmarty->assign('pay_info',$pay_info);
		// if($cus_id)
		// {
		// 	$sql = "SELECT * FROM tbl_customer WHERE id = '$cus_id'";
		// 	$cus1 = $oDb->getRow("Select Address1,Use_same from tbl_transaction order by id DESC Limit 1");
		// 	$oSmarty->assign('cus1',$cus1);
		// }
		// else {
		// 	$sql="Select * from tbl_transaction order by id DESC Limit 1";
		// }
		// $cus = $oDb->getRow($sql);
		// $oSmarty->assign('cus',$cus);

		$cart_content = $_SESSION['cart'];
		$array_name = array();
		$dem = 0;
		if($cart_content)
		{
			// CHECK SAN PHAM CON TON TAI KHONG
			$array_conlai = array(); // LUU TRU LAI 
			foreach($cart_content as $k=>$v)
			{
				$sql = "Select t2.number from tbl_product_item as t1 inner join tbl_product_size as t2 on t1.id = t2.pro_id where t2.pro_id = ".$v['product_id']." and t2.size_id = ".$v['size'];
				//pre($sql);
				$soluong = $oDb->getOne($sql);
				$sotru = $soluong - (int)$v['quantity'];
				//pre($sotru);
				if($sotru < 0)
				{
					$sql = "Select Name from tbl_product_item where id = ".$v['product_id'];
					$p_name = $oDb->getOne($sql);
					$array_name[$dem] = $p_name;
					$dem++;
					unset($cart_content[$k]);
					
				}
				else
				{
					if($v['quantity'] != 0)
					{
						$sql = "insert into tbl_shopping_cart (OrderID,product_id,quantity,Code,size,CreateUser,CreateDate) Values ('" .$tran_id['OrderID']. "','" .$v['product_id']. "','" .$v['quantity']. "','" .$_SESSION['code']. "','" .$v['size']. "','" .$_SESSION['userid_cus']. "','".date("Y-m-d H:i")."')";
						$oDb->query($sql);
						$oDb->query("Update tbl_product_size Set number = number - ".$v['quantity']." WHERE pro_id = ".$v['product_id']." AND size_id = ".$v['size']);
					}
				} // END ELSE
			} // END FOREACH
			$_SESSION['cart'] = $cart_content;
			if(count($array_name) >0)
			{
				$str_ds_sp_loi = implode(",",$array_name);
				$str_loi = "alert('These product are not enough stock for your selection'); : ".$str_ds_sp_loi;
				echo '<script type="text/javascript">alert("'.$str_loi.'")</script>';
				echo '<script type="text/javascript">location.href="'.SITE_URL.'cart/checkout/";</script>';
			} // END IF KIEM TRA CO HAY KHONG
			else
			{
				$this->clear_cart();
				$payid = 925;
				if($_SESSION["payment"]){
					$payid = $_SESSION["payment"];
				}
				if($_SESSION['memotoseller']) {
					$memotoseller = $_SESSION['memotoseller'];
				} else {
					$memotoseller = '';
				}
				
				$oDb->autoExecute("tbl_transaction",array('AgeID'=>1,'PayID'=>$payid ,'memotoseller'=>$memotoseller ,'CreateDate'=>date("Y-m-d H:i")),DB_AUTOQUERY_UPDATE,"id='".$tran_id['id']."'");

				$oSmarty->assign('OrderID',$tran_id['OrderID']);
				$sql = "SELECT Content FROM tbl_infocheck_item WHERE Status=1";
				$infocheck = $oDb->getOne($sql);
				$oSmarty->assign('infocheck',$infocheck);
				//======== garung ===================
				if($cus['Email'] == '' || (($cus["FullName"] == '') && ($cus["LastName"] == ''))) {
					$sql = "SELECT * FROM tbl_customer WHERE id = ".$tran_id['CreateUser'];
					$cus = $oDb->getRow($sql);
				}
				//=======================
				$to 		= $cus['Email'].",Info.jolipretty@gmail.com";
				$subject 	= "Order confirmation!";	
				
					if(!$cus_id)
					{
						if($cus["Use_same"]==1)
						{
							$eName = $cus["FullName"]." ".$cus["LastName"];
							$eAddress = $cus["Address"];
							$eCity = $cus["City"];
							$ePostal = $cus["Postal"];
						}
						else
						{
							$eName = $cus["FullName1"]." ".$cus["LastName1"];
							$eAddress = $cus["Address1"];
							$eCity = $cus["City1"];
							$ePostal = $country_name1;
						}
					}
			    	else
			    	{
			    		if($cus1["Use_same"]==1)
						{
							$eName = $cus["FullName"]." ".$cus["LastName"];
							$eAddress = $cus["Address"];
							$eCity = $cus["Yahoo"];
							$ePostal = $cus["Postal"];
							$eCountry = $country_name;
						}
						else
						{
							$eName = $cus["FullName1"]." ".$cus["LastName1"];
							$eAddress = $cus["Address1"];
							$eCity = $cus["City1"];
							$ePostal = $cus["Postal1"];
							$eCountry = $country_name1;
						}
			    	}

					$message = "Dear {$eName},<br /><br />
								Thank you for shopping with us. <br />
								Please find below information about your order. Your order will be secured after Paypal payment is complete. Otherwise, your order will expire in 30 mins.<br />
								<b>1. Order info:</b> <br /> 
								Order #".$tran_id['OrderID']."<br />". $cart_mail.
								" <table width='100%' cellpadding='3' cellspacing='0'  style='font-size:12px; font-weight:700;'>
									<tr>
										<td width='505px'></td>
										<td style='color:#000;font-weight:bold; '>Discount %:</td>
										<td > SGD $ {$discount}</td>
									</tr>
									<tr>
										<td width='500px'></td>
										<td style='color:#000;font-weight:bold; '>Total shipping & Handling Fees:</td>
										<td > SGD $ {$price_ship}</td>
									</tr>
									<tr>
										<td width='500px'></td>
										<td style='color:#000;font-weight:bold; '>Total (Tax incl):</td>
										<td  style='color:#ff0000;'> SGD $ {$total1}</td>
									</tr>
								 </table>  <br /> 
								 <b>2. Shipping:</b> ".$name_ship." <br />
								 <table width='100%' cellpadding='3' cellspacing='0'  style='font-size:12px; font-weight:700;'>
									<tr>
										<td style='color:#cd232c;font-weight:bold; background: #B9BABE; ' >DELIVERY ADDRESS</td>
										<td style='color:#cd232c;font-weight:bold; background: #B9BABE; ' >BILLING ADDRESS</td>
									</tr>
									<tr>
										<td style='color:#cd232c; background: #EBECEE; '>
											{$eName}<br>
											{$eAddress}<br>
											{$eCity}<br>
											{$ePostal}<br>
											{$eCountry}<br>
										</td>
										<td style='color:#cd232c; background: #EBECEE; '>
											{$eName}<br>
											{$eAddress}<br>
											{$eCity}<br>
											{$ePostal}<br>
											{$eCountry}<br>
										</td>
									</tr>
								 </table><br>
								With love, <br />
								Jolipretty Team" ;	
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
				$headers .= "From: info.jolipretty@gmail.com <info.jolipretty@gmail.com>" . "\r\n";
				$result=@mail($to,$subject,$message,$headers);
			}
		} 
	}
	function checkout_success()
	{
		global $oDb, $oSmarty;
		$sql = "SELECT * FROM tbl_payment_item WHERE Status=1 Order by CreateDate DESC";
		$pay = $oDb->getAll($sql);
		$oSmarty->assign('pay',$pay);  
		
		$cus_id = $_SESSION['userid_cus'];
		$oSmarty->assign('cus_id',$cus_id);
		if($cus_id)
		{
			$sql = "SELECT * FROM tbl_customer WHERE id = '$cus_id'";
			$cus1 = $oDb->getRow("Select Address1,Use_same from tbl_transaction order by id DESC Limit 1");
			$oSmarty->assign('cus1',$cus1);
		}
		else {
			$sql="Select * from tbl_transaction order by id DESC Limit 1";
		}
		$cus = $oDb->getRow($sql);
		$oSmarty->assign('cus',$cus);
		$country_name1 = $oDb->getOne("SELECT Name FROM tbl_country WHERE id={$cus['CityID1']}");
		$country_name = $oDb->getOne("SELECT Name FROM tbl_country WHERE id={$cus['CityID']}");
		$oSmarty->assign("country_name",$country_name);
		$oSmarty->assign("country_name1",$country_name1);
		// Giỏ hàng
		$cart = $_SESSION['cart'];
		// Thông tin các sản phẩm có trong giỏ hàng
		$product = $this->getProducts($cart);
		// Tổng giá trị đơn hàng
		$total = $this->getTotal($cart);
		$discount=$_SESSION['code']*$total/100;
		$oSmarty->assign('discount',$discount);
		$oSmarty->assign('result',$product);
		$oSmarty->assign('total',$total);
		$cart_mail = $cart;
		$cart_mail =$oSmarty->fetch('cart_mail.tpl');
		$cart = $oSmarty->fetch('cart_content.tpl');
		$oSmarty->assign('cart_content',$cart);
		// ===== them CreateUser ============
		$sql = "SELECT id,OrderID,ShipID,CreateUser FROM tbl_transaction WHERE CreateUser = '$cus_id' Order by id DESC Limit 1";
		$tran_id = $oDb->getRow($sql);
		$price_ship = $oDb->getOne("SELECT Price FROM tbl_shipping_item WHERE id = {$tran_id['ShipID']}");
		$oSmarty->assign('price_ship',$price_ship);

		$name_ship = $oDb->getOne("SELECT Name FROM tbl_shipping_item WHERE id = {$tran_id['ShipID']}");
		$oSmarty->assign('name_ship',$name_ship);

		$total1=$total - $discount + $price_ship;
		$oSmarty->assign('total1',$total1);
		$sql = "SELECT Content FROM tbl_payment_item WHERE Status=1 Order by CreateDate DESC Limit 1";
		$pay_info = $oDb->getOne($sql);
		$oSmarty->assign('pay_info',$pay_info);
		// if($cus_id)
		// {
		// 	$sql = "SELECT * FROM tbl_customer WHERE id = '$cus_id'";
		// 	$cus1 = $oDb->getRow("Select Address1,Use_same from tbl_transaction order by id DESC Limit 1");
		// 	$oSmarty->assign('cus1',$cus1);
		// }
		// else {
		// 	$sql="Select * from tbl_transaction order by id DESC Limit 1";
		// }
		// $cus = $oDb->getRow($sql);
		// $oSmarty->assign('cus',$cus);

		$cart_content = $_SESSION['cart'];
		$array_name = array();
		$dem = 0;
		if($cart_content)
		{
			// CHECK SAN PHAM CON TON TAI KHONG
			$array_conlai = array(); // LUU TRU LAI 
			foreach($cart_content as $k=>$v)
			{
				$sql = "Select t2.number from tbl_product_item as t1 inner join tbl_product_size as t2 on t1.id = t2.pro_id where t2.pro_id = ".$v['product_id']." and t2.size_id = ".$v['size'];
				//pre($sql);
				$soluong = $oDb->getOne($sql);
				$sotru = $soluong - (int)$v['quantity'];
				//pre($sotru);
				if($sotru < 0)
				{
					$sql = "Select Name from tbl_product_item where id = ".$v['product_id'];
					$p_name = $oDb->getOne($sql);
					$array_name[$dem] = $p_name;
					$dem++;
					unset($cart_content[$k]);
					
				}
				else
				{
					if($v['quantity'] != 0)
					{
						$sql = "insert into tbl_shopping_cart (OrderID,product_id,quantity,Code,size,CreateUser,CreateDate) Values ('" .$tran_id['OrderID']. "','" .$v['product_id']. "','" .$v['quantity']. "','" .$_SESSION['code']. "','" .$v['size']. "','" .$_SESSION['userid_cus']. "','".date("Y-m-d H:i")."')";
						$oDb->query($sql);
						$oDb->query("Update tbl_product_size Set number = number - ".$v['quantity']." WHERE pro_id = ".$v['product_id']." AND size_id = ".$v['size']);
					}
				} // END ELSE
			} // END FOREACH
			$_SESSION['cart'] = $cart_content;
			if(count($array_name) >0)
			{
				$str_ds_sp_loi = implode(",",$array_name);
				$str_loi = "alert('These product are not enough stock for your selection'); : ".$str_ds_sp_loi;
				echo '<script type="text/javascript">alert("'.$str_loi.'")</script>';
				echo '<script type="text/javascript">location.href="'.SITE_URL.'cart/checkout/";</script>';
			} // END IF KIEM TRA CO HAY KHONG
			else
			{
				$this->clear_cart();
				$payid = 925;
				if($_SESSION["payment"]) {
					$payid = $_SESSION["payment"];
				}
				if($_SESSION['memotoseller']) {
					$memotoseller = $_SESSION['memotoseller'];
				} else {
					$memotoseller = '';
				}
				$oDb->autoExecute("tbl_transaction",array('AgeID'=>1,'PayID'=>$payid, 'memotoseller' => $memotoseller,'CreateDate'=>date("Y-m-d H:i")),DB_AUTOQUERY_UPDATE,"id='".$tran_id['id']."'");

				$oDb->query("Update tbl_message Set IsRead = (IsRead + 1) WHERE Code = ".$_SESSION['code']);

				$oSmarty->assign('OrderID',$tran_id['OrderID']);
				$sql = "SELECT Content FROM tbl_infocheck_item WHERE Status=1";
				$infocheck = $oDb->getOne($sql);
				$oSmarty->assign('infocheck',$infocheck);
				//======== garung ===================
				if($cus['Email'] == '' || (($cus["FullName"] == '') && ($cus["LastName"] == ''))) {
					$sql = "SELECT * FROM tbl_customer WHERE id = ".$tran_id['CreateUser'];
					$cus = $oDb->getRow($sql);
				}
				//=======================
				$to 		= $cus['Email'];
				$subject 	= "Order confirmation!";	
				
					if(!$cus_id)
					{
						if($cus["Use_same"]==1)
						{
							$eName = $cus["FullName"]." ".$cus["LastName"];
							$eAddress = $cus["Address"];
							$eCity = $cus["City"];
							$ePostal = $cus["Postal"];
						}
						else
						{
							$eName = $cus["FullName1"]." ".$cus["LastName1"];
							$eAddress = $cus["Address1"];
							$eCity = $cus["City1"];
							$ePostal = $country_name1;
						}
					}
			    	else
			    	{
			    		if($cus1["Use_same"]==1)
						{
							$eName = $cus["FullName"]." ".$cus["LastName"];
							$eAddress = $cus["Address"];
							$eCity = $cus["Yahoo"];
							$ePostal = $cus["Postal"];
							$eCountry = $country_name;
						}
						else
						{
							$eName = $cus["FullName1"]." ".$cus["LastName1"];
							$eAddress = $cus["Address1"];
							$eCity = $cus["City1"];
							$ePostal = $cus["Postal1"];
							$eCountry = $country_name1;
						}
			    	}

					$message = "Dear {$eName},<br /><br />
								Thank you for shopping with us. <br />
								Please find below information about your order. Your order will be secured after Paypal payment is complete. Otherwise, your order will expire in 30 mins.<br />
								<b>1. Order info:</b> <br /> 
								Order #".$tran_id['OrderID']."<br />". $cart_mail.
								" <table width='100%' cellpadding='3' cellspacing='0'  style='font-size:12px; font-weight:700;'>
									<tr>
										<td width='505px'></td>
										<td style='color:#000;font-weight:bold; '>Discount %:</td>
										<td > SGD $ {$discount}</td>
									</tr>
									<tr>
										<td width='500px'></td>
										<td style='color:#000;font-weight:bold; '>Total shipping & Handling Fees:</td>
										<td > SGD $ {$price_ship}</td>
									</tr>
									<tr>
										<td width='500px'></td>
										<td style='color:#000;font-weight:bold; '>Total (Tax incl):</td>
										<td  style='color:#ff0000;'> SGD $ {$total1}</td>
									</tr>
								 </table>  <br /> 
								 <b>2. Shipping:</b> ".$name_ship." <br />
								 <table width='100%' cellpadding='3' cellspacing='0'  style='font-size:12px; font-weight:700;'>
									<tr>
										<td style='color:#cd232c;font-weight:bold; background: #B9BABE; ' >DELIVERY ADDRESS</td>
										<td style='color:#cd232c;font-weight:bold; background: #B9BABE; ' >BILLING ADDRESS</td>
									</tr>
									<tr>
										<td style='color:#cd232c; background: #EBECEE; '>
											{$eName}<br>
											{$eAddress}<br>
											{$eCity}<br>
											{$ePostal}<br>
											{$eCountry}<br>
										</td>
										<td style='color:#cd232c; background: #EBECEE; '>
											{$eName}<br>
											{$eAddress}<br>
											{$eCity}<br>
											{$ePostal}<br>
											{$eCountry}<br>
										</td>
									</tr>
								 </table><br>
								With love, <br />
								Jolipretty Team" ;	
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
				$headers .= "From: info.jolipretty@gmail.com <info.jolipretty@gmail.com>" . "\r\n";
				$result=@mail($to,$subject,$message,$headers);
			}
		} // END IF
		// ket thuc kiem tra
		//die();
		$oSmarty->display("checkout_success.tpl");
	}
	function paypal_ipn()
	{
		echo "";
	}
	/**
	 * Hiển thị số lượng hiện có trong giỏ hàng
	 *
	 */
	function show_count()
	{
		global $oDb, $oSmarty;
		$cart = $_SESSION['cart'];
		if($cat)
		foreach($cart as $key=>$val){
			$sql = "SELECT * FROM tbl_product_item WHERE Status=1 AND id={$val['product_id']}";
			$product = $oDb->getAll($sql);
			$cart[$key]['products'] = $product;
		}
		$numRows = count($cart);
		$oSmarty->assign('cart',$cart);
		$oSmarty->assign('count',$numRows);	
		
		if($_SESSION['cart']){
			// Giỏ hàng
			$cart = $_SESSION['cart'];
			// Thông tin các sản phẩm có trong giỏ hàng
			$product = $this->getProducts($cart);
			// Tổng giá trị đơn hàng
			$total = $this->getTotal($cart);
			//pre($cart);
			$oSmarty->assign('result',$product);
			$oSmarty->assign('total',$total);
		}
				
		$oSmarty->display('show_count.tpl');
	}
	
	
	/** 	
	*	Kiểm tra customer đã đăng nhập chưa:
	*	- Nếu chưa đăng nhập sẽ redirect về trang đăng nhập
	*	- Nếu đã đăng nhập sẽ return true.
	**/
	function checkCustomerLogin()
	{
		global $oDb, $oSmarty;
		$cuslogin = $_SESSION['cuslongin'];
		if($cuslogin)
			return true;
		else
			return false;
	}
	
	/**
	 * 	Gủi mail
	 *	Return true nếu gửi mail thành công or false nếu gủi mail thất bại
	 */
	function sendMail($email, $data)
	{
		global $oDb, $oSmarty;
		//$result		= $_SESSION['cart'];
		//$total		= $this->getTotal($result);
		$to 		= $email;
		$subject 	= "Orders jolipretty.com!";	
		$message = "Your success in order jolipretty.com";
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		$headers .= "From: info.jolipretty@gmail.com <info.jolipretty@gmail.com>" . "\r\n";
		return @mail($to,$subject,$message,$headers);
	}
	function getParentID($cat_id)
	{
		global $oSmarty, $oDb;
		 $where = "Status=1";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$_SESSION['lang_id']} ";
		$sql = "SELECT ParentID FROM tbl_product_category WHERE {$where} AND id={$cat_id} ORDER BY Ordering";
		$cat_parent = $oDb->getOne($sql);
		if($cat_parent)
			$result = $this->getParentID($cat_parent);
		else
			$result = $cat_id;
		return $result;
	}
	function getChildrenID($cat_id)
	{
		global $oSmarty, $oDb;
		 $where = "Status=1";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$_SESSION['lang_id']} ";
		$sql = "SELECT ParentID FROM tbl_product_category WHERE {$where} AND id={$cat_id} ORDER BY Ordering";
		$cat_parent = $oDb->getOne($sql);
		if($cat_parent)
			$result = $cat_parent;
		else
			$result = $cat_id;
		return $result;
	}
}


?>