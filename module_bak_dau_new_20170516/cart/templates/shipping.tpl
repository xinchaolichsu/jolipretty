{literal}
<script language="javascript">	
	var null_name = 'Please enter your first name';
	var null_name1 = 'Please enter your last name';
	var null_code = 'Please enter your ZIP code';
	var null_address = 'Please give us the address';
	var null_phone = 'Please enter your handphone number';
	var null_city = 'Please enter your city';
	
	function validateForm( frm )
	{
		var re = /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/;
		if ( frm.txt_name.value == '')
		{
			alert (null_name);
			frm.txt_name.focus();
			return false;
		}
		if ( frm.LastName.value == '')
		{
			alert (null_name1);
			frm.LastName.focus();
			return false;
		}
		if ( frm.address.value == '')
		{
			alert (null_address);
			frm.address.focus();
			return false;
		}
		if ( frm.city.value == '')
		{
			alert (null_city);
			frm.city.focus();
			return false;
		}
		if ( frm.txt_phone.value == '')
		{
			alert (null_phone);
			frm.txt_phone.focus();
			return false;
		}
		if ( frm.postal.value == '')
		{
			alert (null_code);
			frm.postal.focus();
			return false;
		}
	}
				
</script>
{/literal}

<div class="box_center1">
<div class="box_center_title1"><a href="{$smarty.const.SITE_URL}">Home / </a> Address / Shipping</div>
    <div class="box_center_content1" style="color:#5b5b5b; line-height:25px;">
		<form name="frm_address" action="" method="post">
        {if (!$cus_id && $cus.Use_same==0) || ($cus_id && $cus1.Use_same==0)}
        <div class="cart_title">Address</div>
        <span style="width:100%; color:#FF0000;">{$error}</span>
        <!--<div style="width:100%; float:left;">Choose a Delivery address: 
                <select name="place" id="place"  style="height:20px; width:150px;" >
                        <option value="1" {if $smarty.post.place == 1} selected="selected" {/if}>My home address</option>
                        <option value="2" {if $smarty.post.place == 2} selected="selected" {/if}>My office address</option>
                        <option value="3" {if $smarty.post.place == 3} selected="selected" {/if}>My school address</option>
                        <option value="4" {if $smarty.post.place == 4} selected="selected" {/if}>Other address</option>
                   </select>   
        </div>
        <div style="width:46%; margin:10px 0 0 40px; float:left;">
        <input  type="checkbox" name="use_same" checked="checked" /> Use the same address for billing
        </div>-->
        <div style="width:50%; margin:10px 0 0 0; float:left;">
			<div style="width:150px; float:left; margin-top:10px;">First Name <font color="#FF0000">(*)</font></div>
            <input type="text"  class="bg_input" name="txt_name" value=""/><br /><br />
			<div style="width:150px; float:left; margin-top:10px;">Last Name <font color="#FF0000">(*)</font></div>
            <input type="text" class="bg_input" name="LastName"  value=""/><br /><br />
			<div style="width:150px; float:left; margin-top:10px;">Address <font color="#FF0000">(*)</font></div>
            <input type="text"  class="bg_input" name="address" value="" /><br /><br />
			<div style="width:150px; float:left; margin-top:10px;">City <font color="#FF0000">(*)</font></div>
            <input type="text"  class="bg_input" name="city"  value="" /><br /><br />
			<div style="width:150px; float:left; margin-top:10px;">Country <font color="#FF0000">(*)</font></div>
                <select name="country" id="country"  style="height:20px; width:237px;" >
                    {foreach from=$country item=item name=item}
                        <option value="{$item.id}" {if $smarty.post.country == 1} selected="selected" {/if}>{$item.Name}</option>
                    {/foreach}
                </select><br /><br />
			<div style="width:150px; float:left; margin-top:10px;">Postal/Zip code <font color="#FF0000">(*)</font></div>
            <input type="text"  class="bg_input" name="postal"  value="" /><br /><br />
            <div style="width:150px; float:left; margin-top:10px;">Phone <font color="#FF0000">(*)</font></div>	
            <input type="text"  class="bg_input" name="txt_phone"  value="" />
        </div>
        <div style="width:100%; float:left;">
                <input type="submit" class="bg_submit" style="color:#fff; font-size:11px; margin:10px 0 25px 80px; width:180px;"  value="Add a new address" />  
                 
        </div>
        </form>
        <div style="width:50%; float:left;">
        <div class="cart_title" style="color:#3c3c3c; font-size:12px; text-transform:none; font-weight:700;">Delivery address</div>
        <div style="width:100%; padding:0 0 40px 0; float:left;">
        Name: <span style="margin-left:55px;">{$cus.FullName} {$cus.LastName}</span> <br />
        Address:  <span style="margin-left:43px;">{$cus.Address}</span>  <br />
        City: <span style="margin-left:65px;">{$cus.City}{$cus.Yahoo}</span> <br />
        Postal/Zip code: {$cus.Postal}  <br />
        Country: <span style="margin-left:42px;">{$country_name}</span>
        </div>
        </div>
        {/if}
        <div style="width:50%; float:left;">
        <div class="cart_title" style="color:#3c3c3c; font-size:12px; text-transform:none; font-weight:700;">Billing address</div>
        <div style="width:100%; padding:0 0 40px 0; float:left;">
        {if !$cus_id}
            {if $cus.Use_same==1}
                Name: <span style="margin-left:55px;">{$cus.FullName} {$cus.LastName}</span> <br />
            	Address:  <span style="margin-left:43px;">{$cus.Address}</span> <br />
                City: <span style="margin-left:65px;">{$cus.City}</span> <br />
                Postal/Zip code: {$cus.Postal}  <br />
                
            {else}
                Name: <span style="margin-left:55px;">{$cus.FullName1} {$cus.LastName1}</span> <br />
            	Address:  <span style="margin-left:43px;">{$cus.Address1}</span>  <br />
                {$cus.City1} <br />
                Postal/Zip code: {$cus.Postal1}  <br />
                Country: <span style="margin-left:42px;">{$country_name1}</span>
            {/if} 
        {else}
            {if $cus1.Use_same==1}
                Name: <span style="margin-left:55px;">{$cus.FullName} {$cus.LastName}</span> <br />
            	Address:  <span style="margin-left:43px;">{$cus.Address}</span> <br />
                 City: <span style="margin-left:65px;">{$cus.Yahoo} <br />
                Postal/Zip code: {$cus.Postal}  <br />
                Country: <span style="margin-left:42px;">{$country_name}</span>
            {else}
                Name: <span style="margin-left:55px;">{$cus.FullName1} {$cus.LastName1}</span> <br />
            	Address:  <span style="margin-left:43px;">{$cus.Address1}</span> <br />
                 City: <span style="margin-left:65px;">{$cus.City1}</span> <br />
                Postal/Zip code: {$cus.Postal1}  <br />
                Country: <span style="margin-left:42px;">{$country_name1}</span>
            {/if} 
        {/if}
        </div>
        </div>
        <div class="cart_title">Shipping</div>
        <div style="width:100%; float:left;">
        Delivery information<br />
        <form name="frm_shipping" action="" method="post">
        {foreach from=$shipping item=item name=item}
                        <input  type="radio" name="shipping" value="{$item.id}" {if $item.id==$smarty.post.shipping || $smarty.foreach.item.first} checked="checked"{/if} />{$item.Name}  SGD: ${$item.Price}<br />
        {/foreach}
                        <input type="submit" class="bg_submit" style="color:#fff; font-size:11px; float:right;" name="bt_contact" value="Review Order" onclick='location.href="{$smarty.const.SITE_URL}customer/chekout/";'/ />
        				<a class="bg_submit" style="color:#fff; float:right;" onclick="window.history.back();">Back</a>
        </form>
        </div>
    </div>
</div>