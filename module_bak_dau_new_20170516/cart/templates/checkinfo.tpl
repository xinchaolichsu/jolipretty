{literal}
<script language="javascript">	
	var null_name = 'Please enter your first name';
	var null_name1 = 'Please enter your last name';
	var null_email = 'Please enter your email address';
	var null_code = 'Please enter your ZIP code';
	var null_address = 'Please give us the address';
	var null_phone = 'Please enter your handphone number';
	var null_city = 'Please enter your city';
	
	function validateForm( frm )
	{
		var re = /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/;
		if ( frm.txt_name.value == '')
		{
			alert (null_name);
			frm.txt_name.focus();
			return false;
		}
		if ( frm.LastName.value == '')
		{
			alert (null_name1);
			frm.LastName.focus();
			return false;
		}
		if ( frm.txt_email.value == '')
		{
			alert (null_email);
			frm.txt_email.focus();
			return false;
		}
		if ( !frm.txt_email.value.match(re) )
		{
			alert ("Invalid email address");
			$('#txt_email').focus();
			return false;
		} 
		if ( frm.txt_address.value == '')
		{
			alert (null_address);
			frm.txt_address.focus();
			return false;
		}
		if ( frm.txt_city.value == '')
		{
			alert (null_city);
			frm.txt_city.focus();
			return false;
		}
		if ( frm.txt_postal.value == '')
		{
			alert (null_code);
			frm.txt_postal.focus();
			return false;
		}
		if ( frm.txt_phone.value == '')
		{
			alert (null_phone);
			frm.txt_phone.focus();
			return false;
		}
	}
				
</script>
{/literal}
<!-- Contact -->
<div class="box_center1">		
        <div class="box_center_title1"><a href="{$smarty.const.SITE_URL}">Home / </a> {#cart_title#} &nbsp;/&nbsp; {#cart_order#}</div>
<div class="box_center_content1">

	<div style="padding:5px 10px; text-align:left;">
		<form name="frm_booking" method="post" action="" enctype="multipart/form-data" onsubmit="return validateForm(this);">
			<div class="row">
				<div class="col-md-5 col-md-offset-1 col-sm-6 col-xs-12" style="border-top: 1px solid #f2f2f2;padding-top:20px;">
					<div style="font-size:18px; text-transform:uppercase; margin:10px 0 20px 0;"><i class="fa fa-hand-o-right" aria-hidden="true"></i>
 Shipping address</div>
					<div style="width:150px; float:left; margin-top:10px;">First Name <font color="#FF0000">(*)</font></div>
		            <input type="text"  class="bg_input form-control" name="txt_name" value="{$cus.FullName}"/>
					<div style="width:150px; float:left; margin-top:10px;">Last Name <font color="#FF0000">(*)</font></div>
		            <input type="text" class="bg_input form-control" name="LastName"  value="{$cus.LastName}"/>
					<div style="width:150px; float:left; margin-top:10px;">Email <font color="#FF0000">(*)</font></div>
		            <input type="text" class="bg_input form-control" name="txt_email"  value="{$cus.Email}"/>
					<div style="width:150px; float:left; margin-top:10px;">Address <font color="#FF0000">(*)</font></div>
		            <input type="text"  class="bg_input form-control" name="txt_address" value="{$cus.Address}" />
					<div style="width:150px; float:left; margin-top:10px;">City <font color="#FF0000">(*)</font></div>
		            <input type="text"  class="bg_input form-control" name="txt_city"  value="{$cus.Yahoo}" />
		            <br>
					<div style="width:150px; float:left; margin-top:10px;">Country <font color="#FF0000">(*)</font></div>
		                <select name="country" id="country" class="form-control" style="height:35px; width:237px;" >
		                    {foreach from=$country item=item name=item}
		                        <option value="{$item.id}" {if $smarty.post.country == 1} selected="selected" {/if}>{$item.Name}</option>
		                    {/foreach}
		                </select>
					<div style="width:150px; float:left; margin-top:10px;">Postal/Zip code <font color="#FF0000">(*)</font></div>
		            <input type="text"  class="bg_input form-control" name="txt_postal"  value="{$cus.Postal}" />
		            <div style="width:150px; float:left; margin-top:10px;">Phone <font color="#FF0000">(*)</font></div>	
		            <input type="number"  class="bg_input form-control" name="txt_phone"  value="{$cus.Phone}" />
		            <div style="width:100%; margin:10px 0 0 0;">
		            <label class="checkbox-inline"><input type="checkbox" name="use_same" checked="checked" /> Use the same address for billing</label>
		            </div>
		            <div class="div_general"  style="padding-top:10px;"></div>
		        </div>
			</div>
			<br>
			<div class="row">
	            <div class="col-md-12 col-md-offset-1 col-sm-12 col-xs-12">
		            <div class="shipping-component" >
		            	<div class="cart_title" style="border-top: 1px solid #f2f2f2;padding-top:20px;"><i class="fa fa-hand-o-right" aria-hidden="true"></i>
 Shipping</div>
				        <div style="width:100%; float:left;">
					        Delivery information<br /><br>
			            	{foreach from=$shipping item=item name=item}
			                        <input  type="radio" name="shipping" value="{$item.id}" {if $item.id==$smarty.post.shipping || $smarty.foreach.item.first} checked="checked"{/if} /> {$item.Name}  SGD: ${$item.Price}<br />
				        	{/foreach}
			            </div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-md-offset-1 col-sm-8 col-xs-12 left">
					<div style="margin:10px 0 40px 0px;">
						<input type="submit" class="bg_submit btn btn-primary" style="width:200px; color:#fff; padding-bottom: 20px;" name="bt_contact" value="Continue to payment" />
					</div>
				</div>
			</div>
		</form>
	</div>
    
	</div>
</div>