<div style="margin-top:10px; width:100%; float:left;" class="cart_table_desktop">
	 <table class="table table-bordered " width="100%" cellpadding="3" cellspacing="0"  style="font-size:11px">
	   <tr align="center" bgcolor="#fcfbfb">
            <th style="border:1px solid #d0d0d0; border-right:none;" width="20%">{#PRODUCT#}</th>
            <th style="border-bottom:1px solid #d0d0d0;border-top:1px solid #d0d0d0;" width="30%">{#description#}</th>
            <th style="border-bottom:1px solid #d0d0d0;border-top:1px solid #d0d0d0;" width="5%">Qty</th>				   	
			<th style="border:1px solid #d0d0d0; border-left:none;" width="13%">Price</th>			   
       </tr>

	   {foreach item=item from=$result name=item}
	   {assign var="pid" value=$item.product.id}
	   {assign var="delete_link" value="index.php?mod=cart&task=delete&pid=$pid"}      
	   <tr>			   		
           <td bgcolor="#FFFFFF">				   		
		   		<a href='{$smarty.const.SITE_URL}product/{$pid}/{$item.product.Name|remove_marks}.html'><img src="{$smarty.const.SITE_URL}{$item.product.Photo}" height="190" border="0" /></a><br /><br />
		   </td>
           <td  align="left" style="color:#704b3a; font-weight:bold">
           <a href='{$smarty.const.SITE_URL}product/{$pid}/{$item.product.Name|remove_marks}.html' style="color:#6c6c6c; font-weight:bold">{$item.product.Name}</a> <br />
           <span style="color:#424242; font-weight:400;">Size: {$item.product.size}</span>  <br />
           {if $item.product.color_name}
           <p style="color:#424242; font-weight:400;">Color: {$item.product.color_name}</p>  <br /><br />
           {/if}
           <!--<input type="button" class="bg_remove" value="Remove" onclick='if(confirm("Bạn có chắc muốn xóa sản phẩm này khỏi giỏ hàng hiện tại?")) location.href="{$smarty.const.SITE_URL}{$delete_link}";' />--></td> 
           <td  width="110" align="center" valign="middle">
                    {$item.quantity}
		   </td>
		   
		   
		   <td  align="center">
           
           <span style="color:#FF0000; font-weight:bold" id="sp_{$item.product.id}">$ {if $item.product.price_size}{$item.quantity*$item.product.price_size}{elseif $item.product.Price}{$item.quantity*$item.product.Price}{else}{$item.quantity*$item.product.OldPrice}{/if} </span>
           </td>
	   </tr>
	   {foreachelse}
		<tr>
		   <td colspan="" align="center" bgcolor="#FFFFFF" align="center">Your cart is currently empty!</td>
	   </tr>
	   {/foreach}		   
	   
	   {if $result}
	   	<tr >
        	<td colspan="2" style="border-top:1px solid #c7c7c7;"></td>
		   <td bgcolor="#FFFFFF" colspan="" align="left" style="font-weight:bold;border-top:1px solid #c7c7c7;  color:#000000;">Subtotal :</td>	
           <td align="center" style="border-top:1px solid #c7c7c7;"><span id="total" style="font-weight:bold; margin-left:70px; ">SGD $ {$total}</span></td>			   
	   </tr>
	   {/if}
	   
	 </table>
</div>
<div class="cart-mobile" style="margin-top:10px; width:100%; float:left;">
	<table class="table table-bordered " style="font-size:11px" width="100%">
		{foreach item=item from=$result name=item}
	    {assign var="pid" value=$item.product.id}
	    {assign var="delete_link" value="index.php?mod=cart&task=delete&pid=$pid"}
        {if $item.color==0}
		<tr>
			<td>Shop</td>
			<td><a href='{$smarty.const.SITE_URL}product/{$pid}/{$item.product.Name|remove_marks}.html'><img src="{$smarty.const.SITE_URL}{$item.product.Photo}" height="190" border="0" /></a><br /><br /></td>
		</tr>
		<tr>
			<td>Description</td>
			<td></td>
		</tr>
		<tr>
			<td>Qty</td>
			<td><a href='{$smarty.const.SITE_URL}product/{$pid}/{$item.product.Name|remove_marks}.html' style="color:#6c6c6c; font-weight:bold">{$item.product.Name}</a> <br />
           <span style="color:#424242; font-weight:400;">Size: {$item.product.size}</span>  <br /><br /></td>
		</tr>
		<tr>
			<td>Price</td>
			<td>{$item.quantity}</td>
		</tr>
		{/if}
	    {foreachelse}
		<tr>
		   <td colspan="" align="center" bgcolor="#FFFFFF" align="center">Your cart is currently empty!</td>
	    </tr>
	    {/foreach}
		<tr>
			<td>Discount/Voucher</td>
			<td></td>
		</tr>
		{if $result}
	   	<tr >
        	<td colspan="2" style="border-top:1px solid #c7c7c7;"></td>
		   <td bgcolor="#FFFFFF" colspan="" align="left" style="font-weight:bold;border-top:1px solid #c7c7c7;  color:#000000;">Subtotal :</td>	
           <td align="center" style="border-top:1px solid #c7c7c7;"><span id="total" style="font-weight:bold; margin-left:70px; ">SGD $ {$total}</span></td>	 
	    </tr>
	    {/if}
		<tr>
			<td><a href="{$smarty.const.SITE_URL}product/"><input class="continue_cart" type="button" value="Continue shopping" ></a></td>
			<td><a href="{$smarty.const.SITE_URL}product/"><input class="continue_cart" type="button" value="Continue shopping" ></a></td>
		</tr>
	</table>
</div>
		

