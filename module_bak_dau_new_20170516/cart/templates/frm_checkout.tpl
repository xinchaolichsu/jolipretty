{literal}
<script language="javascript">	
	
	function hinhthucpay()
		{
			pay=$("input[name='payment']:checked").val();
			url = "/index.php?mod=cart&task=show_payment&ajax=true&pay_id="+pay,
				 $.get(url,function(result){
					$("#pay_info").html(result);	
				}); 
		}
</script>
{/literal}
<!-- Contact -->

<div class="box_center1 container">		
    <div class="col-md-10 col-md-offset-2 col-sm-10 col-sm-offset-2 col-xs-12">
        <div class="box_center_title1"><a href="{$smarty.const.SITE_URL}">Home /</a> {#cart_title#} &nbsp;/&nbsp; {#cart_order#}</div>
        <div class="box_center_content1 ">
    		<div class="cart_title">Payment: Review Order</div>
    		{$cart_content}
             <table class="table table-bordered" width="100%" cellpadding="3" cellspacing="0"  style="font-size:12px; font-weight:700;">
             {if $discount}
             	<tr>
                	<td style="color:#000;font-weight:bold; ">Discount %:</td>
                	<td > SGD $ {$discount}</td>
                </tr>
                {/if}
             	<tr>
                	<td style="color:#000;font-weight:bold; ">Total shipping & Handling Fees:</td>
                	<td > SGD $ {$price_ship}</td>
                </tr>
             	<tr>
                	<td style="color:#000;font-weight:bold; ">Total (Tax incl):</td>
                	<td  style="color:#ff0000;"> SGD $ {$total1}</td>
                </tr>
             </table>

            <br />
            {if (!$cus_id && $cus.Use_same==0) || ($cus_id && $cus1.Use_same==0)}
                <div style="width:50%; float:left;">
                <div class="cart_title" style="color:#3c3c3c; font-size:12px; text-transform:none; font-weight:700;">Delivery address</div>
                <div style="width:100%; padding:0 0 40px 0; float:left;">
                <table class="table">
                    <tbody>
                        <tr>
                            <td>Name:</td>
                            <td>{$cus.FullName} {$cus.LastName}</td>
                        </tr>
                        <tr>
                            <td>Address:</td>
                            <td>{$cus.Address}</td>
                        </tr>
                        <tr>
                            <td>City:</td>
                            <td>{$cus.City}{$cus.Yahoo}</td>
                        </tr>
                        <tr>
                            <td>Postal/Zip code:</td>
                            <td>{$cus.Postal}</td>
                        </tr>
                        <tr>
                            <td>Country:</td>
                            <td>{$country_name}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            </div>
            {/if}
            <div style="width:100%; float:left;">
            <hr>
            <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="cart_title" style="color:#3c3c3c; font-size:12px; text-transform:none; font-weight:700;">Billing address</div>
                <div style="width:100%; padding:0 0 40px 0; float:left;">
                    {if !$cus_id}
                        {if $cus.Use_same==1}
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>Name:</td>
                                        <td>{$cus.FullName} {$cus.LastName}</td>
                                    </tr>
                                    <tr>
                                        <td>Address:</td>
                                        <td>{$cus.Address}</td>
                                    </tr>
                                    <tr>
                                        <td>City:</td>
                                        <td>{$cus.City}</td>
                                    </tr>
                                    <tr>
                                        <td>Postal/Zip code:</td>
                                        <td>{$cus.Postal}</td>
                                    </tr>
                                </tbody>
                            </table>                            
                        {else}
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>Name:</td>
                                        <td>{$cus.FullName} {$cus.LastName}</td>
                                    </tr>
                                    <tr>
                                        <td>Address:</td>
                                        <td>{$cus.Address1}</td>
                                    </tr>
                                    <tr>
                                        <td>City:</td>
                                        <td>{$cus.City1}</td>
                                    </tr>
                                    <tr>
                                        <td>Postal/Zip code:</td>
                                        <td>{$cus.Postal1}</td>
                                    </tr>
                                    <tr>
                                        <td>Country:</td>
                                        <td>{$country_name1}</td>
                                    </tr>
                                </tbody>
                            </table>
                        {/if} 
                    {else}
                        {if $cus1.Use_same==1}
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>Name:</td>
                                        <td>{$cus.FullName} {$cus.LastName}</td>
                                    </tr>
                                    <tr>
                                        <td>Address:</td>
                                        <td>{$cus.Address}</td>
                                    </tr>
                                    <tr>
                                        <td>City:</td>
                                        <td>{$cus.City}</td>
                                    </tr>
                                    <tr>
                                        <td>Postal/Zip code:</td>
                                        <td>{$cus.Postal}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- Name: <span style="margin-left:55px;">{$cus.FullName} {$cus.LastName}</span> <br />
                        	Address:  <span style="margin-left:43px;">{$cus.Address}</span> <br />
                             City: <span style="margin-left:65px;">{$cus.Yahoo} <br />
                            Postal/Zip code: {$cus.Postal}  <br />
                            Country: <span style="margin-left:42px;">{$country_name}</span> -->
                        {else}
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>Name:</td>
                                        <td>{$cus.FullName} {$cus.LastName}</td>
                                    </tr>
                                    <tr>
                                        <td>Address:</td>
                                        <td>{$cus.Address1}</td>
                                    </tr>
                                    <tr>
                                        <td>City:</td>
                                        <td>{$cus.City1}</td>
                                    </tr>
                                    <tr>
                                        <td>Postal/Zip code:</td>
                                        <td>{$cus.Postal1}</td>
                                    </tr>
                                    <tr>
                                        <td>Country:</td>
                                        <td>{$country_name1}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <!-- Name: <span style="margin-left:55px;">{$cus.FullName1} {$cus.LastName1}</span> <br />
                        	Address:  <span style="margin-left:43px;">{$cus.Address1}</span> <br />
                             City: <span style="margin-left:65px;">{$cus.City1}</span> <br />
                            Postal/Zip code: {$cus.Postal1}  <br />
                            Country: <span style="margin-left:42px;">{$country_name1}</span> -->
                        {/if} 
                    {/if}
                </div> 
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="memo-to-seller">
                    <div class="form-group" >
                        <label for="comment" style="font-size:14px; border-top: 1px solid #f2f2f2; padding-top: 20px;">Memo to Seller:</label>
                        <textarea class="form-control" rows="3" name="memotoseller" id="memotoseller"></textarea>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">   
        		<form id="frm_booking" name="frm_booking" method="post" action="" enctype="multipart/form-data" >
        		    <div class="cart_title" style="font-size:14px; border-top: 1px solid #f2f2f2; padding-top: 20px;">Payment method</div>
                    <div style="float:left; width:100%; margin-top:10px;">
                    	{*{foreach from=$pay item=item name=item}*}
                            {*<input  type="radio" name="payment" value="{$item.id}" {if $item.id==$smarty.post.payment || $smarty.foreach.item.first} checked="checked"{/if} onclick="hinhthucpay();" />{$item.Name}<br />*}
                        {*{/foreach}*}
                        <label>
                            <input  type="radio" name="shipping" value="0" checked  />
                            <br>
                            <img src="https://www.paypalobjects.com/webstatic/mktg/logo/PP_AcceptanceMarkTray-NoDiscover_243x40.png" border="0" />
                        </label>
        			</div>
                    <div style="width:100%; margin-top:-40px; float:right;" id="pay_info">{$pay_info}</div></div>
                    <div style="width:100%; margin:10px 0 0 0; padding:20px 0; float:left;">
                        <input  type="checkbox" name="agree" checked="checked" id="agree" required /> I agree with the terms of services and i adhere to them unconditionally
                    </div>
        			<div style="float:left; width:100%; margin-top:10px;">
        				<!-- <input type="submit" class="bg_submit" style="color:#fff; font-size:11px; float:right;" name="bt_payment" value="Check out" /> -->
                        <input type="button" class="bg_submit btn btn-primary" style="color:#fff; font-size:11px; float:right;" name="bt_payment" value="Check out" onclick="checkout_paypal()" />
                		<a class="bg_submit btn btn-warning" style="color:#fff; float:right;" onclick="window.history.back();">Back</a>
        			</div>
        		</form>    
            </div>
            <div style="margin-bottom: 20px;"></div>
    	</div>
    </span>
</div>
{literal}
<script type="text/javascript">
    function checkout_paypal()
    {
        var memotoseller = $('#memotoseller').val();
        $.ajax({
            type: "POST",
            data: {memo: memotoseller},
            url: "/index.php?mod=cart&task=kiemtra_conhang&ajax",
            success: function(response_text){
                console.log(response_text);
                if(response_text == 1)
                {
                    alert("There is not enough stock !!!");window.location.href="/";
                }
                else
                {
                    var isChecked = $("#agree")[0].checked;
                    if (isChecked) {
                        // document.getElementById("frm_booking").submit(); 
                        document.getElementById("frmPayPal").submit(); 
                    } else {
                        alert('Please agree with the terms of service');
                    }
                }
            }
        });
    }
</script>
{/literal}

<!-- <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top" id="frmPayPal">

    <input type="hidden" name="USER" value="info.jolipretty_api1.gmail.com">
    <input type="hidden" name="PWD" value="LKP5MDHW8SKEUXH3">
    <input type="hidden" name="SIGNATURE" value="AFcWxV21C7fd0v3bYYYRCpSSRl31AdRB4eTbHo-VvKRCOqVzz-yadgst">

    <input type="hidden" name="cmd" value="_cart">
    <input type='hidden' name='business' value='info.jolipretty@gmail.com'>
    <input type='hidden' name='upload' value='1'>
    <input type='hidden' name='return' value='http://jolipretty.com/cart/checkout_success/'>
    <input type='hidden' name='cancel_return' value=''>
    <input type='hidden' name='currency_code' value='SGD'>
    
    {foreach item=item from=$result name=item}
        {if $smarty.foreach.item.iteration == 1}<input type="hidden" name="shipping_1" value="{$price_ship}">{/if}

        <input type='hidden' name='item_name_{$smarty.foreach.item.iteration}' value='{$item.product.Name}'>
        <input type='hidden' name='amount_{$smarty.foreach.item.iteration}' value='{if $item.product.Price}{$item.product.Price}{else}{$item.product.OldPrice}{/if}'>
        <input type='hidden' name='quantity_{$smarty.foreach.item.iteration}' value='{$item.quantity}'>
        <input type='hidden' name='on0_{$smarty.foreach.item.iteration}' value='Size'>
        <input type='hidden' name='os0_{$smarty.foreach.item.iteration}' value='{$item.product.size}'>
    {/foreach}

</form> -->

<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" id="frmPayPal">

    <input type="hidden" name="USER" value="info_api1.jolipretty.com">
    <input type="hidden" name="PWD" value="9GMBSKM5A6G8NBCX">
    <input type="hidden" name="SIGNATURE" value="ApfOvY3gXSeA.31UEo2f20bzR2VUAVZqybQ0JPIdv5J5qCJOIah9h5mO">

    <input type="hidden" name="cmd" value="_cart">
    <input type='hidden' name='business' value='info.jolipretty@gmail.com'>
    <input type='hidden' name='upload' value='1'>
    <input type='hidden' name='return' value='http://jolipretty.com'>
    <input type='hidden' name='cancel_return' value=''>
    <input type='hidden' name='currency_code' value='SGD'>
    
    {foreach item=item from=$result name=item}
        {if $smarty.foreach.item.iteration == 1}<input type="hidden" name="shipping_1" value="{$price_ship}">{/if}

        <input type='hidden' name='item_name_{$smarty.foreach.item.iteration}' value='{$item.product.Name}'>
        <input type='hidden' name='amount_{$smarty.foreach.item.iteration}' value='{if $item.product.price_size}{$item.product.price_size}{elseif $item.product.Price}{$item.product.Price}{else}{$item.product.OldPrice}{/if}'>
        <input type='hidden' name='quantity_{$smarty.foreach.item.iteration}' value='{$item.quantity}'>
        <input type='hidden' name='on0_{$smarty.foreach.item.iteration}' value='Size'>
        <input type='hidden' name='os0_{$smarty.foreach.item.iteration}' value='{$item.product.size}'>
    {/foreach}

</form>