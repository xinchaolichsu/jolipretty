<div class="box_center">		
        	<div class="box_center_title_content">
            	<div style="float:left; font-weight:bold;">{#PARTNER#} {$full_cat_name}</div>
        	</div>
<div class="box_center_content"> 
    	<div style="width:97%; padding:0px 10px; height:auto; float:left;">
			{if $detail_item.Photo}<img src="{$smarty.const.SITE_URL}{$detail_item.Photo}" width="180" height="140" alt="{$detail_item.Name}" style="float:left; margin-right:10px; border:1px solid #cccccc" />{/if}         
			<strong style="color:#383838;">{$detail_item.Name}</strong> <font style="color:#383838;">({$detail_item.CreateDate})</font><br><br>   
			<font style="text-align:justify; color:#383838;">{$detail_item.Summarise}</font><br /><br />
			<div style="float:left;"><font style="text-align:justify; color:#383838;">{$detail_item.Content}</font></div>
        </div>		
		{if $other_item}
		<div style="width:95%; padding:20px; height:auto; float:left;">
			<font style="font-weight:bold;">{#other_partner#}</font><br />
			{foreach item=item from=$other_item}
				<li style="margin-left: 20px;"><a href="{$smarty.const.SITE_URL}partner/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{$item.Name}</a></li>
			{/foreach}
		</div>
		{/if}
	</div>
</div>