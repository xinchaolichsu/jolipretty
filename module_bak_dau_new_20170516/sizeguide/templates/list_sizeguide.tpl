<div class="box_center1">
	<div class="box_center_title1"><a href="{$smarty.const.SITE_URL}">HOME / </a> Size Guide </div>
    <div class="box_center_content1 left">
    	{foreach from = $listItem item = item name = item}
        {if $smarty.foreach.item.total==1}
    	<div class="detailMod 5555">
            <div class="content">
            	{$item.Content}
            </div>
        </div>
        {else}
       	<div class="listMod" {if $smarty.foreach.item.last} style="border:none;"{/if}>
                	<div class="name">
                    <div class="date">
                    	{$item.CreateDate|date_format:"%d/%m/%Y"}
                    </div>
                    	<a href="{$smarty.const.SITE_URL}about/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{$item.Name}</a>
                    </div>
            	<div class="img">
                	{if $item.Photo}
                    	<a href="{$smarty.const.SITE_URL}about/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}"><img src="{$item.Photo}" class="photo" alt="{$item.Name}"/></a>
					{/if}
                </div>
                <div class="info">
                    <div class="sum">
                    	{$item.Summarise|truncate:260}
                    </div>
                    <div class="view">
                    	<a href="{$smarty.const.SITE_URL}about/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{#details#}</a>
                    </div>
                </div>
            </div>
           {/if}
   	{/foreach}
        {if $num_rows > $limit}
        <div class="listPage">
            {$num_rows|page:$limit:$smarty.get.page:$paging_path}
        </div>
        {/if} 
	</div>
</div>

