<?php
/**
 * VietEsoft
 * ===============================================================================================
 * Copyright 2010.
 * Website: http://www.vietesfot.com, http://thietkewebsitetrongoi.net
 * -----------------------------------------------------------------------------------------------
 * Phát triển bới VietEsoft
 * -----------------------------------------------------------------------------------------------
 * Chú ý: Tất cả các hàm, phương thức được viết mới do mình tự định nghĩa phải để lại comment gồm:
 	- Người viết
	- Ngày-tháng-năm
	- Chức năng
 * ===============================================================================================
 * Hiển thị tin tức
*/
if (!defined('IN_VES')) die('Hacking attempt');
class sizeguide extends VES_FrontEnd 
{	
	var $table; 		/* Khai báo bảng dữ liệu */
	var $mod;			/* Khai báo mod */
	var $id;			/* Khai báo id */
	var $LangID;		/* Khai báo id ngôn ngữ */
	var $field;			/* Khai báo trường dữ liệu */
	var $pre_table;		/* Khai báo bảng danh mục dữ liệu */	
	function __construct(){		
		$this -> table = 'tbl_sizeguide_item';
		$this -> pre_table = "tbl_about_category";
		$this -> id = mysql_real_escape_string(($_REQUEST['id']=='')?'0':$_REQUEST['id']);
		$this -> field = "*";
		$this -> LangID = $_SESSION['lang_id'];
		parent::__construct($this->table);
	}
	function run($task= "")
	{
		if($task==''){$task = $_GET['task'];}
	    switch($task)
			{	
				/* Hiển thị trang chủ */
				case 'home':
					$this -> home();
				break;
				case 'menu':
					$this -> menu();
				break;
				/* Hiển thị chi tiết một bản ghi */
				case 'details':
					$this -> details();
				break;
				/* Hiển thị dach sách bản ghi theo danh mục đã chọn */
				case 'category':
					$this->listItem($_GET['id']);
				break;
				default:
                    /* List toàn bộ bản ghi */
					$this->listItem();
                    
                    /*List toàn bộ bản ghi theo từng danh mục*/
                    //$this->listbyCategory();
				break;	
			}
	}
	/**
	 * Page info
	 *
	 * @param $task
	 */
	function getPageinfo($task= "")
	{
	 	global $oSmarty, $oDb;
		switch ($task) {
			case 'category':
				$id = $_GET['id'];
				if ($id)
				{
					$sql = "SELECT Name FROM {$this->pre_table} WHERE id=$id";	
					$row = $oDb->getOne($sql);	
				}
				$aPageinfo=array('title'=> $row." | " . parent::get_config_vars("about","").' | '. PAGE_TITLE, 'keyword'=>PAGE_KEYWORD.",".$row, 'description'=>PAGE_DESCRIPTION);
				$aPath[] = array('link' => '', 'path' => 'about');
				break;
            case 'details':
                $id = $_GET['id'];
                if ($id)
                {
                    $sql = "SELECT Name FROM {$this->table} WHERE id=$id";    
                    $row = $oDb->getOne($sql);    
                }
                $aPageinfo=array('title'=> $row." | " . parent::get_config_vars("about","").' | '. PAGE_TITLE, 'keyword'=>PAGE_KEYWORD.",".$row, 'description'=>PAGE_DESCRIPTION);
                $aPath[] = array('link' => '', 'path' => 'about');
                break;
			default:			
				$aPageinfo=array('title'=> parent::get_config_vars("sizeguide","").' | ' . PAGE_TITLE, 'keyword'=>PAGE_KEYWORD.",".$row, 'description'=>PAGE_DESCRIPTION);
				$aPath[] = array('link' => '', 'path' => 'about');
				break;
		}
		
		$oSmarty->assign('aPageinfo', $aPageinfo);
		$oSmarty->assign("aPath", $aPath);
	}
	
	/**
	 * Hiển thị trang chủ
	 *
	 */
	/**
	 * Display in home
	 *
	 */
	function home()
	{
		global $oSmarty, $oDb;
        $where = " Status=1 ";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		$sql = "SELECT * FROM {$this->pre_table} WHERE {$where} ORDER BY Ordering ASC";
		$about = $oDb->getAll($sql);
		foreach ($about as $k=>$v)
		{
			$sql = "SELECT * FROM {$this->table} WHERE {$where} AND CatID={$v['id']} ORDER BY Ordering ASC";
			$sub = $oDb->getAll($sql);
			$about[$k]['sub'] = $sub;
		}
		$oSmarty->assign("about",$about);
		$oSmarty->display('home_about.tpl');
	}
	
	function menu()
	{
		global $oSmarty, $oDb;            
        $where = "Status=1";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		$sql = "SELECT * FROM {$this->pre_table} WHERE {$where} AND ParentID = 0 ORDER BY Ordering ASC";
		$cat = $oDb->getAll($sql);
		$oSmarty->assign("cat",$cat);
		$tech = $oDb->getAll("SELECT * FROM tbl_tech_item WHERE {$where} Order by CreateDate DESC Limit 3");
		if ($_GET['task']=='category' && $_GET['id'] || $_GET['task']=='details')
		{
			if ($_GET['task']=='category')
			{
				
					$oSmarty->assign("parent1",$_GET['id']);
			}	
			else
			{
				$parent_id1 = $oDb->getOne("SELECT CatID FROM {$this->table} WHERE id = {$_GET['id']}");
				$oSmarty->assign("parent1",$parent_id1);
			}
		}
		$oSmarty->display('menu_about.tpl');
	}
	/**
	 * Hiển thị chi tiết 1 bản ghi
	 *
	 */
	function details()
	{
		global $oDb,$oSmarty;
		$id = $_GET['id'];
		$sql = "SELECT * FROM {$this->table} WHERE Status=1 AND id={$id}";
		$detail_item = $oDb->getRow($sql);		
		$oSmarty->assign('detail_item',$detail_item);
		
		$detail_item["Content"] = str_replace('560','520',$detail_item["Content"]);
		$detail_item["Content"] = str_replace('315','315',$detail_item["Content"]);
		
		if($detail_item['CatID']){
			$full_cat_name = $this->getFullCatName($detail_item['CatID']);
			$oSmarty->assign("full_cat_name",$full_cat_name);
			$str_cat_id = $this->getFullCatId($detail_item['CatID']);
			$sql = "SELECT * FROM {$this->table} WHERE Status=1 AND id<>{$id} AND CatID IN ({$str_cat_id}) ORDER BY CreateDate DESC LIMIT 10";
		} else {
			$sql = "SELECT * FROM {$this->table} WHERE Status=1 AND id<>{$id} ORDER BY CreateDate DESC LIMIT 10";
		}
		$other_item = $oDb->getAll($sql);		
		$oSmarty->assign('other_item',$other_item);
		
		$template = 'detail_about.tpl';
		
		$oSmarty->display($template);
	}
	
	/**
	 * Lấy id danh mục
	 * Trả về: chuỗi id danh  mục
	 */
	function getFullCatId($cat_id,$first = true)
	{
		global $oSmarty, $oDb;
        $where = "Status=1";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		$str_cat = "";
		$sql = "SELECT id FROM {$this->pre_table} WHERE {$where} AND ParentID={$cat_id} ORDER BY Ordering";
		$cat = $oDb->getCol($sql);
		if ($cat)
		{
			$str_cat = implode(',',$cat);
			$temp_str_cat = "";
			foreach ($cat as $key=>$value)
			{
				$temp_str_cat .= $this->getFullCatId($value,false);
			}
			$str_cat .=",".$temp_str_cat;			
		}
		if ($first)  
			$str_cat .=$cat_id;
		return $str_cat;
	}
	
	/**
	 * Lấy tên danh mục
	 * Trả về: chuỗi tên danh mục
	 */
	function getFullCatName($cat_id)
	{
		global $oSmarty, $oDb;
        $where = "Status=1";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		$sql = "SELECT Name FROM {$this->pre_table} WHERE {$where} AND id={$cat_id} ORDER BY Ordering";
		$cat_name = $oDb->getOne($sql);
		$sql = "SELECT ParentID FROM {$this->pre_table} WHERE {$where} AND id={$cat_id} ORDER BY Ordering";
		$parent = $oDb->getOne($sql);
		if ($parent)
		{
			$result = $this->getFullCatName($parent)."&nbsp; / &nbsp;".$cat_name;
		}else 
			$result = "&nbsp; / &nbsp;".$cat_name;
		return $result;
	}
	
	/**
	 * Hiển thị toàn bộ danh sách bản ghi
	 *
	 */
	function listItem($cat_id=0)
	{
		global $oDb,$oSmarty;
		$where = " Status=1 ";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
			$url = $_SERVER['REQUEST_URI'];
			if ($_GET['page'])
				$url = str_replace("/".$_GET['page']."/","",$url);
			$page_link = "$url/i++/";		
			$template = 'list_sizeguide.tpl';		
			$this->getListItem($cat_id,$page_link,"",5,$template);
	}
	function getListItem($cat_id=0,$page_link,$cond="",$limit=5,$template="list_sizeguide.tpl",$order="ORDER BY Ordering ASC")
	{
		global $oDb,$oSmarty;
		$where = " Status=1 ";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		if ($cat_id!=0)
		{
			$str_cat_id = $this->getFullCatId($cat_id);
			$where .=" AND CatID IN ({$str_cat_id})";
			$full_cat_name = $this->getFullCatName($cat_id);
			$oSmarty->assign("full_cat_name",$full_cat_name);
		}
		if ($cond != "")
			$where .= " AND ".$cond;
		$listItem = parent::Paging($limit, $this->field, $where,$page_link, $order);
		foreach ($listItem as $k=>$v)
		{
			$listItem[$k]["Content"] = str_replace('560','520',$listItem[$k]["Content"]);
			$listItem[$k]["Content"] = str_replace('315','315',$listItem[$k]["Content"]);
		}
		$oSmarty->assign("listItem",$listItem);
		$oSmarty->display($template);
	}
}
?>