<div class="box_center">
	<div class="box_center_title">{#ABOUT#} {$full_cat_name}</div>
    <div class="box_center_content">
    	<div class="detailMod 4444">
            <div class="info">
                <div class="name">
                                    <div class="date">
                    	{$detail_item.CreateDate|date_format:"%d/%m/%Y"}
                    </div>

                    {$detail_item.Name}
                </div>
                <div class="sum">
                    {$detail_item.Summarise}
                </div>
            </div>
            <div class="content">
            	{$detail_item.Content}
            </div>
        </div>
<!--            {if $other_item}
            <div class="other_item">
                <div class="title">{#other_news#}</div>
                {foreach item=item from=$other_item}
                    <div class="name">
                    	<a href="{$smarty.const.SITE_URL}about/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{$item.Name}</a>
                        <span>{$item.CreateDate|date_format:"%d/%m/%Y"}</span>
					</div>
                {/foreach}
            </div>
            {/if}
-->        
    </div>
</div>
