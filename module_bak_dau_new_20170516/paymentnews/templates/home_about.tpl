{foreach item=cat from=$about name=cat}
<div class="box_about" {if $smarty.foreach.cat.iteration%4!=0} style="border-right:1px solid #ccc;"{/if}>
	<div class="box_about_title">{$cat.Name}</div>
       {foreach item=item from=$cat.sub name=item}
       		<div class="line_about"><a href="{$smarty.const.SITE_URL}about/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{$item.Name}</a></div>
		{/foreach}
</div>
{/foreach}