<?php
class contactBackEnd extends Bsg_Module_Base{
	var $smarty;
	var $db;
	var $url="";
    var $datagrid;
	var $id;
	var $imgPath;
	var $imgPathShort;
	var $table='';
	var $arrAction;
	public function __construct($oSmarty, $oDb, $oDatagrid)
	{	
		$this -> smarty = $oSmarty;
		$this -> db = $oDb;		
        $this -> datagrid = $oDatagrid;
		$this -> id=$_REQUEST[id];		
		$this -> table	="tbl_contact_item";	 		
		parent::__construct($oDb);		
		$this->bsgDb->setTable($this->table);
	}
	
	function run($task)
	{	
		switch( $task ){
			case 'add':
				$this -> addItem();
				break;
			case 'edit':
				$this -> editItem();
				break;
			case 'delete':
				$this -> deleteItem();
				break;
			case 'delete_all':
				$this -> deleteItems();
				break;
			case 'change_status':				
				$this -> changeStatus($_GET['id'], $_GET['status']);
				break;
			case 'public_all':						
				$this -> changeStatusMultiple( 1 );
				break;
			case 'unpublic_all':						
				$this -> changeStatusMultiple( 0 );
				break;
			case 'save_order':
				$this -> saveOrder();
				break;
			default:					
				$this -> listItem( $_GET['msg'] );		
				break;
		}
	}
	
	function getPageInfo()
	{
		return true;
	}

	function addItem()
	{
		$this -> getPath("Thông tin liên hệ > Thêm mới liên hệ");		
		$this -> buildForm();
	}
	
	function editItem()
	{
		$id = $_GET['id'];
		$this -> getPath("Thông tin liên hệ > Chỉnh sửa liên hệ");	
		$row = $this -> bsgDb -> getRow( $id );
		$this -> buildForm( $row );
	}

	function deleteItem()
	{
		global  $oDb;
		$id = $_GET["id"];
		$this -> bsgDb -> deleteWithPk( $id );
		$msg = "Xóa liên hệ thành công!";
		$this -> listItem( $msg );
	}
	
	function deleteItems()
	{
		$aItems	 = $_GET['arr_check'];
		if(is_array( $aItems) && count( $aItems) > 0){
			$sItems = implode( ',', $aItems );
			$this -> bsgDb -> deleteWithPk( $sItems );
		}
		$msg = "Xóa (các) liên hệ thành công!";
		$this -> listItem( $msg );
	}
	
	function changeStatusMultiple( $status = 0 )
	{
		$aItems	 = $_GET['arr_check'];
		if(is_array( $aItems) && count( $aItems) > 0){
			$sItems = implode( ',', $aItems );
			$this -> bsgDb -> updateWithPk( $sItems, array("Status" => $status) );
		}
		$msg = "Sửa trạng thái liên hệ thành công!";
		$this -> listItem( $msg );
	}
	
	function buildForm( $data=array() , $msg = ''){
		
		$form = new HTML_QuickForm('frmAdmin','post',$_COOKIE['re_dir']."&task=".$_GET['task'], '', "style='padding:10px 15px 0 20px;'");
		
		$form -> setDefaults($data);
		
		
		$form -> addElement('text', 'Email', 'Email', array('size' => 50, 'maxlength' => 255));
		
		
		$form -> addElement('text', 'Subject', 'Tiêu đề', array('size' => 50, 'maxlength' => 255));
		
		$content_editor=parent::editor('Content',$data['Content'],array('width'=>'800', 'height'=>'400'));
        $form -> addElement('static',NULL,'Nội dung',$content_editor);
		        
        $date_time = parent::date_time('CreateDate',($data['CreateDate']) ? ($data['CreateDate']) : date("Y-m-d h:i"));			
		$form -> addElement('static',NULL,'Ngày gửi',$date_time);
		
		$form -> addElement('checkbox', 'Status', 'Kích hoạt');
		
		$btn_group[] = $form -> createElement('submit',null,'Hoàn tất',array("style"=> "border:1px solid gray; padding:0 5px 0 5px;"));		
        $btn_group[] = $form -> createElement('button',null,'Quay lại',array('onclick'=>'window.location.href = \''.$_COOKIE['re_dir'].'\'', "style"=> "border:1px solid gray;"));      
        $form -> addGroup($btn_group);
      
		$form->addElement('hidden', 'id', $data['id']);		
		
		if( $form -> validate())
		{
			
			$aData  = array(
				"Email" => $_POST['Email'],
				"Subject" => $_POST['Subject'],
				"Content" => $_POST['Content'],
				"CreateDate" => $_POST['CreateDate'],
				"Status" 	=> $_POST['Status'],
			);
			if( !$_POST['id'] ){
				
				 $id = $this -> bsgDb -> insert($aData);				 
				 $msg = "Thêm liên hệ thành công! ";
			}else {
				$id = $_POST['id'];				
				$this -> bsgDb -> updateWithPk($id, $aData);
				$msg = "Chỉnh sửa liên hệ thành công ";
			}
			
			$this -> redirect($_COOKIE['re_dir']. "&msg={$msg}");
		}
		
		$form->display();
	}
	
	function deleteImage($id, $field, $path){
		if($id == '')
			return;
		$imgpath = $path.$this->db->getOne("SELECT $field FROM ".$this->table." WHERE id = $id");
		if(is_file($imgpath))			
			@unlink($imgpath);
	}
	
	function changeStatus( $itemId , $status ){
		$aData = array( 'Status' => $status );
		$this -> bsgDb -> updateWithPk( $itemId, $aData );
		$msg = "Thay đổi trạng thái thành công";
		$this->listItem($msg);
	}
	
	function listItem( $sMsg= '' )
	{		
		global $oDb;
		global $oDatagrid;				
		
		$root_path = "Thông tin liên hệ > Danh sách liên hệ";						
		$submit_url= "/index.php?".$_SERVER['QUERY_STRING'];
		
		$table = $this -> table;
		
		$arr_filter= array(
			array(
				'field' => 'Subject',
				'display' => 'Tiêu đề',
				'type' => 'text',
				'name' => 'filter_subject',
				'selected' => $_REQUEST['filter_subject'],
				'filterable' => true
			),			
			array(
				'field' => 'Status',
				'display' => 'Trạng thái',				
				'name' => 'filter_show',
				'selected' => $_REQUEST['filter_show'],
				'options' => array('Vô hiệu','Kích hoạt'),
				'filterable' => true
			)
			
		); 
		
		$arr_cols= array(		
			
			array(
				"field" => "id",					
				"primary_key" =>true,
				"display" => "Id",				
				"align" => "center",
				"sortable" => true
			),	
			array(
				"field" => "Subject",
				"display" => "Tiêu đề",
				"align"	=> 'left',				
				"datatype" => "text",
				"sortable" => true
			),
			array(
				"field" => "Email",
				"display" => "Email",
				"align"	=> 'left',				
				"datatype" => "text",
				"sortable" => true
			),
			array(
				"field" => "Status",
				"display" => "Trạng thái",				
				"datatype" => "publish",
				"sortable" => true
			),		
			array(
				"field" => "CreateDate",
				"display" => "Ngày gửi",
				"align"	=> 'left',				
				"datatype" => "date",
				"sortable" => true
			)	
		);		
		
		$arr_check = array(
			array(
				"task" => "delete_all",
				"confirm"	=> "Xác nhận xóa?",
				"display" => "Xóa"
			),
			array(
				"task" => "public_all",
				"confirm"	=> "Xác nhận thay đổi trạng thái?",
				"display" => "Kích hoạt"
			),
			array(
				"task" => "unpublic_all",
				"confirm"	=> "Xác nhận thay đổi trạng thái?",
				"display" => "Vô hiệu"
			)			
		);
		if( $sMsg )
			$oDatagrid -> setMessage( $sMsg );
		$oDatagrid->display_datagrid($table, $arr_cols, $arr_filter, $submit_url, $this -> getAct() ,120, $root_path, false ,$arr_check);		
		
	}		
	
}

?>