{literal}
<script language="javascript" type="text/javascript">
	function gotoPageResultProduct(page)
	{		
		arr_url = page.split('#');
		var page = arr_url[1];
		$.ajax({
			   type:"GET",
			   url:"/?mod=product&task=search&ajax&page="+page+"&id={/literal}{$smarty.get.id}{literal}",
			   data:"catID={/literal}{$smarty.get.catID}{literal}&priceID={/literal}{$smarty.get.priceID}{literal}&inova={/literal}{$smarty.get.inova}{literal}",
			   success: function(response_text){			   	
				   $("#result-product").html(response_text);
			   }
		   });
	}
	$(document).ready(function(){
		$("#result-product span.span_a_class a").click(function () {
			var page = $(this).attr("href");
			gotoPageResultProduct(page);  
		});
	});
</script>
{/literal}
{config_load file=$smarty.session.lang_file} 
<div id="result-product">
<div class="box_center1">    
	<div class="box_center_title1">Search Results</div>
		<div class="box_center_content1"> 
        {foreach from=$listItem item=item name=item}
            <div class="home_item" {if $smarty.foreach.item.iteration%4==0} style="margin-right:0;"{/if}>
            	<div class="home_img">
                  <a href="{$smarty.const.SITE_URL}product/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}"><img src="{$smarty.const.SITE_URL}{$item.Photo}" width="213" height="310"  alt="{$item.Name}" /></a>	
                  {if $item.SoldOut}<div class="sold_out">{$item.SoldOut}</div>{/if}	  
                  </div>
                  <div class="sold_outT">{if $item.SoldOut}<font color="#019faf">{$item.SoldOut}</font>{elseif $item.Is_new}<font color="#1bca03">New arrival</font>{/if}</div>
                  <a class="home_name" href="{$smarty.const.SITE_URL}product/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{$item.Name|truncate:40}</a>
                  <div class="home_summarise"></div>
                  <div class="home_price">
                  	SGD <span>$ {if $item.Price}{$item.Price}{else}{$item.OldPrice|number_format:0:".":"."}{/if} </span>
                  	<del> {if $item.Price}$ {$item.OldPrice}{/if}</del>
                  </div>  
             </div>
            {foreachelse}
                <div style="font-size:11px; text-align:center;">{#cannot_find#}</div>
            {/foreach}
          
            {if $num_rows > $limit}
            <div class="listPage">
                {$num_rows|page:$limit:$smarty.get.page:$paging_path}
            </div>
            {/if} 
        </div>
    </div>
	</div>
</div>

