<div class="box_center">		
    <div class="box_center_title">{#result_search#}</div>
		<div class="box_center_content">
			{foreach from=$listItem item=item name=item}
            {if $smarty.foreach.item.index%3==0}<div class="div_general">{/if}
                <div class="listProduct">
                    <div class="listProductImg">
                        <a href="{$smarty.const.SITE_URL}product/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}"><img src="{$smarty.const.SITE_URL}{$item.Photo}" width="256" height="386" style="padding:3px;" alt="{$item.Name}" style="float:left;"/></a>
                    </div>
                    <div class="listProductName">
                         <a href="{$smarty.const.SITE_URL}product/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{$item.Name}</a>
                    </div>
                    <div class="listProductCode">
                        {#product_code#} : <span>{$item.Code}</span>
                    </div>
                    <div class="listProductCart">
                        <a href="{$smarty.const.SITE_URL}cart/{$item.id}/{$item.Name|remove_marks}.html">{#cart_title#} : </a>
                    </div>
                </div>
            {if $smarty.foreach.item.iteration%3==0 || ($smarty.foreach.item.total%3!=0 && $smarty.foreach.item.last)}</div>{/if}                 
            {foreachelse}
                <div style="font-size:11px; text-align:center;">{#updating#}</div>
            {/foreach}
              
            {if $num_rows > $limit}
            <div class="listPage">
                {$num_rows|page:$limit:$smarty.get.page:$paging_path}
            </div>
            {/if}  	
	</div>
</div>