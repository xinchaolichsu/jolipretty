<div class="box_left">
	<div class="side_title">{#product_category#}</div>
	<div class="side_content">
    	{if $cat}
    	{foreach from=$cat item=cat name=cat}
        	<div class="product_cat">
            	<a href="{$smarty.const.SITE_URL}product/c-{$cat.id}/{$cat.Name|remove_marks}.html" title="{$cat.Name}" {if $smarty.get.id== $cat.id} style="color:#FF0000;"{/if}> 
        			{if $cat.IsNew}<span style="color:#FC3100; font-weight:700;">{/if}{$cat.Name} {if $cat.IsNew}</span>{/if}
				</a>  
                {if $cat.IsNew}<img src="{$cat.Photo2}" style="float:right;" />{/if}   
			</div>  	
            {if $cat.sub}
            	{foreach from=$cat.sub item=sub name=sub}
                    <div class="product_sub">
                        <a href="{$smarty.const.SITE_URL}product/c-{$sub.id}/{$sub.Name|remove_marks}.html" title="{$sub.Name}">
                        	{$sub.Name}
						</a>
                    </div>
                {/foreach}
            {/if}            
        {/foreach}
        {/if}    
	</div>
</div>
