<div id="sort-product">
<div id="sort_pro">
<input type="hidden" value="{$cat_id}" name="hdn_cat" id="hdn_cat" />
<div class="box_center1" style="width:1000px;">		
    {if $cat_photo}<img src="{$smarty.const.SITE_URL}{$cat_photo}" width="1000" height="280" style="margin:0 0 8px 0;" alt="image" />{/if}
	<div class="box_center_title1">{php}loadModule("product","getLink");{/php}
		</div>
    </div>
		<div class="box_center_content1"> 
			{foreach from=$listItem item=item name=item}
            <div class="home_item" {if $smarty.foreach.item.iteration%4==0} style="margin-right:0;"{/if}>
            	<div class="home_img">
                  <a href="{$smarty.const.SITE_URL}product/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}"><img src="{$smarty.const.SITE_URL}{$item.Photo}" width="213" height="310"  alt="{$item.Name}" /></a>	
                  {if $item.SoldOut}<div class="sold_out">{$item.SoldOut}</div>{/if}	  
                  </div>
                  <div class="sold_outT">{if $item.SoldOut}<font color="#019faf">{$item.SoldOut}</font>{elseif $item.Is_new}<font color="#1bca03">New arrival</font>{/if}</div>
                  <a class="home_name" href="{$smarty.const.SITE_URL}product/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{$item.Name|truncate:40}</a>
                  <div class="home_summarise"></div>
                  <div class="home_price">
                  	SGD <span>$ {if $item.Price}{$item.Price}{else}{$item.OldPrice|number_format:0:".":"."}{/if} </span>
                  	<del> {if $item.Price}$ {$item.OldPrice}{/if}</del>
                  </div>  
             </div>
            {foreachelse}
                <div style="font-size:11px; text-align:center;">{#updating#}</div>
            {/foreach}
            {if $num_rows > $limit}
            <div class="listPage">
                {$num_rows|page:$limit:$smarty.get.page:$paging_path}
            </div>
            {/if} 
	</div>
</div>
</div>


