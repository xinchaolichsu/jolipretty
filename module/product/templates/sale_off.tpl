<div class="box_saleoff">
<div class="saleoff_title">{#product_off#}<a href="{$smarty.const.SITE_URL}product/san-pham-khuyen-mai.html">{#view_more#}</a></div>
	<div class="saleoff_content">
	<ul id="mycarousel" class="jcarousel-skin-tango">	
    	{foreach from=$sale_off item=item name=item}
        <li>
            <div class="sale_item">
            		{if $item.Sale_off}<div class="sale_off">{$item.Sale_off}%</div>{/if}
                   <a href="{$smarty.const.SITE_URL}product/{$item.id}/{$item.Name|remove_marks}.html" title=""><img src="{$smarty.const.SITE_URL}{$item.Photo}" width="195" height="290" style="border:1px solid #e3e3e3; margin:1px 15px 5px 1px;"  alt="{$item.Name}" /></a>
                	<a class="home_name" href="{$smarty.const.SITE_URL}product/{$item.id}/{$item.Name|remove_marks}.html" title="">{$item.Name}</a>
                <div class="sale_price"> 
                     <a class="home_detail" href="{$smarty.const.SITE_URL}cart/{$item.id}/{$item.Name|remove_marks}.html">{#add_cart#}</a>
                	<del>{$item.OldPrice|number_format:0:".":"."} đ</del><br /> <span>{$item.price|number_format:0:".":"."}</span> đ 
                </div>
            </div>
        </li>
        {foreachelse}
            <div style="font-size:11px; text-align:center;">{#updating#}</div>
        {/foreach} 
    </ul>        
    </div>
</div>
