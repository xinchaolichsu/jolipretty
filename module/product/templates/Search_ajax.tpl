{literal}
	<script language="javascript" type="text/javascript">
		function gotoPageProduct_search(page)
		{		
			arr_url = page.split('#');
			var page = arr_url[1];
			$.ajax({
				   type:"GET",
				   url:"/?mod=product&task=Search_ajax&ajax&page="+page+"&id={/literal}{$smarty.get.id}{literal}",
				   data: "listPro={/literal}{$smarty.get.listPro}{literal}",
				   success: function(response_text){			   	
					   $("#product_search_result").html(response_text);
				   }
			   });
		}
		$(document).ready(function(){
			$("#product_search_result span.span_a_class a").click(function () {
				var page = $(this).attr("href");
				gotoPageProduct_search(page);  
			});
		});
	</script>
	{/literal}
	{config_load file=$smarty.session.lang_file}
	<div id="product_search_result">
		<div class="box_home">
			<div class="box_home_title">
				<div class="title_home">Kết quả tìm kiếm</div>
			</div>
			<div class="box_home_content" style="padding-left:13px;{if $num_rows < $limit}padding-bottom:10px;{/if} width:745px; *padding-left:0; *width:758px;">
			<div style="width:95%; color:#505050; text-align:left; padding:15px 0 0px 30px;">Kết quả tìm kiếm:&nbsp;Có&nbsp;<span style="color:#ff6c00; font-weight:bold;">{$num_rows}</span>&nbsp;sản phẩm</div>
			{foreach from=$listItem item=item name=item}
				<div class="item_pro">
					{if $item.Photo}<a href="{$smarty.const.SITE_URL}product/{$item.id}/p-{$item.parent}/children-{$item.children}/child-{$item.child}/{$item.Name|remove_marks}.html" title="{$item.Name}" style=" float:left;"><img src="{$smarty.const.SITE_URL}{$item.Photo}" width="150" height="112" alt="{$item.Name}" style="border:1px solid #719893;"/></a>{/if}
					<div style="padding:10px 5px 2px 5px; width:140px; float:left;"><a href="{$smarty.const.SITE_URL}product/{$item.id}/p-{$item.parent}/children-{$item.children}/child-{$item.child}/{$item.Name|remove_marks}.html" title="{$item.Name}" style="color:#006789; font-weight:bold;">{$item.Name|truncate:50}</a></div>
					<div style="float:left; padding-left:5px; width:145px; color:#4a4a4a;">Mã SP : {$item.Code}</div>
					<div style="float:left; padding-left:5px; width:145px; color:#4a4a4a;">Giá bán :<span style="color:#ff0000;">{$item.Price|number_format:0:".":"."} đ</span></div>
					<div><a href="{$smarty.const.SITE_URL}product/{$item.id}/p-{$item.parent}/children-{$item.children}/child-{$item.child}/{$item.Name|remove_marks}.html" title="{$item.Name}" class="detail" style="opacity:1;filter:alpha(opacity=100);"
	onmouseover="this.style.opacity=0.85;this.filters.alpha.opacity=85"
	onmouseout="this.style.opacity=1;this.filters.alpha.opacity=100">Chi tiết  >></a></div>
					<div class="add_cart">
					<a href="{$smarty.const.SITE_URL}cart/{$item.id}/{$item.Name|remove_marks}.html">Đặt mua</a>
					</div>
				</div>
				{foreachelse}
				<div style="float:left; width:95%; padding:15px 0 15px 0; text-align:center; color:#FF6600;">Không tìm thấy sản phẩm phù hợp</div>
				{/foreach}
				{if $num_rows > $limit}
				<div style="width:95%; height:auto; float:left; text-align:right; padding:5px; margin-top:5px;">
					<font style="font-size:11px;">Page</font> &nbsp;&nbsp;{$num_rows|page:$limit:$smarty.get.page:$paging_path}
				</div>
				{/if}
			</div>
		</div>
	</div>