<!-- <script type="text/javascript" src="{$smarty.const.SITE_URL}lib/jcarousel1/lib/jquery.jcarousel.min.js"></script> -->
<script type="text/javascript" src="{$smarty.const.SITE_URL}lib/jcarousel-master/dist/jquery.jcarousel.js"></script>
<link rel="stylesheet" type="text/css" href="{$smarty.const.SITE_URL}lib/jcarousel1/skins/tango/skin.css" />
<!-- <link href="/lib/cloud-zoom/images/cloud-zoom.css" rel="stylesheet" type="text/css" /> -->
<link href="/lib/cloud-zoom/cloud-zoom.css" rel="stylesheet" type="text/css" />
<!-- <script type="text/javascript" src="/lib/cloud-zoom/images/cloud-zoom.1.0.2.js"></script> -->
<script type="text/javascript" src="/lib/cloud-zoom/cloud-zoom.js"></script>
<script type="text/javascript" src="{$smarty.const.SITE_URL}lib/lightbox/js/jquery.lightbox-0.5.js"></script>
<link rel="stylesheet" type="text/css" href="{$smarty.const.SITE_URL}lib/lightbox/css/jquery.lightbox-0.5.css" media="screen" />
{literal}
<script type="text/javascript">
function mycarousel_initCallback(carousel)
{
    // Disable autoscrolling if the user clicks the prev or next button.
    carousel.buttonNext.bind('click', function() {
        carousel.startAuto(0);
    });

    carousel.buttonPrev.bind('click', function() {
        carousel.startAuto(0);
    });

    // Pause autoscrolling if the user moves with the cursor over the clip.
    carousel.clip.hover(function() {
        carousel.stopAuto();
    }, function() {
        carousel.startAuto();
    });
};

jQuery(document).ready(function() {
    jQuery('#mycarousel').jcarousel({
        auto: 0,
        wrap: 'circular',
        vertical: 'true',
		initCallback: mycarousel_initCallback
    });
    jQuery('#mycarousel-mobile').owlCarousel({
        loop:true,
        nav:true,
        dots:true,
        dotsClass: 'carousel-indicators',
        dotClass: 'dot',
        autoplayTimeout:10000,
        autoplay:true,
        responsiveClass:true,
        items:1,
        smartSpeed:450,
        navText : ['<i class="pe-7s-angle-left"></i>','<i class="pe-7s-angle-right"></i>'],
    });
});
	$(function() {
		$('#gallery a').lightBox();
		$('#size_all').change(function() {
		    var val = $("#size_all option:selected");
		    var item_id= val.attr("item_id"),
		    item_number= val.attr("item_number"),
		    item_price_size= val.attr("item_price_size"),
		    item_Content= val.attr("item_Content");
		   	click_size(item_id,item_number,item_Content, item_price_size);
		});
	});
		

	function show_img(id){
		var windowWidth = $(window).width();
		if(windowWidth > 600 || windowWidth > '600') {
			$("#Main_img").fadeOut();
			$.ajax({
	            type: "POST",
	            url: "/index.php?mod=product&task=show_img&ajax",
	            data: "photo_id="+id,
	            success: function(response_text){
	                $("#Main_img").html(response_text);
					$("#Main_img").fadeIn();
	            }
	        });
		}
	}

	function priceForYou(qty){
	    $("#Priceforyou").fadeOut();
		$.ajax({
            type: "GET",
            url: "/index.php?mod=product&task=Priceforyou&ajax",
            data: "qty="+qty+"&id={/literal}{$smarty.get.id}{literal}",
			success: function(response_text){
                $("#Priceforyou").html(response_text);
				$("#Priceforyou").fadeIn();
            }
        });
	}
	function sub_qty()
	{
		var qty = $('#qty').val();
		qty--;
		if(qty<=0)
		{
			alert('Number less than 0, the system will automatically reset the number 1');	
			qty=1;
			$('#qty').val(qty);
			priceForYou(qty);
		}
		else
		$('#qty').val(qty);
		priceForYou(qty);
	}
	function add_qty()
	{
		var qty = $('#qty').val();
		var curr_size_numb = $('#curr_size_numb').val(); 
		qty++;
		if(curr_size_numb > 0 && qty>curr_size_numb)
		{
			alert('There’s not enough product in stock');
			$('#qty').val(curr_size_numb);
			priceForYou(curr_size_numb);
		}
		else
		{
			$('#qty').val(qty);
			priceForYou(qty);
		}
	}
	function add_cart(id)
	{
		var pid = id;
		var qty = $("#qty").val();
		var size = $("#size").val();
		var color = $("#color_all").val();
		var curr_size_numb = $('#curr_size_numb').val();
		if(size==0)
		{
			alert('Please choose your size');
			return false;
		}
		if(qty<=0)
		{
			alert('Please choose your quantity');
			return false;
		}
		if(color == '')
		{
			alert('Please choose your color');
			return false;
		}
		// location.href="/index.php?mod=cart&ajax&task=add&pid="+pid+"&qty="+qty+"&size="+size+"&curr_size_numb="+curr_size_numb+"&color="+color;
		location.href="/index.php?mod=cart&ajax&task=add&pid="+pid+"&qty="+qty+"&size="+size+"&color="+color;
	}
	
	function detail_product_ajax(id)
	{	
		$("#tab_detail_product").children("div").each(function(i){
			$(this).removeClass("detail_product_active");
			$(this).addClass("detail_product_inactive");
		});
		$("#"+id).hover(
						function(){
							$(this).removeClass("detail_product_inactive").addClass("detail_product_active");
						},
						function(){
							$(this).removeClass("detail_product_inactive").addClass("detail_product_active");
						}
						);
		$.ajax({
			type: "POST",
			url: "/index.php?mod=product&task=detail_product_ajax&ajax",
			data: "pro_id="+id+"&id={/literal}{$smarty.get.id}{literal}",
			success: function(response_text){
				$("#ajax_detail_product").html(response_text);
			}
		});
	}
	function click_size(id,number,content, price_size)
	{	
		//===== garung =====
		if(price_size > 0 || price_size != '0'){
			var price_s = document.getElementById("price_size_tag");
			price_s.innerHTML = price_size;
			var price_root = document.getElementById("price_root");
			price_root.style.display = "none";
		}
		if(content && number < 1)
		{
			number == 1;
			$('#qty').val(number);
			var ele = document.getElementById("click_showhide");
			if(ele.style.display == "none") {
					ele.style.display = "block";
					document.getElementById("size_email").value=id;
					
			  }
			var ele3 = document.getElementById("soldout");
					ele3.style.display = "block";
					ele3.innerHTML=content;
			var ele1 = document.getElementById("item_stock");
					ele1.style.display = "none";
			var ele2 = document.getElementById("addcart_showhide");
					ele2.style.display = "none";
		
		}
		else if(number < 1)
		{
			number == 1;
			$('#qty').val(number);
			var ele = document.getElementById("click_showhide");
			if(ele.style.display == "none") {
					ele.style.display = "block";
					document.getElementById("size_email").value=id;
					
			  }
			var ele3 = document.getElementById("soldout");
					ele3.style.display = "block";
					ele3.innerHTML="Your selection: Temporarily Unavaiable";
			var ele1 = document.getElementById("item_stock");
					ele1.style.display = "none";
			var ele2 = document.getElementById("addcart_showhide");
					ele2.style.display = "none";
		}
		else
		{
			var ele = document.getElementById("click_showhide");
			if(ele.style.display == "block") {
					ele.style.display = "none";
			  }
			var ele1 = document.getElementById("item_stock");
					ele1.style.display = "block";
			var ele2 = document.getElementById("addcart_showhide");
					ele2.style.display = "block";
		}
		if(content != '')
		{
				$("#item_stock").html(' <font color="#FF0000">'+content+'</font>');
		}
		else
		{
			if (number>1)
			{
			
				$("#item_stock").html('<span style="color:#999999">Your selection:</span> <font color="#FF0000">Available</font>');
			}
			else
				$("#item_stock").html('<span style="color:#999999">Your selection:</span> <font color="#FF0000">Only '+number+' item in stock</font>');
			var qty = $("#qty").val();
			if(qty > number)
			{
				if(number <= 0 || qty <= 0) {
					alert('Number less than 0, the system will automatically reset the number 1');	
					number == 1;
				}
				$('#qty').val(number);
				priceForYou(number);
			}
		}
		if(number <= 0 || qty <= 0) {
			alert('Number less than 0, the system will automatically reset the number 1');	
			number == 1;
		}
		$("#curr_size_numb").val(number);
		
		$("#size_all").children("option").each(function(i){
			$(this).removeClass("sl_textA");
			$(this).addClass("sl_text");
		});
		$('#size').val(id);
		$("#size_all").children("option").each(function(i){
			$(this).hover(
			function(){
				$(this).removeClass("sl_text").addClass("sl_textA");
			},
			function(){
				$(this).removeClass("sl_textA").addClass("sl_text");
			}
			);
				
		});
		$("#size"+id).hover(
			function(){
				$("#size"+id).removeClass("sl_text").addClass("sl_textA");
			},
			function(){
				$("#size"+id).removeClass("sl_text").addClass("sl_textA");
			}
			);								
	}
	
	function click_showhide(size) {
		var ele = document.getElementById("click_showhide");
		if(ele.style.display == "none") {
				ele.style.display = "block";
				document.getElementById("size_email").value=size;
				
		  }
		var ele = document.getElementById("item_stock");
				ele.style.display = "none";
	}

	
</script>
{/literal}
<div class="box_center" style="border:none; width:100%;">
	<div class="box_center_title1 breadcrumb-product " style="margin-left: 3% !important;">{php}loadModule("product","getLink");{/php} </div>
	<div class="box_center_content1">
		<div class="row">
			<div class="col-md-5 col-md-offset-1 col-sm-12 col-xs-12 pd_left0 mrg_left4p">
				<div class="detail_photo">
					<div class="col-md-3 col-sm-3 col-xs-12 pd_left0 slide-pro-desktop">
		                <ul id="mycarousel" class="jcarousel-skin-tango">	
		                        {foreach from=$image item=item name=item}
		                        <li id="gallery">
		                              <a href="{$smarty.const.SITE_URL}upload/product/{$item.Photo_Name}"><img src="{$smarty.const.SITE_URL}upload/product/{$item.Photo_Name}" onmouseover="show_img({$item.Photo_ID});" width="100%" height="100%"  alt="{$item.Name}" style="margin-bottom:10px;cursor:pointer;" /></a>
		                        </li>
		                        {/foreach}
		                </ul>
		            </div>
		            <div class="col-md-9 col-sm-9 col-xs-12 slide-pro-desktop">
		                <div id="Main_img" style="float:left;">
		                    <div class="zoom-section">
		                        <div class="zoom-small-image">
		                        {foreach from=$image item=item name=item}
		                        {if $smarty.foreach.item.first}
		                            <a href='{$smarty.const.SITE_URL}upload/product/{$item.Photo_Name}' class = 'cloud-zoom' rel="tint: '',tintOpacity:0.7 ,smoothMove:5,zoomWidth:460, adjustY:-3, adjustX:80"><img src="{$smarty.const.SITE_URL}upload/product/{$item.Photo_Name}" width="100%" height="auto" style="border:1px solid #efefef; margin-left:3px;" alt="anh" /></a>
		                        {/if}
		                        {/foreach}
		                        </div>
		                    </div>
		                    <div class="zoom-desc"></div>                 
						</div>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12 slide-pro-mobile"> 
						<ul id="mycarousel-mobile" class="owl-carousel">	
	                        {foreach from=$image item=item name=item}
	                        <li id="gallery">
	                              <a href="{$smarty.const.SITE_URL}upload/product/{$item.Photo_Name}"><img src="{$smarty.const.SITE_URL}upload/product/{$item.Photo_Name}" onmouseover="show_img({$item.Photo_ID});" width="100%" height="100%"  alt="{$item.Name}" style="margin-bottom:10px;cursor:pointer;" /></a>
	                        </li>
	                        {/foreach}
		                </ul>
					</div>
				</div>
			</div>
			<div class="col-md-6 col-sm-12 col-xs-12 right-detail"> 
				<div class="col-md-12 col-sm-12 col-xs-12">
			        <div class="detai_pro_info col-md-12 col-sm-12">
			        	<div class="dt_pro_name">{$detail_item.Name}</div>              
			            <div class="dt_pro_price">SGD $ <span id="price_size_tag"></span><span id="price_root" style="font-weight:700;">{if $detail_item.Price}{$detail_item.Price}{else}{$detail_item.OldPrice|number_format:0:".":"."}{/if} </span>   <del>{if $detail_item.Price}$ {$detail_item.OldPrice|number_format:0:".":"."}{/if}</del></div>
			            <div class="dt_sec_size">
			            	<div class="col-md-4 col-sm-12 col-xs-12">
				            	<div class="center" style="font-size: 16px; margin-bottom: 10px; margin-top: 15px">Size :</div> 
				            	<select id="size_all" style="color:#704b3a;" class="form-control">
				            		<option value="">Choose size</option>
				        		{foreach from=$pro_size item=item name=item}
				                    <option class="sl_text" id="size{$item.id}" item_id="{$item.id}" item_number="{$item.number}" item_price_size="{$item.price_size}" item_Content="{$item.Content}" value="{$item.id}">{$item.size}</option>
				                {/foreach}
				                </select>
				                    <div id="item_stock" class="item_stock"></div>
				                    <input type="hidden" id="curr_size_numb" value="" />
				            </div>
				            {if $arr_color}
				            <div class="col-md-4 col-sm-12 col-xs-12">
				            	<div class="center" style="font-size: 16px; margin-bottom: 10px; margin-top: 15px">Color :</div> 
				            	<select id="color_all" style="color:#704b3a;" class="form-control">
				        		{foreach from=$arr_color item=item name=item}
				                    <option id="color{$item.id}" value="{$item.id}">{$item.Name}</option>
				                {/foreach}
				                </select>
				            </div>
				            {/if}
				            <div class="col-md-4 col-sm-12 col-xs-12">
				            	<div class="center" style="font-size: 16px; margin-bottom: 10px; margin-top: 15px">Quantity :</div> 
				            	<div class="add_quantity">
			                        <div class="number">
			                            <input type="text" class="form-control" id="qty" name="qty" value="1" style="" onblur="priceForYou(this.value);" min="0"/>
			                            <input type="hidden" id="color" name="color" value="0" />
			                            <input type="hidden" id="size" name="size" value="0" />
			                        </div>
			                        <div class="click_arrow">
			                            <div class="add_qty" onclick="add_qty();"></div>
			                            <div class="sub_qty" onclick="sub_qty();"></div>
			                        </div>
			                    </div>
				            </div>
			            </div>
			            <div id="click_showhide" style="display:none;">
			               <input type="hidden" id="curr_size_numb" value="" />
			               {php}
			                    loadModule("regismail","home1");
			               {/php}
			            </div>
			            <div id="addcart_showhide" class="col-md-12 col-sm-12">
			                <!-- <div class="dt_pro_code" style="margin-top:10px; width:130px;">
			                    <span style="float:left;  color:#7c7c7c; font-weight:400; margin:-5px 0 5px 0;">{#quantity#}</span>
			                    <div class="add_quantity">
			                        <div class="number">
			                            <input type="text" class="form-control" id="qty" name="qty" value="1" style="" onblur="priceForYou(this.value);" />
			                            <input type="hidden" id="color" name="color" value="0" />
			                            <input type="hidden" id="size" name="size" value="0" />
			                        </div>
			                        <div class="click_arrow">
			                            <div class="add_qty" onclick="add_qty();"></div>
			                            <div class="sub_qty" onclick="sub_qty();"></div>
			                        </div>
			                    </div>
			                </div> -->
			                <button type="button" class="btn btn-primary dt_add_cart" onclick="add_cart('{$detail_item.id}');">{#add_cart#}</button>
			                <!-- <div class="dt_add_cart" onclick="add_cart('{$detail_item.id}');">
			                      <span>{#add_cart#}</span>
			                </div> -->
			            </div>
			        </div>
		        </div>
		        <!--END RIGHT-->
		        <div class="col-md-12 col-sm-12 col-xs-12">
		        	<div style="float:left; margin:60px 0 0 18px; width: 100%">
				        <div class="box_center_content1 right-detail-content" style="padding-left:15px;clear:both;position:relative; width:100%; border:1px solid #ccc;  float:left; ">
				            <div class="tab_detail_product" id="tab_detail_product" >
					            <div class="detail_product_active col-xs-12 col-sm-4 col-md-4" id="chi_tiet" onClick='detail_product_ajax(this.id);'>
					                <div class="detail_product_center">{#product_detail#}</div>
					            </div>  
					            <div class="detail_product_inactive col-xs-12 col-sm-4 col-md-4" id="comment" onClick='detail_product_ajax(this.id);'>
					                <div class="detail_product_center">Measurements</div>
					            </div>
					            <div class="detail_product_inactive col-xs-12 col-sm-4 col-md-4" id="care" onClick='detail_product_ajax(this.id);'>
					                <div class="detail_product_center">Product care</div>
					            </div>
				            </div>        
				            <div id="ajax_detail_product" class="ajax_detail_product_mobile" style="padding:10px 10px 10px 10px; line-height:18px;">
				                {$detail_item.Content}                    
				            </div>
				        </div>
			        </div>
			        {if $detail_item.Keyword}
			        <div class="tag_land"> 
			            <div class="tag_land_title">Tag:</div>  
			            <div class="tag_land_content">
			            {foreach from=$arr_keyword item=item}
			                <a href="{$smarty.const.SITE_URL}product/tag/{$item|remove_marks}.html" title="{$item}">{$item}</a>;&nbsp;
			            {/foreach}
			            </div>
			        </div>
			        {/if}
		        </div>
		    </div>
		</div>
	</div>
</div>
