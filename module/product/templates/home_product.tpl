<div class="box_center">
	<div class="box_center_title"><div class="title1">{#product_category#}</div></div>
	<div class="box_center_content">
        {foreach from=$cat item=item name=item}
            <div class="home_item" style=" {if $smarty.foreach.item.iteration%3==0}margin-right:0;{/if}">
            	<div class="home_img">
                   <a href="{$smarty.const.SITE_URL}product/c-{$item.id}/{$item.Name|remove_marks}.html" title=""><img src="{$smarty.const.SITE_URL}{$item.Photo}" width="228" height="161" style=" border-radius:6px;  display:block;"  alt="{$item.Name}" /></a>			</div>
                	<a class="home_name1" href="{$smarty.const.SITE_URL}product/c-{$item.id}/{$item.Name|remove_marks}.html" title="">{$item.Name|truncate:28}</a>
            </div>
        {foreachelse}
            <div style="font-size:11px; text-align:center;">{#updating#}</div>
        {/foreach}
    </div>
</div>
