{literal}
<script language="javascript" type="text/javascript">
	function gotoPagePrice(page)
	{		
		arr_url = page.split('#');
		var page = arr_url[1];
		//var cat_id = $("#hdn_cat").val();	
		$.ajax({
			   type:"GET",
			   url:"/?mod=product&task=sort_by&ajax&page="+page+"&id={/literal}{$smarty.get.id}{literal}",	
			   /*url:"/?mod=product&task=sort_by&ajax&page="+page+"&cat_id="+cat_id&id={/literal}{$smarty.get.id}{literal}",*/	
			   success: function(response_text){			   	
				   $("#sort-product").html(response_text);
			   }
		   });
	}
	$(document).ready(function(){
		$("#sort-product span.span_a_class a").click(function () {
			var page = $(this).attr("href");
			gotoPagePrice(page);  
		});
	});
</script>
{/literal}
<div id="sort-product">
{config_load file=$smarty.session.lang_file}

{literal}
<script type="text/javascript">
$(document).ready(function(){
    $("#sort_price").change(function(){
        var sort_price = $("#sort_price").val();
		var cat_id = $("#hdn_cat").val();		
		$.ajax({
			type: "GET",
			url: "/index.php?mod=product&task=sort_by&ajax",
			data: "&sort_price="+sort_price+"&cat_id="+cat_id,
			success: function(response_text){
			$("#sort_pro").html(response_text);
			}
		});
    });    
})
</script>
{/literal}

{literal}
<script> 
	function pro_no_id_change(no_id)
		{	
			var cat_id = $("#hdn_cat").val();
			$("#changeno_id").children("div").each(function(i){
				$(this).removeClass("active");
				$(this).addClass("item");
			});
			$("#"+no_id).removeClass("item").addClass("active");
			$.ajax({
				type: "GET",
				url: "/index.php?mod=product&task=sort_by&ajax",
				data: "&no_id="+no_id+"&cat_id="+cat_id,
				success: function(response_text){
					$("#sort_pro").html(response_text);
				}
			});
		}
</script>
{/literal}


<input type="hidden" value="{$cat_id}" name="hdn_cat" id="hdn_cat" />
<div id="sort_pro">
<div class="box_center">		
	<div class="product_center_title">   
    	<div class="ct_le">
            {php}
                loadModule("product","getLink");
            {/php}  
		</div>
        <div class="ct_ri">          
            {if $num_rows > $limit}
                {$num_rows|page:$limit:$smarty.get.page:$paging_path}
            {/if} 
		</div>
    </div>
    
    
    <div class="box_product_title">
    	<div class="pro_title_left">       
    	<select id="sort_price" name="sort_price" style="color:#704b3a;">
            <option value="0" selected="selected">{#Sort_by#}</option>
            <option value="1" {if $smarty.get.sort_price==1} selected="selected"{/if}>{#sort_by_price_asc#}</option>
            <option value="2" {if $smarty.get.sort_price==2} selected="selected"{/if}>{#sort_by_price_desc#}</option>
         </select> 
		</div>
        <div class="pro_title_cen">
        	<!-- AddThis Button BEGIN -->
            <a class="addthis_button" href="http://www.addthis.com/bookmark.php?v=300&amp;pubid=ra-4f83ed2107499a13"><img src="http://s7.addthis.com/static/btn/v2/lg-share-en.gif" width="125" height="16" alt="Bookmark and Share" style="border:0"/></a>
            {literal}
            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f83ed2107499a13"></script>
            {/literal}
            <!-- AddThis Button END -->
        </div>
        <div class="pro_title_right">
        	{if $count > 10}
        	<div class="no_t1">{#view#}</div>
        	<div id="changeno_id">
                <div {if $no_id==10} class="active" {else} class="item" {/if} id="10" onClick="pro_no_id_change(this.id)">10</div>
                {if $count > 19}
                	<div {if $no_id==19} class="active" {else} class="item" {/if} id="20" onClick="pro_no_id_change(this.id)">19</div>
                {/if}
                {if $count > 30}
                	<div {if $no_id==30} class="active" {else} class="item" {/if} id="30" onClick="pro_no_id_change(this.id)">30</div>
				{/if}
            </div>
            <div class="no_t2">{#PRODUCT#}</div>
            {/if}
        </div>
        
    </div>
    
    
    
    
		<div class="box_center_content"> 
        <div class="div_general">
			{foreach from=$listItem item=item name=item}
            {if $smarty.foreach.item.index%4==0}<div class="div_general">{/if}
            <div class="home_item">
            		{if $item.Sale_off}<div class="sale_off">{$item.Sale_off}%</div>{/if}
                   <a href="{$smarty.const.SITE_URL}product/{$item.id}/{$item.Name|remove_marks}.html" title=""><img src="{$smarty.const.SITE_URL}{$item.Photo}" width="189" height="282" style="border:1px solid #e3e3e3; margin:1px 15px 5px 1px;"  alt="{$item.Name}" /></a>
                	<a class="home_name" href="{$smarty.const.SITE_URL}product/{$item.id}/{$item.Name|remove_marks}.html" title="">{$item.Name}</a>
                <div class="home_price"> 
                     <a class="home_detail" href="{$smarty.const.SITE_URL}cart/{$item.id}/{$item.Name|remove_marks}.html">{#add_cart#}</a>
                	<span style="line-height:30px;">{$item.price|number_format:0:".":"."} d</span> 
                </div>
            </div>
            {if $smarty.foreach.item.iteration%4==0 || ($smarty.foreach.item.total%4!=0 && $smarty.foreach.item.last)}</div>{/if}                 
            {foreachelse}
                <div style="font-size:11px; text-align:center;">{#updating#}</div>
            {/foreach}
          
            {if $num_rows > $limit}
            <div class="listPage">
                {$num_rows|page:$limit:$smarty.get.page:$paging_path}
            </div>
            {/if} 
		</div>
	</div>
</div></div>

</div>

