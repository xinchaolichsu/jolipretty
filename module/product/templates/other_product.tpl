{literal}
<script language="javascript" type="text/javascript">
	function gotoPageProduct(page)
	{		
		arr_url = page.split('#');
		var page = arr_url[1];
		$.ajax({
			   type:"GET",
			   url:"/?mod=product&task=other_product&ajax&page="+page+"&id={/literal}{$smarty.get.id}{literal}",			   
			   success: function(response_text){			   	
				   $("#other-product").html(response_text);
			   }
		   });
	}
	$(document).ready(function(){
		$("#other-product span.span_a_class a").click(function () {
			var page = $(this).attr("href");
			gotoPageProduct(page);  
		});
	});
</script>
{/literal}
<div id="other-product">
{config_load file=$smarty.session.lang_file}
<div class="box_center" style="margin-top:15px; width:780px;">
	<div class="box_center_title" style="text-indent:0; width:772px; margin-bottom:20px; background:#edf5fb; height:37px; line-height:37px; border:1px solid #c0c8d0; text-transform:uppercase;">{#product_other#}</div>
		<div style="width:780px; float:left;"> 
			{foreach from=$listItem item=item name=item}
            <div class="home_item" {if $smarty.foreach.item.iteration%3==0} style="margin-right:0;"{/if}>
            	<div class="home_img1">
                  <a href="{$smarty.const.SITE_URL}product/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}"><img src="{$smarty.const.SITE_URL}{$item.Photo}" width="90" height="105"  alt="{$item.Name}" /></a>		  
                  </div>
                  <a class="home_name1" href="{$smarty.const.SITE_URL}product/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{$item.Name|truncate:40}</a>
                  <div class="home_summarise">{$item.Summarise|truncate:120}</div>
                  <div class="home_price">{#price#}:<span> {$item.OldPrice|number_format:0:".":"."} d<span></div>  
             </div>
            {foreachelse}
                <div style="font-size:11px; text-align:center;">{#updating#}</div>
            {/foreach}
	</div>
</div>
</div>