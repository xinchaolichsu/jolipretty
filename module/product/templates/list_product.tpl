<div id="sort-product">

{literal}

<style type="text/css">

  .menu-left a{

    color: #000;

  }

</style>

<script language="javascript" type="text/javascript">

	function gotoPagePrice(page)

	{		

		arr_url = page.split('#');

		var page = arr_url[1];

        var sort_price = $("#sort_price").val();

		var cat_id = $("#hdn_cat").val();

		var store_id = $("#hdn_store").val();

		var new_id = $("#hdn_new").val();	

		$.ajax({

			   type:"GET",

			   url:"/?mod=product&task=sort_by&ajax&sort_price="+sort_price+"&cat_id="+cat_id+"&page="+page+"&store="+store_id+"&tinmoi="+new_id+"&id={/literal}{$smarty.get.id}{literal}",	

			   success: function(response_text){			   	

				   $("#sort-product").html(response_text);

			   }

		   });

	}

	$(document).ready(function(){

		$("#sort-product span.span_a_class a").click(function () {

			var page = $(this).attr("href");

			gotoPagePrice(page);  

		});

	});

</script>

{/literal}

{config_load file=$smarty.session.lang_file}

{literal}

<script type="text/javascript">

$(document).ready(function(){

    $("#sort_price").change(function(){

        var sort_price = $("#sort_price").val();

		var cat_id = $("#hdn_cat").val();	

		$.ajax({

			type: "GET",

			url: "/index.php?mod=product&task=sort_by&ajax",

			data: "&sort_price="+sort_price+"&cat_id="+cat_id,

			success: function(response_text){

			$("#sort_pro").html(response_text);

			}

		});

    });  

	

    $("#sort_size").change(function(){

        var sort_size = $("#sort_size").val();

		var cat_id = $("#hdn_cat").val();	

		$.ajax({

			type: "GET",

			url: "/index.php?mod=product&task=sort_by&ajax",

			data: "&sort_size="+sort_size+"&cat_id="+cat_id,

			success: function(response_text){

			$("#sort_pro").html(response_text);

			}

		});

    }); 



    $(".sort_size_left").click(function(){

        var sort_size = $(this).attr('sizeid');

    var cat_id = $("#hdn_cat").val();

    $.ajax({

      type: "GET",

      url: "/index.php?mod=product&task=sort_by&ajax",

      data: "&sort_size="+sort_size+"&cat_id="+cat_id,

      success: function(response_text){

      $("#sort_pro").html(response_text);

      }

    });

    }); 

	

	$("#show_all").click(function(){

		var store_id = $("#hdn_store").val();

		var new_id = $("#hdn_new").val();	

		$.ajax({

			type: "GET",

			url: "/index.php?mod=product&task=show_all&ajax",

			data: "store="+store_id+"&tinmoi="+new_id,

			success: function(response_text){

			$("#sort_pro").html(response_text);

			}

		});

    }); 	

	  

  $("#sort_color").change(function(){

    var color_id = $("#sort_color").val();

    $.ajax({

      type: "GET",

      url: "/index.php?mod=product&task=filterByColor&ajax",

      data: "color_id="+color_id,

      success: function(response_text){

        if(response_text == '')

        {

          $("#sort_pro").html("Updating ...");  

        } else {

          $("#sort_pro").html(response_text);

        }

      }

    });

    });   



  $(".color_left").click(function(){

    var color_id = $(this).attr('colorid');

    $.ajax({

      type: "GET",

      url: "/index.php?mod=product&task=filterByColor&ajax",

      data: "color_id="+color_id,

      success: function(response_text){

        if(response_text == '')

        {

          $("#sort_pro").html("Updating ...");  

        } else {

          $("#sort_pro").html(response_text);

        }

      }

    });

    }); 



  $("#filterbyprice").click(function(){

    var minprice = $("#minprice").val();

    var maxprice = $("#maxprice").val();

    $.ajax({

      type: "GET",

      url: "/index.php?mod=product&task=filterByPrice&ajax",

      data: "&minprice="+minprice+"&maxprice="+maxprice,

      success: function(response_text){

        $("#sort_pro").html(response_text);

        $("#minprice").val(minprice);

        $("#maxprice").val(maxprice);

      }

    });

  });



  $('#pro_available').change(function(){ 

    if($(this)[0].checked) {

      $.ajax({

        type: "POST",

        url: "/index.php?mod=product&task=available&ajax",

        data: "",

        success: function(response_text){

          if(response_text == '')

          {

            $("#sort_pro").html("Updating ...");  

          } else {

            $("#sort_pro").html(response_text);

          }

          $('#pro_available').attr('checked', true);

        }

      });

    } 

    else {

      $.ajax({

        type: "POST",

        url: "/index.php?mod=product&task=all&ajax",

        data: "",

        success: function(response_text){

          if(response_text == '')

          {

            $("#sort_pro").html("Updating ...");  

          } else {

            $("#sort_pro").html(response_text);

          }

          $('#pro_available').removeAttr('checked');

        }

      });

    }

  });

  // $('#btn_clearall').click(function(){

  //   $.ajax({

  //       type: "POST",

  //       url: "/index.php?mod=product&task=all&ajax",

  //       data: "",

  //       success: function(response_text){

  //         if(response_text == '')

  //         {

  //           $("#sort_pro").html("Updating ...");  

  //         } else {

  //           $("#sort_pro").html(response_text);

  //         }

  //         $('#pro_available').removeAttr('checked');

  //       }

  //     });

  // });



})

</script>

{/literal}

<div id="sort_pro">

<input type="hidden" value="{$cat_id}" name="hdn_cat" id="hdn_cat" />

<input type="hidden" value="{if $smarty.get.store}{$smarty.get.store}{/if}" id="hdn_store" />

<input type="hidden" value="{if $smarty.get.tinmoi}1{/if}" id="hdn_new" />

<div class="box_center1 listproduct" style="width:93.7%; margin-bottom: 20px;">		

    {if $cat_photo}<img src="{$smarty.const.SITE_URL}{$cat_photo}" width="1000" height="280" style="margin:0 0 8px 0;" alt="image" />{/if}

        <!-- <div class="pro_title_left" style="float:right; margin-right:10px;">

        	<select id="sort_size" name="sort_size" style="color:#704b3a;" class="form-control">

                <option value="0" selected="selected">Size</option>

            {foreach from=$cat_size item=item name=item}

                <option value="{$item.id}" {if $smarty.get.sort_size==$item.id} selected="selected"{/if}>{$item.Name}</option>

            {/foreach}

             </select> 

    		</div> -->

        <div class="pro_title_left" style="float:right; margin-right:10px;">

          <!-- ============================ -->

        <!-- {if $colors}

        <select id="sort_color" name="sort_color" style="color:#704b3a;" class="form-control">

            <option value="0">Color:</option>

            {foreach from=$colors item=item name=item}

            <option class="opt-select" value="{$item.id}" {if $smarty.get.color_id==$item.id} selected="selected"{/if}>{$item.Name}</option>

            {/foreach}

         </select> 

         {/if} -->

         <!-- ================= -->

        </div>

        	<div class="pro_title_left" style="float:right; margin-right:20px;">       

    	<select id="sort_price" name="sort_price" style="color:#704b3a;" class="form-control">

            <option value="0" selected="selected">{#Sort_by#}</option>

            <option value="1" {if $smarty.get.sort_price==1} selected="selected"{/if}>{#sort_by_price_asc#}</option>

            <option value="2" {if $smarty.get.sort_price==2} selected="selected"{/if}>{#sort_by_price_desc#}</option>

         </select> 

		</div>

		<div class="pro_title_left" style="float:right; margin-right:20px; /*color:#704b3a;*/ cursor:pointer; margin-top: 13px; padding: 0px 4px; background-color: #666666; color: white;" id="show_all" onclick="show_all()" >Show all</div>

    </div>

    <div class="menu-left">

        <div class="col-md-2 col-sm-2" style="padding-right: 0px">

        	<div class="box_sidebar1" style="padding-left: 20px; padding-right: 0px; text-align: left;">

        		{if $menus}

          		{foreach from=$menus item=item name=item}

                {if $item.remove != true}

                  <a href="{$smarty.const.SITE_URL}product/c-{$item.id}/{$item.Name|remove_marks}.html"><div style="font-size: 14px; font-weight: normal; margin-bottom: 10px;">{$item.Name}</div></a>

                  <hr style="margin: 0px  !important;">

                  {if $item.child != ''}

                    {foreach from=$item.child item=sub_item name=sub_item}

                      <div style="font-size: 13px; font-weight: normal; margin-bottom: 10px;"><a href="{$smarty.const.SITE_URL}product/c-{$sub_item.id}/{$sub_item.Name | remove_marks}.html">+ {$sub_item.Name}</a></div>

                    {/foreach}

                  {/if}

                  <br>

                {/if}

          		{/foreach}

        		{/if}

            <br>

            <div class="availability">

              <input type="checkbox" value="" id="pro_available"> Only available product

            </div>

            <br>

            <a href="{$smarty.const.SITE_URL}product/"><button class="btn btn-primary center" id="btn_clearall">CLEAR ALL</button></a>

        	</div>

        </div>

        <div class="col-md-10 col-sm-10">

    		<div class="box_center_content1" style=""> 

    			{foreach from=$listItem item=item name=item}

                <div class=" col-md-3 col-sm-4 col-xs-12 one-product" {if $smarty.foreach.item.iteration%4==0} style="margin-right:0; margin-bottom: 15px; height: 380px"{/if}>

                	<div class="home_img" onmouseover="ptlist_show({$item.id})" onmouseout="ptlist_show({$item.id})">

                      <a href="{$smarty.const.SITE_URL}product/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">

                        <img src="{$smarty.const.SITE_URL}{$item.Photo}" class="ptlist ptmain_{$item.id}" width="213" height="100%"  alt="{$item.Name}" />

                        <img src="{$smarty.const.SITE_URL}{$item.photo_hover}" class="ptlist pthover_{$item.id} ptlist_hide" width="213" height="100%"  alt="{$item.Name}" />

                      </a>	

                      {if $item.SoldOut}<div class="sold_out">{$item.SoldOut}</div>{/if}	  

                      </div>

                      <div style="">

                        <a class="home_name center" href="{$smarty.const.SITE_URL}product/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{$item.Name|truncate:40}</a>

                        <div class="home_price center" style="margin-left: -31px; width: 60%;">

                          SGD <span> {if $item.Price}{$item.Price}{else}{$item.OldPrice|number_format:0:".":"."}{/if} </span>

                          <del> {if $item.Price} SGD {$item.OldPrice}{/if}</del>

                        </div>

                        {if $item.Color != ''}

                        <div class="div-more-color" style="margin-left: -31px; width: 87%;">

                          <a href="{$smarty.const.SITE_URL}product/{$item.id}/{$item.Name|remove_marks}.html" class="more-color center" style="color: #696464;">More colors</a>

                        </div>

                        {/if}

                      </div>

                 </div>

                {foreachelse}

                    <div style="font-size:11px; text-align:center;">{#updating#}</div>

                {/foreach}

              

                {if $num_rows2 > $limit}

                <div class="listPage">

                    {$num_rows2|page:$limit:$smarty.get.page:$paging_path}

                </div>

                {/if} 

    	  </div>

    </div>

  </div>

  {literal}

  <script type="text/javascript">

    function ptlist_show(id)

    {
      var windowWidth = $(window).width();
      if(windowWidth > 768) {
        $(".ptmain_"+id).toggle();
        $(".pthover_"+id).toggle();
      }

    }

  </script>

  <style type="text/css">

    .ptlist_hide{display: none;}

    .colorprovz{color:#F99186}

  </style>

  {/literal}

</div>

</div>

</div>







