<?php
class product_regismailBackEnd extends Bsg_Module_Base{
	var $smarty;
	var $db;
	var $url="";
    var $datagrid;
	var $id;
	var $imgPath;
	var $imgPathShort;
	var $table='';
	var $arrAction;
	public function __construct($oSmarty, $oDb, $oDatagrid)
	{	
		$this -> smarty = $oSmarty;
		$this -> db = $oDb;		
        $this -> datagrid = $oDatagrid;
		$this -> id=$_REQUEST[id];		
		$this -> table	="tbl_product_regismail";	 		
		parent::__construct($oDb);		
		$this->bsgDb->setTable($this->table);
	}
	
	function run($task)
	{	
		switch( $task ){
			case 'export_error':
				$this -> export_error();
				break;
			case 'add':
				$this -> addItem();
				break;
			case 'edit':
				$this -> editItem();
				break;
			case 'delete':
				$this -> deleteItem();
				break;
			case 'delete_all':
				$this -> deleteItems();
				break;
			case 'change_status':				
				$this -> changeStatus($_GET['id'], $_GET['status']);
				break;
			case 'public_all':						
				$this -> changeStatusMultiple( 1 );
				break;
			case 'unpublic_all':						
				$this -> changeStatusMultiple( 0 );
				break;
			case 'save_order':
				$this -> saveOrder();
				break;
			default:					
				$this -> listItem( $_GET['msg'] );		
				break;
		}
	}
	
	function getPageInfo()
	{
		return true;
	}
	
	function export_error()
	{
		$this -> getPath("Xuất danh sách subscirbed ");	
		$this -> exportErr();
	}
	
	function exportErr()
	{
        global $oDb,$oSmarty;
		require_once 'lib/PHPExcel/Classes/PHPExcel.php';
		require_once 'lib/PHPExcel/Classes/PHPExcel/Writer/Excel2007.php';
		$objPHPExcel = new PHPExcel(); // Khai bao đối tượng export
		$objPHPExcel->setActiveSheetIndex(0); // Bắt đầu từ dòng 0
		$objPHPExcel->getDefaultStyle()->getFont()->setName('Times New Roman')->setSize(12); // Xét font cho file excel
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(6);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
		// set Height Row
		$objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);
		// đặt chữ ỏ giữa dòng
		$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$objPHPExcel->getActiveSheet()->getStyle("A2:D2")->getFont()->setBold(true);  // đậm nhạt
		$objPHPExcel->getActiveSheet()->SetCellValue('A2', "STT");  // add giá trị
		$objPHPExcel->getActiveSheet()->SetCellValue('B2', "Email");  // add giá trị
		$objPHPExcel->getActiveSheet()->SetCellValue('C2', "Sản phẩm");  // add giá trị
		$objPHPExcel->getActiveSheet()->SetCellValue('D2', "Size");  // add giá trị
		
		//select
		$aItems	 = $_GET['arr_check'];
		if(is_array( $aItems) && count( $aItems) > 0)
		{
			$sItems = implode( ',', $aItems );
				$sql = "SELECT Email, (Select Name from tbl_product_item where id=tbl_product_regismail.pro_id) as product, (Select Name from tbl_size where id=tbl_product_regismail.size_id) as size from tbl_product_regismail where Status=1 AND id IN ({$sItems}) order by CreateDate DESC ";
				$all_error = $oDb->getAll($sql);
				foreach($all_error as $key=>$value)
				{			
					$row = $key + 3;
					$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, $key+1);  // add giá trị	
					$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $value['Email']);  // add giá trị
					$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $value['product']);  // add giá trị
					$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, $value['size']);  // add giá trị
					
					$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
					$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
					$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->getAlignment()->setWrapText(true);
				}
		}
		$str = date("d.m.Y");
		//Set tiêu đề cho 1 Sheet trong excel
		$objPHPExcel->getActiveSheet()->setTitle("Danh sách subscirbed sản phẩm");
		//Save vào đường dẫn bằng câu lệnh save
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save("upload/excel/danhsach_subscirbed_san_pham".$str.".xls");
		echo '<script language="javascript">
					window.location ="'.SITE_URL.'upload/excel/danhsach_subscirbed_san_pham'.$str.'.xls";</script>';	
	}

	function addItem()
	{
		$this -> getPath("Email nhận bản tin > Thêm mới email");		
		$this -> buildForm();
	}
	
	function editItem()
	{
		$id = $_GET['id'];
		$this -> getPath("Email nhận bản tin > Chỉnh sửa email");	
		$row = $this -> bsgDb -> getRow( $id );
		$this -> buildForm( $row );
	}

	function deleteItem()
	{
		global  $oDb;
		$id = $_GET["id"];
		$this -> bsgDb -> deleteWithPk( $id );
		$msg = "Xóa email thành công!";
		$this -> listItem( $msg );
	}
	
	function deleteItems()
	{
		$aItems	 = $_GET['arr_check'];
		if(is_array( $aItems) && count( $aItems) > 0){
			$sItems = implode( ',', $aItems );
			$this -> bsgDb -> deleteWithPk( $sItems );
		}
		$msg = "Xóa (các) email thành công!";
		$this -> listItem( $msg );
	}
	
	function changeStatusMultiple( $status = 0 )
	{
		$aItems	 = $_GET['arr_check'];
		if(is_array( $aItems) && count( $aItems) > 0){
			$sItems = implode( ',', $aItems );
			$this -> bsgDb -> updateWithPk( $sItems, array("Status" => $status) );
		}
		$msg = "Sửa trạng thái email thành công!";
		$this -> listItem( $msg );
	}
	
	function buildForm( $data=array() , $msg = ''){
		global $oSmarty, $oDb;
		$form = new HTML_QuickForm('frmAdmin','post',$_COOKIE['re_dir']."&task=".$_GET['task'], '', "style='padding:10px 15px 0 20px;'");
		
		$form -> setDefaults($data);
		 echo "<script type=\"text/javascript\" src=\"/view/js/lang.js\"></script>";
        if (MULTI_LANGUAGE)
        {
            $lang = parent::loadLang();
            $form -> addElement('select', 'LangID', 'Ngôn ngữ', $lang, array('id'=>'product_form_lang'));		
            if ($_GET['task']=='edit')
                $selected_langid = $data['LangID'];
            else
                $selected_langid = $oDb->getOne("SELECT id FROM tbl_sys_lang ORDER BY id ASC LIMIT 1");	
        }
        else
            $selected_langid = 0;
		$product=$oDb->getOne("Select Name from tbl_product_item where id='".$data['pro_id']."'");
		$form -> addElement('static', '','Sản phẩm','<div style=" border:1px solid #ccc; height:25px; line-height:25px;">'.$product.'</div>');	
		$size=$oDb->getOne("Select Name from tbl_size where id='".$data['size_id']."'");
		$form -> addElement('static', '','Size','<div style=" border:1px solid #ccc; height:25px; line-height:25px;">'.$size.'</div>');	
		$form -> addElement('text', 'Email', 'Email', array('size' => 50, 'maxlength' => 255));
        $date_time = parent::date_time('CreateDate',($data['CreateDate']) ? ($data['CreateDate']) : date("Y-m-d h:i"));			
		$form -> addElement('static',NULL,'Ngày gửi',$date_time);
		
		$form -> addElement('checkbox', 'Status', 'Kích hoạt');
		
		$btn_group[] = $form -> createElement('submit',null,'Hoàn tất',array("style"=> "border:1px solid gray; padding:0 5px 0 5px;"));		
        $btn_group[] = $form -> createElement('button',null,'Quay lại',array('onclick'=>'window.location.href = \''.$_COOKIE['re_dir'].'\'', "style"=> "border:1px solid gray;"));      
        $form -> addGroup($btn_group);
      
		$form->addElement('hidden', 'id', $data['id']);		
		
		if( $form -> validate())
		{
			
			$aData  = array(
				"Email" 	=> $_POST['Email'],
				"CreateDate" => $_POST['CreateDate'],
				"Status" 	=> $_POST['Status'],
			);
			if( !$_POST['id'] ){
				
				 $id = $this -> bsgDb -> insert($aData);				 
				 $msg = "Thêm email thành công! ";
			}else {
				$id = $_POST['id'];				
				$this -> bsgDb -> updateWithPk($id, $aData);
				$msg = "Chỉnh sửa email thành công ";
			}
			
			$this -> redirect($_COOKIE['re_dir']. "&msg={$msg}");
		}
		
		$form->display();
	}
	
	function getCategory($lang_id=0){
		$table = 'tbl_news_category';
        $cond = '';
        if ($lang_id!=0)
            $cond .= " LangID = {$lang_id} ";
		$result = parent::multiLevel( $table, "id", "ParentID", "*", $cond, "Ordering ASC");
		
		$category = array();
		foreach ($result as $value => $key)
		{
			if( $key['level'] > 0){				
				$name = $this -> getPrefix( $key['level']).$key['Name'];
			}
			else 
				$name = $key['Name'];
			$category[$key['id']] = $name;
		}
		
		return $category;
	}
	
	function getPlace($lang_id=0)
	{
		global $oDb, $oDatagrid;
		$sql = "SELECT id,Name FROM tbl_regismail_category Order by Ordering";
		$result = $oDb->getAssoc($sql);
		return $result;
	}
	
	function getDoctor($lang_id=0)
	{
		global $oDb, $oDatagrid;
		$sql = "SELECT id,Name FROM tbl_about_item where CatID=33 Order by Ordering";
		$result = $oDb->getAssoc($sql);
		return $result;
	}

	function deleteImage($id, $field, $path){
		if($id == '')
			return;
		$imgpath = $path.$this->db->getOne("SELECT $field FROM ".$this->table." WHERE id = $id");
		if(is_file($imgpath))			
			@unlink($imgpath);
	}
	
	function changeStatus( $itemId , $status ){
		$aData = array( 'Status' => $status );
		$this -> bsgDb -> updateWithPk( $itemId, $aData );
		$msg = "Thay đổi trạng thái thành công";
		$this->listItem($msg);
	}
	
	function listItem( $sMsg= '' )
	{		
		global $oDb;
		global $oDatagrid;				
		
		$root_path = "Email nhận bản tin > Danh sách email";						
		$submit_url= "/index.php?".$_SERVER['QUERY_STRING'];
		
		$table = $this -> table;
		
		$arr_filter= array(
			array(
				'field' => 'Status',
				'display' => 'Trạng thái',				
				'name' => 'filter_show',
				'selected' => $_REQUEST['filter_show'],
				'options' => array('Vô hiệu','Kích hoạt'),
				'filterable' => true
			)
			
		); 
		
		$arr_cols= array(		
			
			array(
				"field" => "Email",
				"display" => "Email",
				"align"	=> 'left',				
				"datatype" => "text",
				"sortable" => true
			),
			array(
				"field" => "Status",
				"display" => "Trạng thái",				
				"datatype" => "publish",
				"sortable" => true
			),		
			array(
				"field" => "CreateDate",
				"display" => "Ngày gửi",
				"align"	=> 'left',				
				"datatype" => "date",
				"sortable" => true
			)	
		);		
		
		$arr_check = array(
			array(
				"task" => "delete_all",
				"confirm"	=> "Xác nhận xóa?",
				"display" => "Xóa"
			),
			array(
				"task" => "public_all",
				"confirm"	=> "Xác nhận thay đổi trạng thái?",
				"display" => "Kích hoạt"
			),
			array(
				"task" => "unpublic_all",
				"confirm"	=> "Xác nhận thay đổi trạng thái?",
				"display" => "Vô hiệu"
			)			
		);
		if( $sMsg )
			$oDatagrid -> setMessage( $sMsg );
		$oDatagrid->display_datagrid($table, $arr_cols, $arr_filter, $submit_url, $this -> getAct() ,120, $root_path, false ,$arr_check);		
		
	}		
	
}

?>