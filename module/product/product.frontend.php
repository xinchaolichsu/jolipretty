<?php
class product extends VES_FrontEnd 
{	
	var $table;
	var $mod;
    var $url_mod;
	var $id;
	var $LangID;
	var $field;
	var $pre_table;	
	function __construct(){		
		$this->table = 'tbl_product_item';
		$this->pre_table = "tbl_product_category";
		$this->mod = 'product';
        $this->url_mod = "product";
		$this->id = mysql_real_escape_string(($_REQUEST['id']=='')?'0':$_REQUEST['id']);
		$this->field = "*";
		$this->LangID = $_SESSION['lang_id'];
		parent::__construct($this->table);
	}
	function run($task= "")
	{
	    switch($task) 
			{	
				
				case 'getLink':
					$this -> getLink($_GET['id']);
				break;
				case 'product_qty':
					$this -> product_qty();
				break;
				case 'Priceforyou':
					$this -> Priceforyou();
				break;
				case 'home':
					$this -> home();
				break;
				case 'menu':
					$this -> menu();
				break;
				case 'show_img':
					$this -> show_img();
				break;
				case 'main_img':
					$this -> main_img();
				break;
				case 'sort_by':
					$this -> sort_by();
				break;
				case 'show_all':
					$this -> show_all();
				break;
				case 'search':
					$this -> search();
				break;
				/* Hiển thị form tìm kiếm sản phẩm */
				case 'frm_search':
                    $this -> getFrmSearch();
                break;
				case 'productSearch':
					$this -> productSearch();
				break;
				case 'details':
					$this -> details();
				break;
				case 'detail_product_ajax':
					$this -> detail_product_ajax($_GET['id']);
				break;
				case 'other_product':
					$this -> other_product();
				break;
				case 'category':
					$this->listItem($_GET['id']);
				break;
				case 'tinmoi':
					$this->tinmoi();
				break;
				case 'filterByPrice':
					$this->filterByPrice();
					break;
				case 'filterByColor':
					$this->filterByColor();
					break;
				case 'available':
					$this->available();
					break;
				case 'all':
					$this->all();
					break;
				default:
                    /* List toàn bộ (với site không có danh mục)*/
					$this->listItem();
                    
                    /*List theo danh mục (với site có danh mục)*/
                    //$this->listbyCategory();
				break;	
			}
	}
	/**
	 * Page info
	 *
	 * @param $task
	 */
	function getPageinfo($task= "")
	{
	 	global $oSmarty, $oDb;
		switch ($task) {
			case 'category':
				$id = $_GET['id'];
				if ($id)
				{
					$sql = "SELECT * FROM {$this->pre_table} WHERE id=$id";	
					$row = $oDb->getRow($sql);	
				}
				if($row['Title'])
					$title = $row['Title'];
				else
					$title = PAGE_TITLE;
				if($row['Description'])
					$description = $row['Description'];
				else
					$description = PAGE_DESCRIPTION;
				if($row['Keyword'])
					$keyword = $row['Keyword'];
				else
					$keyword = PAGE_KEYWORD;
				//$aPageinfo=array('title'=> $row." | " . parent::get_config_vars("product","Sản-phẩm").' | '. PAGE_TITLE, 'keyword'=>PAGE_KEYWORD.",".$row, 'description'=>PAGE_DESCRIPTION);
				$aPageinfo=array('title'=> $title, 'keyword'=>$keyword, 'description'=>$description);
				$aPath[] = array('link' => '', 'path' => 'product');
				break;
            case 'details':
                $id = $_GET['id'];
                if ($id)
                {
                    $sql = "SELECT * FROM {$this->table} WHERE id=$id";    
                    $row = $oDb->getRow($sql);    
                }
				if($row['Title'])
					$title = $row['Title'];
				else
					$title = PAGE_TITLE;
				if($row['Description'])
					$description = $row['Description'];
				else
					$description = PAGE_DESCRIPTION;
				if($row['KeywordG'])
					$keyword = $row['KeywordG'];
				else
					$keyword = PAGE_KEYWORD;
                //$aPageinfo=array('title'=> $row." | " . parent::get_config_vars("product","Sản-phẩm").' | '. PAGE_TITLE, 'keyword'=>PAGE_KEYWORD.",".$row, 'description'=>PAGE_DESCRIPTION);
				$aPageinfo=array('title'=> $title, 'keyword'=>$keyword, 'description'=>$description);
                $aPath[] = array('link' => '', 'path' => 'product');
                break;
			default:
				if($_GET['store']==1)
				$aPageinfo=array('title'=> parent::get_config_vars("","Back in stock").' | ' . PAGE_TITLE, 'keyword'=>PAGE_KEYWORD.",".$row, 'description'=>PAGE_DESCRIPTION);
				elseif($_GET['tinmoi']==1)
				$aPageinfo=array('title'=> parent::get_config_vars("","What's new").' | ' . PAGE_TITLE, 'keyword'=>PAGE_KEYWORD.",".$row, 'description'=>PAGE_DESCRIPTION);
				else
				$aPageinfo=array('title'=> parent::get_config_vars("PRODUCT","").' | ' . PAGE_TITLE, 'keyword'=>PAGE_KEYWORD.",".$row, 'description'=>PAGE_DESCRIPTION);
				$aPath[] = array('link' => '', 'path' => 'product');
				break;
		}
		
		$oSmarty->assign('aPageinfo', $aPageinfo);
		$oSmarty->assign("aPath", $aPath);
	}
	/**
	 * Display in home
	 *
	 */
	 
	 function home()
		{			
		global $oDb,$oSmarty;
		$where = " Status=1 ";
		if (MULTI_LANGUAGE)
		$where .= " AND LangID={$_SESSION['lang_id']} ";
		$sql = "SELECT * FROM {$this->pre_table} WHERE {$where} AND ParentID=0 AND ShowHome=1 ORDER BY Ordering ASC Limit 15"; 
		$cat = $oDb->getAll($sql);
		$oSmarty->assign("cat",$cat);
		//============ garung ===============
		$sql = "Select count(*) from {$this->table} where Status=1 AND LangID={$_SESSION['lang_id']} ";
		$count = $oDb->getOne($sql);
		$oSmarty->assign("count",$count);

		$sql = "SELECT * FROM {$this->pre_table} WHERE Status=1 "; 
		$listCat = $oDb->getAll($sql);
		$oSmarty->assign("listCat",$listCat);

		$sql_color = "Select * from tbl_product_color";
		$colors = $oDb->getAll($sql_color);
		$oSmarty->assign("colors",$colors);
		//======================================
		$oSmarty->display("home_product.tpl");
		}
	function show_all()
	{			
		global $oDb,$oSmarty;
		$where = " Status=1 ";
		if (MULTI_LANGUAGE)
		$where .= " AND LangID={$_SESSION['lang_id']} ";
		if($_GET['store']==1)
			$where .=" AND ShowHome =1";
		if($_GET['tinmoi']==1)
			$where .=" AND Is_new=1";
		$sql = "SELECT * FROM {$this->table} WHERE {$where} ORDER BY Ordering ASC"; 
		$listItem = $oDb->getAll($sql);
		foreach($listItem as $k=>$v)
		{
			$sum_qua = $oDb->getOne("Select sum(number) from tbl_product_size where  pro_id={$v['id']}");
			$listItem[$k]['sum_qua'] = $sum_qua;			
		}
		//============ garung ===============
		$sql = "Select count(*) from {$this->table} where Status=1 AND LangID={$_SESSION['lang_id']} ";
		$count = $oDb->getOne($sql);
		$oSmarty->assign("count",$count);

		$sql = "SELECT * FROM {$this->pre_table} WHERE Status=1 "; 
		$listCat = $oDb->getAll($sql);
		$oSmarty->assign("listCat",$listCat);

		$sql_color = "Select * from tbl_product_color";
		$colors = $oDb->getAll($sql_color);
		$oSmarty->assign("colors",$colors);
		//======================================
		$sql = "Select count(*) from {$this->table} where {$where} {$order} LIMIT {$limit}";
		$count = $oDb->getOne($sql);
		$oSmarty->assign("count",$count);
		$oSmarty->assign("listItem",$listItem);

		$where = " Status=1 ";
		$sql = "SELECT * FROM {$this->pre_table} WHERE {$where}  ORDER BY Ordering ASC Limit 15"; 
		$listCat = $oDb->getAll($sql);
		$oSmarty->assign("listCat",$listCat);
		//==================================
		$idHasParent = "SELECT ParentID FROM tbl_product_category WHERE Status = 1 AND ParentID <> 0";
		$hasParent = $oDb->getAll($idHasParent);
		$arr_id = array();
		foreach ($hasParent as $key_id => $value_sub) {
			$arr_id[$key_id] = $value_sub['ParentID'];
		}

		$head_sql = "SELECT * FROM tbl_product_category WHERE Status = 1 ORDER BY Ordering DESC";
		$menus = $oDb->getAll($head_sql);
		foreach ($menus as $keymenu => $menu_item) {
			$menu_item['id'] = (int)$menu_item['id'];
			if(in_array($menu_item['id'], $arr_id)) 
			{
				$menus[$keymenu]['child'] = $this->getSubCategory($menu_item['id'], 'tbl_product_category');
			} else {
				$menus[$keymenu]['child'] = '';
			}
			if(in_array($menu_item['ParentID'], $arr_id))
			{
				$menus[$keymenu]['remove'] = true;
			} else {
				$menus[$keymenu]['remove'] = false;
			}
		}
		$oSmarty->assign("menus",$menus);
		//==================================

		$oSmarty->display("list_product.tpl");
	}
	//================== garung =================
	function filterByPrice()
	{
		global $oDb,$oSmarty;
		$where = " Status=1 ";
		if (MULTI_LANGUAGE)
		$where .= " AND LangID={$_SESSION['lang_id']} ";
		// ===== list size =======
		$sql = "Select count(*) from {$this->table} where Status=1 AND LangID={$_SESSION['lang_id']} ";
		$count = $oDb->getOne($sql);
		$oSmarty->assign("count",$count);

		$sql = "SELECT * FROM {$this->pre_table} WHERE Status=1  ORDER BY Ordering ASC Limit 15"; 
		$listCat = $oDb->getAll($sql);
		$oSmarty->assign("listCat",$listCat);
		$sql = "SELECT id,Name FROM tbl_size where Status=1 Order by Ordering ASC ";
		$cat_size= $oDb->getAll($sql);
		$oSmarty->assign('cat_size',$cat_size);

		$sql_color = "Select * from tbl_product_color";
		$colors = $oDb->getAll($sql_color);
		$oSmarty->assign("colors",$colors);
		//=========================
		if(($_GET['minprice'] >= 0) && ($_GET['maxprice'] >= 0)){
			$where .= "AND (Price BETWEEN {$_GET['minprice']} AND {$_GET['maxprice']})";
			$sql = "SELECT * FROM {$this->table} WHERE {$where} "; 
			$listItem = $oDb->getAll($sql);
			$oSmarty->assign("listItem",$listItem);
			$oSmarty->display("list_product.tpl");
		}
		else
		{
			$oSmarty->assign("listItem",$listItem);
			$oSmarty->display("list_product.tpl");
		}
	}

	function filterByColor()
	{
		global $oDb,$oSmarty;
		$where = " Status=1 ";
		if (MULTI_LANGUAGE)
		$where .= " AND LangID={$_SESSION['lang_id']} ";
		// ===== list size =======
		$sql = "Select count(*) from {$this->table} where Status=1 AND LangID={$_SESSION['lang_id']} ";
		$count = $oDb->getOne($sql);
		$oSmarty->assign("count",$count);

		$sql = "SELECT * FROM {$this->pre_table} WHERE Status=1  ORDER BY Ordering ASC Limit 15"; 
		$listCat = $oDb->getAll($sql);
		$oSmarty->assign("listCat",$listCat);
		$sql = "SELECT id,Name FROM tbl_size where Status=1 Order by Ordering ASC ";
		$cat_size= $oDb->getAll($sql);
		$oSmarty->assign('cat_size',$cat_size);

		$sql_color = "Select * from tbl_product_color";
		$colors = $oDb->getAll($sql_color);
		$oSmarty->assign("colors",$colors);
		//==================
		if($_GET['color_id'] == 0)
		{	
			$sql_color_ajax = "SELECT * FROM {$this->table}"; 
		} else {
			$sql_color_ajax = "SELECT * FROM {$this->table} WHERE Color LIKE '%{$_GET['color_id']}%'"; 
		}
		$listItem = $oDb->getAll($sql_color_ajax);
		$oSmarty->assign("listItem",$listItem);

		$oSmarty->display("list_product.tpl");
	}
	//==========================================
	function sale_off()
	{	
		global $oDb, $oSmarty;
		$where = "Status=1";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";		
		$sql = "SELECT * FROM {$this->table} WHERE {$where} AND ShowOff = 1 ORDER BY CreateDate DESC";
		$sale_off = $oDb->getAll($sql);		
		foreach($sale_off as $k=>$v)
		{
			if($v['Sale_off'])
			$price=$v['OldPrice']-($v['Sale_off'] * $v['OldPrice'])/100;
			else 
			$price=$v['OldPrice'];
			$sale_off[$k]['price'] = $price;			
		}
		$oSmarty->assign("sale_off",$sale_off);
		$oSmarty->display('sale_off.tpl');
	}
	
	function sale_off1($cat_id=0)
	{
		global $oDb,$oSmarty;
		$url = $_SERVER['REQUEST_URI'];
		if ($_GET['page'])
			$url = str_replace("/".$_GET['page']."/","",$url);
		$page_link = "$url/i++/";
		$cond = 'ShowOff=1';		
		$oSmarty->assign("cat_id",$cat_id);
		$_SESSION['cat_link'] = $cat_id;
		$this->getListItem($cat_id,$page_link, $cond,$limit=15,$template="list_product.tpl", $order="ORDER BY Sale_off DESC");
	}
	
	function view_more()
	{	
		global $oDb, $oSmarty;
		$where = "Status=1";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";		
		$sql = "SELECT * FROM {$this->table} WHERE {$where} ORDER BY ViewNumber DESC Limit 25";
		$view_more = $oDb->getAll($sql);		
		foreach($view_more as $k=>$v)
		{
			if($v['Sale_off'])
			$price=$v['OldPrice']-($v['Sale_off'] * $v['OldPrice'])/100;
			else 
			$price=$v['OldPrice'];
			$view_more[$k]['price'] = $price;			
		}
		$oSmarty->assign("view_more",$view_more);
		$oSmarty->display('view_more.tpl');
	}
	
	function view_more1($cat_id=0)
	{
		global $oDb,$oSmarty;
		$url = $_SERVER['REQUEST_URI'];
		if ($_GET['page'])
			$url = str_replace("/".$_GET['page']."/","",$url);
		$page_link = "$url/i++/";
		$cond = '';		
		$oSmarty->assign("cat_id",$cat_id);
		$_SESSION['cat_link'] = $cat_id;
		$this->getListItem($cat_id,$page_link, $cond,$limit=15,$template="list_product.tpl", $order="ORDER BY ViewNumber DESC");
	}
	
		function check_view($product_id, $count) 
		{
			global $oDb;
			$view = $_SESSION['view'];
			$flag = false;
			foreach($view as $k=>$v){
				if($v['product_id'] == $product_id){
					$view[$k]['count'] = intval($v['count']+$count);
					$flag = true;
				}
			}
			$_SESSION['view'] = $view;
			return $flag;
		}

	function add_view($product_id, $count=1) {
		global $oDb;
		if( $product_id < 1 ) return;
		
		// Nếu giỏ hàng đã tồn tại
		if( $_SESSION['view'] ){
			// Nếu sản phẩm đã tồn tại thì tăng số luot xem len 1
			$check = $this->check_view($product_id, $count);
			if(!$check){
				$view = array(
							'product_id' 	=> $product_id,
							'count' 	=> $count
						);
				$_SESSION['view'][] = $view;
			}
		}else{
		// Nếu giỏ hàng chưa tồn tại
			$view = array(
							'product_id' => $product_id,
							'count'	=> $count
						);
			$_SESSION['view'][] = $view;
		} 			
	}
	function show_view()
	{
		global $oDb, $oSmarty;
		$view = $_SESSION['view'];
		$url = $_SERVER['REQUEST_URI'];
		if ($_GET['page'])
			$url = str_replace("/".$_GET['page']."/","",$url);
		$page_link = "#i++/";
		$page = isset($_GET['page'])?$_GET['page']:1;
		$limit = 100;
		if(!empty($view))
		{
			foreach ($view as $key => $value)
			{
			$sql = "SELECT * FROM tbl_product_item WHERE id='".$value['product_id']."' AND Status=1";
				$products = $oDb->getRow( $sql );
				if($products['Sale_off'])
				$price=$products['OldPrice']-($products['Sale_off'] * $products['OldPrice'])/100;
				else 
				$price=$products['OldPrice'];
				$view[$key]['price'] = $price;		
				// Kiển tra sản phẩm có tồn tại hoặc có được active hay không?
				if($products){
					$sql2="select id from tbl_product_category where id={$products['CatID']}";
					$child=$oDb->getOne($sql2);
					$view[$key]['product'] = $products;
				}
			}
		}
		$array_page = array();
		$b = $limit*($page-1);
		$e = $limit*$page;
		if(!empty($view))
		{
			$count=0;
			foreach($view as $key=>$value)
			{
				if($key>=$b && $key<$e)
				$array_page[] = $value;
				$count=$count+1;
			}
		}
		$view = $array_page;
		$sql = "SELECT * FROM tbl_product_item WHERE Status=1 Order by CreateDate DESC Limit 2";
		$products_new = $oDb->getAll( $sql );
		foreach($products_new as $k=>$v)
		{
			if($v['Sale_off'])
			$price=$v['OldPrice']-($v['Sale_off'] * $v['OldPrice'])/100;
			else 
			$price=$v['OldPrice'];
			$products_new[$k]['price'] = $price;			
		}
		$oSmarty->assign("products_new",$products_new);
		$count_view = count($view);
		$oSmarty->assign("view",$view);
		$oSmarty->assign("count",$count-1);
		$oSmarty->assign("array_page",$array_page);
		$oSmarty->assign("count_view",$count_view);
		$oSmarty->assign("limit",$limit);
		$oSmarty->assign("paging_path",$page_link);
		$oSmarty->display('show_view.tpl');
	}
	
	function getLink($id)
	{
		global $oSmarty, $oDb;
		if($id == "")
				$id = $_SESSION['cat_link'];
		if($_GET['task']=='category' || $_GET['task']=='sort_by')
		{
			$catLink = $this->getFullCatLink($id);
			//pre($id);
		}
		else if($_GET['task']=='details')
		{
			$sql = "SELECT CatID FROM {$this->table} WHERE  id={$id}";
			$cat_id = $oDb->getOne($sql);
			$catLink = $this->getFullCatLink($cat_id);
		}
		$oSmarty->assign('catLink',$catLink);
		$oSmarty->display('catLink.tpl');
	}	
	
	/*function category_product_ajax()
	{	
		global $oSmarty, $oDb;
		$fieldItem = "{$this->field}";
		$where = " Status=1 ";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		$cat_id =  $_GET['cat_id'];
		$limit = $_GET['limit'];
		$sort_by = $_GET['sort_by'];
		//die($limit);
		$str_cat = $this->getFullCatId($cat_id);
		$cond =" CatID IN ({$str_cat}) ";
        $url = $_SERVER['REQUEST_URI'];
		if ($_GET['page'])
			$url = str_replace("/".$_GET['page']."/","",$url);
		$page_link = "#i++/";
		$where .= " AND ".$cond;
		if($sort_by!='')
		$order = "ORDER BY Price {$sort_by}";
		else
		$order = "ORDER BY CreateDate DESC";
		$template = "category_product_ajax.tpl";
		$item_product = parent::Paging($limit, $fieldItem, $where,$page_link, $order);
			foreach($item_product as $k=>$v)
				{
					$sql2="select id from tbl_product_category where id={$v['CatID']}";
					$child=$oDb->getOne($sql2);
					$parent = $this->getParentID($child);
					$children = $this->getChildrenID($child);
					$item_product[$k]['parent'] = $parent;
					$item_product[$k]['child'] = $child;
					$item_product[$k]['children'] = $children;
					$pos_src = strpos($v['Photo'],'/upload/');
					$pos_jpg = strpos($v['Photo'],'.');
					$len = $pos_jpg-$pos_src+4;
					$src = substr($v['Photo'],$pos_src,$len);
					$item_product[$k]['src'] = $src;
				}
		$oSmarty->assign('item_product',$item_product);
		$oSmarty->display($template);
	}*/
	
	function show_img()
	{
		global $oSmarty, $oDb;
		$photo_id = $_POST['photo_id'];
		$sql = "SELECT Photo_Name FROM tblproduct_photo WHERE Photo_ID={$photo_id}";
		$show_img = $oDb->getRow($sql);
		$oSmarty->assign('show_img',$show_img);
		$oSmarty->display('show_img.tpl');
	}
	
	function main_img()
	{
		global $oSmarty, $oDb;
		$id = $_POST['id'];
		$sql = "SELECT * FROM tbl_product_item WHERE id={$id}";
		$img = $oDb->getRow($sql);
		$oSmarty->assign('img',$img);
		$oSmarty->display('main_img.tpl');
	}
		
	function Priceforyou()
	{
		global $oSmarty, $oDb;
		$qty = $_GET['qty'];
		$id = $_GET['id'];
		$sql = "SELECT * FROM tbl_product_item WHERE id={$id}";
		$item = $oDb->getRow($sql);
		if($item['Price'] != '')
			$priceforyou = $qty*$item['Price'];
		else
		$priceforyou = $qty*$item['OldPrice'];
		$oSmarty->assign("priceforyou",$priceforyou);
		$oSmarty->display("priceforyou.tpl");
	}
	
	function other_product()
	{
		global $oSmarty, $oDb;
        $id =  $_GET['id'];
		$cat_id = $oDb->getOne("SELECT CatID FROM {$this->table} WHERE id={$id} AND Status=1");
		
        $url = $_SERVER['REQUEST_URI'];
		if ($_GET['page'])
			$url = str_replace("/".$_GET['page']."/","",$url);
		$page_link = "#i++/";
		$cond = "id<>{$id} AND CatID='".$cat_id."'";
		$limit = 6;
		$template = "other_product.tpl";
		$this->getListItem(0,$page_link,$cond,$limit,$template);
	}
	
	function tinmoi()
	{
		global $oSmarty, $oDb;		
        $url = $_SERVER['REQUEST_URI'];
		if ($_GET['page'])
			$url = str_replace("/".$_GET['page']."/","",$url);
		$page_link = "#i++/";
		$cond = " Is_new=1 ";
		$limit = 8;
		$template = "list_product.tpl";
		$this->getListItem(0,$page_link,$cond,$limit,$template);
	}
	
    
	function menu()
	{	
		global $oDb, $oSmarty;        
        $where = " Status=1 ";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		$sql = "SELECT * FROM {$this->pre_table} WHERE {$where} AND ParentID = 0 ORDER BY Ordering ASC";
		$cat = $oDb->getAll($sql);
		foreach($cat as $key=>$val)
		{ 
			$sql = "SELECT * FROM {$this->pre_table} WHERE ParentID = {$val['id']} AND Status = 1 ORDER BY Ordering ";
			$sub = $oDb->getAll($sql);
			$cat[$key]['sub'] = $sub;
		} 
		$template = 'menu_product.tpl';		
		$oSmarty->assign("cat",$cat);
		$oSmarty->display($template);
	}	
	

		
	function details()
	{
		global $oDb,$oSmarty;
		$id = $_GET['id'];
		$sql = "SELECT * FROM {$this->table} WHERE Status=1 AND id={$id}";
		$detail_item = $oDb->getRow($sql);
		if(!empty($detail_item['Color']))
		{
			$arr_color = array();
			$colors = json_decode($detail_item['Color']);
			foreach ($colors as $key => $color) {
				$sql_color = "SELECT * FROM tbl_product_color WHERE id={$color['id']}";
				$color_item = $oDb->getRow($sql_color);
				$arr_color[$key]['id'] = $color_item['id'];
				$arr_color[$key]['Name'] = $color_item['Name'];
			}
		}
		$oSmarty->assign('arr_color',$arr_color);

		$sum_qua = $oDb->getOne("Select sum(number) from tbl_product_size where  pro_id={$detail_item['id']}");
		$oSmarty->assign('sum_qua',$sum_qua);
		$oSmarty->assign('detail_item',$detail_item);
		
		$full_cat_name = $this->getFullCatName($detail_item['CatID']);
		$oSmarty->assign("full_cat_name",$full_cat_name);
		
		$oDb->query("UPDATE tbl_product_item SET ViewNumber = ViewNumber + 1 WHERE id=$id");		
		
		$sql3 = "SELECT * FROM tblproduct_photo WHERE Photo_ProductID={$id} Order By Photo_ProductID DESC";
		$image = $oDb->getAll($sql3);
		$oSmarty->assign("image",$image);
		
		
		$sql = "SELECT size_id,number,Content, price_size, (select Ordering from tbl_size where id=tbl_product_size.size_id) as Ordering FROM tbl_product_size WHERE pro_id={$id} Order by Ordering";
		$pro_size = $oDb->getAll($sql);
		foreach ($pro_size as $k=>$v)
		{
			$sql = "SELECT Name FROM tbl_size WHERE id={$v['size_id']}";
			$size = $oDb->getOne($sql);
			$pro_size[$k]['size']=$size;
			$pro_size[$k]['id']=$v['size_id'];
			
			/*if($_SESSION['cart'])
			{
				$cart_session=$_SESSION['cart'];
				foreach($cart_session as $k1=>$v1){
					if($v1['product_id'] == $id && $v1['size'] == $v['size_id']){
						$pro_size[$k]['number']=$pro_size[$k]['number'] - $v1['quantity'];
					}
				}
			}*/
		}	
		$oSmarty->assign("pro_size",$pro_size);
		
		$arr_keyword = explode(',',$detail_item['Keyword']);
		$oSmarty->assign('arr_keyword',$arr_keyword);//pre($detail_item['Keyword']);pre($arr_keyword);
		
		//het chi tiet
		$this->add_view($id);
		
		$oSmarty->display('detail_product.tpl');
	}
	
	function detail_product_ajax()
	{
			global $oSmarty, $oDb;
			if (MULTI_LANGUAGE)
				$where .= " AND LangID = {$this->LangID} ";
			$pro_id=$_POST['pro_id'];
			$id=$_POST['id'];
			$sql = "SELECT * FROM tbl_product_item WHERE  id={$id} ";
			$detail_ajax= $oDb->getRow($sql);
			$oSmarty->assign('detail_ajax',$detail_ajax);
			if ($pro_id=='chi_tiet')
				echo $detail_ajax['Content'];
				
			if ($pro_id=='comment')
			{
				$sql = "SELECT Summarise FROM tbl_product_item WHERE id={$id}";
				$get_comment = $oDb->getOne($sql);
				$oSmarty->assign('get_comment',$get_comment);
				$oSmarty->display('detail_product_ajax.tpl');
			}
			if ($pro_id=='care')
			{
				$sql = "SELECT Care FROM tbl_product_item WHERE id={$id}";
				$get_care = $oDb->getOne($sql);
				$oSmarty->assign('get_care',$get_care);
				$oSmarty->display('detail_product_care.tpl');
			}
	}
	
	
	function getFullCatId($cat_id,$first = true)
	{
		global $oSmarty, $oDb;
        $where = "Status=1";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		$str_cat = "";
		$sql = "SELECT id FROM {$this->pre_table} WHERE {$where} AND ParentID={$cat_id} ORDER BY Ordering";
		$cat = $oDb->getCol($sql);
		if ($cat)
		{
			$str_cat = implode(',',$cat);
			$temp_str_cat = "";
			foreach ($cat as $key=>$value)
			{
				$temp_str_cat .= $this->getFullCatId($value,false);
			}
			$str_cat .=",".$temp_str_cat;			
		}
		if ($first)  
			$str_cat .=$cat_id;
		return $str_cat;
	}
	function getFullCatName($cat_id)
	{
		global $oSmarty, $oDb;
        $where = "Status=1";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		$sql = "SELECT Name FROM {$this->pre_table} WHERE {$where} AND id={$cat_id} ORDER BY Ordering";
		$cat_name = $oDb->getOne($sql);
		$sql = "SELECT ParentID FROM {$this->pre_table} WHERE {$where} AND id={$cat_id} ORDER BY Ordering";
		$parent = $oDb->getOne($sql);
		if ($parent)
		{
			$result = $this->getFullCatName($parent)."&nbsp; / &nbsp;".$cat_name;
		}else 
			$result = "&nbsp; / &nbsp;".$cat_name;
		return $result;
	}
	function getFullCatLink($cat_id)
	{
		global $oSmarty, $oDb;
        $where = "Status=1";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		$sql = "SELECT Name FROM {$this->pre_table} WHERE {$where} AND id={$cat_id} ORDER BY Ordering";
		$cat_name = $oDb->getOne($sql);
		$parent_id = $this->getParentID($cat_id);
		$sql = "SELECT ParentID FROM {$this->pre_table} WHERE {$where} AND id={$cat_id} ORDER BY Ordering";
		$parent = $oDb->getOne($sql);
		if ($parent)
		{
			$result = $this->getFullCatLink($parent).'&nbsp; &nbsp;<img src="/view/images/icon/icon-link.jpg" /> &nbsp; / <a href="'.SITE_URL.'product/c-'.$cat_id.'/'.remove_marks($cat_name).'.html" title="'.$cat_name.'">'.$cat_name.'</a>';
		}else 
			$result = '/ <a href="'.SITE_URL.'product/c-'.$cat_id.'/'.remove_marks($cat_name).'.html" title="'.$cat_name.'">'.$cat_name.'</a>';
		return $result;
	}
	
	function getParentID($cat_id)
	{
		global $oSmarty, $oDb;
		 $where = "Status=1";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		$sql = "SELECT ParentID FROM {$this->pre_table} WHERE {$where} AND id={$cat_id} ORDER BY Ordering";
		$cat_parent = $oDb->getOne($sql);
		if($cat_parent)
			$result = $this->getParentID($cat_parent);
		else
			$result = $cat_id;
		return $result;
	}
	function getChildrenID($cat_id)
	{
		global $oSmarty, $oDb;
		 $where = "Status=1";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		$sql = "SELECT ParentID FROM {$this->pre_table} WHERE {$where} AND id={$cat_id} ORDER BY Ordering";
		$cat_parent = $oDb->getOne($sql);
		if($cat_parent)
			$result = $cat_parent;
		else
			$result = $cat_id;
		return $result;
	}
	
	function getLinkCatName($cat_id)
	{
		global $oSmarty, $oDb;
        $where = "Status=1";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		$sql = "SELECT id,Name FROM tbl_product_category WHERE {$where} AND id={$cat_id} ORDER BY Ordering";
		$cat_name = $oDb->getRow($sql);
		//pre($cat_name); die();
		$cat_name = $cat_name['Name'];
		
		
		//$sql = "SELECT ParentID FROM tbl_product_category WHERE {$where} AND id={$cat_id} ORDER BY Ordering";
		//$parent_id = $oDb->getOne($sql);
		
		if ($parent_id)
		{
			$result = $cat_name."&nbsp;-&nbsp;".$this->getLinkCatName($parent_id);
		}else 
			$result = $cat_name;
		return $result;
	}

	function getPriceLeft()
	{
		global $oDb,$oSmarty;	
		$id = $_GET['id'];		
		$task_price = $_GET['task'];	
		$lands_price ="";
		if($task_price =='price')
		{
			$sql = "SELECT * FROM tbl_product_price WHERE id={$id}";
			$price = $oDb->getRow($sql);
			$lands_price= " And OldPrice >= {$price['PriceB']} AND OldPrice <= {$price['PriceE']} ";			
			return $lands_price;
		}		
	}
	

	
	function sort_by()
	{	
		global $oDb,$oSmarty;	
		if($cat_id!='')
			$sql = "SELECT Photo FROM tbl_product_category WHERE  id={$cat_id} ";
		else
			$sql = "SELECT Photo FROM tbl_product_category where Status=1 Order by Ordering ASC Limit 1  ";
		$cat_photo= $oDb->getOne($sql);
		$oSmarty->assign('cat_photo',$cat_photo);
		$sql = "SELECT id,Name FROM tbl_size where Status=1 Order by Ordering ASC ";
		$cat_size= $oDb->getAll($sql);
		$oSmarty->assign('cat_size',$cat_size);
		$url = $_SERVER['REQUEST_URI'];
		if ($_GET['page'])
			$url = str_replace("/".$_GET['page']."/","",$url);
		$page_link = "#i++/";	
		$cond = " 1=1 ";
		$limit = "8";

		$sort_price = $_GET['sort_price'];
		$sort_size = $_GET['sort_size'];
		$cat_id = $_GET['cat_id'];	
			
		if($cat_id != 0)
		{
			$str_cat = $this->getFullCatId($cat_id);
			$cond .= " CatID IN ({$str_cat}) ";			
		}
		if($sort_size!=0)
		{
			$sql = "SELECT DISTINCT(pro_id) FROM tbl_product_size where size_id =".$sort_size;
			$size_pro= $oDb->getCol($sql);
			if($size_pro)
				$str_size = implode(",",$size_pro);
			else
				$str_size = 0;	
			$cond .= " AND id IN ({$str_size}) ";			
		}
		if($sort_price == 0)
			$order="ORDER BY CreateDate DESC ";
		if($sort_price == 1)
			$order="ORDER BY OldPrice ASC";
		if($sort_price == 2)
			$order="ORDER BY OldPrice DESC";
		if($sort_price != "")
			$_SESSION['order'] = $order;
		else
			if($_SESSION['order'] != "")
				$order = $_SESSION['order'];
		$template = "list_product.tpl";		
		//============ garung ===============
		$sql = "Select count(*) from {$this->table} where Status=1 AND LangID={$_SESSION['lang_id']} ";
		$count = $oDb->getOne($sql);
		$oSmarty->assign("count",$count);

		$sql = "SELECT * FROM {$this->pre_table} WHERE Status=1 "; 
		$listCat = $oDb->getAll($sql);
		$oSmarty->assign("listCat",$listCat);

		$sql_color = "Select * from tbl_product_color";
		$colors = $oDb->getAll($sql_color);
		$oSmarty->assign("colors",$colors);
		//======================================
		//==================================
		$idHasParent = "SELECT ParentID FROM tbl_product_category WHERE Status = 1 AND ParentID <> 0";
		$hasParent = $oDb->getAll($idHasParent);
		$arr_id = array();
		foreach ($hasParent as $key_id => $value_sub) {
			$arr_id[$key_id] = $value_sub['ParentID'];
		}

		$head_sql = "SELECT * FROM tbl_product_category WHERE Status = 1 ORDER BY Ordering DESC";
		$menus = $oDb->getAll($head_sql);
		foreach ($menus as $keymenu => $menu_item) {
			$menu_item['id'] = (int)$menu_item['id'];
			if(in_array($menu_item['id'], $arr_id)) 
			{
				$menus[$keymenu]['child'] = $this->getSubCategory($menu_item['id'], 'tbl_product_category');
			} else {
				$menus[$keymenu]['child'] = '';
			}
			if(in_array($menu_item['ParentID'], $arr_id))
			{
				$menus[$keymenu]['remove'] = true;
			} else {
				$menus[$keymenu]['remove'] = false;
			}
		}
		$oSmarty->assign("menus",$menus);
		//==================================
		$this->getListItem(0,$page_link,$cond,$limit,$template,$order,$a=1);		
	}
	
	function listItem($cat_id=0)
	{
		global $oDb,$oSmarty;
		$where = " Status=1 ";
		if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		if($cat_id!='')
			$sql = "SELECT Photo FROM tbl_product_category WHERE  id={$cat_id} ";
		else
			$sql = "SELECT Photo FROM tbl_product_category where Status=1 Order by Ordering ASC Limit 1  ";
		$cat_photo= $oDb->getOne($sql);
		$oSmarty->assign('cat_photo',$cat_photo);
		$sql = "SELECT id,Name FROM tbl_size where Status=1 Order by Ordering ASC ";
		$cat_size= $oDb->getAll($sql);
		$oSmarty->assign('cat_size',$cat_size);
		$url = $_SERVER['REQUEST_URI'];
		if ($_GET['page'])
			$url = str_replace("/".$_GET['page']."/","",$url);
		$page_link = "$url/i++/";
		$cond = '';		
		$oSmarty->assign("cat_id",$cat_id);
		$_SESSION['cat_link'] = $cat_id;
		if($_GET['keyword2'])
			$template = 'list_keyword_tag.tpl';
		else
			$template = 'list_product.tpl';
		$limit = 8;
		if($cat_id || $_GET['tinmoi'])
			$limit = 100;
		//============ garung ===============
		$sql = "Select count(*) from {$this->table} where Status=1 AND LangID={$_SESSION['lang_id']} ";
		$count = $oDb->getOne($sql);
		$oSmarty->assign("count",$count);

		$sql = "SELECT * FROM {$this->pre_table} WHERE Status=1 Order by Ordering ASC"; 
		$listCat = $oDb->getAll($sql);
		$oSmarty->assign("listCat",$listCat);

		$sql_color = "Select * from tbl_product_color";
		$colors = $oDb->getAll($sql_color);
		$oSmarty->assign("colors",$colors);
		//======================================
		//==================================
		$idHasParent = "SELECT ParentID FROM tbl_product_category WHERE Status = 1 AND ParentID <> 0";
		$hasParent = $oDb->getAll($idHasParent);
		$arr_id = array();
		foreach ($hasParent as $key_id => $value_sub) {
			$arr_id[$key_id] = $value_sub['ParentID'];
		}

		$head_sql = "SELECT * FROM tbl_product_category WHERE Status = 1 ORDER BY Ordering DESC";
		$menus = $oDb->getAll($head_sql);
		foreach ($menus as $keymenu => $menu_item) {
			$menu_item['id'] = (int)$menu_item['id'];
			if(in_array($menu_item['id'], $arr_id)) 
			{
				$menus[$keymenu]['child'] = $this->getSubCategory($menu_item['id'], 'tbl_product_category');
			} else {
				$menus[$keymenu]['child'] = '';
			}
			if(in_array($menu_item['ParentID'], $arr_id))
			{
				$menus[$keymenu]['remove'] = true;
			} else {
				$menus[$keymenu]['remove'] = false;
			}
		}
		$oSmarty->assign("menus",$menus);
		//==================================
		$this->getListItem($cat_id,$page_link, $cond,$limit,$template, "ORDER BY Ordering ASC");
	}
	
	function getListItem($cat_id=0,$page_link,$cond="",$limit="8",$template="list_product.tpl",$order="ORDER BY Ordering ASC",$a=0)
	{
		global $oDb,$oSmarty;
		
		$where = " Status=1 ";
		if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		
			
		if($a==0)
		{
			$_SESSION['order'] = ""; $_SESSION['limit'] = "";	
		}	
		//pre($_SESSION['order']." xxxxxxxx ".$_SESSION['limit']);
				
		if ($cat_id!=0)
		{
			$str_cat_id = $this->getFullCatId($cat_id);
			$where .=" AND CatID IN ({$str_cat_id})";
			$full_cat_name = $this->getFullCatName($cat_id);
			$oSmarty->assign("full_cat_name",$full_cat_name);
			$full_cat_link = $this->getFullCatLink($cat_id);
			$oSmarty->assign("full_cat_link",$full_cat_link);
		}
		if($_GET['keyword2'])
		{
			$where .=" AND Keyword1 like '%".$_GET['keyword2']."%'";
		}
		if($_GET['store']==1)
			$where .=" AND ShowHome =1";
		if($_GET['tinmoi']==1)
			$where .=" AND Is_new=1";
		if ($cond != "")
			$where .= " AND ".$cond;
		//pre($where);
		$listItem = parent::Paging($limit, $this->field, $where,$page_link, $order);
		foreach($listItem as $k=>$v)
		{
			$sum_qua = $oDb->getOne("Select sum(number) from tbl_product_size where  pro_id={$v['id']}");
			$listItem[$k]['sum_qua'] = $sum_qua;		
			$photoHover = $oDb->getAll("Select Photo_Name from tblproduct_photo where  Photo_ProductID={$v['id']}");
			$dem = 0;
			foreach ($photoHover as $keydem => $valuedem) {
				$dem += 1;
			}
			$listItem[$k]['photo_hover'] = 'upload/product/'.($photoHover[$dem-1]['Photo_Name']);
		}
		$sql = "Select count(*) from {$this->table} where {$where} {$order} LIMIT {$limit}";
		$count = $oDb->getOne($sql);
		$oSmarty->assign("count",$count);
		$oSmarty->assign("listItem",$listItem);
		$oSmarty->display($template);
	}
	
	
	function search()
	{
		global $oSmarty, $oDb;
		$limit = 12;
		$where = "Status=1";
		$keyword = $_GET['keyword'];
		$url = $_SERVER['REQUEST_URI'];
		if ($_GET['page'])
			$url = str_replace('&page='.$_GET['page'],'',$url);
		$page_link = $url.'&page=i++';
		$cond .= " (upper(Name) LIKE '%".$keyword."%' OR  upper(Content) LIKE '%".$keyword."%')";
		$count_key = $oDb->getOne("SELECT count(*) FROM {$this->table} WHERE {$where} AND {$cond}");
		$oSmarty->assign("count_keyword",$count_key);
		$oSmarty->assign("keyword",$keyword);
		$template = 'ResultSearch.tpl';
		$this->getListItem(0, $page_link, $cond, $limit, $template);
	}
	
	function getFrmSearch()
    {
        global $oSmarty, $oDb;
		$cus_id=$_SESSION["userid_cus"];
		$oSmarty->assign("cus_id",$cus_id);
		$sql = "SELECT FullName,LastName  FROM tbl_customer WHERE id={$cus_id}";
        $customer = $oDb->getRow($sql);
		$oSmarty->assign("customer",$customer);
		$oSmarty->display('frm_search.tpl');
	}
		
	function getPrice()
    {
        global $oDb;
		$where = " Status=1 ";
		if (MULTI_LANGUAGE)
		$where .= " AND LangID={$_SESSION['lang_id']} ";
        	$sql = "SELECT id,Name FROM tbl_product_price WHERE {$where} Order by Ordering ASC";
        $price = $oDb->getAll($sql);
        return $price;
	}
	
	function getAge()
    {
        global $oDb;
		$where = " Status=1 ";
		if (MULTI_LANGUAGE)
		$where .= " AND LangID={$_SESSION['lang_id']} ";
        	$sql = "SELECT id,Name FROM tbl_age WHERE {$where} Order by Ordering ASC";
        $rs = $oDb->getAll($sql);
        return $rs;
	}
	
	
	function getCategoryKind($parent_id=0,$spacing='')
	{
		global $oDb,$oSmarty;
		
		$sql = "SELECT * FROM tbl_product_kind WHERE Status=1 AND ParentID = {$parent_id} ORDER BY Ordering ASC";
		$result = $oDb->getAll($sql);
		if($result){
			foreach($result as $key => $val){
				$html .= '<option value="'.$val['id'].'">';
				$html .= $spacing.' '.$val['Name'];
				$html .= $this->getCategoryKind($val['id'],$spacing.'&nbsp;-&nbsp;');
				$html .= '</option>';
				
			}
		}
		return $html;
	}
	
	function productSearch()
    {
        global $oDb,$oSmarty;
		$url = $_SERVER['REQUEST_URI'];
		if ($_GET['page'])
			$url = str_replace("/".$_GET['page']."/","",$url);
		$page_link = "#i++/";
		$limit = 15;
		$cond = '';
		
		$price = $_GET['price'];
		$age  = $_GET['age'];
		
/*		if($price != '' && $price != 'undefined')
            $cond[] = "PriceID = '$price'";
*/			
		if($price != '' && $price != 'undefined')
		{
			$sql = "SELECT * FROM tbl_product_price WHERE id={$price}";
			$price = $oDb->getRow($sql);
			$cond[] = "(OldPrice >= {$price['PriceB']} AND OldPrice <= {$price['PriceE']}) ";
		}
			
		if($age != '' && $age != 'undefined')		
            $cond[] = " id IN (Select pro_id from tbl_product_age where age_id = '$age')";					
		
		if(!empty($cond))
			$strCond = implode(' AND ',$cond);

		$template = "ResultSearch.tpl";
		$this->getListItem($cat_id,$page_link,$strCond,$limit,$template);
	}
	
	function getCategory($parent_id)
	{
		global $oDb,$oSmarty;
		
		$sql = "SELECT * FROM {$this->pre_table} WHERE Status=1 AND ParentID = {$parent_id} ORDER BY Ordering ASC";
		$result = $oDb->getAll($sql);
		if($result){
			$html = '<ul style="display:none">';
			foreach($result as $key => $val){
				$html .= '<li>';
				$html .= '<a href="'.SITE_URL.'product/c-'.$val['id'].'/'.remove_marks($val['Name']).'.html">'.$val['Name'].'</a>';
				$html .= $this->getCategory($val['id']);
				$html .= '</li>';				
			}
			$html .= '</ul>';
		}
		return $html;
	}
	
	function loadProductCate()
	{
		global $oDb,$oSmarty;
		if(!isset($_POST['size']))
		{
			return NULL;
		}
		$size = $_POST['size'];
		$sql = "SELECT pro_id,  FROM tbl_product_size WHERE size_id = {$size}  INNER JOIN {$this->table} ON tbl_product_size.pro_id = {$this->table}.id";
		$result = $oDb->getAll($sql);
		return $result;
	}
	
	function available()
	{
		global $oDb,$oSmarty;
		//================================
		$sql = "Select count(*) from {$this->table} where Status=1 AND LangID={$_SESSION['lang_id']} ";
		$count = $oDb->getOne($sql);
		$oSmarty->assign("count",$count);

		$sql = "SELECT * FROM {$this->pre_table} WHERE Status=1  ORDER BY Ordering ASC Limit 15"; 
		$listCat = $oDb->getAll($sql);
		$oSmarty->assign("listCat",$listCat);
		$sql = "SELECT id,Name FROM tbl_size where Status=1 Order by Ordering ASC ";
		$cat_size= $oDb->getAll($sql);
		$oSmarty->assign('cat_size',$cat_size);

		$sql_color = "Select * from tbl_product_color";
		$colors = $oDb->getAll($sql_color);
		$oSmarty->assign("colors",$colors);
		//================================
		//==================================
		$idHasParent = "SELECT ParentID FROM tbl_product_category WHERE Status = 1 AND ParentID <> 0";
		$hasParent = $oDb->getAll($idHasParent);
		$arr_id = array();
		foreach ($hasParent as $key_id => $value_sub) {
			$arr_id[$key_id] = $value_sub['ParentID'];
		}

		$head_sql = "SELECT * FROM tbl_product_category WHERE Status = 1 ORDER BY Ordering DESC";
		$menus = $oDb->getAll($head_sql);
		foreach ($menus as $keymenu => $menu_item) {
			$menu_item['id'] = (int)$menu_item['id'];
			if(in_array($menu_item['id'], $arr_id)) 
			{
				$menus[$keymenu]['child'] = $this->getSubCategory($menu_item['id'], 'tbl_product_category');
			} else {
				$menus[$keymenu]['child'] = '';
			}
			if(in_array($menu_item['ParentID'], $arr_id))
			{
				$menus[$keymenu]['remove'] = true;
			} else {
				$menus[$keymenu]['remove'] = false;
			}
		}
		$oSmarty->assign("menus",$menus);
		//==================================
		$sql = "SELECT a.* FROM tbl_product_item a INNER JOIN tbl_product_size b ON b.pro_id = a.id WHERE status = 1 AND b.number > 0 GROUP BY a.id";
		$listItem = $oDb->getAll($sql);
		// $page_link = "#i++/";
		// $listItem = parent::Paging(8, $this->field, "",$page_link, "ORDER BY Ordering ASC", "INNER JOIN tbl_product_size b ON b.pro_id = tbl_product_item.id WHERE status = 1 AND b.number > 0");
		$oSmarty->assign("listItem",$listItem);

		$oSmarty->display("list_product.tpl");
	}

	function all()
	{
		global $oDb,$oSmarty;
		//================================
		$sql = "Select count(*) from {$this->table} where Status=1 AND LangID={$_SESSION['lang_id']} ";
		$count = $oDb->getOne($sql);
		$oSmarty->assign("count",$count);

		$sql = "SELECT * FROM {$this->pre_table} WHERE Status=1  ORDER BY Ordering ASC Limit 15"; 
		$listCat = $oDb->getAll($sql);
		$oSmarty->assign("listCat",$listCat);
		$sql = "SELECT id,Name FROM tbl_size where Status=1 Order by Ordering ASC ";
		$cat_size= $oDb->getAll($sql);
		$oSmarty->assign('cat_size',$cat_size);

		$sql_color = "Select * from tbl_product_color";
		$colors = $oDb->getAll($sql_color);
		$oSmarty->assign("colors",$colors);
		//================================
		//==================================
		$idHasParent = "SELECT ParentID FROM tbl_product_category WHERE Status = 1 AND ParentID <> 0";
		$hasParent = $oDb->getAll($idHasParent);
		$arr_id = array();
		foreach ($hasParent as $key_id => $value_sub) {
			$arr_id[$key_id] = $value_sub['ParentID'];
		}

		$head_sql = "SELECT * FROM tbl_product_category WHERE Status = 1 ORDER BY Ordering DESC";
		$menus = $oDb->getAll($head_sql);
		foreach ($menus as $keymenu => $menu_item) {
			$menu_item['id'] = (int)$menu_item['id'];
			if(in_array($menu_item['id'], $arr_id)) 
			{
				$menus[$keymenu]['child'] = $this->getSubCategory($menu_item['id'], 'tbl_product_category');
			} else {
				$menus[$keymenu]['child'] = '';
			}
			if(in_array($menu_item['ParentID'], $arr_id))
			{
				$menus[$keymenu]['remove'] = true;
			} else {
				$menus[$keymenu]['remove'] = false;
			}
		}
		$oSmarty->assign("menus",$menus);
		//==================================
		$sql = "SELECT * FROM tbl_product_item WHERE status = 1";
		$listItem = $oDb->getAll($sql);
		$page_link = "#i++/";
		$listItem = parent::Paging(8, $this->field, "status = 1",$page_link, "ORDER BY Ordering ASC");
		$oSmarty->assign("listItem",$listItem);

		$oSmarty->display("list_product.tpl");
	}

	function getSubCategory($parent_id=0, $table='')
	{
		global $oDb,$oSmarty;
		$where = "Status=1";
		if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$_SESSION['lang_id']} ";
		$sql = "SELECT * FROM {$table} WHERE {$where} AND ParentID={$parent_id} ORDER BY Ordering ASC";
		$result = $oDb->getAll($sql);
		return $result;
	}
}
?>