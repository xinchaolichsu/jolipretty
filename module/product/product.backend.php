<?php
class productBackEnd extends Bsg_Module_Base{
	var $smarty;
	var $db;
	var $url="";
    var $datagrid;
	var $id;
	var $imgPath;
	var $imgPathShort;
	var $table='';
	var $arrAction;
	public function __construct($oSmarty, $oDb, $oDatagrid)
	{	
		$this -> smarty = $oSmarty;
		$this -> db = $oDb;		
        $this -> datagrid = $oDatagrid;
		$this -> id=$_REQUEST[id];		
		$this -> table	="tbl_product_item";
		$this -> pre_table	="tbl_product_item";
		parent::__construct($oDb);		
		$this->bsgDb->setTable($this->table);
		$this->imgPath = SITE_DIR."upload/product/";
		$this->imgPathShort = "/upload/product/";
        // parent::loadJs('product.js');
		parent::loadJs('qfamsHandler.js');
	}
	
	function run($task)
	{	
		switch( $task ){
            case 'add':
				$this -> addItem();
				break;
			case 'export_error':
				$this -> export_error();
				break;
			case 'edit':
				$this -> editItem();
				break;
			case 'color':
                $this -> moreItem();
                break;
			case 'delete':
				$this -> deleteItem();
				break;
			case 'delete_all':
				$this -> deleteItems();
				break;
			case 'delete_img':
				$this->delete_img();
				break;
			case 'change_status':				
				$this -> changeStatus($_GET['id'], $_GET['status']);
				break;
			case 'public_all':						
				$this -> changeStatusMultiple( 1 );
				break;
			case 'unpublic_all':						
				$this -> changeStatusMultiple( 0 );
				break;
			case 'save_order':
				$this -> saveOrder();
				break;
            case 'ajax_filter':
                $this -> ajax_filter();
                break;
            case 'ajax_form':
                $this->ajax_form();
                break;
			case 'changeLang':
                $this->changeLang();
                break;
			default:					
				$this -> listItem( $_GET['msg'] );		
				break;
		}
	}
	
	function getPageInfo()
	{
		return true;
	}
	
	function export_error()
	{
		$this -> getPath("Xuất đanh sách thành viên");	
		$this -> exportErr();
	}
	function exportErr()
	{
        global $oDb,$oSmarty;
		require_once 'lib/PHPExcel/Classes/PHPExcel.php';
		require_once 'lib/PHPExcel/Classes/PHPExcel/Writer/Excel2007.php';
		$objPHPExcel = new PHPExcel(); // Khai bao đối tượng export
		$objPHPExcel->setActiveSheetIndex(0); // Bắt đầu từ dòng 0
		$objPHPExcel->getDefaultStyle()->getFont()->setName('Times New Roman')->setSize(12); // Xét font cho file excel
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(6);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(50);
		// set Height Row
		$objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);
		// đặt chữ ỏ giữa dòng
		$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$objPHPExcel->getActiveSheet()->getStyle("A2:C2")->getFont()->setBold(true);  // đậm nhạt
		$objPHPExcel->getActiveSheet()->SetCellValue('A2', "Thứ tự");  // add giá trị
		$objPHPExcel->getActiveSheet()->SetCellValue('B2', "Sản phẩm");  // add giá trị
		$objPHPExcel->getActiveSheet()->SetCellValue('C2', "Số lượng");  // add giá trị
		//select
		$aItems	 = $_GET['arr_check'];
		if(is_array( $aItems) && count( $aItems) > 0)
		{
			$sItems = implode( ',', $aItems );
				$sql = "SELECT Name,id from tbl_product_item where id IN ({$sItems}) order by CreateDate ASC ";
				$all_error = $oDb->getAll($sql);
				foreach($all_error as $key=>$value)
				{			
					
					$sql = "SELECT number,(select Name from tbl_size where id=tbl_product_size.size_id) as name_size from tbl_product_size where pro_id = ({$value['id']}) order by id ASC ";
					$all_size = $oDb->getAll($sql);
					$size123='';
					foreach($all_size as $k=>$v)
					{		
						$size123 .= '  Size ' .$v['name_size'] . ': ' . $v['number'] . ', '; 
					}	
					$row = $key + 3;
					$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, $key+1);  // add giá trị	
					$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $value['Name']);  // add giá trị	
					$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $size123);  // add giá trị	
					$objPHPExcel->getActiveSheet()->getStyle('B'.$row.':C'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
					$objPHPExcel->getActiveSheet()->getStyle('B'.$row.':C'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
					$objPHPExcel->getActiveSheet()->getStyle('B'.$row.':C'.$row)->getAlignment()->setWrapText(true);
				}
		}
		$str = date("d.m.Y");
		//Set tiêu đề cho 1 Sheet trong excel
		$objPHPExcel->getActiveSheet()->setTitle("Danh sách sản phẩm");
		//Save vào đường dẫn bằng câu lệnh save
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save("upload/excel/danhsach_sanpham_".$str.".xls");
		echo '<script language="javascript">
					window.location ="'.SITE_URL.'upload/excel/danhsach_sanpham_'.$str.'.xls";</script>';	
	}
    
    function ajax_form()
    {
        global $oDb;
        $lang_id = $_REQUEST['lang_id'];
        $category = $this->getCategory($lang_id);
        $option_str = parent::changeOption($category);
        echo $option_str;
    }
    
    function ajax_filter()
    {
        global $oDb;
        $lang_id = $_REQUEST['lang_id'];
        $category = array(0 => "---Tất cả---" ) + $this->getCategory($lang_id);
        $option_str = parent::changeOption($category);
        echo $option_str;
    }

	function addItem()
	{
		$this -> getPath("Quản lý sản phẩm > Thêm mới sản phẩm");		
		$this -> buildForm();
	}
	
	function editItem()
	{
		$id = $_GET['id'];
		$this -> getPath("Quản lý sản phẩm > Chỉnh sửa sản phẩm");	
		$row = $this -> bsgDb -> getRow( $id );
		$this -> buildForm( $row );
	}
	
	function moreItem()
    {
        $id = $_GET['id'];
		echo '<script language="javascript">
		window.open ("/index.php?mod=admin&amod=color&atask=color&sys=&tab=true&id='.$id.'","color");
			</script>';	
		
		/*echo '<script language="javascript">
		location.href = "/index.php?mod=admin&amod=color&atask=color&sys=&tab=true&id='.$id.'";
			</script>';	*/
    }
	
    function changeLang()
    {
        global $oDb;
        $LangID = $_REQUEST['lang'];
        if (intval($LangID))
            $category = $this->getCategory($LangID);
        else 
            $category = array();
        $options = "<select name=\"CatID\" id=\"CatID\">";
        $options .= '<option value="">- - - Danh mục gốc - - -</option>';
        foreach ($category as $key => $val) {
            $options.='<option value="'.$key.'">'.$val.'</option>';
        }
        $options .="</select>";
        echo $options;
    }
	
	function deleteItem()
	{
		global  $oDb;
		$id = $_GET["id"];
		$this -> bsgDb -> deleteWithPk( $id );
        $this->deleteImage($id, "Photo", $this->imgPath);
		$msg = "Xóa sản phẩm thành công!";
		$this -> listItem( $msg );
	}
	
	function deleteItems()
	{
		$aItems	 = $_GET['arr_check'];
		
		if(is_array( $aItems) && count( $aItems) > 0){
			$sItems = implode( ',', $aItems );
			$this -> bsgDb -> deleteWithPk( $sItems );
            $this->deleteImage($sItems, "Photo", $this->imgPath);
		}
		$msg = "Xóa (các) sản phẩm thành công!";
		$this -> listItem( $msg );
	}
	
	function delete_img()
	{
		global $oDb;
		$id = $_POST['id_travel'];
		$sql = "Update tbl_product_item SET Photo = '' where id = {$id}";
        $this -> deleteImage($id, "Photo", $this->imgPath);
		$oDb->query($sql); 
	}
	
	function getImage($product_id)
	{
		global $oDb;
		$sql = "SELECT Photo_ID,Photo_Name FROM tblproduct_photo WHERE Photo_ProductID=$product_id";
		$result = $oDb->getAll($sql);
		//pre($result);die();
		return $result;
	}
	
	function changeStatusMultiple( $status = 0 )
	{
		$aItems	 = $_GET['arr_check'];
		if(is_array( $aItems) && count( $aItems) > 0){
			$sItems = implode( ',', $aItems );
			$this -> bsgDb -> updateWithPk( $sItems, array("Status" => $status) );
		}
		$msg = "Sửa trạng thái sản phẩm thành công!";
		$this -> listItem( $msg );
	}
	
	function checkColor($pro_color, $color_id)
	{
		foreach ($pro_color as $value) {
			if($value == $color_id) {
				return true;
			}
		}
		return false;
	}

	function buildForm( $data=array() , $msg = ''){
		global $oDb;
        
		$form = new HTML_QuickForm('frmAdmin','post',$_COOKIE['re_dir']."&task=".$_GET['task'], '', "style='padding:10px 15px 0 20px;'");		
		
		$form -> setDefaults($data);//pre($data);
		echo "<script type=\"text/javascript\" src=\"/view/js/lang.js\"></script>"; 
		
		if (MULTI_LANGUAGE)
        {
		    $lang = parent::loadLang();
			 $url = "?mod=admin&amod={$_REQUEST['amod']}&atask={$_GET['atask']}&task=changeLang&ajax";
		    $form -> addElement('select', 'LangID', 'Ngôn ngữ', $lang, array("onchange"=>"changeLang(this.value,'{$url}', 'CatID')"));
			$value_lang = array_keys($lang);
			$selected_langid = $data['LangID']?$data['LangID']:$value_lang[0];
        }
        else
            $selected_langid = 0;

		$category = array(0=>'--- Chọn danh mục ---') + $this->getCategory($selected_langid);
		$form -> addElement('select', 'CatID', 'Danh mục: <br> (Giữ ctrl và chọn nhiều)', $category, array('id'=>'CatID', 'multiple'=>true));
						
		$form -> addElement('text', 'Name', 'Tên sản phẩm', array('size' => 50, 'maxlength' => 255));
		$form -> addElement('text', 'OldPrice', 'Giá', array('size' => 50, 'maxlength' => 255));	
		$form -> addElement('text', 'Price', 'Giá khuyến mại', array('size' => 50, 'maxlength' => 255));
		$form -> addElement('text', 'SoldOut', 'Tình trạng mặt hàng', array('size' => 50, 'maxlength' => 255));
		
		$id123 = $_POST['id'];
		if(!$_POST['id'])	
			$id123 = $data['id'];
		if($id123 == "")
			$id123 = "0";
		$size=$this->getSize($id123);
		$form -> addElement('static', '','Loại size',$size);
		
		$form->addElement('file', 'Photo', 'Ảnh', array('id'=>'imgFile'));
		if($_GET['task']=='edit' && $data['Photo'])
		{
			$form->addElement('static', null, '',"<a href='".SITE_URL.$data['Photo']."' onclick='return hs.expand(this)' class='highslide'><img src='".SITE_URL.$data['Photo']."' width=100 hight=100 border=0></a>");
			$form -> addElement('static',NULL,'',"<a style=\" cursor:pointer;\" id='delete123_img' onclick='delete_image_travel(".$data['id'].")'><img src='".SITE_URL."view/images/delete.jpg"."' border=0>Xóa ảnh</a>");
		}		
		
		if($_GET['task']=='edit' && $data['id'])
		{
			$arr_image_out = $this->getImage($data['id']);
		}
		$img_url = "/upload/product/";
		$images_out=parent::multiImage('image_out',$this->imgPath,$arr_image_out,$img_url,array('width'=>'400', 'height'=>'400'));
        $form -> addElement('static',NULL,'Chùm ảnh',$images_out);
        
        $sql_color = "SELECT * FROM tbl_product_color";
		$colors = $oDb->getAll($sql_color);

		if(!empty($data['Color']))
		{
			$product_color = json_decode($data['Color']);
		} else {
			$product_color = '';
		}

    	$html .= "<div class=''";
		if($colors) {
			foreach ($colors as $color) {
				$html .= '<div class="checkbox" style="width: 60px; float:left; margin-right: 10px">
			  			<label><input type="checkbox" name="color[]" value="'.$color["id"].'" '.($this->checkColor($product_color, $color["id"]) ? 'checked' : '').'> '.$color["Name"].'</label>
					</div>';
			}
		}
    	$html .= "</div>";

		$form -> addElement('static', NULL, 'Chọn màu: ', $html);
		
		//$form->addElement('textarea','Summarise','Thông tin về size',array('style'=>'width:600px; height:200px; font-family:"Arial"'));
		$content_editor1=parent::editor('Summarise',$data['Summarise'],array('width'=>'800', 'height'=>'400'));
        $form -> addElement('static',NULL,'Thông tin về size',$content_editor1);
		$content_editor2=parent::editor('Care',$data['Care'],array('width'=>'800', 'height'=>'400'));
        $form -> addElement('static',NULL,'Chăm sóc sản phẩm',$content_editor2);
		//$form->addElement('textarea','Care','Chăm sóc sản phẩm',array('style'=>'width:600px; height:200px; font-family:"Arial"'));
		$content_editor=parent::editor('Content',$data['Content'], array('width'=>'800', 'height'=>'400'));
		$content_editor=parent::editor('Content',$data['Content'],array('width'=>'800', 'height'=>'400'));
        $form -> addElement('static',NULL,'Giới thiệu',$content_editor);
		$form -> addElement('text', 'Keyword', 'Tag', array('size' => 100, 'maxlength' => 255));
        $date_time = parent::date_time('CreateDate',($data['CreateDate']) ? ($data['CreateDate']) : date("Y-m-d H:i"));			
		$form -> addElement('static',NULL,'Ngày tạo',$date_time);
		$form -> addElement('text', 'Ordering', 'Thứ tự', array('size' => 10, 'maxlength' => 50));
		$form -> addElement('checkbox', 'Is_new', 'Mẫu mới');
		$form -> addElement('checkbox', 'ShowHome', 'Back in stock');
		$form -> addElement('checkbox', 'Status', 'Kích hoạt');
		
		$btn_group[] = $form -> createElement('submit',null,'Hoàn tất',array("style"=> "border:1px solid gray; padding:0 5px 0 5px;"));		
        $btn_group[] = $form -> createElement('button',null,'Quay lại',array('onclick'=>'window.location.href = \''.$_COOKIE['re_dir'].'\'', "style"=> "border:1px solid gray;"));      
        $form -> addGroup($btn_group);
      
		$form->addElement('hidden', 'id', $data['id']);		
		
       // $form -> addRule('Name','Tiêu đề không được để trống','required',null,'client');
		if($_GET['task']=='add')
		$form -> addRule('Photo','Ảnh đại diện không được để trống','required',null,'client');
		
		if( $form -> validate())
		{
			if($_FILES['Photo']['name']!='')
			{
				$this->deleteImage($this->id, "Photo", $this->imgPath);
				$FileName = rand().'_'.$_FILES['Photo']['name'];												
				parent::crop_image($_FILES['Photo']['tmp_name'],$FileName, $this->imgPath);				
				$_POST['Photo'] = $this->imgPathShort.$FileName;				
			}
			$json_color = '';
			if(!empty($_POST['color']))
			{
				$json_color = json_encode($_POST['color']);
			}

			$data_size = array();
			$z = 0;
			$sql = "Select id from tbl_size where Status = 1 Order by Ordering ASC";
			$sizes = $oDb->getCol($sql);
			for($i=0;$i<count($sizes);$i++)
			{
				if($_POST['size_'.$i] != "")
				{
					//=== garung =======
					$data_size[$z] = array("number" => $_POST['size_'.$i],"size_id" => $sizes[$i],"Content" => $_POST['content_'.$i], 'price_size' => $_POST['price_size_'.$i]);
					$z++;
				}
			}
			$keyword=$this->remove_marks($_POST['Keyword']);

			$aData  = array(
				"LangID" 		=> $_POST['LangID']?$_POST['LangID']:1,
				"Name" 		=> $_POST['Name'],
				"Keyword" 		=> $_POST['Keyword'],
				"Keyword1" 		=> $keyword,
				"OldPrice" 	=> $_POST['OldPrice'],
				"Price" 	=> $_POST['Price'],
				"SoldOut" 	=> $_POST['SoldOut'],
                // "CatID" 	=> $_POST['CatID'],
				"Summarise" 	=> $_POST['Summarise'],
				"Care" 	=> $_POST['Care'],
				"Content" 	=> $_POST['Content'],
				"CreateDate" 	=> $_POST['CreateDate'],
				"ShowHome" 	=> $_POST['ShowHome'],
				"Ordering" 	=> $_POST['Ordering'],
				"Is_new" 	=> $_POST['Is_new'],
				"Status" 	=> $_POST['Status'],
				"Color"     => $json_color
			);
			if ($_POST['Photo']!=''){
				$aData['Photo'] = $_POST['Photo'];
			}
			// if ($_POST['Photo']!=''){
			// 	$tmp_photo = $_POST['Photo'];
			// }
			// else
			// {
			// 	$tmp_photo = '';
			// }
				
			if( !$_POST['id'] ){
				// =========== garung ===================
				foreach ($_POST['CatID'] as $key => $value) {
					$aData["CatID"] = $value;
					// $aData['Photo'] = $tmp_photo;
					$id = $this -> bsgDb -> insert($aData);
					$this->setProAndSize($id,$_POST['size']);	
					 if($data_size)
					 {
					 	for($i=0;$i<count($data_size);$i++)
						{
							$data_size[$i]['pro_id'] = $id;
							$this->bsgDb->add_rec('tbl_product_size',$data_size[$i]);
						}
					 }
					 parent::deleteChumAnh($id);
					 $this->addMultiImageToDatabase("image_out","tblproduct_photo",array("Photo_ProductID"=>$id));
					 $this->deleteMultiPhoto("image_out","upload/product/","tblproduct_photo");
				}
				// ============ end =====================
				 $msg = "Thêm sản phẩm thành công! ";
			}else {
				$id = $_POST['id'];	
				// =========== garung ===================
				foreach ($_POST['CatID'] as $key => $value) {
					$aData["CatID"] = $value;
					// $aData['Photo'] = $tmp_photo;
					$this -> bsgDb -> updateWithPk($id, $aData);
					$this->setProAndSize($id,$_POST['size']);
					  if($data_size)
					 {
					 	$sql = "Delete from tbl_product_size where pro_id = {$id}";
						$oDb->query($sql);
					 	for($i=0;$i<count($data_size);$i++)
						{
							$data_size[$i]['pro_id'] = $id;
							$this->bsgDb->add_rec('tbl_product_size',$data_size[$i]);
						}
					 }	
				}
				//================= end ============	
				$msg = "Chỉnh sửa sản phẩm thành công ";
				parent::deleteChumAnh($id);
				$this->addMultiImageToDatabase("image_out","tblproduct_photo",array("Photo_ProductID"=>$id));
				$this->deleteMultiPhoto("image_out","upload/product/","tblproduct_photo");
			}	
			$this -> redirect($_COOKIE['re_dir']. "&msg={$msg}");
		}
		
		$form->display();
	}
	
	function setProAndSize($pro_id,$arr_size_id)
    {
		global $oDb;
		$sql = "DELETE FROM tbl_product_size WHERE pro_id=$pro_id";
        $this->bsgDb->querySql($sql);
		if(is_array($arr_size_id) && !empty($arr_size_id))
		{
			//pre(arr_size_id);
			foreach($arr_size_id as $val)
			{				
				$data  = array(
					"pro_id"         => $pro_id,
					"number"         => $val
				);
				$this->bsgDb->add_rec('tbl_product_size',$data);				
			}
		}
    }
	
	
	function getSize($id=0){
		global $oDb;
		$sql = "SELECT id,Name FROM tbl_size WHERE Status=1 Order by Ordering ASC";
		$size = $oDb->getAll($sql);
		$cart = '<div style="width:400px;height:200px;overflow-x:auto;margin:10px 10px; "><input type="hidden" id="hdnGetPara" value=""> <table width="400px" cellpadding="0" cellspacing="0"  bgcolor="#fff">';
		foreach ($size as $k => $v) {
			$sql = "SELECT number,Content, price_size FROM tbl_product_size WHERE  pro_id={$id} AND size_id={$v[id]}";
			$size123 = $oDb->getRow($sql);
			$cart .= '	<tr height="30px">';
			$cart .= '	<td bgcolor="#FFFFFF" ><strong>'.$v['Name'].'</strong></td>';
			
			$cart .='   <td>Số lượng: <input type="text" size="5" name="size_'.$k.'"  value="'.$size123['number'].'"/></td>';
			// garung ===========
			$cart .='   <td>Giá size: <input type="text" size="5" name="price_size_'.$k.'"  value="'.$size123['price_size'].'"/></td>';
			$cart .='   <td>Thông báo: <input type="text" name="content_'.$k.'"  value="'.$size123['Content'].'"/></td>';
			$cart .='	</tr>';
		}
			$cart .='</table>';
			$cart .='	</div>';
		return $cart; 
	}
	
	function deleteImage($id, $field, $path){
	//pre($id);die();
	//echo gettype($id);
		if($id == '')
			return;
		if(is_string($id))
		$imgpath = $path.$this->db->getOne("SELECT $field FROM ".$this->table." WHERE id IN ($id)");
		
		else
		$imgpath = $path.$this->db->getOne("SELECT $field FROM ".$this->table." WHERE id = $id");
		//pre($imgpath);die();
		if(is_file($imgpath))			
			@unlink($imgpath);
	}
	

	
	
	
	function getCategory($lang_id=0){
		global $oDb;
		$table = 'tbl_product_category';
		$sql = "SELECT id,Name FROM {$table} WHERE Status=1 Order by Ordering ASC";
		$category = $oDb->getAssoc($sql);
		return $category;
	}
	
	function getCity($lang_id=0){
		global $oDb;
		$table = 'tbl_product_place';
		$sql = "SELECT id,Name FROM {$table} WHERE Status=1 Order by Ordering ASC";
		$city = $oDb->getAssoc($sql);
		return $city;
	}
	
	
	function changeStatus( $itemId , $status ){
		$aData = array( 'Status' => $status );
		$this -> bsgDb -> updateWithPk( $itemId, $aData );
		return true;
	}
	
	function saveOrder(){	
		$aItem = $_GET['Ordering'];
		if(is_array($aItem) && count( $aItem ) > 0){
			// save order for item.
			foreach( $aItem as $key => $value){
				if( !is_numeric($value)) $value = 0;				
				$this -> bsgDb -> updateWithPk( $key, array('Ordering' => $value ));
			}
		}	
		$msg = "Lưu thứ tự danh mục thành công!";
		$this -> listItem( $msg );
	}
	
	function listItem( $sMsg= '' )
	{		
		global $oDb;
		global $oDatagrid;				
		
		$root_path = "Quản lý sản phẩm > Danh sách sản phẩm";						
		$submit_url= "/index.php?".$_SERVER['QUERY_STRING'];
		
		$table = $this -> table;
		$arr_filter= array(
			array(
				'field' => 'Name',
				'display' => 'Tiêu đề',
				'type' => 'text',
				'name' => 'filter_title',
				'selected' => $_REQUEST['filter_title'],
				'filterable' => true
			)
        );
            
		$arr_filter[] = array(
				'field' => 'CatID',
				'display' => 'Danh mục',				
				'name' => 'filter_cat',
				'selected' => $_REQUEST['filter_cat'],
				'options' => $this->getCategory(),
				'filterable' => true
			);
		$arr_filter[] = array(
				'field' => 'Status',
				'display' => 'Trạng thái',				
				'name' => 'filter_show',
				'selected' => $_REQUEST['filter_show'],
				'options' => array('Vô hiệu','Kích hoạt'),
				'filterable' => true
			);
		
		$arr_cols= array(		
			
			array(
				"field" => "Name",
				"display" => "Tiêu đề",
				"align"	=> 'left',		
                'link' => SITE_URL.'index.php?mod=admin&amod='.$_GET['amod'].'&atask='.$_GET['atask'].'&tab=1&frame&task=edit',		
				"datatype" => "text",
				"sortable" => true
			)
			
        );
		/*$arr_cols[] = array(
                "field" => "CreateUser",
                "display" => "Người post",
                "sql"    => "SELECT username FROM tbl_customer WHERE id = CreateUser",
                "align"    => 'left',                
                "datatype" => "text",
                "sortable" => true
            );*/
        $arr_cols[] = array(
                "field" => "CatID",
                "display" => "Danh mục",
                "sql"    => "SELECT name FROM tbl_product_category WHERE id = CatID",
                "align"    => 'left',                
                "datatype" => "text",
                "sortable" => true
            );
		
		$arr_cols_more = array(
            array(
				"field" => "Photo",
				"display" => "Ảnh",
				"datatype" 		=> "img",
				"img_path"		=> SITE_URL,
				"sortable" => true,
				"order_default" => "asc"
			),	
            array(
				"field" => "Ordering",
				"display" => "Thứ tự",
				"datatype" 		=> "order",
				"sortable" => true,
				"order_default" => "asc"
			),
			array(
				"field" => "Is_new",
				"display" => "Mẫu mới",				
				"datatype" => "boolean",
				"sortable" => true
			),
			array(
				"field" => "ShowHome",
				"display" => "Back in stock",				
				"datatype" => "boolean",
				"sortable" => true
			),
			array(
				"field" => "Status",
				"display" => "Trạng thái",				
				"datatype" => "publish",
				"sortable" => true
			),		
		);		
        
        $arr_cols = array_merge($arr_cols,$arr_cols_more);
		
		$arr_check = array(
			array(
				"task" => "delete_all",
				"confirm"	=> "Xác nhận xóa?",
				"display" => "Xóa"
			),
			array(
				"task" => "public_all",
				"confirm"	=> "Xác nhận thay đổi trạng thái?",
				"display" => "Kích hoạt"
			),
			array(
				"task" => "unpublic_all",
				"confirm"	=> "Xác nhận thay đổi trạng thái?",
				"display" => "Vô hiệu"
			)			
		);
        
		if( $sMsg )
			$oDatagrid -> setMessage( $sMsg );
		$oDatagrid->display_datagrid($table, $arr_cols, $arr_filter, $submit_url, $this -> getAct() ,120, $root_path, false ,$this->getActCheck());		
		
	}		
	function ChooseColor(){}
}

?>