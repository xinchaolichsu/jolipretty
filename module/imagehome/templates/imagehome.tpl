{if $listItem}
<div class="container pd0">
    <div class="col-md-12 col-xs-12 col-sm-12">
        {foreach from=$listItem item=item name=item}
        <div class="col-md-6 col-sm-6 col-xs-12 ite-pro-home">
            <a href="{$item.Link}">
                <img alt="" class="img-responsive img-product-home" src="{$smarty.const.SITE_URL}{$item.Photo}"/>
            </a>
        </div>
        {/foreach}
    </div>
</div>
{/if}