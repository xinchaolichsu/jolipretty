<?php
class videoBackEnd extends Bsg_Module_Base{
    var $smarty;
    var $db;
    var $url="";
    var $datagrid;
    var $id;
    var $imgPath;
    var $imgPathShort;
    var $table='';
    var $arrAction;
    public function __construct($oSmarty, $oDb, $oDatagrid)
    {    
        $this -> smarty = $oSmarty;
        $this -> db = $oDb;        
        $this -> datagrid = $oDatagrid;
        $this -> id=$_REQUEST[id];        
        $this -> table    ="tbl_video";             
        parent::__construct($oDb);        
        $this->bsgDb->setTable($this->table);
       $this -> imgPath = SITE_DIR."upload/video/";
		$this -> imgPathShort = "/upload/video/";
    }
    
    function run($task)
    {    
        switch( $task ){
            case 'add':
                $this -> addItem();
                break;
            case 'edit':
                $this -> editItem();
                break;
            case 'delete':
                $this -> deleteItem();
                break;
            case 'delete_all':
                $this -> deleteItems();
                break;
            case 'change_status':                
                $this -> changeStatus($_GET['id'], $_GET['status']);
                break;
            case 'public_all':                        
                $this -> changeStatusMultiple( 1 );
                break;
            case 'unpublic_all':                        
                $this -> changeStatusMultiple( 0 );
                break;
            case 'save_order':
                $this -> saveOrder();
                break;
            default:                    
                $this -> listItem( $_GET['msg'] );        
                break;
        }
    }
    
    function getPageInfo()
    {
        return true;
    }

    function addItem()
    {
        $this -> getPath("Quản lý video > Thêm mới video");        
        $this -> buildForm();
    }
    
    function editItem()
    {
        $id = $_GET['id'];
        $this -> getPath("Quản lý video > Chỉnh sửa video");    
        $row = $this -> bsgDb -> getRow( $id );
        $this -> buildForm( $row );
    }

    function deleteItem()
    {
        global  $oDb;
        $id = $_GET["id"];
        $this -> bsgDb -> deleteWithPk( $id );
        $msg = "Xóa thành công!";
        $this -> listItem( $msg );
    }
    
    function deleteItems()
    {
        $aItems     = $_GET['arr_check'];
        if(is_array( $aItems) && count( $aItems) > 0){
            $sItems = implode( ',', $aItems );
            $this -> bsgDb -> deleteWithPk( $sItems );
        }
        $msg = "Xóa thành công!";
        $this -> listItem( $msg );
    }
    
    function changeStatusMultiple( $status = 0 )
    {
        $aItems     = $_GET['arr_check'];
        if(is_array( $aItems) && count( $aItems) > 0){
            $sItems = implode( ',', $aItems );
            $this -> bsgDb -> updateWithPk( $sItems, array("Status" => $status) );
        }
        $msg = "Sửa trạng thái thành công!";
        $this -> listItem( $msg );
    }
    
    function buildForm( $data=array() , $msg = ''){
        
        $form = new HTML_QuickForm('frmAdmin','post',$_COOKIE['re_dir']."&task=".$_GET['task'], '', "style='padding:10px 15px 0 20px;' enctype=\"multipart/form-data\"");
        
        $form -> setDefaults($data);                
        /*echo "<script type=\"text/javascript\" src=\"/view/js/upload.js\"></script>";*/
       
        
        $form -> addElement('text', 'Name', 'Tiêu đề', array('size' => 50, 'maxlength' => 255));
        //$form -> addElement('select','Type','Kiểu',array('up'=>'UploadFile','you'=>'Youtobe'));
       // $form -> addElement('select','Type','Kiểu',array('you'=>'Youtobe','up'=>'UploadFile'));
	    $form -> addElement('select','Type','Kiểu',array('you'=>'Youtobe'));
        
		/*$link_file[] = &HTML_QuickForm::createElement('text', 'Code', '', array('size' => 50,'id'=>'Code'));
        $url_target = "/upload/video/";
        $id_element = "Code";
        
        $link_file[] = &HTML_QuickForm::createElement('button',null,'Upload',array('onclick'=>'window.open(\'/lib/swfupload/demo/index.php?id_element='.$id_element.'&url_target='.$url_target.'\', \'UploadFile\',\'location=0,statusbar=0,scrollbars=0,menubar=0,toolbar=no,width=600,height=200\')'));
        
        $form -> addGroup($link_file,"","Đường dẫn");*/
		
		$form->addElement('file', 'Photo', 'Ảnh', array('id'=>'imgFile'));
		
		if($_GET['task']=='edit')
			$form->addElement('static', null, '',"<a href='".SITE_URL.$data['Photo']."' onclick='return hs.expand(this)' class='highslide'><img src='".SITE_URL.$data['Photo']."' width=100 hight=100 border=0></a>");
		
		$content_editor=parent::editor('Content',$data['Content'],array('width'=>'800', 'height'=>'400'));
        $form -> addElement('static',NULL,'Youtobe',$content_editor);
		
		$date_time = parent::date_time('CreateDate',($data['CreateDate']) ? ($data['CreateDate']) : date("Y-m-d h:i"));			
		$form -> addElement('static',NULL,'Ngày tạo',$date_time);
        
        $form -> addElement('checkbox', 'Status', 'Kích hoạt');
        
        $btn_group[] = $form -> createElement('submit',null,'Hoàn tất',array("style"=> "border:1px solid gray; padding:0 5px 0 5px;"));        
        $btn_group[] = $form -> createElement('button',null,'Quay lại',array('onclick'=>'window.location.href = \''.$_COOKIE['re_dir'].'\'', "style"=> "border:1px solid gray;"));      
        $form -> addGroup($btn_group);
      
        $form->addElement('hidden', 'id', $data['id']);        
        
        //$form -> addRule('Name','Tiêu đề không được để trống','required',null,'client');
        
        if( $form -> validate())
        {
            if($_FILES['Photo']['name']!='')
			{
				$this->deleteImage($this->id, "Photo", $this->imgPath);
				$FileName = rand().'_'.$_FILES['Photo']['name'];												
				parent::crop_image($_FILES['Photo']['tmp_name'],$FileName, $this->imgPath);				
				$_POST['Photo'] = $this->imgPathShort.$FileName;				
			}
            $aData  = array(
				"LangID" 		=> $_POST['LangID']?$_POST['LangID']:1,
                "Name" => $_POST['Name'],
				"Type" => $_POST['Type'],
				"Content" => $_POST['Content'],
                "Code" => $_POST['Code'],
				"CreateDate" 	=> $_POST['CreateDate'],
                "Status"     => $_POST['Status'],
            );
               
			if ($_POST['Photo']!='')
				$aData['Photo'] = $_POST['Photo']; 
            if( !$_POST['id'] ){
                $id = $this -> bsgDb -> insert($aData);                 
                $msg = "Thêm thành công! ";
            }else {    
                
                $id = $_POST['id'];    
                $this -> bsgDb -> updateWithPk($id, $aData);
                $msg = "Chỉnh sửa thành công ";
            }
            
            $this -> redirect($_COOKIE['re_dir']. "&msg={$msg}");
        }
        
        $form->display();
    }
    
    function deleteImage($id, $field, $path){
        if($id == '')
            return;
        $imgpath = $path.$this->db->getOne("SELECT $field FROM ".$this->table." WHERE id = $id");
        if(is_file($imgpath))            
            @unlink($imgpath);
    }
    
    function getCategory(){
        global $oDb;
        $table = 'tbl_video_category';
        $sql = "SELECT id,Name FROM $table";
        $category = $oDb->getAssoc($sql);        
        return $category;
    }
    
    function changeStatus( $itemId , $status ){
        $aData = array( 'Status' => $status );
        $this -> bsgDb -> updateWithPk( $itemId, $aData );
        return true;
    }
    
    function listItem( $sMsg= '' )
    {        
        global $oDb;
        global $oDatagrid;                
        
        $root_path = "Quản lý video > Danh sách video";                        
        $submit_url= "/index.php?".$_SERVER['QUERY_STRING'];
        
        $table = $this -> table;
        
        $arr_filter= array(
            /*array(
                'field' => 'Name',
                'display' => 'Tiêu đề',
                'type' => 'text',
                'name' => 'filter_title',
                'selected' => $_REQUEST['filter_title'],
                'filterable' => true
            ),*/
            
            array(
                'field' => 'Status',
                'display' => 'Trạng thái',                
                'name' => 'filter_show',
                'selected' => $_REQUEST['filter_show'],
                'options' => array('Vô hiệu','Kích hoạt'),
                'filterable' => true
            )
            
        ); 
        
        $arr_cols= array(        
            
            array(
                "field" => "id",                    
                "primary_key" =>true,
                "display" => "Id",                
                "align" => "center",
                "sortable" => true
            ),    
            array(
                "field" => "Name",
                "display" => "Tiêu đề",
                "align"    => 'left',                
                "datatype" => "text",
                "sortable" => true
            ),
			array(
				"field" => "Photo",
				"display" => "Ảnh",
				"datatype" 		=> "img",
				"img_path"		=> SITE_URL,
				"sortable" => true,
				"order_default" => "asc"
			),
			/*array(
				"field" => "Type",
				"display" => "Kiểu",
				"align"	=> 'left',				
				"datatype" => "value_set",
				"case"	=> array('up'=>"UploadFile",'you'=>'Youtobe'),
				"sortable" => true
			),*/
			array(
				"field" => "CreateDate",
				"display" => "Ngày tạo",
				"align"	=> 'left',				
				"datatype" => "date",
				"sortable" => true
			),
            array(
                "field" => "Status",
                "display" => "Trạng thái",                
                "datatype" => "publish",
                "sortable" => true
            ),        
        );        
        
        $arr_check = array(
            array(
                "task" => "delete_all",
                "confirm"    => "Xác nhận xóa?",
                "display" => "Xóa"
            ),
            array(
                "task" => "public_all",
                "confirm"    => "Xác nhận thay đổi trạng thái?",
                "display" => "Kích hoạt"
            ),
            array(
                "task" => "unpublic_all",
                "confirm"    => "Xác nhận thay đổi trạng thái?",
                "display" => "Vô hiệu"
            )            
        );
        if( $sMsg )
            $oDatagrid -> setMessage( $sMsg );
        $oDatagrid->display_datagrid($table, $arr_cols, $arr_filter, $submit_url, $this -> getAct() ,120, $root_path, false ,$arr_check);        
        
    }        
    function setDefault($cat_id)
    {
        $sql = "UPDATE  tbl_gallery_item SET Default_Photo=0 WHERE CatID=$cat_id";
        $this -> bsgDb->querySql($sql);
    }
}

?>