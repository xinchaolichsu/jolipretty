<div class="box_center">
	<div class="box_center_title">{#VIDEO#}</div>
    <div class="box_center_content">
    	{foreach from = $video item = item name = item}
        	<div class="listMod" {if $smarty.foreach.item.last} style="border:none;"{/if}>
            	<div class="img">
                	{if $item.Photo}
                    	<a href="{$smarty.const.SITE_URL}video/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}"><img src="{$item.Photo}" class="photo" alt="{$item.Name}"/></a>
					{/if}
                </div>
                <div class="info">
                	<div class="name">
                    	<a href="{$smarty.const.SITE_URL}video/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{$item.Name}</a>
                    </div>
                    <div class="date">
                    	{$item.CreateDate|date_format:"%H:%M - %d/%m/%Y"}
                    </div>
                    <div class="sum">
                    	{$item.Summarise|truncate:200}
                    </div>
                    <div class="view">
                    	<a href="{$smarty.const.SITE_URL}video/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{#details#}</a>
                    </div>
                </div>
            </div>
    	{/foreach}
        {if $num_rows > $limit}
        <div class="listPage">
            {$num_rows|page:$limit:$smarty.get.page:$paging_path}
        </div>
        {/if} 
	</div>
</div>




{********
<div class="center-title">
{#video#}
</div>
<hr class="center-line" />
<div class="listItem">
{foreach from = $video name = item item = item}
    <div class="one-gallery"{if $smarty.foreach.item.index % 3 == 0} style="margin-left: 0px;"{/if}>
        <a href="/video/{$item.id}-{$item.Name|remove_marks}.html"><img src="/upload/video/{$item.Photo}" width="195" height="140" style="margin-bottom: 5px;" /></a>
        
        <a href="/video/{$item.id}-{$item.Name|remove_marks}.html">{$item.Name_lang}</a>
    </div>
{/foreach}
</div>
*********}