<script src="/lib/flowplayer/js/flashembed.min.js" type="text/javascript"></script>
{literal}
<script> 
	function playVideo(video_name)
	{
			flashembed("mvmain_center", 
		  	{
				src:'/lib/flowplayer/player/FlowPlayerLight.swf',
				width: 560,
				height: 315
		  	},
		  	{config: {   
				autoPlay: false,
				autoBuffering: true,
				showVolumeSlider : false,
				showFullScreenButton : true,
				wmode : 'transparent',
				showMenu: false,
				showScrubber : true,
		      	/*controlBarBackgroundColor:'0xc1eab4',*/
				initialScale: 'fit',
				videoFile: '/upload/video/'+video_name
		  	}}
			);	
	}
	function changeName_center(video_id)
	{
		url='/index.php?mod=video&task=video_name_ajax_center&ajax&video_id='+video_id;
			$.get(url, function(huy){
			$("#video_name_ajax_center").html(huy);
		});
	}
	function changContent_center(video_id)
	{
		url='/index.php?mod=video&task=video_content_ajax_center&ajax&video_id='+video_id;
			$.get(url, function(huy){
			$("#main_you_center").html(huy);
		});
	}
	function changCode_center(video_id)
	{
		url='/index.php?mod=video&task=video_code_ajax_center&ajax&video_id='+video_id;
			$.get(url, function(huy){
			$("#main_you_center").html(huy);
		});
	}
</script>

{/literal}
<script type="text/javascript">	
	var s_fi = '{$video.Code}';
</script>   
{literal}	
<script type="text/javascript">
	playVideo(s_fi);
</script>	
{/literal}
<!--Begin Video-->
<div class="box_center">
	<div class="box_center_title">
    	<div class="box_center_title">
            <div class="box_center_title_left">{#video#}</div>
            <div class="box_center_title_left_img"></div>
        </div>
        <div class="box_center_title_left_img"></div>
	</div>	
	<div class="box_center_content">
    
		<div  style="width:100%; float:left;">
			<div id="main_you_center">
				{if $video.Type=='up'}
					<div id="mvmain_center" align="center"></div>
				{else}
					<div align="center">{$video.Content}</div>
				{/if}
			</div>
			<div id="video_name_ajax_center" style="text-align:center; color:#8c3433; font-weight:bold; padding:5px 0 5px 0;">{$video.Name}</div>
		</div>
		<div style="float:left; width:100%;">
			<div style="float:left; width:100%; height:20px; line-height:20px; font-weight:bold; color:#9a2b00; text-indent:10px; border-bottom:1px solid #bc6c2d; margin-bottom:8px;">{#other_video#}</div>
			<div style="width:97%; float:left; padding-left:15px; line-height:18px;">
				{foreach from = $other_video item = item name = item}
					{if $item.Type=='up'}
					-&nbsp;<a onclick="changCode_center('{$item.id}'); changeName_center({$item.id})" style="color:#4c4c4e; cursor:pointer">{$item.Name}</a>&nbsp;<span style="color:#8e3531">({$item.CreateDate|date_format:"%d/%m/%Y"})</span><br />
					{else}
					-&nbsp;<a onclick="changContent_center('{$item.id}'); changeName_center({$item.id})" style="color:#4c4c4e; cursor:pointer;">{$item.Name}</a>&nbsp;<span style="color:#8e3531">({$item.CreateDate|date_format:"%d/%m/%Y"})</span><br />
					{/if}
				{foreachelse}
					<div style="width:95%; height:auto; float:left; text-align:center; padding:5px; color:red;">{if $smarty.get.task=='productSearch'}{#foundno#}{else}{#updating#}{/if}</div>
				{/foreach}
			</div>            		
		</div>
        
	</div>
</div>
<!--End Video-->
