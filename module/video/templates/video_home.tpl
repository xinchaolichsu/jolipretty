<script src="/lib/flowplayer/js/flashembed.min.js" type="text/javascript"></script>
{literal}
<script> 
	function playVideo(video_name)
	{
			flashembed("mvmain", 
		  	{
				src:'/lib/flowplayer/player/FlowPlayerLight.swf',
				width: 210,
				height: 200
		  	},
		  	{config: {   
				autoPlay: false,
				autoBuffering: true,
				showVolumeSlider : false,
				showFullScreenButton : true,
				wmode : 'transparent',
				showMenu: false,
				showScrubber : true,
		     	<!--controlBarBackgroundColor:'0xc1eab4',-->
				initialScale: 'fit',
				videoFile: '/upload/video/'+video_name
		  	}}
			);	
	}
	function changeName(video_id)
	{
		url='/index.php?mod=video&task=video_name_ajax&ajax&video_id='+video_id;
			$.get(url, function(huy){
			$("#video_name_ajax").html(huy);
		});
	}
	function changContent(video_id)
	{
		url='/index.php?mod=video&task=video_content_ajax&ajax&video_id='+video_id;
			$.get(url, function(huy){
			$("#main_you").html(huy);
		});
	}
	function changCode(video_id)
	{
		url='/index.php?mod=video&task=video_code_ajax&ajax&video_id='+video_id;
			$.get(url, function(huy){
			$("#main_you").html(huy);
		});
	}
	function Allvideo()
	{
		url='/index.php?mod=video&task=video_all_ajax&ajax';
			$.get(url, function(huy){
			$("#other_video").html(huy);
		});
		$('#view_all_video').remove();
	}
</script>

{/literal}
<script type="text/javascript">	
	var s_fi = '{$video.Code}';
</script>   
{literal}	
<script type="text/javascript">
	playVideo(s_fi);
</script>	
{/literal}
<!--Begin Video-->
<div class="box_left">
	<div class="side_top"></div>
	<div class="side_title">Video</div>
    <div class="side_content">
    
	<div style="float:left; width:100%; padding-top:10px;">
		<div  style="width:90%;" align="center">
        		<div id="video_name_ajax" style="text-align:center; color:#000; width:100%; padding:3px 10px 3px 10px; font-weight:bold;">
                    {$video.Name}
                </div>
			<div id="main_you">
				{if $video.Type=='up'}
					<div id="mvmain" style="padding-left:15px;"></div>
				{else}
					<div align="center" style="padding-left:15px;">{$video.Content}</div>
				{/if}
			</div>
		</div>
		<div class="video_list" style="float:left; padding-left:20px; width:210px;">
			<div style="width:100%; color:#ae6d37; font-weight:bold; padding-bottom:0px; padding-top:5px;">{#other_video#} :</div>
				<table cellpadding="0" cellspacing="6" width="100%">
				{foreach from = $other_video item = item name = item}
					{if $item.Type=='up'}<tr><td class="video_item"><a onclick="changCode('{$item.id}'); changeName({$item.id})" style="cursor:pointer">{$item.Name|truncate:45}</a>&nbsp;<span style="color:#8e3531">({$item.CreateDate|date_format:"%d/%m/%Y"})</span></td></tr>
					{else}<tr><td class="video_item"><a onclick="changContent('{$item.id}'); changeName({$item.id})" style="cursor:pointer">{$item.Name|truncate:45}</a>&nbsp;<span style="color:#8e3531">({$item.CreateDate|date_format:"%d/%m/%Y"})</span></td></tr>
					{/if}
				{/foreach}
				</table>
			<div style="width:100%; color:#ae6d37; font-weight:bold; text-align:right; cursor:pointer;"><span onclick="hrefUrl('{$smarty.const.SITE_URL}video/all/');">{#view_all#}</span></div>
		</div>
	</div>
    
	
    </div>
	<div class="side_bottom"></div>
</div>
<!--End Video-->