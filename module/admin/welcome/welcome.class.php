<?php
class welcome extends Bsg_Module_Base{
	var $smarty;
	var $db;
	var $url="";
    var $datagrid;
	var $id;
	var $table='';
	var $arrAction;
	
	public function __construct($oSmarty, $oDb, $oDatagrid)
	{
	
		$this -> smarty = $oSmarty;
		$this -> db = $oDb;		
        $this -> datagrid = $oDatagrid;
		$this -> id=$_REQUEST[id];		
		$this -> table = "tbl_sys_menu";	 		
		parent::__construct($oDb);		
		$this->bsgDb->setTable($this->table);	
	    //parent::getRootPath(" Quản trị hệ thống > Danh sách");
	}
	
	function run($task)
	{
		global $oDb;
				
				switch ($task)
				{						
					default:								
						$this->showpanel();				
					break;
				}
	}
	
	
	function getPageInfo($task){
		return true;
	} 
	
	function showpanel(){
	    global $oDb,$oSmarty; 
	    $arr_link = $oDb->getAll("SELECT * FROM tbl_sys_menu WHERE ParentID = 0 AND Status = 1");
	    
	    foreach ($arr_link as $modules)
	    {
	    	if ($this->check_permission('0') && $modules['Editable']==0)
	    		$sys_module[] = $modules;
	    	elseif ($modules['Editable']==1 && $this->checkPermission($modules['id']))
	    		$site_module[] = $modules;
	    }
	    
		$oSmarty->assign('sys_module',$sys_module); 
		$oSmarty->assign('site_module',$site_module);
		$oSmarty->display('welcome.tpl'); 
	}
	
	function check_permission($module_id)
	{
		global $oDb;
		$permission = $oDb->getCol("SELECT ModuleID FROM tbl_sys_users_module WHERE UserID = {$_SESSION['userid']}");
		
		if (in_array($module_id,$permission) || $_SESSION['userid']==1)
			return true;
		else 
			return false;
	}
	
}
?>
