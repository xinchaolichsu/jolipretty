var ImageUploadAllow = ".(jpg|gif|jpeg|png|bmp)$" ;
var oUploadAllowedExtRegex = new RegExp( ImageUploadAllow, 'i' ) ;

function CheckUpload()
{
	var sFile = document.getElementById('imgFile').value ;

	if ( ImageUploadAllow.length > 0 && !oUploadAllowedExtRegex.test( sFile ) )
	{
		alert('File upload không đúng định dạng!.');
		return false ;
	}

	return true ;
}