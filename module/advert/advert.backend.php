<?php
/**
 * VietEsoft
 * ===============================================================================================
 * Copyright 2010.
 * Website: http://www.vietesfot.com, http://thietkewebsitetrongoi.net
 * -----------------------------------------------------------------------------------------------
 * Phát triển bới VietEsoft
 * -----------------------------------------------------------------------------------------------
 * Chú ý: Tất cả các hàm, phương thức được viết mới do mình tự định nghĩa phải để lại comment gồm:
 	- Người viết
	- Ngày-tháng-năm
	- Chức năng
 * ===============================================================================================
 * Quản lý quảng cáo
*/
if (!defined('IN_VES')) die('Hacking attempt');
class advertBackEnd extends Bsg_Module_Base{
	var $smarty;
	var $db;
	var $url="";
    var $datagrid;
	var $id;
	var $imgPath;
	var $imgPathShort;
	var $table='';
	var $arrAction;
	public function __construct($oSmarty, $oDb, $oDatagrid)
	{	
		$this -> smarty = $oSmarty;
		$this -> db = $oDb;		
        $this -> datagrid = $oDatagrid;
		$this -> id=$_REQUEST[id];		
		$this -> table	="tbl_advert";	 		
		parent::__construct($oDb);		
		$this->bsgDb->setTable($this->table);
		$this->imgPath = SITE_DIR."upload/advert/";
		$this->imgPathShort = "/upload/advert/";
	}
	
	function run($task)
	{	
		switch( $task ){
			case 'add':
				$this -> addItem();
				break;
			case 'edit':
				$this -> editItem();
				break;
			case 'delete':
				$this -> deleteItem();
				break;
			case 'delete_all':
				$this -> deleteItems();
				break;
			case 'change_status':				
				$this -> changeStatus($_GET['id'], $_GET['status']);
				break;
			case 'public_all':						
				$this -> changeStatusMultiple( 1 );
				break;
			case 'unpublic_all':						
				$this -> changeStatusMultiple( 0 );
				break;
			case 'save_order':
				$this -> saveOrder();
				break;
			default:					
				$this -> listItem( $_GET['msg'] );		
				break;
		}
	}
	
	function getPageInfo()
	{
		return true;
	}

	function saveOrder(){	
		$aItem = $_GET['Ordering'];
		if(is_array($aItem) && count( $aItem ) > 0){
			// save order for item.
			foreach( $aItem as $key => $value){
				if( !is_numeric($value)) $value = 0;				
				$this -> bsgDb -> updateWithPk( $key, array('Ordering' => $value ));
			}
		}	
		$msg = "Lưu thứ tự danh mục thành công!";
		$this -> listItem( $msg );
	}
	function addItem()
	{
		$this -> getPath("Quản lý quảng cáo > Thêm mới quảng cáo");		
		$this -> buildForm();
	}
	
	function editItem()
	{
		$id = $_GET['id'];
		$this -> getPath("Quản lý quảng cáo > Chỉnh sửa quảng cáo");	
		$row = $this -> bsgDb -> getRow( $id );
		$this -> buildForm( $row );
	}

	function deleteItem()
	{
		global  $oDb;
		$id = $_GET["id"];
		$this -> bsgDb -> deleteWithPk( $id );
		$msg = "Xóa quảng cáo thành công!";
		$this -> listItem( $msg );
	}
	
	function deleteItems()
	{
		$aItems	 = $_GET['arr_check'];
		if(is_array( $aItems) && count( $aItems) > 0){
			$sItems = implode( ',', $aItems );
			$this -> bsgDb -> deleteWithPk( $sItems );
		}
		$msg = "Xóa (các) quảng cáo thành công!";
		$this -> listItem( $msg );
	}
	
	function changeStatusMultiple( $status = 0 )
	{
		$aItems	 = $_GET['arr_check'];
		if(is_array( $aItems) && count( $aItems) > 0){
			$sItems = implode( ',', $aItems );
			$this -> bsgDb -> updateWithPk( $sItems, array("Status" => $status) );
		}
		$msg = "Sửa trạng thái quảng cáo thành công!";
		$this -> listItem( $msg );
	}
	
	function buildForm( $data=array() , $msg = ''){
		
		$form = new HTML_QuickForm('frmAdmin','post',$_COOKIE['re_dir']."&task=".$_GET['task'], '', "style='padding:10px 15px 0 20px;'");
		
		$form -> setDefaults($data);
		
		if(MULTI_LANGUAGE)
		{
			$lang = parent::loadLang();
			$form -> addElement('select', 'LangID', 'Ngôn ngữ', $lang);
		}
		
		$form -> addElement('text', 'Name', 'Tiêu đề', array('size' => 50, 'maxlength' => 255));
		
		$form -> addElement('select','Type','Kiểu',array('image'=>'Ảnh','FLA'=>'Flash'));
		
		//$form -> addElement('select','Type','Kiểu',array('image'=>'Ảnh'));
		
		$form -> addElement('text', 'Link', 'Đường dẫn', array('size' => 50, 'maxlength' => 255));
		$form -> addElement('text', 'TextAdvert', 'Text quảng cáo', array('size' => 50, 'maxlength' => 255));
		
		$form->addElement('file', 'Photo', 'Ảnh');
		
		if($_GET['task']=='edit')
			$form->addElement('static', null, '',"<a href='".SITE_URL.$data['Photo']."' onclick='return hs.expand(this)' class='highslide'><img src='".SITE_URL.$data['Photo']."' width=100 hight=100 border=0></a>");
			
		$form -> addElement('text', 'Height', 'Chiều cao (Flash)', array('size' => 10, 'maxlength' => 50));
		
		$form -> addElement('text', 'Ordering', 'Thứ tự', array('size' => 10, 'maxlength' => 50));
		
		//$form -> addElement('checkbox', 'Is_left', 'Quảng cáo trái (Rộng 180px)');
		//$form -> addElement('checkbox', 'trai', 'Chạy trái (Rộng 120px)');
		//$form -> addElement('checkbox', 'phai', 'Chạy phải (Rộng 120px)');		
		
		//form -> addElement('checkbox', 'center', 'Quảng cáo top');		
		//$form -> addElement('checkbox', 'Is_right', 'Quảng cáo phải (Rộng 240px)');		
		//$form -> addElement('checkbox', 'giua1', 'Top Home(810x115 px)');
		//$form -> addElement('checkbox', 'giua2', 'Bottom Home(810x115 px)');
		//$form -> addElement('checkbox', 'giua3', 'Top banner');		
		//$form -> addElement('checkbox', 'popup', 'Popup');
		
		
		
		
		$form -> addElement('checkbox', 'Status', 'Kích hoạt');
		
		$btn_group[] = $form -> createElement('submit',null,'Hoàn tất',array("style"=> "border:1px solid gray; padding:0 5px 0 5px;"));		
        $btn_group[] = $form -> createElement('button',null,'Quay lại',array('onclick'=>'window.location.href = \''.$_COOKIE['re_dir'].'\'', "style"=> "border:1px solid gray;"));      
        $form -> addGroup($btn_group);
      
		$form->addElement('hidden', 'id', $data['id']);		
		
		if( $form -> validate())
		{
			if($_FILES['Photo']['name']!='')
			{
				$this->deleteImage($this->id, "Photo", $this->imgPath);
				$FileName = rand().'_'.$_FILES['Photo']['name'];												
				parent::crop_image($_FILES['Photo']['tmp_name'],$FileName, $this->imgPath);				
				$_POST['Photo'] = $this->imgPathShort.$FileName;				
			}
			
			$aData  = array(
				"Name" => $_POST['Name'],
				"LangID" => $_POST['LangID'],
				"popup" => $_POST['popup'],
				"TextAdvert" => $_POST['TextAdvert'],
				"Type" => $_POST['Type'],
				"Height" => $_POST['Height'],
				"Link" => $_POST['Link'],
				"Ordering" 	=> $_POST['Ordering'],
				"Status" 	=> $_POST['Status'],
			);
			if ($_POST['Photo']!='')
				$aData['Photo'] = $_POST['Photo'];
				
			if( !$_POST['id'] ){
				
				 $id = $this -> bsgDb -> insert($aData);				 
				 $msg = "Thêm quảng cáo thành công! ";
			}else {
				$id = $_POST['id'];				
				$this -> bsgDb -> updateWithPk($id, $aData);
				$msg = "Chỉnh sửa quảng cáo thành công ";
			}
			
			$this -> redirect($_COOKIE['re_dir']. "&msg={$msg}");
		}
		
		$form->display();
	}
	
	function deleteImage($str_id, $field, $path){
		if($str_id == '')
			return;
		$arr_id = explode(',', $str_id);
		foreach($arr_id as $id){
			$imgpath = $path.$this->db->getOne("SELECT {$field} FROM ".$this->table." WHERE id = {$id}");
			if(is_file($imgpath))			
				@unlink($imgpath);
		}
	}
	
	function changeStatus( $itemId , $status ){
		$aData = array( 'Status' => $status );
		$this -> bsgDb -> updateWithPk( $itemId, $aData );
		$msg = "Sửa trạng thái quảng cáo thành công!";
		$this -> listItem( $msg );
		//return true;
	}
	
	function listItem( $sMsg= '' )
	{		
		global $oDb;
		global $oDatagrid;				
		
		$root_path = "Quản lý quảng cáo > Danh sách quảng cáo";						
		$submit_url= "/index.php?".$_SERVER['QUERY_STRING'];
		
		$table = $this -> table;
		
		$arr_filter= array(
			array(
				'field' => 'Name',
				'display' => 'Tiêu đề',
				'type' => 'text',
				'name' => 'filter_title',
				'selected' => $_REQUEST['filter_title'],
				'filterable' => true
			),
		);
		if(MULTI_LANGUAGE)
            $arr_filter[] = array(
                    'field' => 'LangID',
                    'display' => 'Ngôn ngữ',                
                    'name' => 'filter_cat',
                    'selected' => $_REQUEST['filter_cat'],
                    'options' => parent::loadLang(),
                    'filterable' => true
                );			
			$arr_filter[] = array(
				'field' => 'Status',
				'display' => 'Trạng thái',				
				'name' => 'filter_show',
				'selected' => $_REQUEST['filter_show'],
				'options' => array('Vô hiệu','Kích hoạt'),
				'filterable' => true
			);
		
		$arr_cols= array(		
			
			array(
				"field" => "id",					
				"primary_key" =>true,
				"display" => "Id",				
				"align" => "center",
				"sortable" => true
			),	
			array(
				"field" => "Name",
				"display" => "Tiêu đề",
				'link' => SITE_URL.'index.php?mod=admin&amod='.$_GET['amod'].'&atask='.$_GET['atask'].'&tab=1&frame&task=edit',
				"align"	=> 'left',				
				"datatype" => "text",
				"sortable" => true
			),
		);
			if(MULTI_LANGUAGE)
		 	$arr_cols[] = array(
               "field" => "LangID",
				"display" => "Ngôn ngữ",
				"sql"	=>	"SELECT name FROM tbl_sys_lang WHERE id= LangID",
				"align"	=> 'left',				
				"datatype" => "text",
				"sortable" => true
            );
			$arr_cols_more = array(
			array(
				"field" => "Type",
				"display" => "Kiểu",
				"align"	=> 'left',				
				"datatype" => "value_set",
				"case"	=> array('image'=>"Ảnh",'FLA'=>'Flash'),
				"case"	=> array('image'=>"Ảnh"),
				"sortable" => true
			),
			array(
				"field" => "Link",
				"display" => "Đường dẫn",
				"align"	=> 'left',				
				"datatype" => "text",
				"sortable" => true
			),	
			array(
				"field" => "Photo",
				"display" => "Ảnh",
				"datatype" 		=> "img",
				"img_path"		=> SITE_URL,
				"sortable" => true,
				"order_default" => "asc"
			),
			/*array(
				"field" => "popup",
				"display" => "Popup",				
				"datatype" => "boolean",
				"sortable" => true
			),*/
			array(
				"field" => "Ordering",
				"display" => "Thứ tự",
				"datatype" 		=> "order",
				"sortable" => true,
				"order_default" => "asc"
			),
			array(
				"field" => "Status",
				"display" => "Trạng thái",				
				"datatype" => "publish",
				"sortable" => true
			),		
		);	
		$arr_cols = array_merge($arr_cols,$arr_cols_more);	
		
		$arr_check = array(
			array(
				"task" => "delete_all",
				"confirm"	=> "Xác nhận xóa?",
				"display" => "Xóa"
			),
			array(
				"task" => "public_all",
				"confirm"	=> "Xác nhận thay đổi trạng thái?",
				"display" => "Kích hoạt"
			),
			array(
				"task" => "unpublic_all",
				"confirm"	=> "Xác nhận thay đổi trạng thái?",
				"display" => "Vô hiệu"
			)			
		);
		if( $sMsg )
			$oDatagrid -> setMessage( $sMsg );
		$oDatagrid->display_datagrid($table, $arr_cols, $arr_filter, $submit_url, $this -> getAct() ,120, $root_path, false ,$arr_check);		
		
	}		
	
}

?>