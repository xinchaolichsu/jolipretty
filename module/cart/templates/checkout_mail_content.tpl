<center><span style="font-size:18px; font-weight:bold; text-transform:uppercase">Khách đặt hàng qua website Photoking</span></center>
<p>
<span style="font-size:16px; font-weight:bold">Thông tin khách hàng</span><br />
Họ tên: <b>{$info.name}</b><br />
E-mail: <b>{$info.email}</b><br />
Địa chỉ: <b>{$info.address}</b><br />
Điện thoại: <b>{$info.phone}</b><br />
</p>
<p>
<span style="font-size:16px; font-weight:bold">Thông tin sản phẩm</span><br />
{foreach from=$cart item=item name=cart}
{assign var="pid" value=$item.product.id}
<b>{$smarty.foreach.cart.index+1}. {if $item.product_type==1}<a href='{$smarty.const.SITE_URL}{"index.php?mod=product&task=view&id=$pid"|url_friendly}'>{$item.product.title}</a>{else}{$item.product.number}{/if}</b><br />
- Giá sản phẩm: <b>{$item.product.price|number_format:0:".":"."} VNĐ</b><br />
- Số lượng: <b>{$item.quantity}</b><br />
- Thành tiền: <b>{$item.subtotal}</b><br />
-------------------------------<br />
{/foreach}
-- Tổng số: <strong>{$total|number_format:0:".":"."} VNĐ</strong><br />
</p>
<p>
Yêu cầu thêm: <b>{$info.addition_request}</b><br />
</p>