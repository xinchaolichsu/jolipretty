{literal}
<script type="text/javascript">
	function sub_qty(pid,curr_size)
	{
		var qty = $("#qty_"+pid+"_"+curr_size).val();
		qty--;
		if(qty==0)
		{
			alert('Number less than 0, the system will automatically reset the number 1');	
			return false;
		}
		else {
			$("#qty_"+pid+"_"+curr_size).val(qty);
			$.ajax({
				type: "GET",
				url: "/index.php?mod=cart&task=update_quantity&nvu=sub&ajax",
				data: "pid="+pid+"&qty="+qty+"&size="+curr_size,
				success: function(response_text){
					$("#sp_"+pid+"_"+curr_size).html(response_text);
				}
			});	
		}
	}
	function add_qty(pid,curr_size,curr_size_numb)
	{
		var qty = $("#qty_"+pid+"_"+curr_size).val();
		qty++;
		if(qty <= curr_size_numb)
		{
			if(curr_size_numb < 1 )
			{
				alert('There is not enough stock for your selection');
				return false;
			}
			else
			{
				$("#qty_"+pid+"_"+curr_size).val(qty);
					$.ajax({
					type: "GET",
					url: "/index.php?mod=cart&task=update_quantity&nvu=add&ajax",
					data: "pid="+pid+"&qty="+qty+"&size="+curr_size,
					success: function(response_text){
						$("#sp_"+pid+"_"+curr_size).html(response_text);
					}
				});
			}
		}
		else
			alert('There is not enough stock for your selection');
	}
	function priceForYou(qty){
	    $("#Priceforyou").fadeOut();
		$.ajax({
            type: "GET",
            url: "/index.php?mod=product&task=Priceforyou&ajax",
            data: "qty="+qty+"&id={/literal}{$smarty.get.id}{literal}",
			success: function(response_text){
                $("#Priceforyou").html(response_text);
				$("#Priceforyou").fadeIn();
            }
        });
	}
</script>
{/literal}
<div class="box_center1">		
<div class="box_center_content1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
		<form name="frm_cart123" action="" method="post">
			 <table class="" width="100%" cellpadding="4" cellspacing="0"  align="center" style=" color:#454a50; font-size:11px">
               <tr><td colspan="4" style="color:#FF0000; font-size:14px;">{$msg_voucher}</td></tr>
			   <tr align="center" bgcolor="#fcfbfb" style="border:1px solid #d0d0d0;">
                    <th style="border:1px solid #d0d0d0; border-right:none;" width="20%">{#PRODUCT#}</th>
                    <th style="border-bottom:1px solid #d0d0d0;border-top:1px solid #d0d0d0;" width="30%">{#description#}</th>
                    <th style="border-bottom:1px solid #d0d0d0;border-top:1px solid #d0d0d0;" width="5%">Qty</th>				   	
					<th style="border:1px solid #d0d0d0; border-left:none;" width="13%">Price</th>
			   </tr>
			   {foreach item=item from=$result name=item}
			   <tr>			
                   <td bgcolor="#FFFFFF">				   		
				   		<a href='{$smarty.const.SITE_URL}product/{$pid}/{$item.product.Name|remove_marks}.html'><img src="{$smarty.const.SITE_URL}{$item.product.Photo}" height="190" border="0" /></a><br /><br />
				   </td>
                   <td  align="left" style="color:#704b3a; font-weight:bold">
                   <a href='{$smarty.const.SITE_URL}product/{$pid}/{$item.product.Name|remove_marks}.html' style="color:#6c6c6c; font-weight:bold">{$item.product.Name}</a> <br />
                   <span style="color:#424242; font-weight:400;">Size: {$item.product.size}</span> 
                   {if $item.product.color_name}
                   <p style="color:#424242; font-weight:400;">Color: {$item.product.color_name}</p>  <br /><br />
                   {/if}
                   <input type="button" class="bg_remove" value="Remove" onclick='location.href="/index.php?mod=cart&ajax&task=delete&pid={$item.product.id}&size={$item.product.sizeid}";' /></td> 
                   <td  width="110" align="center" valign="middle">
                        <div class="add_quantity">
                            <div class="number">
                                <input type="text" class="totalsp form-control" id="qty_{$item.product.id}_{$item.product.sizeid}" name="qty" value="{$item.quantity}" style="font-weight:bold; color:#704b3a; border:none" onblur="priceForYou(this.value);" />
                            </div>
                            <div class="click_arrow">
                                <div class="add_qty" onclick="add_qty({$item.product.id},{$item.product.sizeid},{$item.product.curr_size_numb});"></div>
                                <div class="sub_qty" onclick="sub_qty({$item.product.id},{$item.product.sizeid},{$item.product.curr_size_numb});"></div>
                            </div>
                        </div>
				   </td>
				   <td  align="center">
                   
                   <span style="color:#FF0000; font-weight:bold" id="sp_{$item.product.id}_{$item.product.sizeid}">$ {if $item.product.price_size}{$item.quantity*$item.product.price_size}{elseif $item.product.Price}{$item.quantity*$item.product.Price}{else}{$item.quantity*$item.product.OldPrice}{/if} </span>
                   </td>
			   </tr>
			   {foreachelse}
				<tr>
				   <td colspan="9" align="center" bgcolor="#FFFFFF"  style="color:#FF6000;">Your cart is currently empty!</td>
			   </tr>
			   {/foreach}		   
			   
			   {if $result}
			   	<tr>
				   <td bgcolor="#FFFFFF" colspan="2" align="right" style="font-weight:bold; text-transform:uppercase; color:#000000;">
                   <input class="continue_cart" type="button" value="{#continue#}" onclick='location.href="{$smarty.const.SITE_URL}product/"' />
                   <td align="center" style="color:#2b2a2a; font-weight:700;" > {#total#} :</td>
                   <td align="center" ><span id="total" style="font-weight:bold; color:#6c6c6c">&nbsp;&nbsp;$ {$total} </span></td>
                  <!-- <input type="text" style="border:none;background:#FFF;color:#F00;font-weight:bold;" name="total123" id="total123" disabled="disabled" value="{$total|number_format:0:".":"."}" />
                   <input type="hidden" id="hdn_sum" name="hdn_sum" value="{$total}" />
                   </td>				   -->
			   </tr>
			   {/if}
               <tr bgcolor="#ededed" height="42px" style="margin:10px 0; padding:10px 0;">
               		<td style="color:#7f7f7f; text-transform:uppercase; text-indent:10px;">Discount/Voucher code</td>
                    <td>
                    <form name="frm_voucher" action="" method="post"> 
                    <input class="voucher" name="voucher" type="text" value="" style="width:280px; height:26px; background:#fff; border:none;" />
                    <input class="payment_cart btn btn-primary" type="submit" style="width:60px; height:28px;" value="add" />
                    </form>
                    </td>					
               		<td></td>
                    <td></td>
               </tr>
				<tr height="50px">
                	<td align="right" colspan="9">
                </td>
			   </tr>
			 </table>
		    <div class="col-md-12 col-sm-12 col-xs-12 right">
                {if $result}
                {assign var="checkout_link" value="cart/checkinfo/"}
                {assign var="clear_link" value="/index.php?mod=cart&ajax&task=clear"}
                <input style="height: 35px;" class="payment_cart btn btn-primary" type="button" value="{#next#}" {if $useid} onclick='location.href="{$smarty.const.SITE_URL}cart/checkinfo/";' {else} onclick='location.href="{$smarty.const.SITE_URL}customer/login_cart/";' {/if} />
                {/if}
            </div>
		</form>
	</div>
</div>

