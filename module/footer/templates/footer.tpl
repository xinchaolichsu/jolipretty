<div class="col-xs-12 col-sm-12 col-md-12 lis-footer left">
    <div class="col-xs-12 col-md-3 end_clothes pd0">
        <ul>
            <li style="font-variant: small-caps;font-size: 16px; color: #000">
                CUSTOMER CARE
            </li>
            <li>
                <a href="{$smarty.const.SITE_URL}shipping/">
                    {#ship#}
                </a>
            </li>
            <li>
                <a href="{$smarty.const.SITE_URL}exchange/">
                    Exchange & Return
                </a>
            </li>
            <li>
                <a href="{$smarty.const.SITE_URL}paymentnews/">
                    Payment
                </a>
            </li>
            <li>
                <a href="{$smarty.const.SITE_URL}sizeguide/">
                    Size guide
                </a>
            </li>
            <li>
                <a href="{$smarty.const.SITE_URL}term/">
                    Terms and Conditions
                </a>
            </li>
            <li>
                <a href="{$smarty.const.SITE_URL}faq/">
                    FAQ
                </a>
            </li>
            <li>
                <a href="{$smarty.const.SITE_URL}contact/">
                    Contact
                </a>
            </li>
        </ul>
    </div>
    <div class="col-xs-12 col-md-3 end_clothes pd0">
        <ul>
            <li style="font-variant: small-caps;font-size: 16px; color: #000">
                ABOUT US
            </li>
            <li>
                <a href="{$smarty.const.SITE_URL}about/">
                    About us
                </a>
            </li>
            <li>
                <a href="{$smarty.const.SITE_URL}mediaadvertising/">
                    Media & Advertising
                </a>
            </li>
        </ul>
    </div>
    <div class="col-xs-12 col-md-3 end_clothes3a pd0">
        <div class="col-xs-12 col-sm-12 end_clothes3 pd0 connect-mobile">
            <div class="connect-with-us" style="font-variant: small-caps;font-size: 16px; margin-bottom: 10px; color: #000">CONNECT WITH US</div>
        </div>
        {php}
            loadModule("share","home");
       {/php}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-3 end_clothes3a pd0">
        {literal}
        <script>
            function regismail_home123(){
            var frm = document.frmregismailHome123;
            var email = frm.Email_regis123.value;
            var apos=email.indexOf("@");
            var dotpos=email.lastIndexOf(".");
            if(email == '' || email == 'Your Email')
            {
                alert("Please enter your email address!");
                frm.Email_regis123.focus();
                return false;
            }
            else if (apos<1||dotpos-apos<2){
                alert("Invalid email address!");
                frm.Email_regis123.select();
                return false;
            }
            return true;
        }
        </script>
        {/literal}
        <div class="col-xs-12 col-sm-12 end_clothes3 pd0" style="padding-left: 0px !important;">
            <i aria-hidden="true" class="fa fa-envelope fa-2x" style="color: #fff;margin-right: 10px">
            </i>
            <span style="font-size: 14px; margin-bottom:10px; color: #000">
                Sign up for Joli Pretty's latest news and special offers.
            </span>
        </div>
        <div class="msg_info">
        </div>
        <form method="post" name="frmregismailHome123" onsubmit="return regismail_home123()">
            <input class="form-control" name="Email_regis123" onclick="this.value=''" style="margin-top:10px; float:left;" type="email" value="Your Email"/>
            <input class="reg_submit" style="margin-top:5px" type="submit" value="Subscribe"/>
        </form>
    </div>
</div>
<script src="{$smarty.const.SITE_URL}lib/bootstrap/themes.js" type="text/javascript"></script>
