<?php
class customerBackEnd extends Bsg_Module_Base{
	var $smarty;
	var $db;
	var $url="";
    var $datagrid;
	var $id;
	var $imgPath;
	var $imgPathShort;
	var $table='';
	var $arrAction;
	public function __construct($oSmarty, $oDb, $oDatagrid)
	{	
		$this -> smarty = $oSmarty;
		$this -> db = $oDb;		
        $this -> datagrid = $oDatagrid;
		$this -> id=$_REQUEST[id];		
		$this -> table	="tbl_customer";	 		
		parent::__construct($oDb);		
		$this->bsgDb->setTable($this->table);
	}
	
	function run($task)
	{	
		switch( $task ){
            case 'change_pass':                
                $this->changePass();
                break;
			case 'sendmsg':
				$this -> sendmsg();
				break;
			case 'export_error':
				$this -> export_error();
				break;
			case 'add':
				$this -> addItem();
				break;
			case 'edit':
				$this -> editItem();
				break;
			case 'delete':
				$this -> deleteItem();
				break;
			case 'delete_all':
				$this -> deleteItems();
				break;
			case 'change_status':				
				$this -> changeStatus($_GET['id'], $_GET['status']);
				break;
			case 'public_all':						
				$this -> changeStatusMultiple( 1 );
				break;
			case 'unpublic_all':						
				$this -> changeStatusMultiple( 0 );
				break;
			case 'save_order':
				$this -> saveOrder();
				break;
			default:					
				$this -> listItem( $_GET['msg'] );		
				break;
		}
	}
	function changePass()
    {
        if($this->isPost())
        {
            $data["password"] = md5($_POST['newpassword']);               
            $this->bsgDb->updateWithPk($this->id, $data);
            $msg = "Đổi mật khẩu thành công";
            parent::redirect($_COOKIE['re_dir']."&msg={$msg}"); 
        }else
        {
            $form = new HTML_QuickForm('frmGuild','post',$_COOKIE['re_dir']."&task=".$_GET['task']);        
            $form->addElement('password', 'newpassword', 'Mật khẩu', array('size' => 50, 'maxlength' => 255));
            $form->addElement('password', 'cfpassword', "Nhập lại mật khẩu", array('size' => 50, 'maxlength' => 255));                
            $btn_group[] = &HTML_QuickForm::createElement('submit',null,'Hoàn tất');
            
            $btn_group[] = &HTML_QuickForm::createElement('button',null,'Quay lại',array('onclick'=>'window.location.href = \''.$_COOKIE['re_dir'].'\''));
          
            $form -> addGroup($btn_group);
      
            $form->addElement('hidden', 'id', $_GET["id"]);
        
            $form -> addRule('newpassword','Nhập lại mật khẩu','required',null,'client');
            $form -> addRule(array('newpassword','cfpassword'),'Hai mật khẩu không trùng nhau','compare',null,'client'); 
            $form->display();
        }            
    }
	function getPageInfo()
	{
		return true;
	}

	function sendmsg()
	{
		global $oDb,$oSmarty;	
		$form = new HTML_QuickForm('frmAdmin','post',"/index.php?mod=admin&amod=customer&atask=customer&tab=0&frame&task=".$_GET['task'], '', "style='padding:10px 15px 0 20px;'");
		$form -> setDefaults($data);
		$aItems	 = $_GET['arr_check'];
		$aItems=implode(',',$aItems);
		//$message =array(0 => "- - - Lựa chon - - -" ) + $this->getMessage();
		//$form -> addElement('select', 'FromID', 'Tin nhắn', $message);
		
		$message=$this->getMessage($data['id']);
		$form -> addElement('static', '','Tin nhắn',$message);
		
		$btn_group[] = $form -> createElement('submit',null,'Hoàn tất',array("style"=> "border:1px solid gray; padding:0 5px 0 5px;"));		
        $btn_group[] = $form -> createElement('button',null,'Quay lại',array('onclick'=>'window.location.href = \''.$_COOKIE['re_dir'].'\'', "style"=> "border:1px solid gray;"));      
        $form -> addGroup($btn_group);
      
		$form->addElement('hidden', 'id', $data['id']);		
		$form->addElement('hidden', 'a', $aItems);	
		if($_POST['tire'])
		{
			//$email_cus = $_POST['FromID'];
			$aItems	 = explode(',',$_POST['a']);//pre($aItems);die();
			$arrTire = $_POST['tire'];
			foreach($arrTire as $k=>$v)
			{
				$sql = "SELECT * FROM tbl_ttc_message WHERE Status=1 AND id={$v}";
				$ttc_msg = $oDb->getAll($sql);
				foreach($ttc_msg as $k1=>$v1)
				{
					foreach($aItems as $key=>$value)
					{
						$aData  = array(
						"IsRead"     => 0,
						"ToID"         => $value,
						"FromID"         => 0,
						"Code"			=> $this->randomPassword(),
						"Sale"			=> $v1['Sale'],
						"BeginDate"			=> $v1['BeginDate'],
						"EndDate"			=> $v1['EndDate'],
						"Subject"         => $v1['Subject'],
						"CreateDate"         => date("Y-m-d H:i"),
						"Content"             => $v1['Content'],
						);
						$res_insert = $oDb -> autoExecute("tbl_message", $aData, DB_AUTOQUERY_INSERT);	
					}
				}
			}
			$msg = "Tin nhắn đã đc gửi đến thành viên";
			$this -> listItem( $msg );
		}
		else
		$form->display();
	}
	
	function randomPassword() {
		
	    $chars = "abcdefghijkmnopqrstuvwxyz023456789";
	    srand((double)microtime()*1000000);
	    $i = 0;
	    $pass = '' ;
	    while ($i <= 5) {
	        $num = rand() % 33;
	        $tmp = substr($chars, $num, 1);
	        $pass = $pass . $tmp;
	        $i++;
	    }
	    return $pass;
	}
	
	function export_error()
	{
		$this -> getPath("Xuất danh sách thành viên");	
		$this -> exportErr();
	}
	function exportErr()
	{
        global $oDb,$oSmarty;
		require_once 'lib/PHPExcel/Classes/PHPExcel.php';
		require_once 'lib/PHPExcel/Classes/PHPExcel/Writer/Excel2007.php';
		$objPHPExcel = new PHPExcel(); // Khai bao đối tượng export
		$objPHPExcel->setActiveSheetIndex(0); // Bắt đầu từ dòng 0
		$objPHPExcel->getDefaultStyle()->getFont()->setName('Times New Roman')->setSize(12); // Xét font cho file excel
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(6);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(23);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(35);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(18);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(18);
		// set Height Row
		$objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);
		// đặt chữ ỏ giữa dòng
		$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$objPHPExcel->getActiveSheet()->getStyle("A2:F2")->getFont()->setBold(true);  // đậm nhạt
		$objPHPExcel->getActiveSheet()->SetCellValue('A2', "STT");  // add giá trị
		$objPHPExcel->getActiveSheet()->SetCellValue('B2', "Email");  // add giá trị
		$objPHPExcel->getActiveSheet()->SetCellValue('C2', "Firstname");  // add giá trị
		$objPHPExcel->getActiveSheet()->SetCellValue('D2', "LastName");  // add giá trị
		$objPHPExcel->getActiveSheet()->SetCellValue('E2', "Birthday");  // add giá trị
		$objPHPExcel->getActiveSheet()->SetCellValue('F2', "Phone");  // add giá trị
		$objPHPExcel->getActiveSheet()->SetCellValue('G2', "Address");  // add giá trị
		$objPHPExcel->getActiveSheet()->SetCellValue('H2', "City");  // add giá trị
		
		//select
		$aItems	 = $_GET['arr_check'];
		if(is_array( $aItems) && count( $aItems) > 0)
		{
			$sItems = implode( ',', $aItems );
				$sql = "SELECT * from tbl_customer where Status=1 AND id IN ({$sItems}) ";
				$all_error = $oDb->getAll($sql);
				foreach($all_error as $key=>$value)
				{			
					$row = $key + 3;
					$objPHPExcel->getActiveSheet()->SetCellValue('A'.$row, $key+1);  // add giá trị	
					$objPHPExcel->getActiveSheet()->SetCellValue('B'.$row, $value['Email']);  // add giá trị	
					$objPHPExcel->getActiveSheet()->SetCellValue('C'.$row, $value['FullName']);  // add giá trị	
					$objPHPExcel->getActiveSheet()->SetCellValue('D'.$row, $value['LastName']);  // add giá trị
					$objPHPExcel->getActiveSheet()->SetCellValue('E'.$row, $value['Date'].'-'.$value['Month'].'-'.$value['Year']);  // add giá trị
					$objPHPExcel->getActiveSheet()->SetCellValue('F'.$row, $value['Phone']);  // add giá trị
					$objPHPExcel->getActiveSheet()->SetCellValue('G'.$row, $value['Address']);  // add giá trị
					$objPHPExcel->getActiveSheet()->SetCellValue('H'.$row, $value['Yahoo']);  // add giá trị
					
					$objPHPExcel->getActiveSheet()->getStyle('B'.$row.':H'.$row)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
					$objPHPExcel->getActiveSheet()->getStyle('B'.$row.':H'.$row)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
					$objPHPExcel->getActiveSheet()->getStyle('B'.$row.':H'.$row)->getAlignment()->setWrapText(true);
				}
		}
		$str = date("d.m.Y");
		//Set tiêu đề cho 1 Sheet trong excel
		$objPHPExcel->getActiveSheet()->setTitle("Danh sách thành viên");
		//Save vào đường dẫn bằng câu lệnh save
		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save("upload/excel/danhsach_thanhvien_".$str.".xls");
		echo '<script language="javascript">
					window.location ="'.SITE_URL.'upload/excel/danhsach_thanhvien_'.$str.'.xls";</script>';	
	}
	
	function addItem()
	{
		$this -> getPath("Quản lý khách hàng > Thêm mới khách hàng");		
		$this -> buildForm();
	}
	
	function editItem()
	{
		$id = $_GET['id'];
		$this -> getPath("Quản lý khách hàng > Chỉnh sửa khách hàng");	
		$row = $this -> bsgDb -> getRow( $id );
		$this -> buildForm( $row );
	}

	function deleteItem()
	{
		global  $oDb;
		$id = $_GET["id"];
		$this -> bsgDb -> deleteWithPk( $id );
		$msg = "Xóa khách hàng thành công!";
		$this -> listItem( $msg );
	}
	
	function deleteItems()
	{
		$aItems	 = $_GET['arr_check'];
		if(is_array( $aItems) && count( $aItems) > 0){
			$sItems = implode( ',', $aItems );
			$this -> bsgDb -> deleteWithPk( $sItems );
		}
		$msg = "Xóa (các) khách hàng thành công!";
		$this -> listItem( $msg );
	}
	
	function changeStatusMultiple( $status = 0 )
	{
		$aItems	 = $_GET['arr_check'];
		if(is_array( $aItems) && count( $aItems) > 0){
			$sItems = implode( ',', $aItems );
			$this -> bsgDb -> updateWithPk( $sItems, array("Status" => $status) );
		}
		$msg = "Sửa trạng thái khách hàng thành công!";
		$this -> listItem( $msg );
	}
	
	function buildForm( $data=array() , $msg = ''){
		global $oSmarty, $oDb;
		$form = new HTML_QuickForm('frmAdmin','post',$_COOKIE['re_dir']."&task=".$_GET['task'], '', "style='padding:10px 15px 0 20px;'");
		
		$form -> setDefaults($data);
		
		$form -> addElement('text', 'Email', 'Email', array('size' => 50, 'maxlength' => 255));
		$form -> addElement('text', 'FullName', 'First name', array('size' => 50, 'maxlength' => 255));
		$form -> addElement('text', 'LastName', 'Last name', array('size' => 50, 'maxlength' => 255));
		$birthday=$oDb->getRow("Select Date,Month,Year from tbl_customer where id='".$data['id']."'");
		$birthday1=$birthday['Date'].'-'.$birthday['Month'].'-'.$birthday['Year'];
		$form -> addElement('static', '','Ngày sinh',$birthday1);
		$form -> addElement('text', 'Address', 'Địa chỉ', array('size' => 50, 'maxlength' => 255));
		$form -> addElement('text', 'Phone', 'Địện thoại', array('size' => 50, 'maxlength' => 255));
        $form -> addElement('text', 'Postal', 'Mã bưu điện', array('size' => 50, 'maxlength' => 255));
        $form -> addElement('text', 'Yahoo', 'Thành phố', array('size' => 50, 'maxlength' => 255));
		$country = array(0=>'--- Đất nước---') + $this->getCountry($selected_langid);
		$form -> addElement('select', 'CityID', 'Danh mục', $country, array('id'=>'CatID'));
       // $form -> addElement('text', 'Skype', 'Công ty', array('size' => 50, 'maxlength' => 255));
		
		$form -> addElement('checkbox', 'Status', 'Kích hoạt');
		
		$btn_group[] = $form -> createElement('submit',null,'Hoàn tất',array("style"=> "border:1px solid gray; padding:0 5px 0 5px;"));		
        $btn_group[] = $form -> createElement('button',null,'Quay lại',array('onclick'=>'window.location.href = \''.$_COOKIE['re_dir'].'\'', "style"=> "border:1px solid gray;"));      
        $form -> addGroup($btn_group);
      
		$form->addElement('hidden', 'id', $data['id']);		
		
		if( $form -> validate())
		{
			
			$aData  = array(
				"username" => $_POST['username'],
				"FullName" => $_POST['FullName'],
				"LastName" => $_POST['LastName'],
				"Address" 	=> $_POST['Address'],
				"Phone" 	=> $_POST['Phone'],
				"Email" 	=> $_POST['Email'],
				"Postal" 	=> $_POST['Postal'],
				"Status" 	=> $_POST['Status'],
                "Yahoo"     => $_POST['Yahoo'],
                "Skype"     => $_POST['Skype'],
			);
				
			if( !$_POST['id'] ){
				
				 $id = $this -> bsgDb -> insert($aData);				 
				 $msg = "Thêm khách hàng thành công! ";
			}else {
				$id = $_POST['id'];				
				$this -> bsgDb -> updateWithPk($id, $aData);
				$msg = "Chỉnh sửa khách hàng thành công ";
			}
			
			$this -> redirect($_COOKIE['re_dir']. "&msg={$msg}");
		}
		
		$form->display();
	}
	
	function deleteImage($id, $field, $path){
		if($id == '')
			return;
		$imgpath = $path.$this->db->getOne("SELECT $field FROM ".$this->table." WHERE id = $id");
		if(is_file($imgpath))			
			@unlink($imgpath);
	}
	
	function getMessage($id=0){
		global $oDb;
		$sql = "SELECT * FROM tbl_ttc_message WHERE Status=1";
		$product = $oDb->getAll($sql);
		$cart = '<div style="width:800px;height:350px;overflow-x:auto;border:1px solid #ccc;margin-bottom:10px; "><input type="hidden" id="hdnGetPara" value=""> <table width="800px" cellpadding="0" cellspacing="0"  bgcolor="#fff">';
		$cart .= '	<tr align="center">';
		$cart .= '	<td style="padding-left:5px;" colspan="3">';
		foreach ($product as $k1 => $v1) {			
				$cart .= '	<span  style="padding-left:5px; float:left;"><strong style="vertical-align:middle;"><input type="checkbox" name="tire[]" id="tire[]" value="'.$v1["id"].'" ';
				$cart .= ' />'.$v1['Subject'].'</strong></span>';
		}
		$cart .='	</td>';
		$cart .='	</tr>';
		$cart .= '</table>';
		$cart .= '</div>';
		return $cart; 
	}
	
	function getCountry($lang_id=0){
		global $oDb;
		$sql="Select id,Name from tbl_country Order by Ordering";
		$country=$oDb->getAssoc($sql);
		return $country;
	}
	
	function changeStatus( $itemId , $status ){
		$aData = array( 'Status' => $status );
		$this -> bsgDb -> updateWithPk( $itemId, $aData );
		return true;
	}
	
	function listItem( $sMsg= '' )
	{		
		global $oDb;
		global $oDatagrid;				
		
		$root_path = "Quản lý khách hàng > Danh sách khách hàng";						
		$submit_url= "/index.php?".$_SERVER['QUERY_STRING'];
		
		$table = $this -> table;
		
		$arr_filter= array(
			array(
				'field' => 'username',
				'display' => 'Tài khoản',
				'type' => 'text',
				'name' => 'filter_title',
				'selected' => $_REQUEST['filter_title'],
				'filterable' => true
			),			
			array(
				'field' => 'Status',
				'display' => 'Trạng thái',				
				'name' => 'filter_show',
				'selected' => $_REQUEST['filter_show'],
				'options' => array('Vô hiệu','Kích hoạt'),
				'filterable' => true
			)
			
		); 
		
		$arr_cols= array(		
			
			array(
				"field" => "id",					
				"primary_key" =>true,
				"display" => "Id",				
				"align" => "center",
				"sortable" => true
			),	
			array(
				"field" => "Email",
				"display" => "Email",
				"align"	=> 'left',				
				"datatype" => "text",
				"sortable" => true
			),
			array(
				"field" => "FullName",
				"display" => "Tên đầy đủ",
				"align"	=> 'left',				
				"datatype" => "text",
				"sortable" => true
			),	
			array(
				"field" => "Phone",
				"display" => "Điện thoại",
				"align"	=> 'left',				
				"datatype" => "text",
				"sortable" => true
			),	
			array(
				"field" => "Address",
				"display" => "Địa chỉ",
				"align"	=> 'left',				
				"datatype" => "text",
				"sortable" => true
			),
            array(
                "field" => "Yahoo",
                "display" => "Yahoo",
                "align"    => 'left',                
                "datatype" => "text",
                "sortable" => true
            ),
            /*array(
                "field" => "Skype",
                "display" => "Skype",
                "align"    => 'left',                
                "datatype" => "text",
                "sortable" => true
            ),*/
			array(
				"field" => "Status",
				"display" => "Trạng thái",				
				"datatype" => "publish",
				"sortable" => true
			),		
		);		
		
		$arr_check = array(
			array(
				"task" => "delete_all",
				"confirm"	=> "Xác nhận xóa?",
				"display" => "Xóa"
			),
			array(
				"task" => "public_all",
				"confirm"	=> "Xác nhận thay đổi trạng thái?",
				"display" => "Kích hoạt"
			),
			array(
				"task" => "unpublic_all",
				"confirm"	=> "Xác nhận thay đổi trạng thái?",
				"display" => "Vô hiệu"
			)			
		);
		if( $sMsg )
			$oDatagrid -> setMessage( $sMsg );
		$oDatagrid->display_datagrid($table, $arr_cols, $arr_filter, $submit_url, $this -> getAct() ,120, $root_path, false ,$arr_check);		
		
	}		
	
}

?>