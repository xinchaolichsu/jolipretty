<?php
class customer  extends VES_FrontEnd {	
    var $table;
    var $mod;
    var $url_mod;
    var $id;
    var $LangID;
    var $field;
    var $pre_table;
    var $imgPath;
    var $imgPathShort;    
    function __construct(){        
        $this->table = 'tbl_customer';
        $this->id = mysql_real_escape_string(($_REQUEST['id']=='')?'0':$_REQUEST['id']);
        $this->field = "*";
        $this->LangID = $_SESSION['lang_id'];
		$this->cust_id = $_SESSION['userid_cus'];
        parent::__construct($this->table);
        $this->imgPath = SITE_DIR."upload/product/";
        $this->imgPathShort = "/upload/product/";
    }
	function run($task)
	{	
		switch ($task)
		{
			case "login":
				$this->login();
				break;
			case "login_cart":
				$this->login_cart();
				break;
            case "post_product":
                $this->post_product();
                break;
            case "delete_product":
                $this->delete_product();
                break;
            case "edit_product":
                $this->edit_product();
                break;
            case "list_post":
                $this->list_post();
                break;
            case "up_date":
                $this->up_date();
                break;
			case "order":
                $this->order();
                break;
			case "order_yet":
                $this->order();
                break;
			case "order_finish":
                $this->order();
                break;
			case "del_order":
                $this->del_order();
                break;
			case "forgot_password":
				$this->forgotPassword();
				break;
			case "logout":
				$this->logout();
				break;
			case "change_pass":
				$this->change_pass();
				break;
			case "register":
				$this->register();
				break;
			case "verification":
				$this->verification();
				break;
			case 'doReset':
				$this -> doReset();
				break;
			case "edit_profile":
				$this->editProfile();
				break;
			case 'register_success':
				$this -> register_success();
				break;
			case 'history':
				$this -> history();
				break;
			case 'frm_login':
				$this -> showFormLogin();
				break;
		}
	}

function crop_image($src, $name_file, $des_dir, $w=640, $h= 480, $active=true){
		
		include_once(SITE_DIR.'core/thumbnail/thumbnail.class.php');		
		$thumb=new Thumbnail($src);		
		$thumb->size($w, $h);
		$thumb->output_format= "JPG";
		
		// Get file extension
		$ext = strtoupper(end(explode(".", basename($name_file))));
		
		if($active==true && ($ext == 'JPG' || $ext == 'JPEG' || $ext == 'PNG' || $ext == 'GIF')){
			$thumb->process();
			$thumb->save($des_dir."/".$name_file);
		}else
			move_uploaded_file($src, $des_dir."/".$name_file);
	
		return $name_file;
	}
	
	function getPageinfo($task= "")
	{
		global $oSmarty;	
		
		switch ($task)
		{
			case "login":
				$aPageinfo=array('title'=>"Joli Pretty", 'keyword'=>'', 'description'=>'');
				$aPath = array(array("link"=> '', "path"=>$oSmarty->get_config_vars("login")));
				break;
			case "login_cart":
				$aPageinfo=array('title'=>"Joli Pretty", 'keyword'=>'', 'description'=>'');
				$aPath = array(array("link"=> '', "path"=>$oSmarty->get_config_vars("login")));
				break;
			case "change_pass":
				$aPageinfo=array('title'=>"Joli Pretty", 'keyword'=>'', 'description'=>'');
				$aPath = array(array("link"=> '', "path"=>$oSmarty->get_config_vars("login")));
				break;
			case "order":
				$aPageinfo=array('title'=>"Joli Pretty", 'keyword'=>'', 'description'=>'');
				$aPath = array(array("link"=> '', "path"=>$oSmarty->get_config_vars("login")));
				break;
			case "frm_login":
				$aPageinfo=array('title'=>"Joli Pretty", 'keyword'=>'', 'description'=>'');
				$aPath = array(array("link"=> '', "path"=>$oSmarty->get_config_vars("login")));
				break;
			case "post_product":
				$aPageinfo=array('title'=>"Joli Pretty", 'keyword'=>'', 'description'=>'');
				$aPath = array(array("link"=> '', "path"=>$oSmarty->get_config_vars("login")));
				break;
			case "list_post":
				$aPageinfo=array('title'=>"Joli Pretty", 'keyword'=>'', 'description'=>'');
				$aPath = array(array("link"=> '', "path"=>$oSmarty->get_config_vars("login")));
				break;
			case "edit_product":
				$aPageinfo=array('title'=>"Joli Pretty", 'keyword'=>'', 'description'=>'');
				$aPath = array(array("link"=> '', "path"=>$oSmarty->get_config_vars("login")));
				break;
			case "forgot_password":
				$aPageinfo=array('title'=>"Joli Pretty", 'keyword'=>'', 'description'=>'');
				$aPath = array(array("link"=> '', "path"=>$oSmarty->get_config_vars("user_forgot_password")));
				break;
			case "logout":
				$this->logout();
				break;
			case "register":
				$aPageinfo=array('title'=>"Joli Pretty", 'keyword'=>'', 'description'=>'');
				$aPath = array(array("link"=> '', "path"=>$oSmarty->get_config_vars("user_register")));
				break;
			case "history":
				$aPageinfo=array('title'=>"Joli Pretty", 'keyword'=>'', 'description'=>'');
				$aPath = array(array("link"=> '', "path"=>$oSmarty->get_config_vars("user_register")));
				break;
			case "register_success":
				$aPageinfo=array('title'=>"Joli Pretty", 'keyword'=>'', 'description'=>'');
				$aPath = array(array("link"=> '', "path"=>$oSmarty->get_config_vars("user_register")));
				break;
			case "edit_profile":
				$aPageinfo=array('title'=>"Joli Pretty", 'keyword'=>'', 'description'=>'');
				$aPath = array(array("link"=> '', "path"=>$oSmarty->get_config_vars("user_edit_profile")));
				break;
		}
		$oSmarty->assign('aPageinfo', $aPageinfo);
		$oSmarty->assign("aPath", $aPath);
	}
	
	function post_product($data=array())
    {
        global $oSmarty, $oDb;
		$where = " Status=1 ";
		if (MULTI_LANGUAGE)
		$where .= " AND LangID={$_SESSION['lang_id']} ";
		$id=$_SESSION["userid_cus"];
		
        if($_SERVER["REQUEST_METHOD"]=="POST")
        {
			$oSmarty->assign('capcha',$_SESSION['key_captcha']);
            if($_POST['txt_captcha']==$_SESSION['key_captcha'])
            {                				
				$date = date("Y-m-d:H-i-s",time());	
                $aData  = array(
				"Name" 			=> $_POST['tex_fullname'],
				"CatID"			=> $_POST['sgd_category'],
				"CityID"		=> $_POST['sgd_city'],
				"YouIs"			=> $_POST['youis'],
				"YouPost"		=> $_POST['you_post'],
				"IsStock"		=> $_POST['IsStock'],
				"OldPrice"		=> $_POST['price'],
				"Name"			=> $_POST['tex_fullname'],
				"Content"		=> $_POST['Content'],
				"UseID"			=> $id,
				"CreateDate" 	=> $date,
				"MakeDate" 		=> $date,
				"Status" 		=> $_POST['Status']?$_POST['Status']:1,
				"LangID" 		=> $_POST['LangID']?$_POST['LangID']:1
                );
				

                if($_FILES['Photo']['name']!='')
                {
                    $FileName = rand().'_'.$_FILES['Photo']['name'];                                                
                    $this->crop_image($_FILES['Photo']['tmp_name'],$FileName, $this->imgPath);                
                    $aData['Photo'] = $this->imgPathShort.$FileName;                
                }
				//pre($aData);//die();	
                if($_GET['task'] == 'post_product')
                    $oDb -> autoExecute("tbl_product_item", $aData, DB_AUTOQUERY_INSERT);
                $url = "/list_post/";
				
				if($_POST['hdStatus'] == "1")
				{ echo "<script language = 'javascript'>
				alert('Đăng sản phẩm thành công!');
				location.href = '".$url."'</script>";
				}
				else
				{
                echo "<script language = 'javascript'>
				alert('Đăng sản phẩm thành công!');
				location.href = '".$url."'</script>";
				}
            }
            else
            {
                $oSmarty->assign("error", parent::get_config_vars("wrong_security_code"));
            }
        } 			
			
		$sql = "SELECT * FROM tbl_product_category WHERE ParentID=0 AND {$where} ORDER BY Ordering ASC";
		$cat = $oDb->getAll($sql);
		foreach($cat as $key=>$value)
		{
			$sql = "SELECT * FROM tbl_product_category WHERE ParentID={$value['id']} AND {$where} ORDER BY Ordering ASC";
			$sub = $oDb->getAll($sql);
			$cat[$key]['sub'] = $sub;
		}
		$oSmarty->assign('cat',$cat);
		
		$sql = "SELECT * FROM tbl_product_place WHERE ParentID=0 AND {$where} ORDER BY Ordering ASC";
		$city = $oDb->getAll($sql);
		$oSmarty->assign('city',$city);
		
		$a= rand(1,9);
		$oSmarty->assign("a", $a);                                                
		
		$b= rand(1,9);
		$oSmarty->assign("b", $b);
		
		$oSmarty->assign("total", $a + $b);
		$oSmarty->assign("data", $data);
        $oSmarty->display('post_product.tpl');
    }
	
	function getImage($product_id)
	{
		global $oDb;
		$sql = "SELECT Photo_ID,Photo_Name FROM tblproduct_photo WHERE Photo_ProductID=$product_id";
		$result = $oDb->getAll($sql);
		//pre($result);die();
		return $result;
	}
	
	function order()
	{
		global $oSmarty, $oDb;
		 if ( $_SESSION["userid_cus"] == "")
        {
            $url = SITE_URL."index.php?mod=customer&task=login";
            echo "<script language = 'javascript'>location.href = '".$url."'</script>";
            exit();
        }
		$where = "CreateUser={$_SESSION['userid_cus']}";		
		
		/*if($_GET['task']=='order_yet')
			$where .= " AND Status=0 ";
		if($_GET['task']=='order_finish')
			$where .= " AND Status=1 ";*/
			
			
		$sql = "SELECT * FROM tbl_shopping_cart WHERE {$where} ORDER BY OrderID DESC";
        $result = $oDb->getAll($sql);
			foreach ($result as $key => $val) {
				
				$products = $oDb->getRow("SELECT * FROM tbl_product_item WHERE id = ".$val['product_id']);
				if($val['color'])
					{
						$sql = "SELECT Name FROM tbl_color WHERE id='".$val['color']."' ";
						$color = $oDb->getOne( $sql );
						$products['color'] = $color;
					}
				if($val['size'])
					{
						$sql = "SELECT Name FROM tbl_size WHERE id='".$val['size']."' ";
						$size = $oDb->getOne( $sql );
						$products['size'] = $size;
					}
				if($val['OrderID'])
					{
						$sql = "SELECT Status FROM tbl_transaction WHERE OrderID='".$val['OrderID']."' ";
						$orderID = $oDb->getOne( $sql );
						$products['orderID'] = $orderID;
					}
					
				$subtotal = ($products['OldPrice'] - $products['OldPrice']*$products['Sale_off']/100)*$val['quantity'];
				
				$result[$key]['subtotal'] = $subtotal;				
				$result[$key]['Name'] = $products['Name'];
				$result[$key]['Photo'] = $products['Photo'];
				$result[$key]['OldPrice'] = $products['OldPrice'];
				$result[$key]['tt'] = $key+1;
				$result[$key]['Code'] = $products['Code'];
				$result[$key]['size'] = $products['size'];
				$result[$key]['color'] = $products['color'];
				$result[$key]['Status'] = $products['orderID'];
				
				$total = $total + $subtotal;
			}
			//pre($result);
		$oSmarty->assign('result',$result);
        $oSmarty->display('cart_manager.tpl');
		
	}
	
	function del_order()
    {
        if ( $_SESSION["userid_cus"] == "")
        {
            $url = SITE_URL."index.php?mod=customer&task=login";
            echo "<script language = 'javascript'>location.href = '".$url."'</script>";
            exit();
        }
        global $oSmarty, $oDb;
        $sql = "SELECT * FROM tbl_shopping_cart WHERE id='{$_GET['id']}'";
		//pre($sql);
        $detail_item = $oDb->getRow($sql);     
        if($detail_item['CreateUser'] == $_SESSION["userid_cus"])  
        {
            $oDb->query("DELETE FROM tbl_shopping_cart WHERE id='{$_GET['id']}'");
            $url = "/customer/order/";
            echo "<script language = 'javascript'>location.href = '".$url."'</script>";
        }
        else
        {
            echo $this->get_config_vars('access_denied');
            exit();
        }
    }
	
	
    function list_post()
    {
        global $oSmarty, $oDb;
		$where = " Status=1 ";
		if (MULTI_LANGUAGE)
		$where .= " AND LangID={$_SESSION['lang_id']} ";
		$id=$_SESSION["userid_cus"];
        $sql = "SELECT * FROM tbl_product_item WHERE UseID={$id} AND {$where} ORDER BY CreateDate DESC limit 20";
        $list_post = $oDb->getAll($sql);
        $oSmarty->assign('list_post',$list_post);
        $oSmarty->display('list_post.tpl');
    }
	
	function up_date()
	{
		global $oSmarty, $oDb;
		$id=$_GET['id'];
        $makedate = $oDb->getOne("SELECT MakeDate FROM tbl_product_item WHERE id={$id}");
		$time=time() - strtotime($makedate);
		$wait=3600 - $time;
		$date = date("Y-m-d:H-i-s",time());	
		$user= array(						
			"MakeDate" 	=> $date,
		);
		$url = "/list_post/";
		if($time > 3600)
		{
			$oDb -> autoExecute("tbl_product_item", $user, DB_AUTOQUERY_UPDATE, "id='".$id."'");
			echo "<script language = 'javascript'> alert('Đã up tin thành công!'); location.href = '".$url."'</script>";
		}
		else
			echo "<script language = 'javascript'> alert('Tin vừa được up. Vui lòng chờ ".$wait."s để up tin lần tiếp theo!'); location.href = '".$url."'</script>";
	}
	
    function edit_product()
    {
        global $oSmarty, $oDb;
		$where = " Status=1 ";
		if (MULTI_LANGUAGE)
		$where .= " AND LangID={$_SESSION['lang_id']} ";
        if ( $_SESSION["userid_cus"] == "")
        {
            $url = SITE_URL."index.php?mod=customer&task=login";
            echo "<script language = 'javascript'>location.href = '".$url."'</script>";
            exit();
        }
        $sql = "SELECT * FROM tbl_product_item WHERE id={$_GET['id']}";
        $detail_item = $oDb->getRow($sql); 
        if($detail_item['UseID'] == $_SESSION["userid_cus"])  
		{
			if($_SERVER["REQUEST_METHOD"]=="POST")
			{
					$date = date("Y-m-d:H-i-s",time());	
					$aData  = array(
					"Name" 			=> $_POST['tex_fullname'],
					"CatID"			=> $_POST['sgd_category'],
					"CityID"		=> $_POST['sgd_city'],
					"YouIs"			=> $_POST['youis'],
					"YouPost"		=> $_POST['you_post'],
					"IsStock"		=> $_POST['IsStock'],
					"OldPrice"		=> $_POST['price'],
					"Name"			=> $_POST['tex_fullname'],
					"Content"		=> $_POST['Content'],
					"UseID"			=> $detail_item['UseID'],
					"CreateDate" 	=> $date,
					"MakeDate" 		=> $date,
					"Status" 		=> $_POST['Status']?$_POST['Status']:1,
					"LangID" 		=> $_POST['LangID']?$_POST['LangID']:1
					);
					
	
					if($_FILES['Photo']['name']!='')
					{
						$FileName = rand().'_'.$_FILES['Photo']['name'];                                                
						$this->crop_image($_FILES['Photo']['tmp_name'],$FileName, $this->imgPath);                
						$aData['Photo'] = $this->imgPathShort.$FileName;                
					}
					if($_POST["tex_password"]!="")
					{
						$data["password"]= md5($_POST["tex_password"]);
					}					
					pre($aData);//die();	
					if($_GET['task'] == 'edit_product')
						$oDb -> autoExecute("tbl_product_item", $aData, DB_AUTOQUERY_UPDATE, "id='".$_GET['id']."'");
					$url = "/list_post/";
					
					if($_POST['hdStatus'] == "1")
					{ echo "<script language = 'javascript'>
					alert('Sửa sản phẩm thành công!');
					location.href = '".$url."'</script>";
					}
					else
					{
					echo "<script language = 'javascript'>
					alert('Sửa sản phẩm thành công!');
					location.href = '".$url."'</script>";
					}
			} 			
							
			$sql = "SELECT * FROM tbl_product_place WHERE ParentID=0 AND {$where} ORDER BY Ordering ASC";
			$city = $oDb->getAll($sql);
			$oSmarty->assign('city',$city);
			$oSmarty->assign("detail_item", $detail_item);
			$oSmarty->assign("data", $data);
			$oSmarty->display('edit_product.tpl');
		}
        else
        {
            echo $this->get_config_vars('access_denied');
            exit();
        }
    }
	
	function change_pass()
	{
		global $oSmarty, $oDb;
		
		if($_SERVER["REQUEST_METHOD"]=="POST")
		{
			{
				$user= $oDb->getRow("select * from tbl_customer where id='".$_SESSION["userid_cus"]."'");
				if(md5($_POST["tex_password_old"])!=$user['password'])
				{					
					$oSmarty->assign("error", "the old password is not correct!");			
				}
				else
				{
						$user["password"]= md5($_POST["tex_password_new"]);
					
					$oDb -> autoExecute("tbl_customer", $user, DB_AUTOQUERY_UPDATE, "id='".$_SESSION["userid_cus"]."'");
					$subject = "Your password has been reset";
					
					$message = "Dear " .$user['FullName'].", <br />
			
									In response to your request to reset password, we hereby send you the new login information. <br />
									
									Login User ID: ".$user['Email']." <br />
									
									New Password :" .$_POST["tex_password_new"]." <br />
									
									After this, you can log in with your new password.<br />
			
									If you have not the done any request, please contact our customer service at <br />
			
									info.jolipretty@gmail.com.<br />
									Regards,<br />
			
									The Joli Pretty Customer Service";
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
					$headers .= "From: info.jolipretty@gmail.com <info.jolipretty@gmail.com>" . "\r\n";
					$result = @mail( $user['Email'], $subject, $message, $headers );
					$oSmarty->assign("error", "Your password has been successfully updated!");
				}
			}
		}
		$oSmarty->display('change_pass.tpl');
	}
	
    function delete_product()
    {
        if ( $_SESSION["userid_cus"] == "")
        {
            $url = SITE_URL."index.php?mod=customer&task=login";
            echo "<script language = 'javascript'>location.href = '".$url."'</script>";
            exit();
        }
        global $oSmarty, $oDb;
        $sql = "SELECT * FROM tbl_product_item WHERE id='{$_GET['id']}'";
        $detail_item = $oDb->getRow($sql);     
        if($detail_item['UseID'] == $_SESSION["userid_cus"])  
        {
            $oDb->query("DELETE FROM tbl_product_item WHERE id='{$_GET['id']}'");
            $url = "/list_post/";
            echo "<script language = 'javascript'>location.href = '".$url."'</script>";
        }
        else
        {
            echo $this->get_config_vars('access_denied');
            exit();
        }
    }
    

	
	function login()
	{
		global $oSmarty, $oDb;	
		if ( $_SESSION["userid_cus"] != "")
		{
			$url = SITE_URL."index.php?mod=customer&task=frm_login";
			echo "<script language = 'javascript'>location.href = '".$url."'</script>";
			exit();
		}
		
		if($_SERVER['REQUEST_METHOD']=="POST")
		{
			
			$anti_pass = $this->anti_hack($_POST["tex_password"]);
			$anti_user = $this->anti_hack($_POST["tex_username"]);
			if($anti_user == $_POST["tex_username"] && $anti_pass == $_POST["tex_password"])
			{
				$user= $oDb->getRow("select * from tbl_customer where Status=1 AND Email='".$_POST["tex_username"]."'");
				if(is_array($user))
				{
					if($user["password"]==md5($_POST["tex_password"]))
					{		
						$_SESSION['username_cus']	=$user['Email'];
						$_SESSION['userid_cus']	= $user['id'];
						$oSmarty->assign("error", "success");
						echo "<script language = 'javascript'>location.href ='/'</script>";
						exit();
					}
					else
					{
						$oSmarty->assign("error", "log in password is not correct, please try again");
					}
				}
				else
				{
					$oSmarty->assign("error", "Could not find this email");
				}
			}
		}		
		
		$oSmarty->display('login.tpl');
	}
	
	function login_cart()
	{
		global $oSmarty, $oDb;	
        //pre($_SESSION);
		if ( $_SESSION["userid_cus"] != "")
		{
			$url = SITE_URL."index.php?mod=customer&task=frm_login";
			echo "<script language = 'javascript'>location.href = '".$url."'</script>";
			exit();
		}
		
		if($_SERVER['REQUEST_METHOD']=="POST")
		{
			
			$anti_pass = $this->anti_hack($_POST["tex_password"]);
			$anti_user = $this->anti_hack($_POST["tex_username"]);
			if($anti_user == $_POST["tex_username"] && $anti_pass == $_POST["tex_password"])
			{
				$user= $oDb->getRow("select * from tbl_customer where Status=1 AND Email='".$_POST["tex_username"]."'");
				if(is_array($user))
				{
					if($user["password"]==md5($_POST["tex_password"]))
					{		
						$_SESSION['username_cus']	=$user['Email'];
						$_SESSION['userid_cus']	= $user['id'];
						
						$oSmarty->assign("error", "success");
						
						//$url = ;
						//$trackUrl = SITE_URL."index.php"; //base64_decode($_GET['track']);
						echo "<script language = 'javascript'>location.href ='".SITE_URL."/cart/'</script>";
						exit();
					}
					else
					{
						$oSmarty->assign("error", "log in password is not correct, please try again");
					}
				}
				else
				{
					$oSmarty->assign("error", "Could not find this email, please try again");
				}
			}
		}		
		
		$oSmarty->display('login_cart.tpl');
	}
	/**
	 * function nay hien thi form login neu chua dang nhap
	 * neu dang nhap roi thi hien thi cpanel
	*/
	function showFormLogin(){
		global $oSmarty, $oDb;
		if ( $_SESSION["userid_cus"] != "")
			{
				$sql = "SELECT count(*) FROM tbl_message WHERE ToID={$_SESSION['userid_cus']} AND IsRead=0";
				$count = $oDb->getOne($sql);
				$oSmarty->assign("count", $count);
			}
		$version = 0;
		$html = $_SERVER['HTTP_USER_AGENT'];
		if (ereg("MSIE 7.0",$html) == 1 || ereg("MSIE 6.0",$html) == 1)
			$version = 1;
		else
			$version = 2;
		$oSmarty->assign("version", $version);
		$oSmarty -> display("frm_login.tpl");
	}

	function forgotPassword()
	{
		global $oSmarty, $oDb;
		if($_SERVER["REQUEST_METHOD"]=="POST")
		{
			$email = $_POST['tex_email'];
			$sQuery = "SELECT * FROM tbl_customer WHERE Email = '$email'";
			$row = $oDb -> getRow ( $sQuery );
			if ( count($row) == 0 )
			{
				$error = "Could not find this email, please try again";
				$oSmarty->assign( "error", $error );
			}else 
			{
				$msg = $this -> resetPassword( $email, $row['password'],$row['FullName'] );
				$oSmarty->assign( "error", $msg );
			}
		}
		
		$oSmarty->display('forgot_password.tpl');
	}
	
	function resetPassword( $email, $password,$FullName )
	{
		global $oSmarty, $oDb;
		$new_password = $this -> randomPassword();	
		$code = md5( $new_password ); //pre($code);//die();
		//$link_reset = SITE_URL."index.php?mod=customer&task=doReset&email=".$email."&code=".$code."&tmp=".md5($new_password)."&ajax";
		$query = "UPDATE tbl_customer SET `password` = '".$code."' WHERE Email = '".$email."'";
		$oDb -> query ( $query );
		$subject = "Your password has been reset";
		
		$message = "Dear " .$FullName.", <br />

						In response to your request to reset password, we hereby send you the new login information. <br />
						
						Login User ID: ".$email." <br />
						
						New Password :" .$new_password." <br />
						
						After this, you can log in with your new password.<br />

						If you have not the done any request, please contact our customer service at <br />

						info.jolipretty@gmail.com.<br />
						Regards,<br />

						The Joli Pretty Customer Service";
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
		$headers .= "From: info.jolipretty@gmail.com <info.jolipretty@gmail.com>" . "\r\n";
		$result = @mail( $email, $subject, $message, $headers );
		if ( $result )
		{
			$msg = " <b style = 'color:blue'>A new password has been sent to your e-mail address</b>";
		}else 
			$msg = " Error: Can not send mail, please try again or contact the administrator for help ";
		
		return $msg;
	}
	
	function doReset()
	{
		global $oSmarty, $oDb;
		$email = $_GET['email'];
		$code = $_GET['code'];
		$sQuery = "SELECT * FROM tbl_customer WHERE Email = '$email'";	
		$row = $oDb -> getRow ( $sQuery );
		//echo md5($row['password']);
		if ( md5($row['password']) != $code )
		{
			$msg = "<span style = 'color:red'>An error has not happen with the system, not initialize a new password. Invite you to retry or contact cấm administrative assistant to be Prop </span>";
			
		}else if ( md5($row['password']) == $code )
		{
			$query = "UPDATE tbl_customer SET `password` = '".$_GET['tmp']."' WHERE Email = '".$email."'";
			$oDb -> query ( $query );
			$link = SITE_URL."customer/login/";
			$msg = " Initialize a new password successfully! ";
			$continue = "<div>"."invite friends click <b><a style = 'color:#000000' href = '{$link}'>here</a></b> to login!</div>";
		}		
		
		$oSmarty -> assign ( "msg", $msg );
		$oSmarty -> assign ( "continue", $continue );
		$oSmarty -> display( 'reset_password.tpl' );
	}
	
	function logout()
	{
		global $oSmarty, $oDb;	
		
		//$oDb->query("update user set status= '0' where username='".$_SESSION["user_username"]."'");
		unset($_SESSION['username_cus']);
		unset($_SESSION['userid_cus']);
			
		$oSmarty->assign("error", "logout_success");
		$url = SITE_URL;												
		echo "<script language = 'javascript'>location.href = '".$url."'</script>";
	}
	
	function register()
	{
		global $oSmarty, $oDb;
		
		if ( $_SESSION["userid_cus"] != "")
		{
			$url = SITE_URL."index.php?mod=customer&task=edit_profile";
			echo "<script language = 'javascript'>location.href = '".$url."'</script>";
			exit();
		}		
		
		if($_SERVER["REQUEST_METHOD"]=="POST")
		{
			//$dob = date ("Y-m-d", strtotime($_POST['year'].'-'.$_POST['month'].'-'.$_POST['date']) );
			//print_r ($_SESSION); echo $_POST["tex_c"]; die();
				$user= $oDb->getRow("select * from tbl_customer where Email='".$_POST["tex_email"]."' or Phone='".$_POST["tex_phone"]."' ");
				
				if(is_array($user))
				{
					if($user["Email"]==$_POST["tex_email"])
					{
						$oSmarty->assign("error", "This email address is already in use");
					}
					elseif($user["Phone"]==$_POST["tex_phone"])
					{
						$oSmarty->assign("error", "This phone number is already in used, please enter the other phone number");
					}
				}
				else
				{
					$user= array(
						"password" 		=> md5($_POST["tex_password"]),
						"Email" 		=> $_POST["tex_email"],
						"Address"		=> $_POST['tex_address'],
						"Phone"			=> $_POST['tex_phone'],
						"FullName"		=> $_POST['first_name'],
						"LastName"		=> $_POST['last_name'],
						"Postal"		=> $_POST['postal'],
                        "Yahoo"        => $_POST['city'],
                        "CityID"        => $_POST['country'],
						"Date" 			=> $_POST["Date"],
						"Month" 		=> $_POST["Month"],
						"Year" 			=> $_POST["Year"],
						"Status" 		=> 1,
					);
									
					$res = $oDb -> autoExecute("tbl_customer", $user, DB_AUTOQUERY_INSERT);
					$_SESSION['username_cus']	=$_POST["tex_email"];
					$_SESSION['userid_cus']	= mysql_insert_id();
					//========== garung ==================
					$data = array(
						"Email" => $_POST['tex_email'],
						//"Phone" => $_POST['Phone'],
						"CreateDate" => date("Y-m-d"),
						"Status" => "1"
					);
					$oDb -> autoExecute("tbl_regismail_item",$data,DB_AUTOQUERY_INSERT);
					$idemail=$oDb->getOne("select id from tbl_regismail_item where  Email='".$_POST["Email_regis123"]."'");
					//====================================
					//$iduser= $oDb->getOne("select id from tbl_customer where Email='".$_POST["tex_email"]."' ");
					//$link_verifi=SITE_URL."customer/".$iduser."/";
					$email=$_POST["tex_email"];
					$subject = "Thank you for registering at jolipretty.com";
					$message = "Hello " .$_POST['first_name'].",  <br />

								This email is just to confirm that your account has now been activated and you can start shopping   <br />
								
								with us at any time.  <br />
								
								Your login details are as follows, please note once you have logged into your account you can   <br />
								
								change your password, by visiting My Account.  <br />
								
								Please keep this information in a safe place: <br />
								
								Username: ".$_POST["tex_email"]."  <br />
								
								Password: ".$_POST["tex_password"]."  <br />
								
								To login please visit <a href='http://jolipretty.com/'>www.jolipretty.com</a>  <br />
								
								Happy Shopping!  <br />
								
								With love,  <br />
								
								The Joli Pretty Team";
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
					$headers .= "From: info.jolipretty@gmail.com <info.jolipretty@gmail.com>" . "\r\n";
					$result = @mail( $email, $subject, $message, $headers );
					$url = SITE_URL."customer/register_success/";
					echo "<script language = 'javascript'>location.href = '".$url."'</script>";
					exit();
				}
		} 
		else {		
			$a= rand(1,9);
			$oSmarty->assign("a", $a);												
			
			$b= rand(1,9);
			$oSmarty->assign("b", $b);
			
			$oSmarty -> assign ("totalSe", $a+$b );
		}
		
		$country = $oDb->getAll("SELECT id,Name FROM tbl_country WHERE Status=1 ORDER BY Ordering ASC");
		$oSmarty->assign("country",$country);
		//captcha
		$possible = '23456789bcdfghjkmnpqrstvwxyz';
		$code = '';
		$i = 0;
		while ($i < 6) { 
			$code .= substr($possible, mt_rand(0, strlen($possible)-1), 1);
			$i++;
		}
		$_SESSION['key_captcha_code'] = $code;		
		$oSmarty->assign("code",$code);
		$date = array();
		for($i=1;$i<=31;$i++)
		{
			$date[$i]=$i;
		}
		$oSmarty->assign("date",$date);
		$month = array();
		for($i=1;$i<=12;$i++)
		{
			$month[$i]=$i;
		}
		$oSmarty->assign("month",$month);
		$year = array();
		for($i=1960;$i<=2000;$i++)
		{
			$year[$i]=$i;
		}
		$oSmarty->assign("year",$year);
		$oSmarty->display('register.tpl');
	}
	
	function verification()
	{
		global $oSmarty, $oDb;
		$id=$_GET['id'];
		$user= array(						
			"Status" 	=> 1,
		);
		$oDb -> autoExecute("tbl_customer", $user, DB_AUTOQUERY_UPDATE, "id='".$id."'");
		$sql = "SELECT * FROM tbl_customer WHERE id={$id}";
		$user = $oDb->getRow($sql);
		$_SESSION['username_cus']	=$user['username'];
		$_SESSION['userid_cus']	= $user['id'];
		$url = "/customer/edit_profile/";
		echo "<script language = 'javascript'> alert('Xác nhận thành công!'); location.href = '".$url."'</script>";
	}
	
	function register_success()
	{
		global $oSmarty;
		
		$oSmarty -> display ('register_success.tpl');
	}
	
	function editProfile()
	{
		global $oSmarty, $oDb;
		
		if($_SERVER["REQUEST_METHOD"]=="POST")
		{
				//$user= $oDb->getRow("select * from tbl_customer where Email='".$_POST["tex_email"]."'");
				if((is_array($user))&&($user["id"]!=$_SESSION["userid_cus"]))
				{					
					$oSmarty->assign("error", "Email this already exists");			
				}
				else
				{
					$user= array(						
						"Email" 		=> $_POST["tex_email"],
						"Address"		=> $_POST['tex_address'],
						"Phone"			=> $_POST['tex_phone'],
						"FullName"		=> $_POST['first_name'],
						"LastName"		=> $_POST['last_name'],
						"Postal"		=> $_POST['postal'],
                        "Yahoo"        => $_POST['city'],
                        "CityID"        => $_POST['country'],
						"Date" 			=> $_POST["Date"],
						"Month" 		=> $_POST["Month"],
						"Year" 			=> $_POST["Year"],
					);
					//pre($user);//die();
					if($_POST["tex_password"]!="")
					{
						$user["password"]= md5($_POST["tex_password"]);
					}
					
					$oDb -> autoExecute("tbl_customer", $user, DB_AUTOQUERY_UPDATE, "id='".$_SESSION["userid_cus"]."'");
					$oSmarty->assign("error", "Your personal information has been updated successfully");
				}
		}
		$user= $oDb->getRow("select * from tbl_customer where id='".$_SESSION["userid_cus"]."'");
		//pre($user);
		$oSmarty->assign("user", $user);
		
		$country = $oDb->getAll("SELECT id,Name FROM tbl_country WHERE Status=1 ORDER BY Ordering ASC");
		$oSmarty->assign("country",$country);
	
		$date = array();
		for($i=1;$i<=31;$i++)
		{
			$date[$i]=$i;
		}
		$oSmarty->assign("date",$date);
		$month = array();
		for($i=1;$i<=12;$i++)
		{
			$month[$i]=$i;
		}
		$oSmarty->assign("month",$month);
		$year = array();
		for($i=1960;$i<=2000;$i++)
		{
			$year[$i]=$i;
		}
		$oSmarty->assign("year",$year);
		
		$a= rand(1,9);
		$_SESSION["a"]= $a;
		$oSmarty->assign("a", $a);												
		
		$b= rand(1,9);		
		$_SESSION["b"]= $b;
		$oSmarty->assign("b", $b);
		$oSmarty->display('edit_profile.tpl');
	}
	
	function history()
	{
		global $oSmarty, $oDb;
		$orderid = $oDb->getAll("SELECT distinct OrderID FROM tbl_shopping_cart WHERE CreateUser={$_SESSION['userid_cus']} ORDER BY id DESC");
		 foreach ($orderid as $k1 => $v1)
        {
			$history = $oDb->getAll("SELECT *,(SELECT Price FROM tbl_product_item WHERE id=tbl_shopping_cart.product_id) as Price, (SELECT OldPrice FROM tbl_product_item WHERE id=tbl_shopping_cart.product_id) as OldPrice, (SELECT Name FROM tbl_product_item WHERE id=tbl_shopping_cart.product_id) as Name, (SELECT Photo FROM tbl_product_item WHERE id=tbl_shopping_cart.product_id) as Photo, (SELECT IsPay FROM tbl_transaction WHERE OrderID={$v1['OrderID']}) as IsPay, (SELECT IsShip FROM tbl_transaction WHERE OrderID={$v1['OrderID']}) as IsShip, (SELECT Name FROM tbl_size WHERE id=tbl_shopping_cart.size) as size_name   FROM tbl_shopping_cart WHERE OrderID={$v1['OrderID']} ORDER BY id asc");
			$total=0;
			foreach ($history as $k => $v)
			{
				if($v['Price'])
					$price=$v['Price'] * $v['quantity'];
				else
					$price=$v['OldPrice'] * $v['quantity'];
				$history[$k]['price']=$price;
				$total=$total + $price; 
			}
			$total=$total;
			$orderid[$k1]['total']=$total;
			$orderid[$k1]['history']=$history;
		}
		$oSmarty->assign("orderid",$orderid);
		$oSmarty->display('history.tpl');
	}
	
	function getCategory($lang_id=0){
        $table = 'tbl_lands_category';
        $cond = '';
        if ($lang_id!=0)
            $cond .= " LangID = {$lang_id} ";
        $result = $this->multiLevel( $table, "id", "ParentID", "*", $cond, "Ordering ASC");
        
        $category = array();
        foreach ($result as $value => $key)
        {
            if( $key['level'] > 0){                
                $name = $this -> getPrefix( $key['level']).$key['Name'];
            }
            else 
                $name = $key['Name'];
            $category[$key['id']] = $name;
        }
        
        return $category;
    }
	function randomPassword() {
		
	    $chars = "abcdefghijkmnopqrstuvwxyz023456789";
	    srand((double)microtime()*1000000);
	    $i = 0;
	    $pass = '' ;
	    while ($i <= 5) {
	        $num = rand() % 33;
	        $tmp = substr($chars, $num, 1);
	        $pass = $pass . $tmp;
	        $i++;
	    }
	    return $pass;
	}
	
   function anti_hack($string)
   {
   $cautruyvan = $string;
   $tukhoa = array('chr(', 'chr=', 'chr ', ' chr', 'wget ', ' wget', 'wget(',
  			       'cmd=', ' cmd', 'cmd ', 'rush=', ' rush', 'rush ',
                    'union ', ' union', 'union(', 'union=', 'echr(', ' echr', 'echr ', 'echr=',
                    'esystem(', 'esystem ', 'cp ', ' cp', 'cp(', 'mdir ', ' mdir', 'mdir(',
                    'mcd ', 'mrd ', 'rm ', ' mcd', ' mrd', ' rm',
                    'mcd(', 'mrd(', 'rm(', 'mcd=', 'mrd=', 'mv ', 'rmdir ', 'mv(', 'rmdir(',
                    'chmod(', 'chmod ', ' chmod', 'chmod(', 'chmod=', 'chown ', 'chgrp ', 'chown(', 'chgrp(',
                    'locate ', 'grep ', 'locate(', 'grep(', 'diff ', 'kill ', 'kill(', 'killall',
                    'passwd ', ' passwd', 'passwd(', 'telnet ', 'vi(', 'vi ',
                    'insert into', 'select ', 'nigga(', ' nigga', 'nigga ', 'fopen', 'fwrite', ' like', 'like ',
                    '$_request', '$_get', '$request', '$get', '.system', 'HTTP_PHP', '&aim', ' getenv', 'getenv ',
                    'new_password', '&icq','/etc/password','/etc/shadow', '/etc/groups', '/etc/gshadow',
                    'HTTP_USER_AGENT', 'HTTP_HOST', '/bin/ps', 'wget ', 'uname\x20-a', '/usr/bin/id',
                    '/bin/echo', '/bin/kill', '/bin/', '/chgrp', '/chown', '/usr/bin', 'g\+\+', 'bin/python',
                    'bin/tclsh', 'bin/nasm', 'perl ', 'traceroute ', 'ping ', '.pl', '/usr/X11R6/bin/xterm', 'lsof ',
                    '/bin/mail', '.conf', 'motd ', 'HTTP/1.', '.inc.php', 'config.php', 'cgi-', '.eml',
                    'file\://', 'window.open', '<SCRIPT>', 'javascript\://','img src', 'img src','.jsp','ftp.exe',
                    'xp_enumdsn', 'xp_availablemedia', 'xp_filelist', 'xp_cmdshell', 'nc.exe', '.htpasswd',
                    'servlet', '/etc/passwd', 'wwwacl', '~root', '~ftp', '.js', '.jsp', 'admin_', '.history',
                    'bash_history', '.bash_history', '~nobody', 'server-info', 'server-status', 'reboot ', 'halt ',
                    'powerdown ', '/home/ftp', '/home/www', 'secure_site, ok', 'chunked', 'org.apache', '/servlet/con',
                    '<script', '/robot.txt' ,'/perl' ,'mod_gzip_status', 'db_mysql.inc', '.inc', 'select from',
                    'select from', 'drop ', '.system', 'getenv', 'http_', '_php', 'php_', 'phpinfo()', '<?php', '?>', 'sql=');
 
   $kiemtra = str_replace($tukhoa, '*', $cautruyvan);
   return $kiemtra;
 
  /* if ($cautruyvan != $kiemtra)
     {
       $cremotead = $_SERVER['REMOTE_ADDR'];
       $cuseragent = $_SERVER['HTTP_USER_AGENT'];
 
       die( "Phat hien co su tan cong! <br /><br /><b>Viec tan cong nay da bi ngan chan va se duoc ghi nhan lai:</b><br />$cremotead - $cuseragent" );
     }*/
   }
	
	
}
?>