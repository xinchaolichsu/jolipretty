{literal}
<style type="text/css">
#form_contact b{
	float:left;
	width:170px;
	margin-top:5px;
	text-align:left;
}

#form_contact span{
	float:right;
	width: 65%;
	margin-top:5px;
	text-align:left;
}
#form_contact .line{
	float:left;
	width:85%; color:#6a6a6a; 
	padding:3px 0px;
	font-size:11px;
}
</style>
<script type="text/javascript">		
function checkSubmit()
{	
	var re = /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/;
	var re123 = /\s+/;
	var email = $('#tex_email').val();
	
	if( email == "" )
	{
		alert ( "Your e-mail should not be empty" );
		$('#tex_email').focus();
		return false;
	}
	if ( !email.match(re) )
	{
		alert ("Invalid email address");
		$('#tex_email').focus();
		return false;
	} 
	
	if($("#first_name").val()=="")
	{
		alert("Please enter your first name");
		$("#first_name").focus();
		return false;
	}

	if($("#last_name").val()=="")
	{
		alert("Please enter your last name");
		$("#last_name").focus();
		return false;
	}
	if($("#tex_address").val()=="")
	{
		alert("Please enter your postal address");
		$("#tex_address").focus();
		return false;
	}

	if($("#city").val()=="")
	{
		alert("Please enter your city");
		$("#city").focus();
		return false;
	}
	if($("#postal").val()=="")
	{
		alert("Please enter your ZIP code");
		$("#postal").focus();
		return false;
	}
	
	if($("#tex_phone").val()=="" )
	{
		alert("Please enter your handphone number");
		$("#tex_phone").focus();
		return false;
	}
	
	if($("#tex_phone").val()!="" && isNaN($("#tex_phone").val()))
	{
		alert("Invalid handphone number");
		$("#tex_phone").focus();
		return false;
	}
	
	if ( frm.txt_captcha.value == '')
	{
		alert ("You did not enter the security code!");
		frm.txt_captcha.focus();
		return false;
	}

	
	return true;
}

</script>
{/literal}
<div class="box_center1">
<div class="box_center_title1">{#edit_profile#}</div>
<div class="box_center_content1" >
	
		{if $error!=""}
			<div style="color: #FF0000; padding-left: 10px; padding-top: 10px;">
				{$error}
			</div>
		{/if}					
		<div id="form_contact">
        <form action="" method="post" style="margin: 0px; padding: 0px;" onsubmit="return checkSubmit();">
        <div class="box_center" style="width:500px;">
        <div class="box_center_title" style="width:500px;color:#292929; font-size:17px; padding-left:0;">{#login_info#}</div>
        <div class="box_center_content" style=" width:500px;">
                <div class="line">Email address <strong style="color:#FF0000">* </strong>: <span>
                  <input type="text" class="bg_input" id="tex_email" name="tex_email" value="{$user.Email}"/>
                </span></div>
    
                <div class="line">Password <strong style="color:#FF0000">* </strong>:<span>
                  <input type="password" class="bg_input" id="tex_password" name="tex_password" value=""/>
                </span></div>
                
<!--                <div class="line">Repeat password <strong style="color:#FF0000">* </strong>:<span>
                  <input type="password" class="bg_input" id="tex_password_confirm" name="tex_password_confirm" value="{$smarty.request.tex_password_confirm}"/>
                </span></div>
-->                <div class="line">First name <strong style="color:#FF0000">* </strong>: <span>
                  <input type="text" class="bg_input" id="first_name" name="first_name" value="{$user.FullName}"/>
                </span></div>
                
                <div class="line">Last name <strong style="color:#FF0000">* </strong>: <span>
                  <input type="text" class="bg_input" id="last_name" name="last_name" value="{$user.LastName}"/>
                </span></div>
				<div class="line">Birthday :
				<span>
					<select id="date" name="Date" >
					{if $user}{html_options options=$date selected=$user.Date}  {else} {html_options options=$date selected=$smarty.request.Date}{/if}
					</select>&nbsp;&nbsp;-&nbsp;
					<select id="month" name="Month">
					{if $user}{html_options options=$month selected=$user.Month}{else}{html_options options=$month selected=$smarty.request.Month}{/if}
					</select>&nbsp;&nbsp;-&nbsp;
					<select id="year" name="Year">
					{if $user}{html_options options=$year selected=$user.Year}{else}{html_options options=$year selected=$smarty.request.Year}{/if}
					</select>
				</span></div>
            </div>
            </div>
            <div class="box_center" style="width:500px;">
            <div class="box_center_title" style="width:500px; color:#292929; font-size:17px;padding-left:0;">{#add_detail#}</div>
            <div class="box_center_content" style=" width:500px; margin-bottom:20px; overflow:inherit;">
                            
<!--                <div class="line">Company <strong style="color:#FF0000">* </strong>: <span>
                  <input type="text" name="company" class="bg_input" id="company" value="{$smarty.request.company}" />
                </span></div>
-->                                                
                <div class="line">Address <strong style="color:#FF0000">* </strong>: <span>
                 <input type="text" name="tex_address"class="bg_input" id="tex_address" value="{$user.Address}" /> 
                </span></div>
                                                
                <div class="line">City <strong style="color:#FF0000">* </strong>: <span>
                  <input type="text" name="city" class="bg_input" id="city" value="{$user.Yahoo}" />
                </span></div>
                
                <div class="line">Postal/Zip code <strong style="color:#FF0000">* </strong>:<span>
                  <input type="text" name="postal" class="bg_input" id="postal" value="{$user.Postal}" />
                </span></div>
                
                <div class="line">Country <strong style="color:#FF0000">* </strong>:<span>
                    <select name="country" id="country"  style="height:20px; width:150px;" >
                    	{foreach from=$country item=item name=item}
                            <option value="{$item.id}" {if $user.CityID == $item.id} selected="selected" {/if}>{$item.Name}</option>
                        {/foreach}
                    </select>
                </span></div>
                
                <div class="line">Mobile Number <strong style="color:#FF0000">* </strong>:<span>
                  <input type="text" name="tex_phone" class="bg_input" id="tex_phone" value="{$user.Phone}" />
                </span></div>
    
<!--              <span style=" width:169px; font-weight:700; height:20px;float:left;"> Terms member:</span> 
               <div style="width:485px;height:auto;float:left; ">{$terms}</div>
-->               
                
                <div align="center" style="margin:10px 0 0 -100px;">
                        <input type="submit" class="bg_submit" value="Update" />
<!--                        <input type="reset" class="bg_submit" name="bt_reset" value="{#reset#}" />
                        <div class="bg_submit" style="width:148px;"><a href="{$smarty.const.SITE_URL}customer/forgot_password/">{#forgot_pass#}</a></div>
-->                </div>
            <br><br>
            </div>
            </div>
        </form>
            
		</div>
	</div>
    <div class="bg_bottom"></div>
</div>