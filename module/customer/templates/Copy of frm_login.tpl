<div class="box_left">
	<div class="side_title">{#acc_manager#}</div>
	<div class="side_content">
		<div style="height:24px; line-height:24px; background:url(/view/images/icon/profile.jpg) 5px 5px no-repeat" {if $smarty.get.task == 'edit_profile'} class="cus_active" {else} class="cus_item" {/if}>
        	<a href="/customer/edit_profile/">{#profile#}</a>
		</div>
        <div style="height:24px; line-height:24px; background:url(/view/images/icon/change_pass.jpg) 5px 5px no-repeat" {if $smarty.get.task == 'change_pass'} class="cus_active" {else} class="cus_item" {/if}>
        	<a href="/customer/change_pass/">{#change_pass#}</a>
		</div>
        
        <!-- BEGIN CART-->
        <div style="height:24px; line-height:24px; background:url(/view/images/icon/cart_manager.jpg) 5px 5px no-repeat" {if $smarty.get.task == 'order'} class="cus_active" {else} class="cus_item" {/if}>
        	<a href="/customer/order/">{#cart_manager#}</a>
		</div>
        {******
        <div {if $smarty.get.task == 'order_yet'} class="cus_sub_active" {else} class="cus_sub_item" {/if}>
        	<a href="/customer/order-yet/">{#order_cart#}</a>
        </div>
        <div {if $smarty.get.task == 'order_finish'} class="cus_sub_active" {else} class="cus_sub_item" {/if}>
        	<a href="/customer/order-finish/">{#finish_cart#}</a>
        </div>
        ******}
        <!-- END CART -->
        
        <div style="line-height:24px; background:url(/view/images/icon/inbox.jpg) 5px 5px no-repeat" {if $smarty.get.mod == 'message' && $smarty.get.task == 'toMsg'} class="cus_active" {else} class="cus_item" {/if}>
        	<a href="/message/to/">{#inbox#}</a> <br />
            <span style="font-size:11px; color:#704b39">(<b>{$count}</b> {#unread#})</span>
		</div>
		<div style="height:22px; line-height:18px; background:url(/view/images/icon/logout.jpg) 5px 0px no-repeat" class="cus_item">
        	<a href="/customer/logout/">{#logout#}</a>
		</div>
	</div>
</div>


