{literal}
	<script language="javascript">
		function checkForm()
		{
			var re = /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/;
			var email = $('#tex_email').val();
			
			if ( email == '')
			{
				alert ( "You must be fill your email " );
				$('#tex_email').focus();
				return false;
			}
			if ( !email.match(re) )
			{
				alert ( "Invalid email address ");
				$('#tex_email').focus();
				return false;
			}
			
		}
	</script>
{/literal}
<div class="box_center1">
<div class="box_center_title1">{#forgot_pass#}</div>
<div class="box_center_content1">
				{if $error!=""}
				<div style="color: #FF0000; padding-left: 10px; padding-top: 10px;">
					{$error}
				</div>
				{/if}
				<div style="width:100%; color:#6D86DB; text-indent:10px; font-size:13px; float:left;">Please enter your email address to reset your password</div>
				{if $error!="success" && $error!="logout_success"}
				<div style="margin: 10px 10px 0 10px;">				
					<form action="" method="post" onsubmit="return checkForm();" style="margin: 0px; padding: 0px;">
							{*<img style="float: right; border: 1px solid #25261e;" src="{$smarty.const.SITE_URL}themes/{$smarty.session.theme}/images/user.jpg" />*}
	
						Email address<br />
						<input type="text" style="width: 300px;" name="tex_email" id="tex_email" value=""/><br /><br />
						<input type="submit" class="button_1" value="&nbsp;Send&nbsp;" style=""/>
					</form>				
				</div>
				{else}
					{php}
						redirect($_GET["url"], 3);
					{/php}
				{/if}
		</div>
</div>