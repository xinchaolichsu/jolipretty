{literal}
<style type="text/css">
#form_contact b{
	float:left;
	width:170px;
	margin-top:5px;
	text-align:left;
}

#form_contact span{
	float:right;
	width: 85%;
	margin-top:5px;
	text-align:left;
}
#form_contact .line{
	float:left;
	width:85%; color:#6a6a6a; 
	padding:3px 0px;
	font-size:11px;
}
</style>
<script type="text/javascript">	
	function checkchangepass()
	{
		if($("#tex_password_old").val()=="")
		{
			alert("Please enter old password");
			$("#tex_password_old").focus();
			return false;
		}
		if($("#tex_password_new").val()=="")
		{
			alert("Please enter new password");
			$("#tex_password_new").focus();
			return false;
		}
		if($("#tex_password_new").val()!=$("#tex_password_new_confirm").val())
        {
            alert("password & confirmation do not match");
            $("#tex_password_new_confirm").focus();
            return false;
        }
		return true;
	}
</script>
{/literal}
<div class="box_center1">
<div class="box_center_title1">{#change_pass#}</div>
<div class="box_center_content1">
		{if $error!=""}
			<div style="color: red; padding-left: 10px; padding-top: 10px;">
				{$error}
			</div>
		{/if}					
		<div id="form_contact">
			<form action="" method="post" style="margin: 0px; padding: 0px;"  onsubmit="return checkchangepass();">
            	<div class="line">{#old_pass#}:<span>
				  <input type="password" class="bg_input" id="tex_password_old" name="tex_password_old" value="{$smarty.request.tex_password_old}"/>
				</span></div>
            	<div class="line">{#new_pass#} :<span>
				  <input type="password" class="bg_input" id="tex_password_new" name="tex_password_new" value="{$smarty.request.tex_password_new}" onblur="length_pass_new();"/>
				</span></div>
				
				<div class="line">Confirm password :<span>
				  <input type="password" class="bg_input" id="tex_password_new_confirm" name="tex_password_new_confirm" value="{$smarty.request.tex_password_new_confirm}" onblur="check_tex_password_new_confirm();"/>
				</span></div>
                
                <div align="left" style="margin:10px 0 0 50px;">
					<span style="text-align:left; float:left">
						<input type="submit" class="bg_submit" value="{#change#}" />
						<input type="reset" class="bg_submit" name="bt_reset" value="{#reset#}" />
					</span>
				</div>
            </form>
		</div>
    </div>
</div>