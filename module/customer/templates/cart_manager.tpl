{literal}
<script type="text/javascript">
function confirmDel()
{
	if(confirm('Bạn có muốn xóa không?'))
		return true;
	else
		return false;
}
</script>
{/literal}

<div class="box_center">		
        	<div class="box_center_title">
            	{#cart_manager#}
        	</div>
		<div class="box_center_content">
			 <table width="98%" cellpadding="4" cellspacing="2" bgcolor="#704b3a" align="center" style="font-size:11px">
			   <tr align="center">
               		<th style="color:#fff" width="5%">STT</th>
                    <th style="color:#fff" width="20%">Sản phẩm</th>
                    <th style="color:#fff" width="10%">Mã</th>
                    <th style="color:#fff" width="10%">Size</th>
                    <th style="color:#fff" width="10%">Color</th>
                    <th style="color:#fff" width="10%">Số lượng</th>				   	
                    <th style="color:#fff" width="10%">Đơn Giá</th>					
					<th style="color:#fff" width="10%">Thành tiền</th>
                    <th style="color:#fff" width="10%">Trạng thái</th>
                    <th style="color:#fff" width="10%">Chi tiết</th>
			   </tr>
			   {foreach item=item from=$result name= item}
			   {assign var="pid" value=$item.product_id}
			   <tr>			
               	   <td bgcolor="#FFFFFF" align="center" style="color:#006699; font-weight:bold">{$item.tt}</td>  
                   <td bgcolor="#FFFFFF" align="center"><br />				   		
				   		<a href='{$smarty.const.SITE_URL}product/{$pid}/{$item.Name|remove_marks}.html' target="_blank"><img src="{$smarty.const.SITE_URL}{$item.Photo}" height="88" border="0" /></a><br /><br />
						<a href='{$smarty.const.SITE_URL}product/{$pid}/{$item.Name|remove_marks}.html' target="_blank" style="color:#704b3a; font-weight:bold">{$item.Name}</a><br /><br />						
				   </td>
                   <td bgcolor="#FFFFFF" align="center" style="color:#704b3a; font-weight:bold">{$item.Code}</td> 
                   <td bgcolor="#FFFFFF" align="center" style="color:#704b3a; font-weight:bold">{$item.size}</td>	
                   <td bgcolor="#FFFFFF" align="center" style="color:#704b3a; font-weight:bold">{$item.color}</td>
                   <td bgcolor="#FFFFFF" align="center" style="color:#704b3a; font-weight:bold">{$item.quantity}</td>
				   
                   <td bgcolor="#FFFFFF" align="right"><span style="color:#FF0000; font-weight:bold">
                        {$item.OldPrice|number_format:0:".":" "}
                         VNĐ</span>
					</td>
				   
				   <td bgcolor="#FFFFFF" align="right">
                   		<span style="color:#FF0000; font-weight:bold">{$item.quantity*$item.OldPrice|number_format:0:".":"."} VNĐ</span>
                   </td>
                   <td bgcolor="#FFFFFF" align="center" style="color:#704b3a; font-weight:bold">                   
                   		{if $item.Status == 0}
                        	{#order_cart#}
                        {/if}
                        {if $item.Status == 1}
                            {#finish_cart#} <br />
                            <a href="/customer/del-{$item.id}/" style="color: #704b3a;" onclick="return confirmDel()">
                            	<img src="/view/images/icon/delete.jpg" alt="Xóa" />
                            </a>
                        {/if}
                   </td>
                   <td bgcolor="#FFFFFF" align="center"><a style="color:#704b3a; font-weight:bold" target="_blank" href="{$smarty.const.SITE_URL}product/{$pid}/{$item.Name|remove_marks}.html" title="{$item.Name}">{#details#}</a></td> 
			   </tr>
			   {foreachelse}
				<tr>
				   <td colspan="10" align="center" bgcolor="#FFFFFF" align="center" style="color:#704b3a;">Hiện không có đơn hàng nào</td>
			   </tr>
			   {/foreach}		   
			 </table>
	</div>
</div>

