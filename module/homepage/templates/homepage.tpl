<div class="container pd0">
    <div class="col-md-12 col-sm-12 col-xs-12 title-product-home text-center">
        New arrivals
    </div>
</div>
{if $new_items != ''}
<div class="container-fluid pd0 home_menu_bottom">
    <div class="slide-lis owl-mobile">
        <div class="owl-carousel lis-slide2 ">
            {foreach from=$new_items item=item name=item}
            <div>
                <a href="{$smarty.const.SITE_URL}product/{$item.id}/{$item.Name|remove_marks}.html" target="" title="{$item.Name}">
                    <img src="{$smarty.const.SITE_URL}{$item.Photo}"/>
                </a>
            </div>
            {/foreach}
        </div>
    </div>
    {literal}
    <script>
        $('.lis-slide2').owlCarousel({
        loop:true,
        nav:true,
        dots:false,
        autoplayTimeout:10000,
        autoplay:true,
        margin:30,
        responsiveClass:true,
        smartSpeed:450,
        navText : ['<i class="pe-7s-angle-left"></i>','<i class="pe-7s-angle-right"></i>'],
        responsive:{
            0:{
                items:1,
            },
            450:{
                items:1,
            },
            600:{
                items:3,
            },
            1000:{
                items:5,
            }
        }
    });
    </script>
    {/literal}
</div>
{/if}
<hr>
{php}
    loadModule("imagehome");
{/php}