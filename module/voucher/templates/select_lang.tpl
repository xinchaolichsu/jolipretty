<div class="select_lang">
	<div id="lsd-container" class="lsd-dropdown">
        <ul class="" style="height: 22px; display: block; overflow: hidden;">
            <li class="lsd-active">
            <a href="/vn/" title="Vietnamese">
                VN
                <img width="16" height="11" alt="Vietnamese" src="{$smarty.const.SITE_URL}view/images/icon/vn.png">
            </a>
            </li>
            <a href="/en/" title="English">
                <li class="lsd-nonactive">
                    EN
                    <img width="16" height="11" alt="English" src="{$smarty.const.SITE_URL}view/images/icon/en.png">
                </li>
            </a>
        </ul>
    </div>
</div>



{literal}
<script type="text/javascript">
$(document).ready(function(){var b=$("#lsd-container ul"),a=function(c){b.stop().animate({height:22}).removeClass("lsd-active")};b.click(function(c){if($(this).hasClass("lsd-active")){a(b)}else{b.stop().animate({height:78}).addClass("lsd-active")}});$(document).bind("click",function(d){var c=$(d.target).parents();if(!c.hasClass("lsd-dropdown")){a(b)}})});
</script>
{/literal}