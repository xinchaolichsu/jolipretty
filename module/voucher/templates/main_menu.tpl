<script src="{$smarty.const.SITE_URL}lib/sticky_menu/sticky_menu.js" type="text/javascript">
</script>
<script src="/lib/drop_menu/js/jquery.hoverIntent.minified.js" type="text/javascript">
</script>
<script src="/lib/drop_menu/js/jquery.dcmegamenu.1.3.3.js" type="text/javascript">
</script>
<link href="/lib/drop_menu/css/skins/blue.css" rel="stylesheet" type="text/css"/>
{literal}
<script type="text/javascript">
    $(document).ready(function($){
    $('#mega-menu-4').dcMegaMenu({
        //event: 'click',
        //fullWidth: true, /* true, false */
        rowItems: '3', /*1,2,3,4,5 */
        speed: 'medium', /* fast, slow */
        effect: 'slide' /* fade, slide */
    });
});
        $(document).ready(function(){
            $("#header").sticky({topSpacing:0});
        });
</script>
{/literal}
<div id="header">
    <div class="ground_menu">
        <div class="main_menu">
            <div class="blue">
            <div class="col-md-12 pd0 nav-lis">
                <nav class="navbar" role="navigation">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="navbar-collapse navbar-ex1-collapse">
                        <!-- <ul class="nav navbar-nav lis-nav"> -->
                        <ul class="mega-menu" id="mega-menu-4">
                            <li>
                                <a class="menu_home " href="{$smarty.const.SITE_URL}">
                                    <img alt="home" src="{$smarty.const.SITE_URL}view/images/background/home.png"/>
                                </a>
                            </li>
                            <li>
                                <a href="{$smarty.const.SITE_URL}">
                                    <span $smarty.get.mod="" class="menu_item" if}="" onmouseout="$(this).removeClass('menu_active'); $(this).addClass('menu_item');" onmouseover="$(this).removeClass('menu_item'); $(this).addClass('menu_active');" {="" {else}="" {if="" }="">
                                        <span class="menu_bt_left">
                                        </span>
                                        <span class="menu_bt_center">
                                            {#HOME#}
                                        </span>
                                        <span class="menu_bt_right">
                                        </span>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="{$smarty.const.SITE_URL}product/">
                                    <span $smarty.get.mod="product" class="menu_item" if}="" onmouseout="$(this).removeClass('menu_active'); $(this).addClass('menu_item');" onmouseover="$(this).removeClass('menu_item'); $(this).addClass('menu_active');" {="" {else}="" {if="" }="">
                                        <span class="menu_bt_left">
                                        </span>
                                        <span class="menu_bt_center">
                                            {#PRODUCT#}
                                        </span>
                                        <span class="menu_bt_right">
                                        </span>
                                    </span>
                                </a>
                                {if $cap1_pro}
                                <ul>
                                    {foreach from=$cap1_pro item=cap1_pro name=cap1_pro}
                                    <li>
                                        <a href="{$smarty.const.SITE_URL}product/c-{$cap1_pro.id}/{$cap1_pro.Name|remove_marks}.html" title="{$cap1_pro.Name}">
                                            {$cap1_pro.Name}
                                        </a>
                                        {if $cap1_pro.cap2_pro}
                                        <ul>
                                            {foreach from=$cap1_pro.cap2_pro item=cap2_pro name=cap2_pro}
                                            <li>
                                                <a href="{$smarty.const.SITE_URL}product/c-{$cap2_pro.id}/{$cap2_pro.Name|remove_marks}.html" title="{$cap2_pro.Name}">
                                                    {$cap2_pro.Name}
                                                </a>
                                            </li>
                                            {/foreach}
                                        </ul>
                                        {/if}
                                    </li>
                                    {/foreach}
                                </ul>
                                {/if}
                            </li>
                            <li>
                                <a href="{$smarty.const.SITE_URL}news/">
                                    <span $smarty.get.mod="news" class="menu_item" if}="" onmouseout="$(this).removeClass('menu_active'); $(this).addClass('menu_item');" onmouseover="$(this).removeClass('menu_item'); $(this).addClass('menu_active');" {="" {else}="" {if="" }="">
                                        <span class="menu_bt_left">
                                        </span>
                                        <span class="menu_bt_center">
                                            {#NEWS#}
                                        </span>
                                        <span class="menu_bt_right">
                                        </span>
                                    </span>
                                </a>
                                {if $cap1}
                                <ul class="nav navbar-nav lis-nav">
                                    {foreach from=$cap1 item=cap1 name=cap1}
                                    <li class="menu-item">
                                        <a href="{$smarty.const.SITE_URL}news/c-{$cap1.id}/{$cap1.Name|remove_marks}.html" title="{$cap1.Name}">
                                            {$cap1.Name}
                                        </a>
                                        {if $cap1.cap2}
                                        <ul>
                                            {foreach from=$cap1.cap2 item=cap2 name=cap2}
                                            <li>
                                                <a href="{$smarty.const.SITE_URL}news/c-{$cap2.id}/{$cap2.Name|remove_marks}.html" title="{$cap2.Name}">
                                                    {$cap2.Name}
                                                </a>
                                            </li>
                                            {/foreach}
                                        </ul>
                                        {/if}
                                    </li>
                                    {/foreach}
                                </ul>
                                {/if}
                            </li>
                            <li>
                                <a href="{$smarty.const.SITE_URL}pt/">
                                    <span $smarty.get.mod="phongthuy" class="menu_item" if}="" onmouseout="$(this).removeClass('menu_active'); $(this).addClass('menu_item');" onmouseover="$(this).removeClass('menu_item'); $(this).addClass('menu_active');" {="" {else}="" {if="" }="">
                                        <span class="menu_bt_left">
                                        </span>
                                        <span class="menu_bt_center">
                                            {#Feng_Shui#}
                                        </span>
                                        <span class="menu_bt_right">
                                        </span>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="{$smarty.const.SITE_URL}about/">
                                    <span $smarty.get.mod="about" class="menu_item" if}="" onmouseout="$(this).removeClass('menu_active'); $(this).addClass('menu_item');" onmouseover="$(this).removeClass('menu_item'); $(this).addClass('menu_active');" {="" {else}="" {if="" }="">
                                        <span class="menu_bt_left">
                                        </span>
                                        <span class="menu_bt_center">
                                            {#ABOUT#}
                                        </span>
                                        <span class="menu_bt_right">
                                        </span>
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="{$smarty.const.SITE_URL}contact/">
                                    <span $smarty.get.mod="contact" class="menu_item" if}="" onmouseout="$(this).removeClass('menu_active'); $(this).addClass('menu_item');" onmouseover="$(this).removeClass('menu_item'); $(this).addClass('menu_active');" {="" {else}="" {if="" }="">
                                        <span class="menu_bt_left">
                                        </span>
                                        <span class="menu_bt_center">
                                            {#CONTACT#}
                                        </span>
                                        <span class="menu_bt_right">
                                        </span>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <div class="cart_menu">
                {php}
                        loadModule("cart","show_count");
                    {/php}
            </div>
        </div>
    </div>
</div>
