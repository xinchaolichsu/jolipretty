<?php
/**
 * VietEsoft
 * ===============================================================================================
 * Copyright 2010.
 * Website: http://www.vietesfot.com, http://thietkewebsitetrongoi.net
 * -----------------------------------------------------------------------------------------------
 * Phát triển bới VietEsoft
 * -----------------------------------------------------------------------------------------------
 * Chú ý: Tất cả các hàm, phương thức được viết mới do mình tự định nghĩa phải để lại comment gồm:
 	- Người viết
	- Ngày-tháng-năm
	- Chức năng
 * ===============================================================================================
 * Hiển thị hỗ trợ trực tuyến
*/
if (!defined('IN_VES')) die('Hacking attempt');
class support extends VES_FrontEnd 
{	
	var $table; 		/* Khai báo bảng dữ liệu */
	var $mod;			/* Khai báo mod */
	var $id;			/* Khai báo id */
	var $LangID;		/* Khai báo id ngôn ngữ */
	var $field;			/* Khai báo trường dữ liệu */
	var $pre_table;		/* Khai báo bảng danh mục dữ liệu */	
	function __construct(){		
		$this->table = 'tbl_support_online';
		$this->pre_table = "";
		$this->id = mysql_real_escape_string(($_REQUEST['id']=='')?'0':$_REQUEST['id']);
		$this->field = "*";
		$this->LangID = $_SESSION['lang_id'];
		parent::__construct($this->table);
	}
	function run($task= "")
	{
		if($task==''){$task = $_GET['task'];}
	    switch($task)
			{
				case 'home':
					$this -> home();
				break;
				case 'thickbox':
					$this -> thickbox();
					break;
				case 'facebook':
					$this -> facebook();
				break;
				case 'footer':
					$this -> footer();
				break;
				default:
                    /* List toàn bộ bản ghi */
					$this->listItem();
                    
                    /*List toàn bộ bản ghi theo từng danh mục*/
                    //$this->listbyCategory();
				break;	
			}
	}
	/**
	 * Page info
	 *
	 * @param $task
	 */
	function getPageinfo($task= "")
	{
	 	global $oSmarty, $oDb;
		switch ($task) {
			case 'category':
				$id = $_GET['id'];
				if ($id)
				{
					$sql = "SELECT Name FROM {$this->pre_table} WHERE id=$id";	
					$row = $oDb->getOne($sql);	
				}
				$aPageinfo=array('title'=> $row." | " . parent::get_config_vars("support","").' | '. PAGE_TITLE, 'keyword'=>PAGE_KEYWORD.",".$row, 'description'=>PAGE_DESCRIPTION);
				$aPath[] = array('link' => '', 'path' => 'support');
				break;
            case 'details':
                $id = $_GET['id'];
                if ($id)
                {
                    $sql = "SELECT Name FROM {$this->table} WHERE id=$id";    
                    $row = $oDb->getOne($sql);    
                }
                $aPageinfo=array('title'=> $row." | " . parent::get_config_vars("support","").' | '. PAGE_TITLE, 'keyword'=>PAGE_KEYWORD.",".$row, 'description'=>PAGE_DESCRIPTION);
                $aPath[] = array('link' => '', 'path' => 'support');
                break;
			default:			
				$aPageinfo=array('title'=> parent::get_config_vars("support","").' | ' . PAGE_TITLE, 'keyword'=>PAGE_KEYWORD.",".$row, 'description'=>PAGE_DESCRIPTION);
				$aPath[] = array('link' => '', 'path' => 'support');
				break;
		}
		
		$oSmarty->assign('aPageinfo', $aPageinfo);
		$oSmarty->assign("aPath", $aPath);
	}
	
	function footer()
	{
		global $oDb,$oSmarty;
		$where = " Status=1 ";
		if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		$sql="select * from {$this->table } where {$where} Order by Ordering Limit 4";
		$listItem=$oDb->getAll($sql);
		$oSmarty->assign('listItem',$listItem); 
		$oSmarty->display('footer_support.tpl');	
	}
	
	/**
	 * Hiển thị toàn bộ danh sách bản ghi
	 *
	 */
	function home()
	{
		global $oDb,$oSmarty;
		$where = " Status=1 ";	
		if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		$sql="select * from {$this->table} where {$where}";
		$home_support_1=$oDb->getAll($sql);
		$oSmarty->assign('home_support_1',$home_support_1);
		
		$oSmarty->display('home_support.tpl');
	}
	
	function facebook()
	{
		global $oSmarty, $oDb; 
		$oSmarty->display('facebook.tpl');	
	}
	
	function thickbox()
	{
		global $oDb,$oSmarty;
		$where = " Status=1 ";
		if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		if($_SERVER['REQUEST_METHOD']=="POST")
        {
				$data = array(
					"FullName" => $_POST['FullName'],
					"Email" => $_POST['Email'],
					"Content" => $_POST['Content'],
					"CreateDate" => date("Y-m-d"),
					"Status" => "1"
				);
				$oDb -> autoExecute("tbl_contact_item",$data,DB_AUTOQUERY_INSERT);
				$url = SITE_URL;
				echo "<script language = 'javascript'>alert('Thư của bạn đã được gửi đến chúng tôi!');</script>";
				echo "<script language = 'javascript'>self.parent.tb_remove();self.parent.hrefUrl('{$url}')</script>";
				exit();
		}
		else
		{
			$sql="select * from {$this->table } where {$where} Order by Ordering LIMIT 4";
			$listItem=$oDb->getAll($sql);
			$oSmarty->assign('captcha',$_SESSION['key_captcha']); 
			$oSmarty->assign('support',$listItem); 
			$oSmarty->display('thickbox.tpl');
		}
	}
	function listItem()
	{
		global $oDb,$oSmarty;
		$where = " Status=1 ";
		if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		$sql="select * from {$this->table } where {$where} Order by Ordering";
		$listItem=$oDb->getAll($sql);
		//pre($listItem);
		$oSmarty->assign('listItem',$listItem); 
		$oSmarty->display('display_support.tpl');	
	}
	
    	
}
?>