<?php
/**
 * VietEsoft
 * ===============================================================================================
 * Copyright 2010.
 * Website: http://www.vietesfot.com, http://thietkewebsitetrongoi.net
 * -----------------------------------------------------------------------------------------------
 * Phát triển bới VietEsoft
 * -----------------------------------------------------------------------------------------------
 * Chú ý: Tất cả các hàm, phương thức được viết mới do mình tự định nghĩa phải để lại comment gồm:
 	- Người viết
	- Ngày-tháng-năm
	- Chức năng
 * ===============================================================================================
 * Hiển thị tin tức
*/
if (!defined('IN_VES')) die('Hacking attempt');
class faq extends VES_FrontEnd 
{	
	var $table;
	var $mod;
	var $id;
	var $LangID;
	var $field;
	var $pre_table;	
	function __construct(){		
		$this->table = 'tbl_faq';
		$this->mod = 'faq';
		$this->id = mysql_real_escape_string(($_REQUEST['id']=='')?'0':$_REQUEST['id']);
		$this->field = "*";
		$this->LangID = $_SESSION['lang_id'];
		parent::__construct($this->table);
	}
	function run($task= "")
	{
		switch ($task) {
			case 'home':				
				$this->home();
				break;
			case 'load_name':				
				$this->load_name();
				break;	
			case 'getLink':				
				$this->getLink();
				break;	
			case 'details':
				$this->detail($_GET['id']);
				break;
			case 'add':
				$this->add();
				break;	
			case 'load_content':
				$this->load_content();
				break;		
			default:
				global $oSmarty;
					$this->listItem();
				break;
					
		}
		
	}
	function load_name()
	{
		global $oSmarty, $oDb;
        $where = " Status=1 ";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID ={$this->LangID} ";
		$catid=$_POST['id'];
	   	$catname = $oDb->getOne("select Name from tbl_faq_category where id={$catid}");
	  	$oSmarty->assign('catname',$catname);
	   	$sql = "select id,Title from {$this->table} where {$where} AND CatID={$catid} ORDER BY Ordering";
	   	$name = $oDb->getAll($sql);
	  	$oSmarty->assign('name',$name);
		$oSmarty->display('load_name.tpl');
	}
	
	function load_Content()
	{
		global $oSmarty, $oDb;
        $where = " Status=1 ";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID ={$this->LangID} ";
		$id=$_POST['id'];
	   	$sql = "select Content,Title from {$this->table} where id={$id} ";
	   	$content = $oDb->getRow($sql);
	  	$oSmarty->assign('content',$content);
		$oSmarty->display('load_content.tpl');
	}
	
	public function create_editor($id, $content, $att=array('width'=>'450', 'height'=>'250'),$toolBar="Default")
	{
			
			
			require_once(SITE_DIR."core/fckeditor/fckeditor.php");
			$editor = new FCKeditor($id) ;
			$editor->BasePath	= SITE_URL."/core/fckeditor/";
			$editor->Value = $content; 
			$editor->Width=$att['width'];
			$editor->Height=$att['height'];
			$editor->ToolbarSet=$toolBar;
			$editor->Config['SkinPath'] = SITE_URL."core/fckeditor/editor/skins/office2003/";
			//print_r ($editor->Config);
			return $editor->Create();
			
	}	
	function add() {
		global $oDb,$oSmarty;
		$user=$_SESSION["userid_cus"];
		$oSmarty -> assign ( "user", $user );
			if($_SERVER['REQUEST_METHOD']=='POST')
				{
				unset($_POST['cmdok']);
				if (MULTI_LANGUAGE)
            	$langID = $this->LangID;
				$name = $_POST['name'];
				$email = $_POST['email'];
				$title = $_POST['title'];
				$summarise = $_POST['summarise'];
				$content= $_POST['content'];
				$date = date("d-m-Y");
				if (MULTI_LANGUAGE)
				$sql = "insert into {$this->table}(Name,Email,Title,Summarise,Content,CreateDate,Status,LangID) Values ('" .$name. "','" .$email. "','" .$title. "','" .$summarise. "','" .$content. "','" .$date. "', 1 ,".$langID.")";
				else
				$sql = "insert into {$this->table}(Name,Email,Title,Summarise,Content,CreateDate,Status) Values ('" .$name. "','" .$email. "','" .$title. "','" .$summarise. "','" .$content. "','" .$date. "', 1)";
				$oDb->query($sql);
				echo "<script language = 'javascript'>alert('Bạn đã gửi thành công!')</script>";
				echo "<script language = 'javascript'>location.href = '".$_SERVER['HTTP_REFERER']."'</script>";
				}
			else
				{
				$a= rand(0,9);
				$_SESSION["a"]= $a;
				$oSmarty->assign("a", $a);												
				
				$b= rand(0,9);		
				$_SESSION["b"]= $b;
				$c= $a+$b;
				$oSmarty->assign("b", $b);
				$oSmarty->assign("c", $c);
					$editor = $this->create_editor("content", "", $att=array('width'=>'500', 'height'=>'250'),"Basic");
					$oSmarty->assign('editor',$editor);
					$oSmarty->display("form_faq.tpl");
				}	
			
	}
	function comment() {
		global $oDb,$oSmarty;
		$user=$_SESSION["userid_cus"];
		$oSmarty -> assign ( "user", $user );
			if($_SERVER['REQUEST_METHOD']=='POST')
				{
				$id = $_POST['id'];
				if (MULTI_LANGUAGE)
				$langID =$this->LangID;
				$name = $_POST['Name'];
				$content = $_POST['comment'];
				$date = date("d-m-Y H:i:s");
				if (MULTI_LANGUAGE)
				$sql = "insert into tbl_faq_traloi(Name,Content,CreateDate,FaqID,Status,LangID) Values ('" .$name. "','" .$content. "','" .$date. "','" .$id. "', 0 ,".$langID.")";
				else
				$sql = "insert into tbl_faq_traloi(Name,Content,CreateDate,FaqID,Status) Values ('" .$name. "','" .$content. "','" .$date. "','" .$id. "', 0 )";
				$oDb->query($sql);
				echo "<script language = 'javascript'>alert('Bạn đã gửi thành công!')</script>";
				echo "<script language = 'javascript'>location.href = '".$_SERVER['HTTP_REFERER']."'</script>";
				}
	}
	
	function home() {
		global $oDb,$oSmarty;
			global $oDb,$oSmarty;
			$sql = "SELECT * FROM {$this->table} WHERE  Status=1 order by id DESC limit 1 ";		
			$bg = $oDb->getRow($sql);
			$oSmarty->assign('bg',$bg);
			
			$sql = "SELECT * FROM {$this->table} WHERE  Status=1 and id < {$bg['id']} order by id DESC limit 3";		
			$arr = $oDb->getAll($sql);
			$oSmarty->assign('item',$arr);
			$oSmarty->display("faq_home.tpl");
	}
	
	function getLink()
	{
		
		global $oDb, $oSmarty;
		$oSmarty->display("catLink_faq.tpl");
	}
	
	function detail($id)
	{
		
		global $oDb, $oSmarty;
		$where = "Status=1";
		if (MULTI_LANGUAGE)
            $where .= " AND LangID ={$this->LangID} ";
		$user=$_SESSION["userid_cus"];
		$oSmarty -> assign ( "user", $user );
	
	
		$sql = "SELECT * FROM {$this->table} WHERE  id={$id}";
		$rows = $oDb->getRow($sql);
		$oSmarty->assign('item',$rows);
		
		$oDb->query("Update {$this->table} set ViewNumber=ViewNumber+1 where id={$id}");
		$result = $oDb->getOne("Select ViewNumber from {$this->table} where id={$id}");

		$sql = "SELECT count(*) FROM tbl_faq_traloi WHERE  FaqID={$id} AND Status=1 ";
		$count = $oDb->getOne($sql);
		$oSmarty->assign('count',$count);
		$sql = "SELECT * FROM tbl_faq_traloi WHERE  FaqID={$id} AND Status=1 order by id desc";
		$tl = $oDb->getAll($sql);
		$oSmarty->assign('tl',$tl);
		
		//$user1 = $oDb->getRow("Select FullName from tbl_customer where id={$rows['CusID']}");
		//$oSmarty->assign('user1',$user1);
		$oSmarty->display("detail_faq.tpl");
	}
		
	function listItem($cat_id=0)
	{
		global $oDb,$oSmarty;
		$where = " Status=1 ";
		if (MULTI_LANGUAGE)
            $where .= " AND LangID ={$this->LangID} ";
		$sql = "SELECT id,Name FROM tbl_faq_category WHERE Status=1 order by Ordering";
		$cat = $oDb->getAll($sql);
		// var_dump($cat); die;
		// ===== garung ==========
		if(!empty($cat)) {
			foreach ($cat as $key => $value) {
				$subsql = "SELECT id, Title, Content, CreateDate FROM tbl_faq WHERE Status=1 AND CatID = {$value['id']} order by Ordering";
				$sub_cat = $oDb->getAll($subsql);
				$cat[$key]['child'] = $sub_cat; 
			}
		}
		// echo "<pre>";
		// var_dump($cat); die;
		//========================
		$oSmarty->assign('cat',$cat);
		$oSmarty->display("listfaq.tpl");
	}
	function getListItem($cat_id=0,$page_link,$cond="",$limit=8,$template="listfaq.tpl",$order="Order by id DESC")
	{
		global $oDb,$oSmarty;
		$where = " Status=1 ";
		if (MULTI_LANGUAGE)
            $where .= " AND LangID ={$this->LangID} ";
		if ($cond != "")
			$where .=" AND ".$cond;
		$sql = "SELECT COUNT(id) FROM {$this->table} WHERE {$where}";
		$total_record = $oDb->getOne($sql);
		$oSmarty->assign("total_record",$total_record);
		$listItem = parent::Paging($limit, $this->field, $where,$page_link, $order);
		foreach($listItem as $key=>$value)
		{
			$answer= $oDb->getRow("select Content from tbl_faq_traloi where FaqID={$value['id']} AND {$where} order by id DESC ");	
			$listItem[$key]['answer'] = $answer;
			$count_tl = $oDb->getOne("Select count(id) from tbl_faq_traloi where FaqID={$value['id']} AND {$where}");
			$listItem[$key]['count_tl'] = $count_tl;
			//$username = $oDb->getOne("Select FullName from tbl_customer where id={$value['CusID']}");
			//$listItem[$key]['username'] = $username;
		}
		$oSmarty->assign("listItem",$listItem);
		$oSmarty->display($template);
	}
	function getPageinfo($task= "")
	{
	 	global $oSmarty, $oDb;
		switch ($task) {
			default:
				$aPageinfo=array('title'=> 'FAQ - '.PAGE_TITLE, 'keyword'=>PAGE_KEYWORD, 'description'=>PAGE_DESCRIPTION);
				$aPath[] = array('link' => '', 'path' => 'faq');
				break;
		}
		
		$oSmarty->assign('aPageinfo', $aPageinfo);
		$oSmarty->assign("aPath", $aPath);
	}
}
?>