<hr>
<div class="row">
	<div class="col-md-2 col-md-offset-1 col-sm-12 col-xs-12 left"><a href="{$smarty.const.SITE_URL}">HOME / </a> {#FAQ#}</div>
</div>
<br>
<div class="row">
	<div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 pull-left left">
		<div class = "panel-group" id = "accordion">
			{foreach from =$cat name=cat item=cat}
			    <div class = "panel panel-default">
			      <div class = "panel-heading">
			         <h4 class = "panel-title">
			            <a data-toggle = "collapse" data-parent = "#accordion" href = "#collapse{$cat.id}">
			               <i class="fa fa-plus-circle" aria-hidden="true"></i>
 {$cat.Name}
			            </a> 
			         </h4>
			      </div>
			        {if $cat.child != ''}
			        <div id = "collapse{$cat.id}" class = "panel-collapse collapse">
			            <div class = "panel-body">
				            <div class = "panel-group" id = "sub_accordion">
								{foreach from =$cat.child name=subcat item=subcat}
								    <div class = "panel panel-default">
								        <div class = "panel-heading">
								            <h4 class = "panel-title">
									            <a data-toggle = "collapse" href = "#sub_collapse{$subcat.id}" >
									               <i class="fa fa-check-square" aria-hidden="true"></i>
 {$subcat.Title}
									            </a>
								            </h4>
								        </div>
								        <div id = "sub_collapse{$subcat.id}" class = "panel-collapse collapse">
								            <div class = "panel-body">
								            	{$subcat.Content}
								            </div>
								        </div>
								    </div>
							    {/foreach}
							</div>
			            </div>
			        </div>
			        {/if}
			    </div>
		    {/foreach}
		</div>
	</div>
</div>