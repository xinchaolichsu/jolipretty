<?php
/**
 * VietEsoft
 * ===============================================================================================
 * Copyright 2010.
 * Website: http://www.vietesfot.com, http://thietkewebsitetrongoi.net
 * -----------------------------------------------------------------------------------------------
 * Phát triển bới VietEsoft
 * -----------------------------------------------------------------------------------------------
 * Chú ý: Tất cả các hàm, phương thức được viết mới do mình tự định nghĩa phải để lại comment gồm:
 	- Người viết
	- Ngày-tháng-năm
	- Chức năng
 * ===============================================================================================
 * Hiển thị nội dung page
*/
if (!defined('IN_VES')) die('Hacking attempt');
class layout
{	
	function run($task= "")
	{
		global $oSmarty;
        $modul = (isset($_GET["mod"]) && $_GET["mod"]!=""  )?$_GET["mod"]:"home"; // Kiểm tra biến $_GET['mod']
				
		if( is_file("module/layout/templates/{$modul}.tpl")){
			$oSmarty->display($modul.".tpl");
		}else{
			$oSmarty->display("default.tpl");
		}
	}
}
?>
