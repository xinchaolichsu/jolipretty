<div class="box_center">
	<div class="box_center_title">{#recruit#} {$full_cat_name}</div>
    <div class="box_center_content">
    	<div class="detailMod 3333">
            <div class="info">
                <div class="name" style="height:50px;">
                <div class="date">
                    {$detail_item.CreateDate|date_format:"%d/%m/%Y"}
                </div>
                    {$detail_item.Name}
                </div>
                   <div class="share">
                        <div class="addthis_toolbox addthis_default_style addthis_25x25_style" style="padding-left:25px;">
                       <a class="addthis_button_google_plusone_share"></a>
                        <a class="addthis_button_facebook"></a>
                        <a class="addthis_button_zingme"></a>
                        <a class="addthis_button_govn"></a>
                        </div>
                        <script type="text/javascript" src="http://s7.addthis.com/js/250/addthis_widget.js#pubid=ra-4f2a921d35198164"></script>
                   </div>
                <div class="sum">
                    {$detail_item.Summarise}
                </div>
            </div>
            <div class="content">
            	{$detail_item.Content}
            </div>
            
            {if $other_item}
            <div class="other_item">
                <div class="title">{#other_news#}</div>
                {foreach item=item from=$other_item}
                    <div class="name">
                    	<a href="{$smarty.const.SITE_URL}info/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{$item.Name}</a>
                        <span>{$item.CreateDate|date_format:"%d/%m/%Y"}</span>
					</div>
                {/foreach}
            </div>
            {/if}
        
        </div>
    </div>
</div>
