<?php 
if (!defined('IN_VES')) die('Hacking attempt');
class menuBackEnd extends Bsg_Module_Base
{
	var $smarty;			/* Khai báo Smarty */
	var $db;				/* Khai báo db */
    var $datagrid;			/* Khai báo datagrid */
	var $id;				/* Khai báo id */
	var $imgPath;			/* Khai báo đường dẫn ảnh */
	var $imgPathShort;		/* Khai báo đường dẫn ảnh tương đối */
	var $table='';			/* Khai báo bảng dữ liệu */
	var $pre_table='';		/* Khai báo bảng dữ liệu */
	
	public function __construct($oSmarty, $oDb, $oDatagrid)
	{	
		$this -> smarty = $oSmarty;
		$this -> db = $oDb;		
        $this -> datagrid = $oDatagrid;
		$this -> id = $_REQUEST[id];		
		$this -> table	="tbl_home_menu";
		parent::__construct($oDb);		
		$this -> bsgDb -> setTable($this->table);
	}
	
	function run($task)
	{	
		switch( $task ){
			/* Thêm mới 1 bản ghi */
            case 'add':
				$this -> addItem();
				break;
			/* Edit 1 bản ghi được chọn */
			case 'edit':
				$this -> editItem();
				break;
			/* Xóa 1 bản ghi được chọn */
			case 'delete':
				$this -> deleteItem();
				break;
			/* Xóa nhiều bản ghi được chọn */
			case 'delete_all':
				$this -> deleteItems();
				break;
			/* Thay đổi trạng thái bản ghi đã chọn */
			case 'change_status':				
				$this -> changeStatus($_GET['id'], $_GET['status']);
				break;
			/* Unselect status tất cả các bản ghi */
			case 'public_all':						
				$this -> changeStatusMultiple( 1 );
				break;
			/* Select status tất cả các bản ghi */
			case 'unpublic_all':						
				$this -> changeStatusMultiple( 0 );
				break;
			/* Hiển thị select box filter */
            case 'ajax_filter':
                $this -> ajax_filter();
                break;
            case 'ajax_form':
                $this -> ajax_form();
                break;
			/* Hiển thị tất cả danh sách bản ghi theo datagrid */
			default:					
				$this -> listItem( $_GET['msg'] );		
				break;
		}
	}
	
	/**
	 * Page info
	 *
	 */
	function getPageInfo()
	{
		return true;
	}
    
	/**
	 * Tạo select box filter
	 * 
	 */
    function ajax_form()
    {
        global $oDb;
        $category = $this->getCategory($_POST['filter_title']);
        $option_str = parent::changeOption($category);
        echo $option_str;
    }
    
	/**
	 * Tạo select box filter
	 * 
	 */
    function ajax_filter()
    {
        global $oDb;
        $category = array(0 => "---Tất cả---" ) + $this->getCategory($_POST['filter_title']);
        $option_str = parent::changeOption($category);
        echo $option_str;
    }
	
	/**
	 * Thêm mới 1 bản ghi
	 * Form thêm mới
	 */
	function addItem()
	{
		$this -> getPath("Quản lý menu > Thêm mới menu");		
		$this -> buildForm();
	}
	
	/**
	 * Edit 1 bản ghi
	 * Form edit với dữ liệu một bản ghi được chọn
	 */
	function editItem()
	{
        global  $oDb;
		$id = $_GET['id'];
		$this -> getPath("Quản lý menu > Chỉnh sửa menu");	
        if(isset($_GET['id']) && intval($_GET['id']))
        {
            $sql = "SELECT * FROM tbl_home_menu WHERE id = {$id}";
            $row = $oDb->getRow($sql);
        }        
		$this -> buildForm( $row );
	}
	
	/**
	 * Xóa 1 bản ghi được chọn
	 * Hiển thị danh sách bản ghi với bản ghi đã xóa	
	 */
	function deleteItem()
	{
		global  $oDb;
		$id = $_GET["id"];
		$this -> bsgDb -> deleteWithPk( $id );
		$msg = "Xóa menu thành công!";
		$this -> listItem( $msg );
	}
	
	/**
	 * Xóa nhiều bản ghi
	 * 
	 */
	function deleteItems()
	{
		$aItems	 = $_GET['arr_check'];
		if(is_array( $aItems) && count( $aItems) > 0){
			$sItems = implode( ',', $aItems );
			$this -> bsgDb -> deleteWithPk( $sItems );
		}
		$msg = "Xóa (các) menu thành công!";
		$this -> listItem( $msg );
	}
	
	/**
	 * Thay đổi tất cả trạng thái của bản ghi
	 * 
	 */
	function changeStatusMultiple( $status = 0 )
	{
		$aItems	 = $_GET['arr_check'];
		if(is_array( $aItems) && count( $aItems) > 0){
			$sItems = implode( ',', $aItems );
			$this -> bsgDb -> updateWithPk( $sItems, array("isActive" => $status) );
		}
		$msg = "Sửa trạng thái menu thành công!";
		$this -> listItem( $msg );
	}
	
	/**
	 * Lưu thứ tự bản ghi
	 * 
	 */
	function saveOrder(){	
		$aItem = $_GET['Ordering'];
		if(is_array($aItem) && count( $aItem ) > 0){
			// save order for item.
			foreach( $aItem as $key => $value){
				if( !is_numeric($value)) $value = 0;				
				$this -> bsgDb -> updateWithPk( $key, array('Ordering' => $value ));
			}
		}	
		$msg = "Lưu thứ tự danh mục thành công!";
		$this -> listItem( $msg );
	}
	
	/**
	 * Form
	 * $data
	 */
	function buildForm( $data=array() , $msg = ''){
		global $oDb;
        
		$form = new HTML_QuickForm('frmAdmin','post',$_COOKIE['re_dir']."&task=".$_GET['task'], '', "style='padding:10px 15px 0 20px;'");
		
		$form -> setDefaults($data);
        
		$form -> addElement('text', 'Name', 'Tên menu', array('size' => 100, 'maxlength' => 100));
		$form -> addElement('text', 'Link', 'Link liên kết', array('size' => 100));
		$form -> addElement('text', 'OrderBy', 'Thứ tự sắp xếp', array('size' => 20, 'maxlength' => 3));	
		$form -> addElement('checkbox', 'isActive', 'Kích hoạt');
		$btn_group[] = $form -> createElement('submit',null,'Hoàn tất',array("style"=> "border:1px solid gray; padding:0 5px 0 5px;"));		
        $btn_group[] = $form -> createElement('button',null,'Quay lại',array('onclick'=>'window.location.href = \''.$_COOKIE['re_dir'].'\'', "style"=> "border:1px solid gray;"));      
        $form -> addGroup($btn_group);
      
		$form -> addElement('hidden', 'id', $data['id']);		
		
		
		if( $form -> validate())
		{
			$sql = "SELECT id FROM tbl_customer WHERE username='{$_POST['ToName']}'";
			pre($sql);
            $toID = $oDb->getOne($sql);
			$aData  = array(
				"ParentId"   => 0,
				"Name" 			=> $_POST['Name'],
				"Link" 	=> $_POST['Link'],
				"OrderBy" 	=> $_POST['OrderBy'],
                "CreateDate"    => date("Y-m-d H:i"),
                "isActive"      => $_POST['isActive']
			);			
			if( !$_POST['id'] ){				
				 $id = $this -> bsgDb -> insert($aData);				 
				 $msg = "Thêm menu thành công! ";
			}else {
				$id = $_POST['id'];				
				$this -> bsgDb -> updateWithPk($id, $aData);
				$msg = "Chỉnh sửa menu thành công ";
			}
			
			$this -> redirect($_COOKIE['re_dir']. "&msg={$msg}");
		}
		
		$form->display();
	}
	
	/**
	 * Lấy tất cả các danh mục bản ghi
	 * Return: Array category
	 */
	function getCategory(){
        $cond = '';
		$result = parent::multiLevel( $this -> pre_table, "id", "ParentID", "*", $cond, "Ordering ASC");
		
		$category = array();
		foreach ($result as $value => $key)
		{
			if( $key['level'] > 0){				
				$name = $this -> getPrefix( $key['level']).$key['Name'];
			}
			else 
				$name = $key['Name'];
			$category[$key['id']] = $name;
		}
		
		return $category;
	}

	function getMenu($keyword)
	{
		$sql = "SELECT id FROM tbl_home_menu WHERE Name LIKE '%{$keyword}%'";
        $result_menu = $oDb->getOne($sql);
        return $result_menu;
	}
	
	/**
	 * Thay đổi trạng thái với các bản ghi được chọn
	 * Return bool
	 */
	function changeStatus( $itemId , $status ){
		$aData = array( 'isActive' => $status );
		$this -> bsgDb -> updateWithPk( $itemId, $aData );
		return true;
	}
	
	/**
	 * List tất cả các bản ghi
	 * 
	 */
	function listItem( $sMsg= '' )
	{		
		global $oDb;
		global $oDatagrid;				
		
		$root_path = "Quản lý menu > Danh sách menu";						
		$submit_url= "/index.php?".$_SERVER['QUERY_STRING'];
		
		$table = $this -> table;
		
		$arr_filter= array(
			array(
				'field' => 'Name',
				'display' => 'Tên menu',
				'type' => 'text',
				'name' => 'filter_title',
				'selected' => $_REQUEST['filter_title'],
				'filterable' => true
			)
        );
		
		$arr_cols= array(
			array(
				"field" => "Name",
				"display" => "Tên menu",
				"align"	=> 'left',		
                'link' => SITE_URL.'index.php?mod=admin&amod='.$_GET['amod'].'&atask='.$_GET['atask'].'&tab=1&frame&task=edit',		
				"datatype" => "text",
				"sortable" => true
			),
			array(
				"field" => "Link",
				"display" => "Link liên kết",
				"align"	=> 'left',				
				"datatype" => "text",
				"sortable" => true
			),
			array(
				"field" => "OrderBy",
				"display" => "Thứ tự",
				"align"	=> 'left',				
				"datatype" => "text",
				"sortable" => true
			),
			array(
				"field" => "isActive",
				"display" => "Kích hoạt",
				"align"	=> 'left',				
				"datatype" => "publish",
				"sortable" => true
			)
        );
       
		$arr_cols_more = array(	
			array(
				"field" => "CreateDate",
				"display" => "Ngày tạo",
				"align"	=> 'left',				
				"datatype" => "date",
				"sortable" => true
			)	
		);		
        
        $arr_cols = array_merge($arr_cols,$arr_cols_more);
		
		$arr_check = array(
			array(
				"task" => "delete_all",
				"confirm"	=> "Xác nhận xóa?",
				"display" => "Xóa"
			),
		);
		if( $sMsg )
			$oDatagrid -> setMessage( $sMsg );
        // var_dump($oDatagrid); die;
		$oDatagrid->display_datagrid($table, $arr_cols, $arr_filter, $submit_url, $this -> getAct() ,120, $root_path, false ,$this->getActCheck());
	}
}