<div class="box_center">
	<div class="box_center_title" >{#SERVICES#} {$full_cat_name}</div>
    <div class="box_center_content">
		{foreach from=$list_item item=item name=item}
        	<div class="listMod"  style="border:none;">
                	<div class="name">
                    <div class="date">
                    	{$item.CreateDate|date_format:"%d/%m/%Y"}
                    </div>
                    	<a href="{$smarty.const.SITE_URL}services/c-{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{$item.Name}</a>
                    </div>
            	<div class="img">
                	{if $item.Photo}
                    	<a href="{$smarty.const.SITE_URL}services/c-{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}"><img src="{$item.Photo}" class="photo" alt="{$item.Name}"/></a>
					{/if}
                </div>
                <div class="info">
                    <div class="sum">
                    	{$item.Summarise|truncate:260}
                    </div>
                    <div class="view">
                    	<a href="{$smarty.const.SITE_URL}services/c-{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{#details#}</a>
                    </div>
                </div>
            </div>
		{/foreach}
	</div>
</div>

