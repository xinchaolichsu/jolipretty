<div class="box_letter">
	<div class="letter_title">{#sick_say#}</div>
        <ul id="mycarousel13" class="jcarousel-skin-tango">
    	{foreach from = $letter item = item name = item}
        <li>
                <div class="letter_name">{$item.Name}</div>
                <div class="letter_sum">{$item.Summarise|truncate:300}</div>
                <a class="letter_view" href="{$smarty.const.SITE_URL}letter/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{#view_all#}</a>
        </li>
       {/foreach}
       </ul>
</div>