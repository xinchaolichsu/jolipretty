<div class="box_center" style="margin-left:0;">
	<div class="box_center_title">{#inbox#}</div>
    <div class="box_center_content" style=" padding-left:5px;">
        <div style="line-height: 20px;">
            {#from_name#} : <b>{if $msg.FromName}{$msg.FromName}{else}Admin{/if}</b> ({#date_send#} : {$msg.CreateDate|date_format:"%d/%m/%Y %H:%M:%S"})<br />
            {#subject#} : <b>{$msg.Subject}</b><br />
            {#from#} : <b>{#system#}</b><br />
            Voucher code: {$msg.Code} <br />
            {$msg.Content}
        </div>
    </div>
</div>