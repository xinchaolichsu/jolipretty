<?php
/**
 * VietEsoft
 * ===============================================================================================
 * Copyright 2010.
 * Website: http://www.vietesfot.com, http://thietkewebsitetrongoi.net
 * -----------------------------------------------------------------------------------------------
 * Phát triển bới VietEsoft
 * -----------------------------------------------------------------------------------------------
 * Chú ý: Tất cả các hàm, phương thức được viết mới do mình tự định nghĩa phải để lại comment gồm:
 	- Người viết
	- Ngày-tháng-năm
	- Chức năng
 * ===============================================================================================
 * Quản lý các bài viết giới thiệu
*/
if (!defined('IN_VES')) die('Hacking attempt');
class messageBackEnd extends Bsg_Module_Base
{
	var $smarty;			/* Khai báo Smarty */
	var $db;				/* Khai báo db */
    var $datagrid;			/* Khai báo datagrid */
	var $id;				/* Khai báo id */
	var $imgPath;			/* Khai báo đường dẫn ảnh */
	var $imgPathShort;		/* Khai báo đường dẫn ảnh tương đối */
	var $table='';			/* Khai báo bảng dữ liệu */
	var $pre_table='';		/* Khai báo bảng dữ liệu */
	
	public function __construct($oSmarty, $oDb, $oDatagrid)
	{	
		$this -> smarty = $oSmarty;
		$this -> db = $oDb;		
        $this -> datagrid = $oDatagrid;
		$this -> id = $_REQUEST[id];		
		$this -> table	="tbl_message";
		parent::__construct($oDb);		
		$this -> bsgDb -> setTable($this->table);
	}
	
	function run($task)
	{	
		switch( $task ){
			/* Thêm mới 1 bản ghi */
            case 'add':
				$this -> addItem();
				break;
			/* Edit 1 bản ghi được chọn */
			case 'edit':
				$this -> editItem();
				break;
			/* Xóa 1 bản ghi được chọn */
			case 'delete':
				$this -> deleteItem();
				break;
			/* Xóa nhiều bản ghi được chọn */
			case 'delete_all':
				$this -> deleteItems();
				break;
			/* Thay đổi trạng thái bản ghi đã chọn */
			case 'change_status':				
				$this -> changeStatus($_GET['id'], $_GET['status']);
				break;
			/* Unselect status tất cả các bản ghi */
			case 'public_all':						
				$this -> changeStatusMultiple( 1 );
				break;
			/* Select status tất cả các bản ghi */
			case 'unpublic_all':						
				$this -> changeStatusMultiple( 0 );
				break;
			/* Lưu thứ tự bản ghi */
			case 'save_order':
				$this -> saveOrder();
				break;
			/* Hiển thị select box filter */
            case 'ajax_filter':
                $this -> ajax_filter();
                break;
            case 'ajax_form':
                $this -> ajax_form();
                break;
			/* Hiển thị tất cả danh sách bản ghi theo datagrid */
			default:					
				$this -> listItem( $_GET['msg'] );		
				break;
		}
	}
	
	/**
	 * Page info
	 *
	 */
	function getPageInfo()
	{
		return true;
	}
    
	/**
	 * Tạo select box filter
	 * 
	 */
    function ajax_form()
    {
        global $oDb;
        $lang_id = $_REQUEST['lang_id'];
        $category = $this->getCategory($lang_id);
        $option_str = parent::changeOption($category);
        echo $option_str;
    }
    
	/**
	 * Tạo select box filter
	 * 
	 */
    function ajax_filter()
    {
        global $oDb;
        $lang_id = $_REQUEST['lang_id'];
        $category = array(0 => "---Tất cả---" ) + $this->getCategory($lang_id);
        $option_str = parent::changeOption($category);
        echo $option_str;
    }
	
	/**
	 * Thêm mới 1 bản ghi
	 * Form thêm mới
	 */
	function addItem()
	{
		$this -> getPath("Quản lý tin nhắn > Thêm mới tin nhắn");		
		$this -> buildForm();
	}
	
	/**
	 * Edit 1 bản ghi
	 * Form edit với dữ liệu một bản ghi được chọn
	 */
	function editItem()
	{
        global  $oDb;
		$id = $_GET['id'];
		$this -> getPath("Quản lý tin nhắn > Chỉnh sửa tin nhắn");	
        if(isset($_GET['id']) && intval($_GET['id']))
        {
            $sql = "SELECT *,(SELECT username FROM tbl_customer WHERE id = FromID) as FromName,(SELECT username FROM tbl_customer WHERE id = ToID) as ToName FROM tbl_message WHERE id=$id";
            $row = $oDb->getRow($sql);
        }        
		$this -> buildForm( $row );
	}
	
	/**
	 * Xóa 1 bản ghi được chọn
	 * Hiển thị danh sách bản ghi với bản ghi đã xóa	
	 */
	function deleteItem()
	{
		global  $oDb;
		$id = $_GET["id"];
       // $this -> deleteImage($id, "Photo", $this->imgPath);
		$this -> bsgDb -> deleteWithPk( $id );
		$msg = "Xóa tin nhắn thành công!";
		$this -> listItem( $msg );
	}
	
	/**
	 * Xóa nhiều bản ghi
	 * 
	 */
	function deleteItems()
	{
		$aItems	 = $_GET['arr_check'];
		if(is_array( $aItems) && count( $aItems) > 0){
			$sItems = implode( ',', $aItems );
            //$this -> deleteImage($sItems, "Photo", $this->imgPath);
			$this -> bsgDb -> deleteWithPk( $sItems );
		}
		$msg = "Xóa (các) tin nhắn thành công!";
		$this -> listItem( $msg );
	}
	
	/**
	 * Thay đổi tất cả trạng thái của bản ghi
	 * 
	 */
	function changeStatusMultiple( $status = 0 )
	{
		$aItems	 = $_GET['arr_check'];
		if(is_array( $aItems) && count( $aItems) > 0){
			$sItems = implode( ',', $aItems );
			$this -> bsgDb -> updateWithPk( $sItems, array("Status" => $status) );
		}
		$msg = "Sửa trạng thái tin nhắn thành công!";
		$this -> listItem( $msg );
	}
	
	/**
	 * Lưu thứ tự bản ghi
	 * 
	 */
	function saveOrder(){	
		$aItem = $_GET['Ordering'];
		if(is_array($aItem) && count( $aItem ) > 0){
			// save order for item.
			foreach( $aItem as $key => $value){
				if( !is_numeric($value)) $value = 0;				
				$this -> bsgDb -> updateWithPk( $key, array('Ordering' => $value ));
			}
		}	
		$msg = "Lưu thứ tự danh mục thành công!";
		$this -> listItem( $msg );
	}
	
	/**
	 * Form
	 * $data
	 */
	function buildForm( $data=array() , $msg = ''){
		global $oDb;
        
		$form = new HTML_QuickForm('frmAdmin','post',$_COOKIE['re_dir']."&task=".$_GET['task'], '', "style='padding:10px 15px 0 20px;'");
		
		$form -> setDefaults($data);
		
        if (MULTI_LANGUAGE)
        {
            $lang = parent::loadLang();
            $form -> addElement('select', 'LangID', 'Ngôn ngữ', $lang, array('id'=>'news_form_lang'));		
            if ($_GET['task']=='edit')
                $selected_langid = $data['LangID'];
            else
                $selected_langid = $oDb->getOne("SELECT id FROM tbl_sys_lang ORDER BY id ASC LIMIT 1");	
        }
        else
            $selected_langid = 0;
        $sql = "SELECT username FROM tbl_customer";
        $arrCus = $oDb->getCol($sql);
        
       // $form -> addElement('text', NULL, 'Người gửi', array('size' => 50, 'maxlength' => 255,'readonly' => 'readonly'));
        //$form -> addElement('text', 'ToName', 'Người nhận', array('size' => 50, 'maxlength' => 255));
		$form -> addElement('autocomplete', 'ToName', 'Người nhận', $arrCus);
		$form -> addElement('text', 'Subject', 'Tiêu đề', array('size' => 50, 'maxlength' => 255));
			
		$form -> addElement('textarea','Content','Nội dung',array('style'=>'width:600px; height:200px;'));
        
        $date_time = parent::date_time('CreateDate',($data['CreateDate']) ? ($data['CreateDate']) : date("Y-m-d H:i"));			
		$form -> addElement('static',NULL,'Ngày tạo',$date_time);
		//echo 'dat';
		$btn_group[] = $form -> createElement('submit',null,'Hoàn tất',array("style"=> "border:1px solid gray; padding:0 5px 0 5px;"));		
        $btn_group[] = $form -> createElement('button',null,'Quay lại',array('onclick'=>'window.location.href = \''.$_COOKIE['re_dir'].'\'', "style"=> "border:1px solid gray;"));      
        $form -> addGroup($btn_group);
      
		$form -> addElement('hidden', 'id', $data['id']);		
		
		
		if( $form -> validate())
		{
			$sql = "SELECT id FROM tbl_customer WHERE username='{$_POST['ToName']}'";
			pre($sql);
            $toID = $oDb->getOne($sql);
			$aData  = array(
                "FromID"         => 0,
				"IsRead"     => 0,
				"LangID"     => 1,
				"ToID" 			=> $toID,
                "Subject"       => $_POST['Subject'],
                "CreateDate"    => $_POST['CreateDate'],
				"Content" 		=> $_POST['Content'],
			);			
			if( !$_POST['id'] ){				
				 $id = $this -> bsgDb -> insert($aData);				 
				 $msg = "Thêm tin nhắn thành công! ";
			}else {
				$id = $_POST['id'];				
				$this -> bsgDb -> updateWithPk($id, $aData);
				$msg = "Chỉnh sửa tin nhắn thành công ";
			}
			
			$this -> redirect($_COOKIE['re_dir']. "&msg={$msg}");
		}
		
		$form->display();
	}
	
	/**
	 * Xóa hình ảnh của 1 record
	 * $str_id
	 */
	function deleteImage($str_id, $field, $path){
		if($str_id == '')
			return;
		$arr_id = explode(',', $str_id);
		foreach($arr_id as $id){
			$imgpath = $path.$this->db->getOne("SELECT {$field} FROM ".$this->table." WHERE id = {$id}");
			if(is_file($imgpath))			
				@unlink($imgpath);
		}
	}
	
	/**
	 * Lấy tất cả các danh mục bản ghi
	 * Return: Array category
	 */
	function getCategory(){
        $cond = '';
		$result = parent::multiLevel( $this -> pre_table, "id", "ParentID", "*", $cond, "Ordering ASC");
		
		$category = array();
		foreach ($result as $value => $key)
		{
			if( $key['level'] > 0){				
				$name = $this -> getPrefix( $key['level']).$key['Name'];
			}
			else 
				$name = $key['Name'];
			$category[$key['id']] = $name;
		}
		
		return $category;
	}
	
	/**
	 * Thay đổi trạng thái với các bản ghi được chọn
	 * Return bool
	 */
	function changeStatus( $itemId , $status ){
		$aData = array( 'Status' => $status );
		$this -> bsgDb -> updateWithPk( $itemId, $aData );
		return true;
	}
	
	/**
	 * List tất cả các bản ghi
	 * 
	 */
	function listItem( $sMsg= '' )
	{		
		global $oDb;
		global $oDatagrid;				
		
		$root_path = "Quản lý tin nhắn > Danh sách tin nhắn";						
		$submit_url= "/index.php?".$_SERVER['QUERY_STRING'];
		
		$table = $this -> table;
		
		$arr_filter= array(
			array(
				'field' => 'Subject',
				'display' => 'Tiêu đề',
				'type' => 'text',
				'name' => 'filter_title',
				'selected' => $_REQUEST['filter_title'],
				'filterable' => true
			)
        );
        /*if(MULTI_LANGUAGE)
            $arr_filter[] = array(
                    'field' => 'LangID',
                    'display' => 'Ngôn ngữ',                
                    'name' => 'filter_lang',
                    'selected' => $_REQUEST['filter_lang'],
                    'options' => parent::loadLang(),
                    'filterable' => true
                );*/
		
		$arr_cols= array(
			array(
				"field" => "Subject",
				"display" => "Tiêu đề",
				"align"	=> 'left',		
                'link' => SITE_URL.'index.php?mod=admin&amod='.$_GET['amod'].'&atask='.$_GET['atask'].'&tab=1&frame&task=edit',		
				"datatype" => "text",
				"sortable" => true
			)
        );
        /*if (MULTI_LANGUAGE)
			$arr_cols[] = array(
				"field" => "LangID",
				"display" => "Ngôn ngữ",
				"sql"	=> "SELECT name FROM tbl_sys_lang WHERE id = LangID",
				"align"	=> 'left',				
				"datatype" => "text",
				"sortable" => true
			);*/
			  
		$arr_cols_more = array(	
			array(
				"field" => "CreateDate",
				"display" => "Ngày tạo",
				"align"	=> 'left',				
				"datatype" => "date",
				"sortable" => true
			)	
		);		
        
        $arr_cols = array_merge($arr_cols,$arr_cols_more);
		
		$arr_check = array(
			array(
				"task" => "delete_all",
				"confirm"	=> "Xác nhận xóa?",
				"display" => "Xóa"
			),
		);
        
		if( $sMsg )
			$oDatagrid -> setMessage( $sMsg );
		$oDatagrid->display_datagrid($table, $arr_cols, $arr_filter, $submit_url, $this -> getAct() ,120, $root_path, false ,$this->getActCheck());
	}
}

?>
