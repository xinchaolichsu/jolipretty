<?php
/**
 * VietEsoft
 * ===============================================================================================
 * Copyright 2010.
 * Website: http://www.vietesfot.com, http://thietkewebsitetrongoi.net
 * -----------------------------------------------------------------------------------------------
 * Phát triển bới VietEsoft
 * -----------------------------------------------------------------------------------------------
 * Chú ý: Tất cả các hàm, phương thức được viết mới do mình tự định nghĩa phải để lại comment gồm:
 	- Người viết
	- Ngày-tháng-năm
	- Chức năng
 * ===============================================================================================
 * Quản lý các bài viết giới thiệu
*/
if (!defined('IN_VES')) die('Hacking attempt');
class aboutBackEnd extends Bsg_Module_Base
{
	var $smarty;			/* Khai báo Smarty */
	var $db;				/* Khai báo db */
    var $datagrid;			/* Khai báo datagrid */
	var $id;				/* Khai báo id */
	var $imgPath;			/* Khai báo đường dẫn ảnh */
	var $imgPathShort;		/* Khai báo đường dẫn ảnh tương đối */
	var $table='';			/* Khai báo bảng dữ liệu */
	var $pre_table='';		/* Khai báo bảng dữ liệu */
	
	public function __construct($oSmarty, $oDb, $oDatagrid)
	{	
		$this -> smarty = $oSmarty;
		$this -> db = $oDb;		
        $this -> datagrid = $oDatagrid;
		$this -> id = $_REQUEST[id];		
		$this -> table	="tbl_about_item";	 	
		$this -> pre_table	="tbl_about_category";	 	
		parent::__construct($oDb);		
		$this -> bsgDb -> setTable($this->table);
		$this -> imgPath = SITE_DIR."upload/about/";
		$this -> imgPathShort = "/upload/about/";
	}
	
	function run($task)
	{	
		switch( $task ){
			/* Thêm mới 1 bản ghi */
            case 'add':
				$this -> addItem();
				break;
			/* Edit 1 bản ghi được chọn */
			case 'edit':
				$this -> editItem();
				break;
			/* Xóa 1 bản ghi được chọn */
			case 'delete':
				$this -> deleteItem();
				break;
			/* Xóa nhiều bản ghi được chọn */
			case 'delete_all':
				$this -> deleteItems();
				break;
			/* Thay đổi trạng thái bản ghi đã chọn */
			case 'change_status':				
				$this -> changeStatus($_GET['id'], $_GET['status']);
				break;
				
			case 'change_showhome':				
				$this -> changeShowHome($_GET['id'], $_GET['ShowHome']);
				break;
				
			/* Unselect status tất cả các bản ghi */
			case 'public_all':						
				$this -> changeStatusMultiple( 1 );
				break;
			/* Select status tất cả các bản ghi */
			case 'unpublic_all':						
				$this -> changeStatusMultiple( 0 );
				break;
			/* Lưu thứ tự bản ghi */
			case 'save_order':
				$this -> saveOrder();
				break;
			/* Hiển thị select box filter */
            case 'ajax_filter':
                $this -> ajax_filter();
                break;
            case 'ajax_form':
                $this -> ajax_form();
                break;
			case 'changeLang':
                $this->changeLang();
                break;
			/* Hiển thị tất cả danh sách bản ghi theo datagrid */
			default:					
				$this -> listItem( $_GET['msg'] );		
				break;
		}
	}
	
	/**
	 * Page info
	 *
	 */
	function getPageInfo()
	{
		return true;
	}
    
	/**
	 * Tạo select box filter
	 * 
	 */
    function ajax_form()
    {
        global $oDb;
        $lang_id = $_REQUEST['lang_id'];
        $category = $this->getCategory($lang_id);
        $option_str = parent::changeOption($category);
        echo $option_str;
    }
    
	/**
	 * Tạo select box filter
	 * 
	 */
    function ajax_filter()
    {
        global $oDb;
        $lang_id = $_REQUEST['lang_id'];
        $category = array(0 => "---Tất cả---" ) + $this->getCategory($lang_id);
        $option_str = parent::changeOption($category);
        echo $option_str;
    }
	
	/**
	 * Thêm mới 1 bản ghi
	 * Form thêm mới
	 */
	function addItem()
	{
		$this -> getPath("Quản lý tin tức > Thêm mới tin bài");		
		$this -> buildForm();
	}
	
	/**
	 * Edit 1 bản ghi
	 * Form edit với dữ liệu một bản ghi được chọn
	 */
	function editItem()
	{
		$id = $_GET['id'];
		$this -> getPath("Quản lý tin tức > Chỉnh sửa tin bài");	
		$row = $this -> bsgDb -> getRow( $id );
		$this -> buildForm( $row );
	}
	
    function changeLang()
    {
        global $oDb;
        $LangID = $_REQUEST['lang'];
        if (intval($LangID))
            $category = $this->getCategory($LangID);
        else 
            $category = array();
        $options = "<select name=\"CatID\" id=\"CatID\">";
        $options .= '<option value="">- - - Danh mục gốc - - -</option>';
        foreach ($category as $key => $val) {
            $options.='<option value="'.$key.'">'.$val.'</option>';
        }
        $options .="</select>";
        echo $options;
    }
	
	/**
	 * Xóa 1 bản ghi được chọn
	 * Hiển thị danh sách bản ghi với bản ghi đã xóa	
	 */
	function deleteItem()
	{
		global  $oDb;
		$id = $_GET["id"];
        $this -> deleteImage($id, "Photo", $this->imgPath);
		$this -> bsgDb -> deleteWithPk( $id );
		$msg = "Xóa tin bài thành công!";
		$this -> listItem( $msg );
	}
	
	/**
	 * Xóa nhiều bản ghi
	 * 
	 */
	function deleteItems()
	{
		$aItems	 = $_GET['arr_check'];
		if(is_array( $aItems) && count( $aItems) > 0){
			$sItems = implode( ',', $aItems );
            $this -> deleteImage($sItems, "Photo", $this->imgPath);
			$this -> bsgDb -> deleteWithPk( $sItems );
		}
		$msg = "Xóa (các) tin bài thành công!";
		$this -> listItem( $msg );
	}
	
	/**
	 * Thay đổi tất cả trạng thái của bản ghi
	 * 
	 */
	function changeStatusMultiple( $status = 0 )
	{
		$aItems	 = $_GET['arr_check'];
		if(is_array( $aItems) && count( $aItems) > 0){
			$sItems = implode( ',', $aItems );
			$this -> bsgDb -> updateWithPk( $sItems, array("Status" => $status) );
		}
		$msg = "Sửa trạng thái tin bài thành công!";
		$this -> listItem( $msg );
	}
	
	/**
	 * Lưu thứ tự bản ghi
	 * 
	 */
	function saveOrder(){	
		$aItem = $_GET['Ordering'];
		if(is_array($aItem) && count( $aItem ) > 0){
			// save order for item.
			foreach( $aItem as $key => $value){
				if( !is_numeric($value)) $value = 0;				
				$this -> bsgDb -> updateWithPk( $key, array('Ordering' => $value ));
			}
		}	
		$msg = "Lưu thứ tự danh mục thành công!";
		$this -> listItem( $msg );
	}
	
	/**
	 * Form
	 * $data
	 */
	function buildForm( $data=array() , $msg = ''){
		global $oDb;
        
		$form = new HTML_QuickForm('frmAdmin','post',$_COOKIE['re_dir']."&task=".$_GET['task'], '', "style='padding:10px 15px 0 20px;'");
		
		$form -> setDefaults($data);
		echo "<script type=\"text/javascript\" src=\"/view/js/lang.js\"></script>"; 
		
		if (MULTI_LANGUAGE)
        {
		    $lang = parent::loadLang();
			 $url = "?mod=admin&amod={$_REQUEST['amod']}&atask={$_GET['atask']}&task=changeLang&ajax";
		    $form -> addElement('select', 'LangID', 'Ngôn ngữ', $lang, array("onchange"=>"changeLang(this.value,'{$url}', 'CatID')"));
			$value_lang = array_keys($lang);
			$selected_langid = $data['LangID']?$data['LangID']:$value_lang[0];
        }
        else
            $selected_langid = 0;

		//$category = array(0=>'--- Chọn danh mục ---') + $this->getCategory($selected_langid);
		//$form -> addElement('select', 'CatID', 'Danh mục', $category, array('id'=>'CatID'));
			
		$form -> addElement('text', 'Name', 'Tiêu đề', array('size' => 50, 'maxlength' => 255));
		
/*		$form -> addElement('file', 'Photo', 'Ảnh', array('id'=>'imgFile'));
		
		if($_GET['task']=='edit')
			$form->addElement('static', null, '',"<a href='".SITE_URL.$data['Photo']."' onclick='return hs.expand(this)' class='highslide'><img src='".SITE_URL.$data['Photo']."' width=100 hight=100 border=0></a>");
*/			
		//$form -> addElement('textarea','Summarise','Mô tả vắn tắt',array('style'=>'width:600px; height:200px;'));	
		
		$content_editor=parent::editor('Content',$data['Content'],array('width'=>'800', 'height'=>'400'));
        $form -> addElement('static',NULL,'Nội dung',$content_editor);
        
        $date_time = parent::date_time('CreateDate',($data['CreateDate']) ? ($data['CreateDate']) : date("Y-m-d h:i"));			
		$form -> addElement('static',NULL,'Ngày tạo',$date_time);
		$form -> addElement('text', 'Ordering', 'Thứ tự', array('size' => 10, 'maxlength' => 50));
		$form -> addElement('checkbox', 'Status', 'Kích hoạt');
		
		$btn_group[] = $form -> createElement('submit',null,'Hoàn tất',array("style"=> "border:1px solid gray; padding:0 5px 0 5px;"));		
        $btn_group[] = $form -> createElement('button',null,'Quay lại',array('onclick'=>'window.location.href = \''.$_COOKIE['re_dir'].'\'', "style"=> "border:1px solid gray;"));      
        $form -> addGroup($btn_group);
      
		$form -> addElement('hidden', 'id', $data['id']);		
		
        $form -> addRule('Name','Tiêu đề không được để trống','required',null,'client');
		
		if( $form -> validate())
		{
			if($_FILES['Photo']['name']!='')
			{
				$this->deleteImage($this->id, "Photo", $this->imgPath);
				$FileName = rand().'_'.$_FILES['Photo']['name'];												
				parent::crop_image($_FILES['Photo']['tmp_name'],$FileName, $this->imgPath);				
				$_POST['Photo'] = $this->imgPathShort.$FileName;				
			}
			
			$aData  = array(
                "LangID" 		=> $_POST['LangID']?$_POST['LangID']:1,
				"CatID" 		=> $_POST['CatID'],
				"Name" 			=> $_POST['Name'],
				"Summarise" 	=> $_POST['Summarise'],
				"Content" 		=> $_POST['Content'],
				"CreateDate" 	=> $_POST['CreateDate'],
				"Ordering" 		=> $_POST['Ordering'],
				"Status" 		=> $_POST['Status']
			);
			if ($_POST['Photo']!='') $aData['Photo'] = $_POST['Photo'];
				
			if( !$_POST['id'] ){				
				 $id = $this -> bsgDb -> insert($aData);				 
				 $msg = "Thêm tin bài thành công! ";
			}else {
				$id = $_POST['id'];				
				$this -> bsgDb -> updateWithPk($id, $aData);
				$msg = "Chỉnh sửa tin bài thành công ";
			}
			
			$this -> redirect($_COOKIE['re_dir']. "&msg={$msg}");
		}
		
		$form->display();
	}
	
	/**
	 * Xóa hình ảnh của 1 record
	 * $str_id
	 */
	function deleteImage($str_id, $field, $path){
		if($str_id == '')
			return;
		$arr_id = explode(',', $str_id);
		foreach($arr_id as $id){
			$imgpath = $path.$this->db->getOne("SELECT {$field} FROM ".$this->table." WHERE id = {$id}");
			if(is_file($imgpath))			
				@unlink($imgpath);
		}
	}
	
	/**
	 * Lấy tất cả các danh mục bản ghi
	 * Return: Array category
	 */
	function getCategory($lang_id=0){
		$table = $this -> pre_table;
        $cond = '';
        if ($lang_id!=0)
            $cond .= " LangID = {$lang_id} ";
		$result = parent::multiLevel( $table, "id", "ParentID", "*", $cond, "Ordering ASC");
		
		$category = array();
		foreach ($result as $value => $key)
		{
			if( $key['level'] > 0){				
				$name = $this -> getPrefix( $key['level']).$key['Name'];
			}
			else 
				$name = $key['Name'];
			$category[$key['id']] = $name;
		}
		
		return $category;
	}
	/**
	 * Thay đổi trạng thái với các bản ghi được chọn
	 * Return bool
	 */
	function changeStatus( $itemId , $status ){
		$aData = array( 'Status' => $status );
		$this -> bsgDb -> updateWithPk( $itemId, $aData );
		$msg = "Thay đổi trạng thái thành công";
		$this->listItem($msg);
	}
	
	function changeShowHome( $itemId , $ShowHome ){
		$aData = array( 'ShowHome' => $ShowHome );
		$this -> bsgDb -> updateWithPk( $itemId, $aData );
		$msg = "Thay đổi trạng thái thành công";
		$this->listItem($msg);
		//return true;
	}
	
	/**
	 * List tất cả các bản ghi
	 * 
	 */
	function listItem( $sMsg= '' )
	{		
		global $oDb;
		global $oDatagrid;				
		
		$root_path = "Quản lý tin tức > Danh sách tin bài";						
		$submit_url= "/index.php?".$_SERVER['QUERY_STRING'];
		
		$table = $this -> table;
		
		$arr_filter= array(
			array(
				'field' => 'Name',
				'display' => 'Tiêu đề',
				'type' => 'text',
				'name' => 'filter_title',
				'selected' => $_REQUEST['filter_title'],
				'filterable' => true
			)
        );
        if(MULTI_LANGUAGE)
            $arr_filter[] = array(
                    'field' => 'LangID',
                    'display' => 'Ngôn ngữ',                
                    'name' => 'filter_lang',
                    'selected' => $_REQUEST['filter_lang'],
                    'options' => parent::loadLang(),
                    'filterable' => true
                );
			
/*		$arr_filter[] = array(
				'field' => 'CatID',
				'display' => 'Danh mục',				
				'name' => 'filter_cat',
				'selected' => $_REQUEST['filter_cat'],
				'options' => $this->getCategory(),
				'filterable' => true
			);
*/         
		$arr_filter[] = array(
				'field' => 'Status',
				'display' => 'Trạng thái',				
				'name' => 'filter_show',
				'selected' => $_REQUEST['filter_show'],
				'options' => array('Vô hiệu','Kích hoạt'),
				'filterable' => true
			);
		
		$arr_cols= array(
			array(
				"field" => "Name",
				"display" => "Tiêu đề",
				"align"	=> 'left',		
                'link' => SITE_URL.'index.php?mod=admin&amod='.$_GET['amod'].'&atask='.$_GET['atask'].'&tab=1&frame&task=edit',		
				"datatype" => "text",
				"sortable" => true
			)
        );
        if (MULTI_LANGUAGE)
			$arr_cols[] = array(
				"field" => "LangID",
				"display" => "Ngôn ngữ",
				"sql"	=> "SELECT name FROM tbl_sys_lang WHERE id = LangID",
				"align"	=> 'left',				
				"datatype" => "text",
				"sortable" => true
			);
			
/*		$arr_cols[] = array(
				"field" => "CatID",
				"display" => "Danh mục",
				"sql"    => "SELECT name FROM tbl_about_category WHERE id = CatID",
				"align"    => 'left',                
				"datatype" => "text",
				"sortable" => true
			);
*/            
		$arr_cols_more = array(
/*            array(
				"field" => "Photo",
				"display" => "Ảnh",
				"datatype" 		=> "img",
				"img_path"		=> SITE_URL,
				"sortable" => true,
				"order_default" => "asc"
			),
*/			array(
				"field" => "Ordering",
				"display" => "Thứ tự",
				"datatype" 		=> "order",
				"sortable" => true,
				"order_default" => "asc"
			),
			
			array(
				"field" => "Status",
				"display" => "Trạng thái",				
				"datatype" => "publish",
				"sortable" => true
			),		
			
			array(
				"field" => "CreateDate",
				"display" => "Ngày tạo",
				"align"	=> 'left',				
				"datatype" => "date",
				"sortable" => true
			)	
		);		
        
        $arr_cols = array_merge($arr_cols,$arr_cols_more);
		
		$arr_check = array(
			array(
				"task" => "delete_all",
				"confirm"	=> "Xác nhận xóa?",
				"display" => "Xóa"
			),
			array(
				"task" => "public_all",
				"confirm"	=> "Xác nhận thay đổi trạng thái?",
				"display" => "Kích hoạt"
			),
			array(
				"task" => "unpublic_all",
				"confirm"	=> "Xác nhận thay đổi trạng thái?",
				"display" => "Vô hiệu"
			)			
		);
        
		if( $sMsg )
			$oDatagrid -> setMessage( $sMsg );
		$oDatagrid->display_datagrid($table, $arr_cols, $arr_filter, $submit_url, $this -> getAct() ,120, $root_path, false ,$this->getActCheck());		
		
	}		
	
}

?>