<?php
/**
 * VietEsoft
 * ===============================================================================================
 * Copyright 2010.
 * Website: http://www.vietesfot.com, http://thietkewebsitetrongoi.net
 * -----------------------------------------------------------------------------------------------
 * Phát triển bới VietEsoft
 * -----------------------------------------------------------------------------------------------
 * Chú ý: Tất cả các hàm, phương thức được viết mới do mình tự định nghĩa phải để lại comment gồm:
 	- Người viết
	- Ngày-tháng-năm
	- Chức năng
 * ===============================================================================================
 * Hiển thị header
*/
if (!defined('IN_VES')) die('Hacking attempt');
class header
{	
	function run($task= "")
	{
		global $oSmarty, $oDb;
		// Load file ngôn ngữ
		$oSmarty->config_load($_SESSION['lang_file']);
		// banner
		$where = "Status=1";
		if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$_SESSION['lang_id']} ";
		$sql = "SELECT * FROM tbl_banner_item WHERE {$where} ORDER BY Ordering ASC LIMIT 1";
		$banner = $oDb->getRow($sql);
		$oSmarty->assign("banner",$banner);
		//logo
		$sql = "SELECT * FROM tbl_background_item WHERE {$where} ORDER BY CreateDate DESC LIMIT 1";
		$background = $oDb->getRow($sql);
		$oSmarty->assign("background",$background);
		//popup
		$sql = "SELECT * FROM tbl_advert WHERE {$where} AND popup=1 Limit 1";
		$popup = $oDb->getRow($sql);
		$oSmarty->assign("popup",$popup);

		
		// Giới thiệu
		$about_cat = $this->getCategory(0,'tbl_about_category', 'about');
		$oSmarty->assign("about_cat",$about_cat);
		// Sản phẩm
		// $product_cat = $this->getCategory(0,'tbl_product_category', 'product');
		$sql_pro_cat = "SELECT * FROM tbl_product_category WHERE Status=1 AND ParentID=0 ORDER BY Ordering ASC";
		$product_cat = $oDb->getAll($sql_pro_cat);
		$oSmarty->assign("product_cat",$product_cat);
		// Blog
		$blog_cat = $this->getCategory(0,'tbl_blog_category', 'blog');
		$oSmarty->assign("blog_cat",$blog_cat);
		// Tin tức
		$news_cat = $this->getCategory(0,'tbl_news_category', 'news');
		$oSmarty->assign("news_cat",$news_cat);
		// Dịch vụ
		$services_cat = $this->getCategory(0,'tbl_services_category', 'services');
		$oSmarty->assign("services_cat",$services_cat);
		//Sự kiện
		$info_cat = $this->getCategory(0,'tbl_info_category', 'info');
		$oSmarty->assign("info_cat",$info_cat);
		//hình ảnh
		$gallery_cat = $this->getCategory(0,'tbl_gallery_category', 'gallery');
		$oSmarty->assign("gallery_cat",$gallery_cat);
		
		//them menu động
		// $head_sql = "SELECT * FROM tbl_home_menu WHERE isActive = 1 AND ParentId=0 ORDER BY OrderBy DESC";
		// $menus = $oDb->getAll($head_sql);
		// $oSmarty->assign("menus",$menus);
		//==================================
		$idHasParent = "SELECT ParentID FROM tbl_product_category WHERE Status = 1 AND ParentID <> 0";
		$hasParent = $oDb->getAll($idHasParent);
		$arr_id = array();
		foreach ($hasParent as $key_id => $value_sub) {
			$arr_id[$key_id] = $value_sub['ParentID'];
		}

		$head_sql = "SELECT * FROM tbl_product_category WHERE Status = 1 ORDER BY Ordering DESC";
		$menus = $oDb->getAll($head_sql);
		foreach ($menus as $keymenu => $menu_item) {
			$menu_item['id'] = (int)$menu_item['id'];
			if(in_array($menu_item['id'], $arr_id)) 
			{
				$menus[$keymenu]['child'] = $this->getSubCategory($menu_item['id'], 'tbl_product_category');
			} else {
				$menus[$keymenu]['child'] = '';
			}
			if(in_array($menu_item['ParentID'], $arr_id) || ($menu_item['ParentID'] != 0))
			{
				$menus[$keymenu]['remove'] = true;
			} else {
				$menus[$keymenu]['remove'] = false;
			}
		}
		$oSmarty->assign("menus",$menus);
		//==================================
		$oSmarty->display("header.tpl");
	}
	
	/**
	 * Lấy danh mục sản phẩm con theo danh mục cha
	 * Trả về: Danh sách sản phẩm con theo ul -> li
	 */
	function getCategory($parent_id=0, $table='', $mod='')
	{
		global $oDb,$oSmarty;
		$where = "Status=1";
		if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$_SESSION['lang_id']} ";
		$html = '';
		$sql = "SELECT * FROM {$table} WHERE {$where} AND ParentID={$parent_id} ORDER BY Ordering ASC";
		$result = $oDb->getAll($sql);
		if( is_array($result) && !empty($result) ){
			$html .= '<ul style="display:none">';
			foreach($result as $key => $val){
				$html .= '<li>';
				$html .= '<a href="'.SITE_URL.$mod.'/c-'.$val['id'].'/'.remove_marks($val['Name']).'.html">'.$val['Name'].'</a>';
				if($val['id']!=61 && $val['id']!=134 && $_GET['mod']!='news'){
				$html .= $this->getCategory($val['id'],$table,$mod);
				}
				$html .= '</li>';
			}
			$html .= '</ul>';
		}
		return $html;
	}
	function getSubCategory($parent_id=0, $table='')
	{
		global $oDb,$oSmarty;
		$where = "Status=1";
		if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$_SESSION['lang_id']} ";
		$sql = "SELECT * FROM {$table} WHERE {$where} AND ParentID={$parent_id} ORDER BY Ordering ASC";
		$result = $oDb->getAll($sql);
		return $result;
	}
}
?>