{php}
	$useragent = $_SERVER['HTTP_USER_AGENT']; 
	if (!preg_match('|MSIE ([0-9].[0-9]{1,2})|',$useragent,$matched))
	getPageinfo();
{/php}
<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
	<title>{$aPageinfo.title}</title>
	<meta name="title" content="{$aPageinfo.title}" />
	<meta name="robots" content="index, follow" />
	<meta name="keywords" content="{$aPageinfo.keyword}" />
	<meta name="description" content="{$aPageinfo.description}" />
    <meta name="google-site-verification" content="nZgTgqXJHbVP7ijykFEVZwlmINz5EIov8IDMYNWSsLA" />	
    <meta name="geo.placename" content="Vietnamese" />
	<meta name="geo.position" content="21.021821,105.791345" />
	<meta name="geo.region" content="Vietnamese" />
	<meta name="dc.creator" content="" />    
	<meta name="dc.publisher" content="SEO by Vietesoft" />
	<meta name="dc.contributor" content="dich vu thiet ke website Vietesoft" />
	<meta name="author" content="Vietesoft" />
	<meta name="Area" content="" />
	<meta name="owner" content="http;//vinagc.com" />
	<meta  name="copyright" content="Thắng Lợi" />
	<meta name="EMail" content="info@vietesoft.com" /> 
	<link rel="shortcut icon" href="{$smarty.const.SITE_URL}favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="{$smarty.const.SITE_URL}view/css/stylesheet.css" type="text/css" />
	<link rel="stylesheet" href="{$smarty.const.SITE_URL}lib/menu/css.css" />
    <link href="{$smarty.const.SITE_URL}lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
    <link href="{$smarty.const.SITE_URL}view/css/responsive.css" rel="stylesheet" type="text/css"/>
    <link href="{$smarty.const.SITE_URL}view/css/pe-icon-7-stroke/css/pe-icon-7-stroke.css" rel="stylesheet">

    <!-- =============jquery========= -->
    <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script> -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
    <!-- <script src="http://code.jquery.com/jquery-1.4.js"></script> -->
     <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    
	<!-- <script type="text/javascript" src="{$smarty.const.SITE_URL}core/jquery/jquery.js"></script> -->
    <script type="text/javascript" src="{$smarty.const.SITE_URL}lib/menu/js.js"></script>
	<script type="text/javascript" src="{$smarty.const.SITE_URL}lib/nivo-slider/slider.js"></script>
    <!-- Latest compiled and minified CSS -->
    <script src="https://www.atlasestateagents.co.uk/javascript/tether.min.js"></script>
    <script src="{$smarty.const.SITE_URL}lib/bootstrap/bootstrap.min.js"></script>

    
    <script src="{$smarty.const.SITE_URL}lib/owl.carousel/owl.carousel.min.js" type="text/javascript"></script>
    <link href="{$smarty.const.SITE_URL}lib/owl.carousel/assets/owl.carousel.css" rel="stylesheet" type="text/css"/>
    
</head>
<body>
<center>
<div class="container-fluid">
	<div class="page">
    	<div class="top_content_main">
            <div class="welcome-mobile">
                {php}loadModule("product","frm_search");{/php}
            </div>
            <div class="banner" {if $banner.Type == 'flash'}style="height:{$banner.Height}px;"{/if}>
            <a href="{$smarty.const.SITE_URL}">
                {if $banner.Type == 'flash'}
                    <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" width="1000" height="{$banner.Height|default:'100'}" >
                  <param name="movie" value="{$smarty.const.SITE_URL}{$banner.Photo}" />
                  <param name="quality" value="high" />
                  <param name="wmode" value="transparent" />
                  <embed src="{$smarty.const.SITE_URL}{$banner.Photo}" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="1000" height="{$banner.Height|default:'100'}" wmode="transparent"></embed>
                     </object>
                {else}
                    <img class="logo_header" src="{$smarty.const.SITE_URL}{$banner.Photo}" alt="{$banner.Name}"/>
                {/if} 
                </a>
            {php}loadModule("product","frm_search");{/php}
            <div class="show-mobile">
                <nav class="navbar navbar-default header-menu-mobile" role="navigation" >
                    <div class="navbar-header">
                        <!-- <a class="navbar-brand" href="#">HOME</a> -->
                        <a class="shopping-bag-mobile" href="{$smarty.const.SITE_URL}cart/"><i class="fa fa-shopping-bag fa-2x" aria-hidden="true"></i> Shopping bag: {$count}</a>
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                          <span class="sr-only">Toggle navigation</span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav left">
                            <li class="">
                                <a href="{$smarty.const.SITE_URL}"> HOME</a>
                            </li>
                            <li class="">
                                <a href="{$smarty.const.SITE_URL}product/new-in/"> NEW IN</a>
                            </li>
                            {if $menus}
                                {foreach from=$menus item=item name=item}
                                    {if $item.remove != true}
                                        <li class="">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">{$item.Name} {if $item.child != ''} <b class="caret"></b> {/if}</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="{$smarty.const.SITE_URL}product/c-{$item.id}/{$item.Name|remove_marks}.html">{$item.Name}</a></li>
                                                {foreach from=$item.child item=sub_item name=sub_item}
                                                    <li><a href="{$smarty.const.SITE_URL}product/c-{$sub_item.id}/{$sub_item.Name | remove_marks}.html">{$sub_item.Name}</a></li>
                                                {/foreach}
                                            </ul>
                                        </li>
                                    {/if}
                                {/foreach}
                                <li class="divider"></li>
                                {if $cus_id || $abc}
                                    <li>
                                        <a href="#">
                                            {$customer.FullName} {$smarty.get.cookies}{$customer.FirstName}
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{$smarty.const.SITE_URL}customer/logout/">
                                            {#logout#}
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{$smarty.const.SITE_URL}customer/frm_login/">
                                            My account
                                        </a>
                                    </li>
                                {else}
                                    <li>
                                        <a href="{$smarty.const.SITE_URL}customer/login/">
                                            {#login#}
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{$smarty.const.SITE_URL}customer/register/">
                                            {#Sign_up#}
                                        </a>
                                    </li>
                                {/if}
                                <li class="divider"></li>
                                <li>
                                    <form action="{$smarty.const.SITE_URL}" method="get">
                                        <input class="inova form-control" id="search_text" name="keyword" onblur="setValue(this,'... Tìm kiếm sản phẩm ...');" onclick="this.value=''" onfocus="delValue(this, '... Tìm kiếm sản phẩm ...');" type="text" value=" " placeholder="Search ..." />
                                        <input class="btn btn-default" onclick="showHide_search();" type="submit" value="Search"/>
                                        <input name="mod" type="hidden" value="product"/>
                                        <input name="task" type="hidden" value="search"/>
                                    </form>
                                </li>
                            {/if}
                        </ul>
                    </div>
                </nav>
            </div>
            <!-- ================================ -->
            <div class="menu12" style="display: table-cell; vertical-align: middle;">
    			<div class="mlmenu horizontal bluewhite blindv">
                    <ul style="margin-bottom: 0px">
                        <li style="display: block !important">
                            <a rel="canonical" href="{$smarty.const.SITE_URL}product/new-in/">
                                <span {if $smarty.get.store=='1'} class="menu_active" {else} class="menu_item" onmouseover="$(this).removeClass('menu_item'); $(this).addClass('menu_active');" onmouseout="$(this).removeClass('menu_active'); $(this).addClass('menu_item');"{/if}>New In 
                                </span>
                            </a>
                        </li>
                        {if $menus}
                            {foreach from=$menus item=item name=item}
                                {if $item.remove != true}
                                <li style="display: block !important">
                                    <a rel="canonical" href="{$smarty.const.SITE_URL}product/c-{$item.id}/{$item.Name|remove_marks}.html">
                                        <span {if $smarty.get.store=='1'} class="menu_active" {else} class="menu_item" onmouseover="$(this).removeClass('menu_item'); $(this).addClass('menu_active');" onmouseout="$(this).removeClass('menu_active'); $(this).addClass('menu_item');"{/if}>{$item.Name} 
                                        </span>
                                        {if $item.child != ''} <i class="fa fa-sort-desc" aria-hidden="true" style="margin-top: 10px;"></i>{/if}
                                    </a>
                                    {if $item.child != ''}
                                        <ul class="submenu abc">
                                        {foreach from=$item.child item=sub_item name=sub_item}
                                            <li><a rel="canonical" href="{$smarty.const.SITE_URL}product/c-{$sub_item.id}/{$sub_item.Name | remove_marks}.html">
                                                <span {if $smarty.get.store=='1'} class="menu_active font-13" {else} class="menu_item font-13" onmouseover="$(this).removeClass('menu_item'); $(this).addClass('menu_active');" onmouseout="$(this).removeClass('menu_active'); $(this).addClass('menu_item');"{/if}>{$sub_item.Name}
                                                </span>
                                            </a>
                                            </li>
                                        {/foreach}
                                        </ul>
                                    {/if}
                                </li>
                                {/if}
                            {/foreach}
                        {/if}
                    </ul>
              </div>
            </div>
  		</div> 
	</div>
<!-- <div class="welcome">{php}//loadModule("advert","giua3");{/php}</div> -->
<div class="clr"></div>




