<div class="box_center">
	<div class="box_center_title">{#NEWS#} {$full_cat_name}</div>
    <div class="box_center_content">
    	{foreach from = $cat_other item = item name = item}
        {if $cat_id==61 || $cat_id==134}
        	<div class="blog_item" style="margin:0 8px 8px 0;">
                	{if $item.Photo}
                    	<a href="{$smarty.const.SITE_URL}news/c-{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}"><img src="{$item.Photo}" width="175" height="100" style="border-radius:8px; margin-bottom:5px;" alt="{$item.Name}"/></a>
					{/if}
                    <a href="{$smarty.const.SITE_URL}news/c-{$item.id}/{$item.Name|remove_marks}.html" style="color:#0082C8;" title="{$item.Name}">{$item.Name|truncate:40}</a>
            </div>
        {else}
        	<div class="listMod" {if $smarty.foreach.item.last} style="border:none;"{/if}>
                	<div class="name">
                    <div class="date">
                    	{$item.CreateDate|date_format:"%d/%m/%Y"}
                    </div>
                    	<a href="{$smarty.const.SITE_URL}news/c-{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{$item.Name}</a>
                    </div>
            	<div class="img">
                	{if $item.Photo}
                    	<a href="{$smarty.const.SITE_URL}news/c-{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}"><img src="{$item.Photo}" class="photo" alt="{$item.Name}"/></a>
					{/if}
                </div>
                <div class="info">
                    <div class="sum">
                    	{$item.Summarise|truncate:260}
                    </div>
                    <div class="view">
                    	<a href="{$smarty.const.SITE_URL}news/c-{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{#details#}</a>
                    </div>
                </div>
            </div>
        {/if}
    	{/foreach}
    </div>
</div>
