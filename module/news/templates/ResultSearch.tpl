{literal}
<script language="javascript" type="text/javascript">
	function page_news(page)
	{		
		arr_url = page.split('#page=');
		var page = arr_url[1];
		var keyword = $("#key_search").val();
		$.ajax({
			   type:"GET",
			   url:"/?mod=news&task=search&ajax&page="+page+"&keyword="+keyword+"&mod_search=news",			   
			   success: function(response_text){			   	
				   $("#page_news").html(response_text);
			   }
		   });
	}
	$(document).ready(function(){
		$("#page_news span.span_a_class a").click(function () {
			var page = $(this).attr("href");
			page_news(page);  
		});
	});
	
	function page_services(page)
	{		
		arr_url = page.split('#page=');
		var page = arr_url[1];
		var keyword = $("#key_search").val();
		$.ajax({
			   type:"GET",
			   url:"/?mod=news&task=search&ajax&page="+page+"&keyword="+keyword+"&mod_search=services",			   
			   success: function(response_text){			   	
				   $("#page_services").html(response_text);
			   }
		   });
	}
	$(document).ready(function(){
		$("#page_services span.span_a_class a").click(function () {
			var page = $(this).attr("href");
			page_services(page);  
		});
	});
	
	function page_product(page)
	{		
		arr_url = page.split('#page=');
		var page = arr_url[1];
		var keyword = $("#key_search").val();
		$.ajax({
			   type:"GET",
			   url:"/?mod=news&task=search&ajax&page="+page+"&keyword="+keyword+"&mod_search=product",			   
			   success: function(response_text){			   	
				   $("#page_product").html(response_text);
			   }
		   });
	}
	$(document).ready(function(){
		$("#page_product span.span_a_class a").click(function () {
			var page = $(this).attr("href");
			page_product(page);  
		});
	});
	
	function page_blog(page)
	{		
		arr_url = page.split('#page=');
		var page = arr_url[1];
		var keyword = $("#key_search").val();
		$.ajax({
			   type:"GET",
			   url:"/?mod=news&task=search&ajax&page="+page+"&keyword="+keyword+"&mod_search=blog",			   
			   success: function(response_text){			   	
				   $("#page_blog").html(response_text);
			   }
		   });
	}
	$(document).ready(function(){
		$("#page_blog span.span_a_class a").click(function () {
			var page = $(this).attr("href");
			page_blog(page);  
		});
	});
	
	function page_about(page)
	{		
		arr_url = page.split('#page=');
		var page = arr_url[1];
		var keyword = $("#key_search").val();
		$.ajax({
			   type:"GET",
			   url:"/?mod=news&task=search&ajax&page="+page+"&keyword="+keyword+"&mod_search=about",			   
			   success: function(response_text){			   	
				   $("#page_about").html(response_text);
			   }
		   });
	}
	$(document).ready(function(){
		$("#page_about span.span_a_class a").click(function () {
			var page = $(this).attr("href");
			page_blog(page);  
		});
	});
</script>
{/literal}
{config_load file=$smarty.session.lang_file}
<input type="hidden" id="key_search" value="{$smarty.get.keyword}" />
<div class="box_center1">    
	<div class="box_center_title1">Search Results</div>
    <div class="box_center_content1" >
	<div id="page_product">
    	{foreach from = $product item = item name = item}
            <div class="home_item" {if $smarty.foreach.item.iteration%4==0} style="margin-right:0;"{/if}>
            	<div class="home_img">
                  <a href="{$smarty.const.SITE_URL}product/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}"><img src="{$smarty.const.SITE_URL}{$item.Photo}" width="213" height="310"  alt="{$item.Name}" /></a>	
                  {if $item.sum_qua==0}<div class="sold_out">Sold out</div>{/if}	  
                  </div>
                  <div class="sold_outT">{if $item.sum_qua==0}<font color="#019faf">Limited quantity</font>{elseif $item.Is_new}<font color="#1bca03">New arrival</font>{/if}</div>
                  <a class="home_name" href="{$smarty.const.SITE_URL}product/{$item.id}/{$item.Name|remove_marks}.html" title="{$item.Name}">{$item.Name|truncate:40}</a>
                  <div class="home_summarise"></div>
                  <div class="home_price">
                  	<span>SGD $ {$item.OldPrice|number_format:0:".":"."} </span>
                  	<del>{$item.Price}</del>
                  </div>  
             </div>
        {foreachelse}
            <div style="width:100%; float:left; text-align:center; color:#FF0000;">{#not_found#}</div>
    	{/foreach}
        {if $num_rows2 > $limit}
        <div class="listPage">
            {$num_rows2|page:$limit:$smarty.get.page:$paging_path}
        </div>
        {/if}
    </div> 
	</div>
    
</div>

