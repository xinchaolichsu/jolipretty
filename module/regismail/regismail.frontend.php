<?php
/**
 * VietEsoft
 * ===============================================================================================
 * Copyright 2010.
 * Website: http://www.vietesfot.com, http://thietkewebsitetrongoi.net
 * -----------------------------------------------------------------------------------------------
 * Phát triển bới VietEsoft
 * -----------------------------------------------------------------------------------------------
 * Chú ý: Tất cả các hàm, phương thức được viết mới do mình tự định nghĩa phải để lại comment gồm:
 	- Người viết
	- Ngày-tháng-năm
	- Chức năng
 * ===============================================================================================
 * Hiển thị thông tin liên hệ
*/
if (!defined('IN_VES')) die('Hacking attempt');

class regismail
{
	
	function __construct(){		
		$this->id = mysql_real_escape_string(($_REQUEST['id']=='')?'0':$_REQUEST['id']);
		$this->field = "*";
		$this->LangID = $_SESSION['lang_id'];
	}
	
	function run($task="")
	{
		if($task==''){$task = $_GET['task'];}
	    switch($task)
			{
				case 'home':
					$this -> home();
				break;
				case 'home1':
					$this -> home1();
				break;
				case 'delete_regis':
					$this -> delete_regis();
				break;
				case 'place':
					$this -> place();
				break;
				case 'thickbox':
					$this -> thickbox();
				break;
				default:
                    /* List toàn bộ (với site không có danh mục)*/
					$this->listItem();
                    
                    /*List theo danh mục (với site có danh mục)*/
                    //$this->listbyCategory();
				break;	
			}
	}
	function place()
	{	
		global $oSmarty, $oDb;
		$id=$_GET['id'];//pre(111);die();
		$oSmarty->assign("id",$id);
		$oSmarty->display('place.tpl');
	}
	
    function delete_regis()
    {
        global $oSmarty, $oDb;
		$oDb->query("DELETE FROM tbl_regismail_item WHERE id='{$_GET['id']}'");
		echo "<script language = 'javascript'> alert('You have successfully unsubscribed!');location.href='/'</script>";
    }
	
	function thickbox()
	{	
		global $oSmarty, $oDb;
		$id=$_GET['id'];//pre($id);//die();
        $where = " Status=1 ";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		$sql = "SELECT * FROM tbl_regismail_category WHERE {$where} AND id={$id}";
		$place = $oDb->getRow($sql);
		$oSmarty->assign("place",$place);//pre($place);
		$oSmarty->display('thickbox.tpl');
	}
	
	function listItem()
	{	
		global $oSmarty, $oDb;	
        $where = " Status=1 ";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		if ($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			if ($_POST['txt_captcha']==$_SESSION['key_captcha_code']) {	
				if($_POST['sick']) 	
					$sick = $oDb->getOne("SELECT Name FROM tbl_news_category WHERE id={$_POST['sick']}");
				if($_POST['place']) 	
					$place = $oDb->getOne("SELECT Name FROM tbl_regismail_category WHERE id={$_POST['place']}");
				if($_POST['doctor']) 	
					$doctor = $oDb->getOne("SELECT Name FROM tbl_about_item WHERE id={$_POST['doctor']}");
				if($_POST['birthday']=='Ngày sinh') $Birthday='';
				else $Birthday=$_POST['birthday'];
				$hour=$_POST['hour'];
				$minute=$_POST['minute'];
				$day=$hour. ':' .$minute;
				$to = $_SESSION['email_admin'];
				$subject = "Đặt lịch khám";
				$content = $_POST['Content'];		
				$message = "Khách hàng ".$_POST['FullName']." có số điện thoại: ".$_POST['Phone']." tại địa chỉ "
						.$_POST['Address']." có đặt lịch khám với nội dung:<br/><br/> Bệnh cần chữa:".$sick."<br/> Địa điểm khám:".$place."<br/> Bác sĩ khám:".$doctor."<br/> Ngày khám:".$_POST['ExamTime']."<br/> Giờ khám:".$day."<br/> Mô tả triệu chứng:".$content;
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
				//echo $message;
			
				$headers .= 'From: '.$_POST['FullName'].' <'.$_POST['Email'].'>' . "\r\n";		
				
				if (mail($to,$subject,$message,$headers))
					$msg_error = "Thư của bạn đã được gửi tới chúng tôi";
				else 
					$msg_error = "Thư của bạn đã được gửi tới chúng tôi";
				$data = array(
					"IsThem" => $_POST['IsThem'],
					"Name" => $_POST['FullName'],
					"Address" => $_POST['Address'],
					"Phone" => $_POST['Phone'],
					"Sex" => $_POST['sex'],
					"Email" => $_POST['Email'],
					"Birthday" => $Birthday,
					"ExamTime" => $_POST['ExamTime'],
					"Day" => $day,
					"Content" => $_POST['Content'],
					"AddID"	  => $_POST['place'],
					"CatID" => $_POST['sick'],
					"DocID" => $_POST['doctor'],
					"CreateDate" => date("Y-m-d"),
					"Status" => "1"
				);	
				//pre($data);die();			
				$oDb -> autoExecute("tbl_regismail_item",$data,DB_AUTOQUERY_INSERT);
				echo "<script language = 'javascript'> alert('Bạn đã đặt lịch khám thành công!');location.href='/regismail/'</script>";

			}
			else 
				$msg_error = "Mã xác nhận sai!";
	
			$oSmarty -> assign('msg_error',$msg_error);
		}
		$sql = "SELECT * FROM tbl_news_category WHERE {$where} AND ParentID=0 ORDER BY Ordering ASC";
		$sick = $oDb->getAll($sql);
		$oSmarty->assign("sick",$sick);
		$sql = "SELECT * FROM tbl_about_item WHERE {$where} AND CatID=33 ORDER BY Ordering ASC";
		$doctor = $oDb->getAll($sql);
		$oSmarty->assign("doctor",$doctor);
		$sql = "SELECT * FROM tbl_regismail_category WHERE {$where} AND ParentID=0 ORDER BY Ordering ASC";
		$place = $oDb->getAll($sql);
		$oSmarty->assign("place",$place);
		$place1 = $oDb->getOne("SELECT id FROM tbl_regismail_category WHERE {$where} AND ParentID=0 ORDER BY Ordering ASC Limit 1");
		$oSmarty->assign("place1",$place1);
		//captcha
		$possible = '23456789bcdfghjkmnpqrstvwxyz';
		$code = '';
		$i = 0;
		while ($i < 6) { 
			$code .= substr($possible, mt_rand(0, strlen($possible)-1), 1);
			$i++;
		}
		$_SESSION['key_captcha_code'] = $code;		
		$oSmarty->assign("code",$code);
		$oSmarty->display('regismail_home.tpl');
	}
	function home()
	{	
		global $oSmarty, $oDb;	
		
		if ($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			if($_POST["Email_regis123"])
			{
				$user= $oDb->getRow("select * from tbl_regismail_item where  Email='".$_POST["Email_regis123"]."'");
				
				if(is_array($user))
				{
					if($user["Email"]==$_POST["Email_regis123"])
					{
						echo "<script language = 'javascript'>alert('E-mail is already in use.')</script>";
						echo "<script language = 'javascript'>location.href = '".$_SERVER['HTTP_REFERER']."'</script>";
					}
				}
				else
				{
					$data = array(
						"Email" => $_POST['Email_regis123'],
						//"Phone" => $_POST['Phone'],
						"CreateDate" => date("Y-m-d"),
						"Status" => "1"
					);
				$oDb -> autoExecute("tbl_regismail_item",$data,DB_AUTOQUERY_INSERT);
				$idemail=$oDb->getOne("select id from tbl_regismail_item where  Email='".$_POST["Email_regis123"]."'");
				$email=$_POST["Email_regis123"];
				$subject = "Welcome to Jolipretty";
				$message = "Thank you. <br />

							YOU’VE BEEN ADDED TO OUR MAILING LIST. <br />
							
							Starting now, you’ll be among the first to know about events and promotions. To ensure you stay in <br />
							
							the loop, please add info.jolipretty@gmail.com to your address book. <br />
							
							<a href='http://jolipretty.com/product/' target='_blank'>SHOP NOW ></a> <br />
														
							Happy Shopping!  <br />
							The Joli Pretty Team <br />
							<a href=".SITE_URL."regismail/".$idemail."/".$_POST['Email_regis123'].".html target='_blank'>Click here to Unsubcribe</a>";
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
				$headers .= "From: info.jolipretty@gmail.com <info.jolipretty@gmail.com>" . "\r\n";
				$result = @mail( $email, $subject, $message, $headers );
				echo "<script language = 'javascript'>alert('Welcome to Joli Pretty. You’ve been added to our mailing list.')</script>";
				echo "<script language = 'javascript'>location.href = '".$_SERVER['HTTP_REFERER']."'</script>";
				}
			}
		}
		$oSmarty->display('home_regismail.tpl');
	}
	function home1()
	{	
		global $oSmarty, $oDb;	
		if ($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			if($_POST["Email_regis"])
			{
				
					$data = array(
						"Email" => $_POST['Email_regis'],
						"pro_id" => $_GET['id'],
						"size_id"=> $_POST['size_email'],
						"CreateDate" => date("Y-m-d"),
						"Status" => "1"
					);	
					//pre($data);die();
				$oDb -> autoExecute("tbl_product_regismail",$data,DB_AUTOQUERY_INSERT);
				echo "<script language = 'javascript'>alert(' Your email \"".$_POST['Email_regis']."\" has been added to our waiting list')</script>";
				echo "<script language = 'javascript'>location.href = '".$_SERVER['HTTP_REFERER']."'</script>";
			}
		}
		$oSmarty->display('home1_regismail.tpl');
	}
	function getPageinfo($task= "")
	{
		global $oSmarty, $oDb;				
		
		switch ($task)
		{					 
			default:
				$aPageinfo=array('title'=> 'Đặt lịch hẹn | '.PAGE_TITLE, 'keyword'=>PAGE_KEYWORD
						, 'description'=>PAGE_DESCRIPTION);
				break;	
		}
		$oSmarty->assign('aPageinfo', $aPageinfo);
		
	}
}
?>
