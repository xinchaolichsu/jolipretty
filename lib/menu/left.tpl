<link rel="stylesheet" href="{$smarty.const.SITE_URL}lib/menu/css.css">
<!--[if IE]>
    <link rel="stylesheet" href="{$smarty.const.SITE_URL}lib/menu/hack.css">
<![endif]-->
<script type="text/javascript" src="{$smarty.const.SITE_URL}lib/menu/js.js"></script>


{*Category*}
<div class="title_bar_left"><div class="margin_content">{#category_title#}</div></div>

<div class="mlmenu vertical blindv delay inaccesible">
<ul>
{foreach item=parent from=$category}
{assign var="parent_id" value=$parent.Group_ID}
<li>
<a href="{if !$parent.hassub}{$smarty.const.SITE_URL}{"index.php?mod=category&task=detail&id=$parent_id"|url_friendly}{else}#{/if}" {if $parent.hassub}rel="ddsubmenuside{$parent.Group_ID}"{/if}><div class="category_item"><div class="margin_content"><div style="float:left">{$parent.Group_Name}</div> {if $parent.hassub}<img src="{$smarty.const.SITE_URL}themes/{$smarty.session.theme}/images/category_arrow.jpg" style="float:right;" border="0" />{/if}</div></div></a>
    {if $parent.hassub}
    <ul>
        {foreach item=child from=$parent.child}
        {assign var="child_id" value=$child.Group_ID}
        <li>
        <a href="{if !$child.hassub}{$smarty.const.SITE_URL}{"index.php?mod=category&task=detail&id=$child_id"|url_friendly}{else}#{/if}"><div class="category_item" style="background-color:#FFF"><div class="margin_content"><div style="float:left">{$child.Group_Name}</div> {if $child.hassub}<img src="{$smarty.const.SITE_URL}themes/{$smarty.session.theme}/images/category_arrow.jpg" style="float:right;" border="0" />{/if}</div></div></a>
        	{if $child.hassub}
            <ul>
            	{foreach item=subchild from=$child.child}
                {assign var="subchild_id" value=$subchild.Group_ID}
                <li>
        		<a href="{$smarty.const.SITE_URL}{"index.php?mod=category&task=detail&id=$subchild_id"|url_friendly}"><div class="category_item" style="background-color:#FFF"><div class="margin_content"><div style="float:left">{$subchild.Group_Name}</div></div></div></a>
                </li>
                {/foreach}
            </ul>
            {/if}
        </li>
        {/foreach}
    </ul>
{/if}
</li>
{/foreach}
</ul>
</div>

<a href="{$smarty.const.SITE_URL}{"index.php?mod=contact"|url_friendly}"><div class="category_item"><div class="margin_content"><div style="float:left">{#CONTACT#} </div></div></div></a>
{*End Category*}

             
{php} loadModule('weather');{/php}


{*Adv left*}
<div class="title_bar_left"><div class="margin_content">{#adv_left#}</div></div>

<div class="block_content" style="text-align:center; padding-top:0px;">
    {php} loadModule('ad','left');{/php}
</div>
{*End Adv left*}