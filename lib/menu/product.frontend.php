<?php
/**
 * VietEsoft
 * ===============================================================================================
 * Copyright 2010.
 * Website: http://www.vietesfot.com, http://thietkewebsitetrongoi.net
 * -----------------------------------------------------------------------------------------------
 * Phát triển bới VietEsoft
 * -----------------------------------------------------------------------------------------------
 * Chú ý: Tất cả các hàm, phương thức được viết mới do mình tự định nghĩa phải để lại comment gồm:
 	- Người viết
	- Ngày-tháng-năm
	- Chức năng
 * ===============================================================================================
 * Hiển thị sản phẩm
*/
if (!defined('IN_VES')) die('Hacking attempt');
class product extends VES_FrontEnd 
{	
	var $table; 		/* Khai báo bảng dữ liệu */
	var $mod;			/* Khai báo mod */
	var $id;			/* Khai báo id */
	var $LangID;		/* Khai báo id ngôn ngữ */
	var $field;			/* Khai báo trường dữ liệu */
	var $pre_table;		/* Khai báo bảng danh mục dữ liệu */
	function __construct(){		
		$this->table = 'tbl_product_item';
		$this->pre_table = "tbl_product_category";
		$this->mod = 'product';
		$this->id = mysql_real_escape_string(($_REQUEST['id']=='')?'0':$_REQUEST['id']);
		$this->field = "*";
		$this->LangID = $_SESSION['lang_id'];
		parent::__construct($this->table);
	}
	function run($task= "")
	{
		if($task==''){$task = $_GET['task'];}
	    switch($task)
			{
				/* Hiển thị sản phẩm ở trang chủ */
				case 'home':
					$this -> getProductHome();
				break;
				case 'category_product':
					$this -> category_product();
				break;
				/* Hiển thị sản phẩm hot */
				case 'hot':
					$this -> getProductHot();
				break;
				/* Sử lý sản phẩm hiển thị theo request ajax */
				case 'showhome_ajax':
					$this -> getHomeAjax();
				break;
				/* Hiển thị form tìm kiếm sản phẩm */
				case 'frm_search':
                    $this -> getFrmSearch();
                break;
				/* Sư lý tìm kiếm sản phẩm */
                case 'search':
                    $this -> getProductSearch();
                break;
				/* Tìm kiếm sản phẩm nâng cao */
                case 'advSearch':
                    $this -> getAdvSearch();
				break;
				/* Hiển thị menu sản phẩm */
				case 'menu':
					$this -> getProductMenu();
				break;
				/* Hiển thị chi tiết 1 sản phẩm */
				case 'details':
					$this -> getProductDetails();
				break;
				/* Các sản phẩm khác cùng loại */
				case 'other_product':
					$this -> getProductOther();
				break;
				/* Hiển thị các sản phẩm theo danh mục */
				case 'category':
					$this -> getProductList($_GET['id']);
				break;
				default:
                    /* List toàn bộ sản phẩm hiện có */
					$this -> getProductList();
                    
                    /*List toàn bộ sản phẩm theo từng danh mục*/
                    //$this -> getProductCategory();
				break;	
			}
	}
	/**
	 * Page info
	 *
	 * @param $task
	 */
	function getPageinfo($task= "")
	{
	 	global $oSmarty, $oDb;
		switch ($task) {
                   	case 'category':
				$id = $_GET['id'];
                                $page = $_GET['page']?$_GET['page']:1;
				if ($id)
				{
					$sql = "SELECT Name FROM {$this->pre_table} WHERE id=$id";	
					$row = $oDb->getOne($sql);	
				}
				$aPageinfo=array('title'=> $row." " . parent::get_config_vars("PRODUCT","").". Trang ".$page.' '. PRO_TITLE, 'keyword'=>PRO_KEYWORD.". Trang ".$page.",".$row, 'description'=>PRO_DESCRIPTION.". Trang ".$page);
				$aPath[] = array('link' => '', 'path' => 'product');
				break;
            case 'details':
                $id = $_GET['id'];
                if ($id)
                {
                    $sql = "SELECT Name FROM {$this->table} WHERE id=$id";    
                    $row = $oDb->getOne($sql);    
                }
                $aPageinfo=array('title'=> $row." " . parent::get_config_vars("PRODUCT","").' '. PRO_TITLE, 'keyword'=>PRO_KEYWORD.",".$row, 'description'=>PRO_DESCRIPTION);
                $aPath[] = array('link' => '', 'path' => 'product');
                break;
			default:			
                 $page = $_GET['page']?$_GET['page']:1;
				$aPageinfo=array('title'=> parent::get_config_vars("PRODUCT","").' ' . PRO_TITLE.". Trang ".$page, 'keyword'=>PRO_KEYWORD.". Trang ".$page.",".$row, 'description'=>PRO_DESCRIPTION.". Trang ".$page);
				$aPath[] = array('link' => '', 'path' => 'product');
				break;
		}
		
		$oSmarty->assign('aPageinfo', $aPageinfo);
		$oSmarty->assign("aPath", $aPath);
	}
	
	/**
	 * Hiển thị form tìm kiếm sản phẩm
	 *
	 */
	function getFrmSearch()
    {
        global $oSmarty, $oDb;
		$oSmarty->display('frm_search.tpl');
	}
	
	/**
	 * Sư lý tìm kiếm sản phẩm
	 * Hiển thị sản phẩm theo điều kiện tìm kiếm
	 */
	function getAdvSearch()
    {
        global $oDb,$oSmarty;
        $url = $_SERVER['REQUEST_URI'];
        if ($_GET['page'])
            $url = str_replace("&page=".$_GET['page'],"",$url);
        $page_link = $url."&page=i++";
		
		$inova = $_GET['inova'];
        if($inova != '')
        {
            $strCond = "Name LIKE '%".mb_strtolower($inova,"UTF-8")."%' OR Content LIKE '%".mb_strtolower($inova,"UTF-8")."%'";
        }
		
		
        $this->getListItem(0,$page_link,$strCond);
    } 
	 
	/**
	 * Hiển thị sản phẩm ra trang chủ
	 *
	 */ 
	function getProductHome()
	{
		/*global $oSmarty, $oDb;      
		$sql = "SELECT * FROM {$this->table} WHERE Status=1 AND ShowHome=1 ORDER BY CreateDate DESC limit 15";
		$product = $oDb->getAll($sql);
		$oSmarty->assign("product",$product);		
		$oSmarty->display('home_product.tpl');*/
		
		$url = $_SERVER['REQUEST_URI'];
		if ($_GET['page'])
			$url = str_replace("/".$_GET['page']."/","",$url);
		$page_link = "#i++/";	
		$cond = "ShowHome=1 AND Status=1";
		$order="ORDER BY CreateDate DESC";
		//pre($cond); die();
		$limit = 16;
		$template = "home_product.tpl";
		$this->getListItem(0,$page_link,$cond,$limit,$template,$order);
	}
	
	function category_product($parent_id=0)
	{
		global $oSmarty, $oDb;      
		$where = "Status=1";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		$sql = "SELECT * FROM {$this->pre_table} WHERE {$where} AND ShowHome=1 AND ParentID={$parent_id} ORDER BY Ordering ASC";
		//pre($sql); die();
		$product_cat = $oDb->getAll($sql);
			foreach($product_cat as $key=>$value){
				//$str_cat=$this->getFullCatId($value['id']);
				$sql = "SELECT * FROM {$this->pre_table} WHERE Status=1 AND ParentID = {$value['id']} AND ShowHome=1 ORDER BY Ordering DESC LIMIT 5";
				$product_item = $oDb->getAll($sql);
				$product_cat[$key]['product_item']=$product_item;
			}
		//pre($product_cat);
		$oSmarty->assign("product_cat",$product_cat);		
		$oSmarty->display('home_category_product.tpl');
	}
	
	/**
	 * Hiển thị sản phẩm hot
	 *
	 */
	function getProductHot()
	{
		/*global $oSmarty, $oDb;
        $where = "Status=1 AND IsHot=1";            
		$sql = "SELECT * FROM {$this->table} WHERE {$where} ORDER BY CreateDate DESC LIMIT 12";
		$product = $oDb->getAll($sql);
		$oSmarty->assign("product",$product);
		$oSmarty->display('hot_product.tpl');*/
		
		$url = $_SERVER['REQUEST_URI'];
		if ($_GET['page'])
			$url = str_replace("/".$_GET['page']."/","",$url);
		$page_link = "#i++/";	
		$cond = "IsHot=1 AND Status=1";
		$order="ORDER BY CreateDate DESC";
		//pre($cond); die();
		$limit = 16;
		$template = "hot_product.tpl";
		$this->getListItem(0,$page_link,$cond,$limit,$template,$order);
		
		
	}
	
	/**
	 * Hiển thị sản phẩm thông dụng
	 *
	 */
	function getHomeAjax()
	{
		global $oSmarty, $oDb;
		$type = $_POST['type'];
		if($type == 'product_new')
			$cont = "IsNew=1";
		elseif($type == 'product_hot')
			$cont = "IsHot=1";
		else
			$cont = "IsSale=1";
			
		/* Là sản phẩm mới */
		$sql1 = "SELECT * FROM {$this->table} WHERE Status=1 AND {$cont} ORDER BY CreateDate DESC";
		$product = $oDb->getAll($sql1);
		
		$oSmarty->assign('product',$product);
        $content = $oSmarty->fetch("popular_product.tpl");
        echo $content;
	}
	
	/**
	 * Sư lý tìm kiếm sản phẩm
	 * Hiển thị sản phẩm theo điều kiện tìm kiếm
	 */
	function getProductSearch()
	{		
		$url = $_SERVER['REQUEST_URI'];
		if ($_GET['page'])
			$url = str_replace("/".$_GET['page']."/","",$url);
		$page_link = "$url/i++/";
		
		if($_POST['keyword'] != '')
			$cond = "Name LIKE '%".$_POST['keyword']."%'";
		$limit = 12;
		
		$template = 'search_product.tpl';
		
		$this->getListItem($cat_id, $page_link, $cond, $limit, $template);
	}
	
	/**
	 * Lấy danh mục sản phẩm con theo danh mục cha
	 * Trả về: Danh sách sản phẩm con theo ul -> li
	 */
	function getCategory($parent_id)
	{
		global $oDb,$oSmarty;
		
		$sql = "SELECT * FROM {$this->pre_table} WHERE Status=1 AND ParentID = {$parent_id} ORDER BY Ordering ASC";
		$result = $oDb->getAll($sql);
		if($result){
			$html = '<ul style="display:none">';
			foreach($result as $key => $val){
				$html .= '<li>';
				$html .= '<h3><a rel="canonical" href="'.SITE_URL.'product/c-'.$val['id'].'/'.remove_marks($val['Name']).'.html">'.$val['Name'].'</a></h3>';
				$html .= $this->getCategory($val['id']);
				$html .= '</li>';
				
			}
			$html .= '</ul>';
		}
		return $html;
	}
    
	/**
	 * Lấy danh mục sản phẩm
	 * Hiển thị menu danh mục sản phẩm
	 */
	function getProductMenu()
	{
		global $oSmarty, $oDb;            
		
		$sql = "SELECT * FROM {$this->pre_table} WHERE Status=1 AND ParentID = 0 ORDER BY Ordering ASC";
		$cat = $oDb->getAll($sql);
		
		$html = '<ul>';
		foreach($cat as $key => $val){
			$html .= '<li>';
			$html .= '<h2><a rel="canonical" href="'.SITE_URL.'product/c-'.$val['id'].'/'.remove_marks($val['Name']).'.html">'.$val['Name'].'</a></h2>';
			$html .= $this->getCategory($val['id']);
			$html .= '</li>';
		}
		$html .= '</ul>';
		
		$template = 'menu_product.tpl';
		
		$oSmarty->assign("category",$html);
		$oSmarty->display($template);
	}
	
	/**
	 * Hiển thị các sản phẩm khác cùng loại
	 *
	 */
	function getProductOther()
	{
		global $oSmarty, $oDb;
        $id =  $_GET['id'];
		$cat_id = $oDb->getOne("SELECT CatID FROM {$this->table} WHERE id={$id} AND Status=1");
		
        $url = $_SERVER['REQUEST_URI'];
		if ($_GET['page'])
			$url = str_replace("/".$_GET['page']."/","",$url);
		$page_link = "#i++/";
		$cond = "id<>{$id} AND CatID='".$cat_id."'";
		$limit = 12;
		
		$template = 'other_product.tpl';
		
		$this->getListItem(0,$page_link,$cond,$limit,$template);
	}
	
	/**
	 * Hiển thị chi tiết 1 sản phẩm
	 *
	 */
	function getProductDetails()
	{
		global $oDb,$oSmarty;
		$id = $_GET['id'];
		$sql = "SELECT * FROM {$this->table} WHERE Status=1 AND id={$id}";
		$detail_item = $oDb->getRow($sql);		
		$oSmarty->assign('detail_item',$detail_item);
		
		$full_cat_name = $this->getFullCatName($detail_item['CatID']);
		$oSmarty->assign("full_cat_name",$full_cat_name);
		
		// Mổ tả danh mục sản phẩm
		$sql = "SELECT * FROM {$this->pre_table} WHERE id={$detail_item['CatID']} AND Status=1";
		$category = $oDb->getRow($sql);
		$oSmarty->assign("category",$category);
		
		$sql="select * from tbl_support_online where Status=1 ORDER BY Name ASC Limit 2";
		$sp_detail=$oDb->getAll($sql);
		//pre($listItem);
		$oSmarty->assign('sp_detail',$sp_detail); 
		
		$template = 'detail_product.tpl';
		
		$oSmarty->display($template);
	}
	
	/**
	 * Lấy id danh mục
	 * Trả về: chuỗi id danh  mục
	 */
	function getFullCatId($cat_id,$first = true)
	{
		global $oSmarty, $oDb;
        $where = "Status=1";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		$str_cat = "";
		$sql = "SELECT id FROM {$this->pre_table} WHERE {$where} AND ParentID={$cat_id} ORDER BY Ordering";
		$cat = $oDb->getCol($sql);
		if ($cat)
		{
			$str_cat = implode(',',$cat);
			$temp_str_cat = "";
			foreach ($cat as $key=>$value)
			{
				$temp_str_cat .= $this->getFullCatId($value,false);
			}
			$str_cat .=",".$temp_str_cat;			
		}
		if ($first)  
			$str_cat .=$cat_id;
		return $str_cat;
	}
	
	/**
	 * Lấy tên danh mục
	 * Trả về: chuỗi tên danh mục
	 */
	function getFullCatName($cat_id)
	{
		global $oSmarty, $oDb;
        $where = "Status=1";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		$sql = "SELECT Name FROM {$this->pre_table} WHERE {$where} AND id={$cat_id} ORDER BY Ordering";
		$cat_name = $oDb->getOne($sql);
		$sql = "SELECT ParentID FROM {$this->pre_table} WHERE {$where} AND id={$cat_id} ORDER BY Ordering";
		$parent = $oDb->getOne($sql);
		if ($parent)
		{
			$result = $this->getFullCatName($parent)."&nbsp; / &nbsp;".$cat_name;
		}else 
			$result = "&nbsp; / &nbsp;".$cat_name;
		return $result;
	}
	
	/**
	 * Hiển thị toàn bộ danh sách bản ghi
	 *
	 */
	function getProductList($cat_id=0)
	{
		$url = $_SERVER['REQUEST_URI'];
		if ($_GET['page'])
			$url = str_replace("/".$_GET['page']."/","",$url);
		$page_link = "$url/i++/";
		
		$template = 'list_product.tpl';
		
		$this->getListItem($cat_id,$page_link,"",24,$template);
	}
	
	function getListItem($cat_id=0,$page_link,$cond="",$limit=6,$template="list_product.tpl",$order="ORDER BY CreateDate DESC")
	{
		
		global $oDb,$oSmarty;
		$where = " Status=1 ";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
		if ($cat_id!=0)
		{
			// Chuỗi id danh  mục sản phẩm
			$str_cat_id = $this->getFullCatId($cat_id);
			$where .=" AND CatID IN ({$str_cat_id})";
			// Tên danh mục sản phẩm
			$full_cat_name = $this->getFullCatName($cat_id);
			$oSmarty->assign("full_cat_name",$full_cat_name);
			// Mổ tả danh mục sản phẩm
			$sql = "SELECT * FROM {$this->pre_table} WHERE id={$cat_id} AND Status=1";
			$category = $oDb->getRow($sql);
			$oSmarty->assign("cat_info",$category);
		}
		if ($cond != "")
			$where .= " AND ".$cond;
		$listItem = parent::Paging($limit, $this->field, $where,$page_link, $order);
		$oSmarty->assign("listItem",$listItem);
		//pre($listItem);
		$oSmarty->display($template);
	}
    
    /**
	 * Hiển thị các bản ghi theo từng danh mục
	 *
	 */
    function getProductCategory()
    {
        global $oDb, $oSmarty;
        
        $where = " Status=1 ";
        if (MULTI_LANGUAGE)
            $where .= " AND LangID = {$this->LangID} ";
        $sql = "SELECT * FROM {$this->pre_table} WHERE {$where} AND ParentID = 0 ORDER BY Ordering ASC";
        
        $category = $oDb->getAll($sql);
        
        foreach ($category as $key => $value)
        {
            $str_cat_id = $this->getFullCatId($cat_id);
            $where .=" AND CatID IN ({$str_cat_id})";
            $sql = "SELECT * FROM {$this->table} WHERE {$where} ORDER BY CreateDate DESC LIMIT 4";
            $product = $oDb->getAll($sql);
            $category[$key]['product'] = $product;
        }
        
        $oSmarty->assign('category',$category);
        $oSmarty->display('list_product_category.tpl');
    }
	
}
?>