<link href="{$smarty.const.SITE_URL}lib/jquerylightbox/css/jquery.lightbox.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{$smarty.const.SITE_URL}lib/jquerylightbox/js/jquery.lightbox.js"></script>
{literal}
<script type="text/javascript">
	$(function() {
        $('.gallery').lightBox();
    });
</script>
{/literal}

<div class="Module BoxBlack">
	<div class="clr"></div>
	<div class="ContentModule">
		<div style="margin:0px 10px; padding:8px 0 4px 0;">
			<div class="Titlebar" style="padding: 0 0 5px 0;border-bottom:1px solid #18486E;">{$result.Group_Name}</div>
			<div style="margin-top:10px;">
				{if $result.Group_Photo!=''}
				<img src="{$smarty.const.SITE_URL}upload/group/{$result.Group_Photo}" width="200" align="left" border="0" style="margin: 0 10px 5px 0;" >
				{/if}
				<p style="margin-top:5px;">				
				{$result.Group_Content}</p>				
			</div>
			<div class="clr"></div><br><br>
			<div align="right"><a href="#" onclick="window.history.go(-1); return false;" class="readmore">{#goback#}</a></div>
					
		</div>
	</div>
</div>
<div class="clr"></div>