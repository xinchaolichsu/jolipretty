<?php
if (!defined('IN_VES'))
{
    die('Hacking attempt');
}
class baseAttribute extends Bsg_Module_Base
{
	var $smarty;
	var $db;
	var $url="";
    var $datagrid;
	var $id;
	var $imgPath;
	var $imgPathShort;
	var $table='';
	var $arrAction;
	var $attributeType = array(
				'textbox'	=> 'Textbox',
				'listbox'	=> 'Listbox',
				'selectbox'	=> 'Selectbox',
				'radio'		=> 'Radio',
				'checkbox'	=> 'Checkbox',
			);
	function __construct($oSmarty, $oDb, $oDatagrid) {
		$this -> smarty = $oSmarty;
		$this -> db = $oDb;		
        $this -> datagrid = $oDatagrid;
		$this -> id=$_REQUEST[id];		
		$this -> table	="tbl_attribute";	 		
		parent::__construct($oDb);		
		$this->bsgDb->setTable($this->table);			
	}
//-----------------------------------------------------------------------//
	function insert()
	{
		$arr_data = array(
				'Type'	=> $_POST['type'],
				'Name'	=> $_POST['name'],
				'Value'	=> $_POST['value'],
				'Order'	=> $_POST['order'],
				'GroupID'	=> $_POST['group'],
				'AdvanceSearch'	=> $_POST['advance_search'],
			);
		$res = $this->bsgDb -> insert($arr_data);
	}
	function delete($id)
	{
		$this->bsgDb -> deleteWithPk($id);
	}
	function update()
	{
		$arr_data = array(
				'Type'	=> $_POST['type'],
				'Name'	=> $_POST['name'],
				'Value'	=> $_POST['value'],
				'Order'	=> $_POST['order'],
				'GroupID'	=> $_POST['group'],
				'AdvanceSearch'	=> $_POST['advance_search'],
			);
		$table = "tbl_attribute";
		$attribte_id = $_POST['id'];
		if ($attribte_id != '') {
			$this->bsgDb -> updateWithPk($attribte_id, $arr_data);
		}
		
	}
	function getDetail($id)
	{
		$table = "tbl_attribute";
		return $this->bsgDb->getRow($id);
	}
	/**
	 * get id of attribute follow group
	 */
	function getAttribute()
	{
		global $oDb;
		$sql = "SELECT * FROM tbl_attribute";
		$arrAtt = $oDb->getAll($sql);
		return $arrAtt;		
	}
	/**
	 * get Attribute for Manage Product
	 */
	function showAttributeByProductBack($pro_id='')
	{
		global $oDb;
		$form = new HTML_QuickForm();
		$arrAtt = $this->getAttribute();
		//pre($arrAtt);
		foreach ($arrAtt as $key=>$value)
		{				
			$id = $value['id'];
			$name = $value['Name'];
			$type = $value['Type'];
			$values = $value['Value'];
			if ($pro_id != '')
			{
				$sql = "SELECT ProductValue FROM tbl_product_attribute WHERE AttributeID=$id AND ProductID=$pro_id";
				$default = $oDb->getOne($sql);
				//print_r($default);
				$form->setDefaults(array('att_'.$id => $default));
			}
			switch ($type)
			{
				case 'textbox':
					$form -> addElement('text', 'att_'.$id,  $name, array('size' => 30, 'maxlength' => 255) );
					break;
				case 'selectbox':
					$arrValue = explode("\n",$values);
					$form -> addElement('select',  'att_'.$id,$name,$arrValue);
					break;
				case 'listbox':
					$arrValue = explode("\n",$values);
					$form -> addElement('select',  'att_'.$id,$name,$arrValue,array('multiple'=>'multiple'));
					break;
				case 'radio':
					$arrValue = explode("\n",$values);
					$radio = array();
					foreach ($arrValue as $key1=>$value1)
					{
						$radio[] = $form -> createElement('radio', 'att_'.$id,'',$value1,$key1);
					}
					$form -> addGroup($radio,'',$name);
					break;
				case 'checkbox':
					$arrValue = explode("\n",$values);
					$checkbox = array();
					foreach ($arrValue as $key1=>$value1)
					{
						$checkbox[] = $form -> createElement('checkbox', 'att_'.$id,'',$value1,$key1);
					}
					$form -> addGroup($checkbox,'',$name);
					break;
				default:
					break;
			}
		}
		return $form->toHtml();
	}
	function insertProductAttribute($pro_id)
	{
		global $oDb;
		$oDb->query("DELETE FROM tbl_product_attribute WHERE ProductID=".$pro_id);
		$sql = "SELECT id FROM tbl_attribute";
		$arrAttId = $oDb->getCol($sql);
		foreach ($arrAttId as $key=>$value)
		{
			$att_id = $value;
			$att_value = $_POST['att_'.$att_id];
			if(is_array($att_value))
				$att_value = implode(',',$att_value);
			if (trim($att_value) != '')
			{
				$arr_data = array(
					"AttributeID"		=> $att_id,
					"ProductID"		=> $pro_id,
					"ProductValue"	=> $att_value
				 );
				$res = $oDb->autoExecute("tbl_product_attribute", $arr_data, DB_AUTOQUERY_INSERT);
			}
		}
	}
}
?>