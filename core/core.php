<?php
if (!defined('IN_VES'))
{
    die('Hacking attempt');
}

	// Include path functions
	function user_set_include_path($path="core/PEAR")
	{
		global $old_include_path;
		$old_include_path= ini_get("include_path");
		ini_set("include_path", SITE_DIR."$path");	
	}

	function user_rollback_include_path()
	{
		global $old_include_path;
		ini_set("include_path", $old_include_path);
	}
	// End include path functions

	// Include PEAR DB Library
	// Create global variable
	global $oDb;
		
	if(!is_object($oDb))
	{
		user_set_include_path("core/PEAR");
			
		$db		= DB_NAME;
		$user	= DB_USER;
		$pass	= DB_PASSWORD;
		
		include_once('DB.php');
		
		$oDb =&DB::connect("mysql://$user:$pass@localhost/$db");
		$oDb->query("set names 'utf8'");
		$oDb->setFetchMode(DB_FETCHMODE_ASSOC);
		
		user_rollback_include_path();		
	}
	// End include PEAR DB Library
	//setcookie("username",$_SESSION['username_cus'],time()-3600*8640);
	// Load config from databse
	$row = $oDb->getAll( "SELECT Name, Value FROM tbl_sys_config" );
	foreach ($row as $k=>$v){
		$array[$v['Name']] = $v['Value'];
	}
	$_SESSION['email_admin'] = $array['admin_email'];
	define("EMAIL_ADMIN", $array['admin_email']);
	define("PAGE_WELCOME", $array['welcome']);
	define("PAGE_TITLE", $array['page_title']);
	define("PAGE_KEYWORD", $array['page_keyword']);
	define("PAGE_DESCRIPTION", $array['page_description']);
	
	
	// Language proccess...
	if( isset($_SESSION['lang_id'])){
		if(isset($_GET['LangID'])){
			$sql = "SELECT * FROM tbl_sys_lang WHERE id = '".mysql_real_escape_string($_GET['LangID'])."'";
		} else {
			$sql = "SELECT * FROM tbl_sys_lang WHERE id =' ".$_SESSION['lang_id']."'";
		}
		$row = $oDb->getRow($sql);
		$_SESSION['lang_id'] = $row['id'];
		$_SESSION['lang_file'] = $row['config_file'];
		$_SESSION['flag'] = $row['flag'];
	} else {
		$row = $oDb->getRow( "SELECT * FROM tbl_sys_lang ORDER BY id ASC" );
		$_SESSION['lang_id'] = $row['id'];
		$_SESSION['lang_file'] = $row['config_file'];
		$_SESSION['flag'] = $row['flag'];
	}
	
	// Include Smarty template engine
	global $oSmarty;
	if(!is_object($oSmarty))
	{		
		user_set_include_path("core/Smarty");
		
		include_once("Smarty.class.php");
		$oSmarty = new Smarty();
		if( isset($_GET['mod']) && $_GET['mod'] != '' ) $oSmarty -> template_dir = "module/".$_GET['mod']."/templates";		
		$oSmarty -> config_dir = "languages";		
		$oSmarty -> compile_dir = "template_c";
		
		user_rollback_include_path();
	}
	// End include Smarty template engine
	
	// Load module function
	/*
	*	$modul: Module name
	*	$task: Task
	*/
	function loadModule($modul, $task= '', $other= array())
	{
		global $oDb;
		global $oSmarty;
		
		if($task=="" && isset($_REQUEST['task']))
			$task= $_REQUEST['task'];
		
		if(is_dir("module/".$modul."/templates")){
			$oSmarty->template_dir = "module/".$modul."/templates";
		}else
			die("ERROR: NOT FOUND TEMPLATE DIRECTORY: module/".$modul."/templates");
						
		if(file_exists("module/$modul/$modul.frontend.php")) {			
			include_once("module/$modul/$modul.frontend.php");		
			$mod = new $modul();		
			$mod->run($task);
		}else
			die("ERROR: FILE NOT FOUND: module/$modul/$modul.frontend.php");		
	}
	// End load module function
	
	// Get page info functions
	function getPageinfo()
	{
		$modul= $_GET['mod'];
		if($modul=="")
		{
			global $oSmarty;
			$aPageinfo= array(
				"title" => PAGE_TITLE,
				"description" => PAGE_DESCRIPTION,
				"keyword" => PAGE_KEYWORD
			);
			$oSmarty->assign("aPageinfo", $aPageinfo);
		}
		else
		{
			if(file_exists("module/$modul/$modul.frontend.php")) {
				include_once("module/$modul/$modul.frontend.php");
				$mod = new $modul();
				if($task == "")
					$task= $_GET['task'];
				$mod->getPageinfo($task);
			}	
		}
	}
	
	function pre ($value)
	{
		echo "<pre>";
		if(is_array($value))
			print_r($value);
		else 
			echo $value;
		echo "</pre>";
	}
	
	function remove_marks($string)
	{
 		$trans = array ('é' => 'e', '‘' => '', '’' => '', '“' => '', '�?' => '', 'ẻ' => 'e', 'ẽ' => 'e', 'ằ' => 'a', 'ắ' => 'a', '�?' => 'o', 'ẽ' => 'e', '�?' => 'o', 'ẹ' => 'e', 'ặ' => 'a', '�?' => 'e', 'ặ' => 'a', 'à' => 'a', 'á' => 'a', 'ả' => 'a', 'ã' => 'a', 'ạ' => 'a', 'â' => 'a', 'ấ' => 'a', 'ầ' => 'a', 'ẫ' => 'a', 'ẩ' => 'a', 'ậ' => 'a', 'ú' => 'a', 'ù' => 'u', 'ủ' => 'u', 'ũ' => 'u', 'ụ' => 'u', 'à' => 'a', 'á' => 'a', 'ô' => 'o', 'ố' => 'o', 'ồ' => 'o', 'ổ' => 'o', 'ỗ' => 'o', 'ộ' => 'o', 'ó' => 'o', 'ò' => 'o', '�?' => 'o', 'õ' => 'o', '�?' => 'o', 'ê' => 'e', 'ế' => 'e', '�?' => 'e', 'ể' => 'e', 'ễ' => 'e', 'ệ' => 'e', 'í' => 'i', 'ì' => 'i', 'ỉ' => 'i', 'ĩ' => 'i', 'ị' => 'i', 'ơ' => 'o', 'ớ' => 'o', 'ý' => 'y', 'ỳ' => 'y', 'ỷ' => 'y', 'ỹ' => 'y', 'ỵ' => 'y', '�?' => 'o', 'ở' => 'o', 'ỡ' => 'o', 'ợ' => 'o', 'ư' => 'u', 'ừ' => 'u', 'ứ' => 'u', 'ử' => 'u', 'ữ' => 'u', 'ự' => 'u', 'đ' => 'd', 'À' => 'A', '�?' => 'A', 'Ả' => 'A', 'Ã' => 'A', 'Ạ' => 'A', 'Â' => 'A', 'Ấ' => 'A', 'À' => 'A', 'Ẫ' => 'A', 'Ẩ' => 'A', 'Ậ' => 'A', 'Ú' => 'U', 'Ù' => 'U', 'Ủ' => 'U', 'Ũ' => 'U', 'Ụ' => 'U', 'Ô' => 'O', '�?' => 'O', 'Ồ' => 'O', 'Ổ' => 'O', 'Ỗ' => 'O', 'Ộ' => 'O', 'Ê' => 'E', 'Ế' => 'E', 'Ề' => 'E', 'Ể' => 'E', 'Ễ' => 'E', 'Ệ' => 'E', '�?' => 'I', 'Ì' => 'I', 'Ỉ' => 'I', 'Ĩ' => 'I', 'Ị' => 'I', 'Ơ' => 'O', 'Ớ' => 'O', 'Ờ' => 'O', 'Ở' => 'O', 'Ỡ' => 'O', 'Ợ' => 'O', 'Ư' => 'U', 'Ừ' => 'U', 'Ứ' => 'U', 'Ử' => 'U', 'Ữ' => 'U', 'Ự' => 'U', '�?' => 'D', '�?' => 'Y', 'Ỳ' => 'Y', 'Ỷ' => 'Y', 'Ỹ' => 'Y', 'Ỵ' => 'Y', 'a�?' => 'a', 'à' => 'a', 'ả' => 'a', 'ã' => 'a', 'ạ' => 'a', 'ă' => 'a', 'ă�?' => 'a', 'ằ' => 'a', 'ẳ' => 'a', 'ẵ' => 'a', 'ặ' => 'a', 'â' => 'a', 'â�?' => 'a', 'ầ' => 'a', 'ẩ' => 'a', 'ẫ' => 'a', 'ậ' => 'a', 'u�?' => 'u', 'ù' => 'u', 'ủ' => 'u', 'ũ' => 'u', 'ụ' => 'u', 'ư' => 'u', 'ư�?' => 'u', 'ừ' => 'u', 'ử' => 'u', 'ữ' => 'u', 'ự' => 'u', 'i�?' => 'i', 'ì' => 'i', 'ỉ' => 'i', 'ĩ' => 'i', 'ị' => 'i', 'o�?' => 'o', 'ò' => 'o', 'ỏ' => 'o', 'õ' => 'o', 'ọ' => 'o', 'ô' => 'o', 'ô�?' => 'o', 'ồ' => 'ô', 'ổ' => 'o', 'ỗ' => 'o', 'ộ' => 'o', 'ơ' => 'o', 'ơ�?' => 'o', 'ờ' => 'o', 'ở' => 'o', 'ỡ' => 'o', 'ợ' => 'o', 'đ' => 'd', '�?' => 'D', 'y�?' => 'y', 'ỳ' => 'y', 'ỷ' => 'y', 'ỹ' => 'y', 'ỵ' => 'y', 'A�?' => 'A', 'À' => 'A', 'Ả' => 'A', 'Ã' => 'A', 'Ạ' => 'A', 'Ă' => 'A', 'Ă�?' => 'A', 'Ẳ' => 'A', 'Ẵ' => 'A', 'Ặ' => 'A', 'Â' => 'A', 'Â�?' => 'A', 'Ẩ' => 'A', 'Ẫ' => 'A', 'Ậ' => 'A', 'E�?' => 'E', 'È' => 'E', 'Ẻ' => 'E', 'Ẽ' => 'E', 'Ẹ' => 'E', 'Ê�?' => 'E', 'Ề' => 'E', 'Ể' => 'E', 'Ễ' => 'E', 'Ệ' => 'E', 'U�?' => 'U', 'Ù' => 'U', 'Ủ' => 'U', 'Ũ' => 'U', 'Ụ' => 'U', 'Ư' => 'U', 'Ư�?' => 'U', 'Ừ' => 'U', 'Ử' => 'U', 'Ữ' => 'U', 'Ự' => 'U', 'I�?' => 'I', 'Ì' => 'I', 'Ỉ' => 'I', 'Ĩ' => 'I', 'Ị' => 'I', 'O�?' => 'O', 'Ò' => 'O', 'Ỏ' => 'O', 'Õ' => 'O', 'Ọ' => 'O', 'Ô' => 'O', 'Ô�?' => 'O', 'Ổ' => 'O', 'Ỗ' => 'O', 'Ộ' => 'O', 'Ơ' => 'O', 'Ơ�?' => 'O', 'Ờ' => 'O', 'Ở' => 'O', 'Ỡ' => 'O', 'Ợ' => 'O', 'Y�?' => 'Y', 'Ỳ' => 'Y', 'Ỷ' => 'Y', 'Ỹ' => 'Y', 'Ỵ' => 'Y', ' ' => '-','"'=>'','%'=>'');
		return strtr( trim($string), $trans );
	}	
	function remove_marks_name($string)
	{
 		$trans = array ('é' => 'e', '‘' => '', '’' => '', '“' => '', '�?' => '', 'ẻ' => 'e', 'ẽ' => 'e', 'ằ' => 'a', 'ắ' => 'a', '�?' => 'o', 'ẽ' => 'e', '�?' => 'o', 'ẹ' => 'e', 'ặ' => 'a', '�?' => 'e', 'ặ' => 'a', 'à' => 'a', 'á' => 'a', 'ả' => 'a', 'ã' => 'a', 'ạ' => 'a', 'â' => 'a', 'ấ' => 'a', 'ầ' => 'a', 'ẫ' => 'a', 'ẩ' => 'a', 'ậ' => 'a', 'ú' => 'a', 'ù' => 'u', 'ủ' => 'u', 'ũ' => 'u', 'ụ' => 'u', 'à' => 'a', 'á' => 'a', 'ô' => 'o', 'ố' => 'o', 'ồ' => 'o', 'ổ' => 'o', 'ỗ' => 'o', 'ộ' => 'o', 'ó' => 'o', 'ò' => 'o', '�?' => 'o', 'õ' => 'o', '�?' => 'o', 'ê' => 'e', 'ế' => 'e', '�?' => 'e', 'ể' => 'e', 'ễ' => 'e', 'ệ' => 'e', 'í' => 'i', 'ì' => 'i', 'ỉ' => 'i', 'ĩ' => 'i', 'ị' => 'i', 'ơ' => 'o', 'ớ' => 'o', 'ý' => 'y', 'ỳ' => 'y', 'ỷ' => 'y', 'ỹ' => 'y', 'ỵ' => 'y', '�?' => 'o', 'ở' => 'o', 'ỡ' => 'o', 'ợ' => 'o', 'ư' => 'u', 'ừ' => 'u', 'ứ' => 'u', 'ử' => 'u', 'ữ' => 'u', 'ự' => 'u', 'đ' => 'd', 'À' => 'A', '�?' => 'A', 'Ả' => 'A', 'Ã' => 'A', 'Ạ' => 'A', 'Â' => 'A', 'Ấ' => 'A', 'À' => 'A', 'Ẫ' => 'A', 'Ẩ' => 'A', 'Ậ' => 'A', 'Ú' => 'U', 'Ù' => 'U', 'Ủ' => 'U', 'Ũ' => 'U', 'Ụ' => 'U', 'Ô' => 'O', '�?' => 'O', 'Ồ' => 'O', 'Ổ' => 'O', 'Ỗ' => 'O', 'Ộ' => 'O', 'Ê' => 'E', 'Ế' => 'E', 'Ề' => 'E', 'Ể' => 'E', 'Ễ' => 'E', 'Ệ' => 'E', '�?' => 'I', 'Ì' => 'I', 'Ỉ' => 'I', 'Ĩ' => 'I', 'Ị' => 'I', 'Ơ' => 'O', 'Ớ' => 'O', 'Ờ' => 'O', 'Ở' => 'O', 'Ỡ' => 'O', 'Ợ' => 'O', 'Ư' => 'U', 'Ừ' => 'U', 'Ứ' => 'U', 'Ử' => 'U', 'Ữ' => 'U', 'Ự' => 'U', '�?' => 'D', '�?' => 'Y', 'Ỳ' => 'Y', 'Ỷ' => 'Y', 'Ỹ' => 'Y', 'Ỵ' => 'Y', 'a�?' => 'a', 'à' => 'a', 'ả' => 'a', 'ã' => 'a', 'ạ' => 'a', 'ă' => 'a', 'ă�?' => 'a', 'ằ' => 'a', 'ẳ' => 'a', 'ẵ' => 'a', 'ặ' => 'a', 'â' => 'a', 'â�?' => 'a', 'ầ' => 'a', 'ẩ' => 'a', 'ẫ' => 'a', 'ậ' => 'a', 'u�?' => 'u', 'ù' => 'u', 'ủ' => 'u', 'ũ' => 'u', 'ụ' => 'u', 'ư' => 'u', 'ư�?' => 'u', 'ừ' => 'u', 'ử' => 'u', 'ữ' => 'u', 'ự' => 'u', 'i�?' => 'i', 'ì' => 'i', 'ỉ' => 'i', 'ĩ' => 'i', 'ị' => 'i', 'o�?' => 'o', 'ò' => 'o', 'ỏ' => 'o', 'õ' => 'o', 'ọ' => 'o', 'ô' => 'o', 'ô�?' => 'o', 'ồ' => 'ô', 'ổ' => 'o', 'ỗ' => 'o', 'ộ' => 'o', 'ơ' => 'o', 'ơ�?' => 'o', 'ờ' => 'o', 'ở' => 'o', 'ỡ' => 'o', 'ợ' => 'o', 'đ' => 'd', '�?' => 'D', 'y�?' => 'y', 'ỳ' => 'y', 'ỷ' => 'y', 'ỹ' => 'y', 'ỵ' => 'y', 'A�?' => 'A', 'À' => 'A', 'Ả' => 'A', 'Ã' => 'A', 'Ạ' => 'A', 'Ă' => 'A', 'Ă�?' => 'A', 'Ẳ' => 'A', 'Ẵ' => 'A', 'Ặ' => 'A', 'Â' => 'A', 'Â�?' => 'A', 'Ẩ' => 'A', 'Ẫ' => 'A', 'Ậ' => 'A', 'E�?' => 'E', 'È' => 'E', 'Ẻ' => 'E', 'Ẽ' => 'E', 'Ẹ' => 'E', 'Ê�?' => 'E', 'Ề' => 'E', 'Ể' => 'E', 'Ễ' => 'E', 'Ệ' => 'E', 'U�?' => 'U', 'Ù' => 'U', 'Ủ' => 'U', 'Ũ' => 'U', 'Ụ' => 'U', 'Ư' => 'U', 'Ư�?' => 'U', 'Ừ' => 'U', 'Ử' => 'U', 'Ữ' => 'U', 'Ự' => 'U', 'I�?' => 'I', 'Ì' => 'I', 'Ỉ' => 'I', 'Ĩ' => 'I', 'Ị' => 'I', 'O�?' => 'O', 'Ò' => 'O', 'Ỏ' => 'O', 'Õ' => 'O', 'Ọ' => 'O', 'Ô' => 'O', 'Ô�?' => 'O', 'Ổ' => 'O', 'Ỗ' => 'O', 'Ộ' => 'O', 'Ơ' => 'O', 'Ơ�?' => 'O', 'Ờ' => 'O', 'Ở' => 'O', 'Ỡ' => 'O', 'Ợ' => 'O', 'Y�?' => 'Y', 'Ỳ' => 'Y', 'Ỷ' => 'Y', 'Ỹ' => 'Y', 'Ỵ' => 'Y', ' ' => '','"'=>'','%'=>'');
		return strtr( trim($string), $trans );
	}
?>
