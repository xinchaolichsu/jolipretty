<?php
if (!defined('IN_VES'))
{
    die('Hacking attempt');
}
abstract class Bsg_Module_Base
{
	public $db;
	public $smarty;
	public $bsgDb; //Bsg_Db
	
	public $dataGrid;
	public $test='a';
	
	/**
	 * quick form config
	 * 
	 * @var object HTML Quick Form
	 * @var array $aElement
	 * @var array $aElementNotShow
	 * 
	 */
	public $quickForm;
	private $aElement = array();
	private $aElementNotShow = array();

	public function run($task){}
	public function getPageInfo($task){}
	
	public function __construct(&$pearDb, &$smarty = null)
	{
		$this->db	= $pearDb;	
		$this->smarty = $smarty;
		$this->bsgDb = new Bsg_Db($pearDb);
		$this -> arrAction = array(
			array(
				"task" => "add",
				"action" => "",
				"tooltip" => "Add record"		
			),	
			array(
				"task" => "edit",
				"text" => "Edit",
				"icon" => "edit.png",
				"action" => "",
				"tooltip" => "Edit record"		
			),		
			array(
			
				"task" => "delete",
				"text" => "Delete",
				"icon" => "delete.jpg",
				"confirm" => "Xác nhận xóa?",
				"action" => "",
				"tooltip" => "Delete record"
			)
		);
	}
	
	/****************************FUNCTIONS FOR QUICK FORM*************************************/
	/**
	 * initForm
	 *
	 * @param string $task
	 * @param array $aElement
	 * @param string $formName
	 * @param string $method
	 * @param string $action
	 * @param string $target
	 * @param array $attributes
	 * @param boolean $trackSubmit
	 */
	
	public function initForm ($task, $aElement, $formName = '', $method = 'post', $action = '', $target = '', $attributes = null, $trackSubmit = false)
	{
		$this->quickForm = new HTML_QuickForm($formName, $method, $action, $target, $attributes, $trackSubmit);
		$this->quickForm->addElement('hidden', 'task', $task, array());
		
		$this->aElement = $aElement;
	}
	/**
	 * setNotShowElement
	 *
	 * @param array $array
	 */
	public function setElementNotShow ($array = array())
	{
		$this->aElementNotShow = $array;
	}
	
	public function setElementOrder ($arrayOrder = array())
	{
		$aElement = array();
		foreach ($arrayOrder as $element)
		{
			if($this->elementExisted($element))
			{
				$aElement[$element] = $this->aElement[$element];
			}
		}
		foreach ($this->aElement as $element=>$elementInfo)
		{
			if(!in_array($element, $arrayOrder))
			{
				$aElement[$element] = $this->aElement[$element];
			}
		}
		$this->aElement = $aElement;
	}
	
	public function insertElement ($after, $element, $elementInfo)
	{
		if($this->elementExisted($after))
		{
			$aElement = array();
			foreach ($this->aElement as $key=>$value)
			{
				$aElement[$key] = $value;
				if($key == $after)
				{
					$aElement[$element] = $elementInfo;
					break;
				}
			}
			$this->aElement = array_merge($aElement, $this->aElement);
		}
		if($after == 'AtBeginning')
		{
			$this->aElement = array_merge(array($element=>$elementInfo), $this->aElement);
		}
		//else if($after == 'AtEnd')
		else
		{
			$this->aElement[$element] = $elementInfo;
		}
	}
	
	public function elementExisted ($element)
	{
		return isset($this->aElement[$element]) ? true : false;
	}
	
	public function setFormData($aData)
	{
		$this->quickForm->setDefaults($aData);
	}

	/**
	 *  check array $GLOBALS['HTML_QUICKFORM_ELEMENT_TYPES']
	 *
	 */
	public function displayForm()
	{

		foreach ($this->aElement as $key=>$field)
		{
			if(!in_array($key, $this->aElementNotShow))
			{
				$label = $key;//will get from array language (language file)
				$type = $field['InputType'];
				switch($field['InputType'])
				{
					case 'group':
					case 'xbutton':
					case 'autocomplete':
					case 'hierselect':
					case 'html':
					case 'header':
					case 'advcheckbox':
					case 'password':
						break;
					case 'text':
					case 'textarea':
						$this->quickForm->addElement($type, $key, $label, $field['Attributes']);
						break;
					case 'checkbox':
						$this->quickForm->addElement($type, $key, $label, $field['Text'], $field['Attributes']);
						break;
					case 'radio':
						$this->quickForm->addElement($type, $key, $label, $field['Text'], $field['Value'], $field['Attributes']);
						break;
					case 'hidden':
						$this->quickForm->addElement($type, $key, $field['Value'], $field['Attributes']);
						break;
					case 'image':
						$this->quickForm->addElement($type, $field['Src'], $field['Attributes']);
						break;
					case 'file':
						$this->quickForm->addElement($type, $key, $label, $field['Attributes']);
						break;
					case 'select':
						$this->quickForm->addElement($type, $key, $label, $field['Options'], $field['Attributes']);
						break;
					case 'date':
					case 'datetime':
						$this->quickForm->addElement($type, $key, $label, $field['Options'], $field['Attributes']);
						break;
					case 'link':
						$this->quickForm->addElement($type, $key, $label, $field['Href'], $field['Text'], $field['Attributes']);
						break;
					case 'static':
						$this->quickForm->addElement($type, $key, $label, $field['Text']);
						break;
					case 'submit':
					case 'button':
					case 'reset':
						$this->quickForm->addElement($type, $key, $field['Value'], $field['Attributes']);
						break;
				}
			}
		}
		
		$this->quickForm->display();
	}
	
	/****************************FUNCTIONS FOR DATAGRID*************************************/
	public function redir($url)
	{
		if(!headers_sent())
		{
			header("Location: " . $url);
		}
		else 
		{
			echo "<script type='text/javascript' language='javascript'>location.href='{$url}'</script>";
		}
	}
	
	public function isPost()
	{
		return (strtoupper($_SERVER['REQUEST_METHOD'])=='POST') ? true : false;
	}
	
	public function editor($id, $content, $att=array('width'=>'800', 'height'=>'300'))
	{
			
			
			require_once(SITE_DIR."core/fckeditor/fckeditor.php");
			$editor = new FCKeditor($id) ;
			$editor->BasePath	= SITE_URL."/core/fckeditor/";
			$editor->Value = $content; 
			$editor->Width=$att['width'];
			$editor->Height=$att['height'];
			$editor->Config['SkinPath'] = SITE_URL."core/fckeditor/editor/skins/office2003/";
			return $editor->Create();
			
	}	
	function remove_marks($string)
{
 $trans = array ('é' => 'e', '‘' => '', '’' => '', '“' => '', '�?' => '', 'ẻ' => 'e', 'ẽ' => 'e', 'ằ' => 'a', 'ắ' => 'a', '�?' => 'o', 'ẽ' => 'e', '�?' => 'o', 'ẹ' => 'e', 'ặ' => 'a', '�?' => 'e', 'ặ' => 'a', 'à' => 'a', 'á' => 'a', 'ả' => 'a', 'ã' => 'a', 'ạ' => 'a', 'â' => 'a', 'ấ' => 'a', 'ầ' => 'a', 'ẫ' => 'a', 'ẩ' => 'a', 'ậ' => 'a', 'ú' => 'a', 'ù' => 'u', 'ủ' => 'u', 'ũ' => 'u', 'ụ' => 'u', 'à' => 'a', 'á' => 'a', 'ô' => 'o', 'ố' => 'o', 'ồ' => 'o', 'ổ' => 'o', 'ỗ' => 'o', 'ộ' => 'o', 'ó' => 'o', 'ò' => 'o', '�?' => 'o', 'õ' => 'o', '�?' => 'o', 'ê' => 'e', 'ế' => 'e', '�?' => 'e', 'ể' => 'e', 'ễ' => 'e', 'ệ' => 'e', 'í' => 'i', 'ì' => 'i', 'ỉ' => 'i', 'ĩ' => 'i', 'ị' => 'i', 'ơ' => 'o', 'ớ' => 'o', 'ý' => 'y', 'ỳ' => 'y', 'ỷ' => 'y', 'ỹ' => 'y', 'ỵ' => 'y', '�?' => 'o', 'ở' => 'o', 'ỡ' => 'o', 'ợ' => 'o', 'ư' => 'u', 'ừ' => 'u', 'ứ' => 'u', 'ử' => 'u', 'ữ' => 'u', 'ự' => 'u', 'đ' => 'd', 'À' => 'A', '�?' => 'A', 'Ả' => 'A', 'Ã' => 'A', 'Ạ' => 'A', 'Â' => 'A', 'Ấ' => 'A', 'À' => 'A', 'Ẫ' => 'A', 'Ẩ' => 'A', 'Ậ' => 'A', 'Ú' => 'U', 'Ù' => 'U', 'Ủ' => 'U', 'Ũ' => 'U', 'Ụ' => 'U', 'Ô' => 'O', '�?' => 'O', 'Ồ' => 'O', 'Ổ' => 'O', 'Ỗ' => 'O', 'Ộ' => 'O', 'Ê' => 'E', 'Ế' => 'E', 'Ề' => 'E', 'Ể' => 'E', 'Ễ' => 'E', 'Ệ' => 'E', '�?' => 'I', 'Ì' => 'I', 'Ỉ' => 'I', 'Ĩ' => 'I', 'Ị' => 'I', 'Ơ' => 'O', 'Ớ' => 'O', 'Ờ' => 'O', 'Ở' => 'O', 'Ỡ' => 'O', 'Ợ' => 'O', 'Ư' => 'U', 'Ừ' => 'U', 'Ứ' => 'U', 'Ử' => 'U', 'Ữ' => 'U', 'Ự' => 'U', '�?' => 'D', '�?' => 'Y', 'Ỳ' => 'Y', 'Ỷ' => 'Y', 'Ỹ' => 'Y', 'Ỵ' => 'Y', 'a�?' => 'a', 'à' => 'a', 'ả' => 'a', 'ã' => 'a', 'ạ' => 'a', 'ă' => 'a', 'ă�?' => 'a', 'ằ' => 'a', 'ẳ' => 'a', 'ẵ' => 'a', 'ặ' => 'a', 'â' => 'a', 'â�?' => 'a', 'ầ' => 'a', 'ẩ' => 'a', 'ẫ' => 'a', 'ậ' => 'a', 'u�?' => 'u', 'ù' => 'u', 'ủ' => 'u', 'ũ' => 'u', 'ụ' => 'u', 'ư' => 'u', 'ư�?' => 'u', 'ừ' => 'u', 'ử' => 'u', 'ữ' => 'u', 'ự' => 'u', 'i�?' => 'i', 'ì' => 'i', 'ỉ' => 'i', 'ĩ' => 'i', 'ị' => 'i', 'o�?' => 'o', 'ò' => 'o', 'ỏ' => 'o', 'õ' => 'o', 'ọ' => 'o', 'ô' => 'o', 'ô�?' => 'o', 'ồ' => 'ô', 'ổ' => 'o', 'ỗ' => 'o', 'ộ' => 'o', 'ơ' => 'o', 'ơ�?' => 'o', 'ờ' => 'o', 'ở' => 'o', 'ỡ' => 'o', 'ợ' => 'o', 'đ' => 'd', '�?' => 'D', 'y�?' => 'y', 'ỳ' => 'y', 'ỷ' => 'y', 'ỹ' => 'y', 'ỵ' => 'y', 'A�?' => 'A', 'À' => 'A', 'Ả' => 'A', 'Ã' => 'A', 'Ạ' => 'A', 'Ă' => 'A', 'Ă�?' => 'A', 'Ẳ' => 'A', 'Ẵ' => 'A', 'Ặ' => 'A', 'Â' => 'A', 'Â�?' => 'A', 'Ẩ' => 'A', 'Ẫ' => 'A', 'Ậ' => 'A', 'E�?' => 'E', 'È' => 'E', 'Ẻ' => 'E', 'Ẽ' => 'E', 'Ẹ' => 'E', 'Ê�?' => 'E', 'Ề' => 'E', 'Ể' => 'E', 'Ễ' => 'E', 'Ệ' => 'E', 'U�?' => 'U', 'Ù' => 'U', 'Ủ' => 'U', 'Ũ' => 'U', 'Ụ' => 'U', 'Ư' => 'U', 'Ư�?' => 'U', 'Ừ' => 'U', 'Ử' => 'U', 'Ữ' => 'U', 'Ự' => 'U', 'I�?' => 'I', 'Ì' => 'I', 'Ỉ' => 'I', 'Ĩ' => 'I', 'Ị' => 'I', 'O�?' => 'O', 'Ò' => 'O', 'Ỏ' => 'O', 'Õ' => 'O', 'Ọ' => 'O', 'Ô' => 'O', 'Ô�?' => 'O', 'Ổ' => 'O', 'Ỗ' => 'O', 'Ộ' => 'O', 'Ơ' => 'O', 'Ơ�?' => 'O', 'Ờ' => 'O', 'Ở' => 'O', 'Ỡ' => 'O', 'Ợ' => 'O', 'Y�?' => 'Y', 'Ỳ' => 'Y', 'Ỷ' => 'Y', 'Ỹ' => 'Y', 'Ỵ' => 'Y', '"'=>'','%'=>'');
	return strtr( trim($string), $trans );
}	

	public function multiImage($id,$img_path,$arr_image=array(),$img_url,$att=array('width'=>'750', 'height'=>'450'))
	{
		$path = '../core/jquery/plugin';
        $str = '<link type="text/css" rel="stylesheet" href="'.$path.'/thickbox/thickbox.css"></link>
                <script type="text/javascript" src="'.$path.'/thickbox/thickbox.js"></script> 
                <script language="javascript">
				function deletePhoto'.$id.'(file_name,id)
				{
					document.getElementById(\'div_photo_\'+id).style.display = \'none\';
					document.getElementById(\'list_del_photo_'.$id.'\').value += file_name+",";
				}
				</script>               
                ';
        $str .= '<br/ ><a href="/core/upload_multi/content.php?height='.$att["height"].'&width='.$att["width"].'&modal=true&id='.$id.'&des='.$img_path.'&imgurl='.$img_url.'" class="thickbox" title="Upload ảnh" style="margin: 20px;">Upload ảnh</a><br/ ><br/ >';
        $str .= '<div id="listMultiImgae_'.$id.'" style="width:600px; float:left;">';
		if(!empty($arr_image))
		{
        foreach ($arr_image as $value)
        {
        	$str .="<div id=\"div_photo_{$value["Photo_ID"]}\" style=\"width:105px; float:left; margin-left: 10px; margin-bottom: 10px;\"><a href='".$img_url.$value["Photo_Name"]."' onclick='return hs.expand(this)' class='highslide'><img src='".$img_url.$value["Photo_Name"]."' width=100 height=100 align=\"left\" border=0></a>";
        	$str .="<br />";
        	/*if ($value["Photo_Default"]==1)
        		$str .="<input type=\"radio\" name=\"default_photo_$id\" value=\"{$value["Photo_Name"]}\" checked=\"checked\" />";
        	else 
        		$str .="<input type=\"radio\" name=\"default_photo_$id\" value=\"{$value["Photo_Name"]}\" />";
        	$str .="Mặc định";*/
        	$str .="<a href='#'><img src='/view/images/delete.jpg' onclick=\"deletePhoto$id('{$value["Photo_Name"]}','{$value["Photo_ID"]}')\" align=\"right\" border=0></a>";
        	$str .= '</div>';
        }
		}
        $str .= '</div>';
        $str .='<input type="hidden" name="'.$id.'" value="" id="'.$id.'" /> ';
        $str .='<input type="hidden" name="list_del_photo_'.$id.'" value="" id="list_del_photo_'.$id.'" /> ';
        return $str;
	}	
	
	function addMultiImageToDatabase($id,$table,$ref_id=array(),$field_file="Photo_Name")
	{
		if ($_POST && $_POST[$id]!="")
		{
			$str_file_name = $_POST[$id];
			$arr_file_name = explode(",",$str_file_name);	
			//echo gettype($arr_file_name);
			if (is_array($arr_file_name) && !empty($arr_file_name))
			{
				foreach ($arr_file_name as $key=>$filename)
				{
					//$default = ($filename==$_POST["default_photo_".$id])?1:0;
					$arrData = array(
								$field_file	=> $this->remove_marks($filename),
							);
					$arrData = array_merge($ref_id,$arrData);
					$res = $this -> bsgDb ->add_rec($table, $arrData);

					//pre($res);die();
				}
			}
		}
	}
	function deleteMultiPhoto($id,$path,$table)
	{
		if ($_POST && $_POST["list_del_photo_$id"]!="")
		{
			$str_file_name = $_POST["list_del_photo_$id"];
			$arr_file_name = explode(",",$str_file_name);
			$str_del_file_name = "";
			foreach ($arr_file_name as $value)
			{
				@unlink($path.$value);
				$str_del_file_name .= "'$value',";
			}
			$str_del_file_name = substr($str_del_file_name,0,-1);
			$this -> bsgDb -> del_rec($table," Photo_Name IN (".$str_del_file_name.")");
		}
	}
	public function date_time($id, $value)
    {
		$path = '../core';
        $str = '<link type="text/css" rel="stylesheet" href="'.$path.'/calendar/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
                <script type="text/javascript" src="'.$path.'/calendar/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20060118"></script>
                <script language = "javascript">var pathToImages = "'.$path.'/calendar/images/"; </script>
                ';

        $str .= '<input type="text" value="'.$value.'" readonly name="'.$id.'"><input type="button" value="Select" onclick="displayCalendar(document.forms[0].'.$id.',\'yyyy-mm-dd hh:ii\',this,true)">';

        return $str;
    }

    public function date($id, $value)
    {
		$path = '../core';
        $str = '<link type="text/css" rel="stylesheet" href="'.$path.'/calendar/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
                <script type="text/javascript" src="'.$path.'/calendar/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js?random=20081212"></script>
                <script language = "javascript">var pathToImages = "'.$path.'/calendar/images/"; </script>
                ';

        $str .= '<input type="text" value="'.$value.'" readonly name="'.$id.'"  id="'.$id.'" ><input type="button" value="Select" onclick="displayCalendar(document.forms[0].'.$id.',\'yyyy-mm-dd\',this)">';

        return $str;
    }
		
    function loadLang()
    {
    	$lang = $this -> db -> getAssoc('select id, name from tbl_sys_lang order by id');
        return  $lang;
    }
    /*
		// loadJs.
		@ Date create : 1-10-2008
		@ Author : Hoinq
		@ Paramater :
			1. $str_js : list string space ','
	*/	
	function loadJs($str_js)
	{
			$arr_js=explode(',',$str_js);
			if($_GET['task'] != 'ajax_title_type' && !isset($_GET['ajax']))				
				{
					foreach($arr_js as $js)
					echo '<script type="text/javascript" src="'.SITE_URL.'module/admin/js/'.$js.'"></script>';
				}	
	}
	/*
		// ChangeOption.
		@ Date create : 1-10-2008
		@ Author : Hoinq
		@ Paramater :
			1. $arr_option : array option
	*/	
	function changeOption($arr_option, $str='')
	{
			if ($arr_option) {
				if($str!='')
					$option.='<option value ="">'.$str.'</option>';
				foreach ($arr_option as $key => $value) {
					$option .='<option value = "'.$key.'">'.$value.'</option>';
				}
			}			
	echo $option;
	}
	function getOption($arr_option, $name='', $root='',$selected='')
	{
		$str="<select name=".$name." id=".$name."'_id'>";
			if ($arr_option) {
				if($str!='')
					$str.='<option value ="0">'.$root.'</option>';
				foreach ($arr_option as $key => $value) {
					if($selected==$key)
							$selected='selected="selected"';
					$str .='<option value = "'.$key.'" '.$selected.'>'.$value.'</option>';
				}
			}
		$str .='</select>';
		return $str;
	}
	function redirect($url,$type="location") {
		$url = $url!='' ? $url : $this->url;
		echo '<script language = "javascript">
				location.href = "'.$url.'";
				</script>
		';
	}
	function get_config_vars ($var = '', $default = '') {
		global $oSmarty;
		$value = $oSmarty->get_config_vars($var);
		
		if ($value != '') {
			return (string) $value;
		} else {
			return $default;
		}
	}
	function getRootPath ($default = '') {
		if ($default != '') {
			return $default;
		}
		$sub = isset($_GET['sub']) ? $_GET['sub'] : NULL;
		$mod = isset($_GET['mod']) ? $_GET['mod'] : NULL;
		$task = isset($_GET['task']) ? $_GET['task'] : NULL;
		if ($sub) {
			$root_Path=$this->get_config_vars($sub . "_sub_root_path");
		}
		if($task) {
			$root_Path=$this->get_config_vars($task."_task_".$sub."_root_path");
			
		}
		$root_path = str_replace(">","<span style='postision:absolute;top:0px;'><img src='/admin/images/root.gif' border='0' style='cursor:pointer;' /></span>",$root_Path);
		 $str = "
                    <style>
                        html,body, td{
                            margin:0px;
                            overflow:auto;
                            font-family:Arial, Helvetica, sans-serif;
                            font-size:12px;
                        }

                        #root
                        {
                            width: 100%;
                            height: 29px;
                            text-align: left;
                            font-weight: bold;
                          	background-color:#F2F1F1;
							color:#000049;
							border-bottom:1px solid #D0D4D2;
                        }
                    </style>
                    <table cellspacing='0' cellpadding='0' width='100%'>
                        <tr>
                            <td valign='middle' id='root'>

                                &nbsp;&nbsp;".$root_path."
                            </td>
                        </tr>
                    </table>
                ";

      
		if(!isset($_GET['ajax']))						
			echo $str;
					
	}
	
	/*
		// get Path .
		@ Date create : 2008-21-1
		@ Author : Khanhnk
		@ Paramater :
			1. $sRawPath : Raw Path. Replace character >> to image seperator.
	*/	
	function getPath( $sRawPath = ''){
		$root_path = str_replace(">>","<span style='postision:absolute;top:0px;'><img src='".SITE_URL."core/datagrid/templates/images/root3.gif' border='0' style='cursor:pointer;' /></span>",$sRawPath);
		
		 $str = "   <style>                        
						html,body, td{
                            margin:0px;
                            overflow:auto;
                            font-family:Arial, Helvetica, sans-serif;
                            font-size:12px;
                        }
                        #root
                        {
                            width: 100%;
							height: 27px;	
							text-align: left;
							font-weight: bold;		
							background-color:#006182;
							
                        }
                    </style>
                    <table cellspacing='0' cellpadding='0' width='100%'>
                        <tr>
                            <td valign='middle' id='root' style='background:url(".SITE_URL."core/datagrid/templates/images/panel_title.gif) repeat-x'>

                                &nbsp;&nbsp;".$root_path."
                            </td>
                        </tr>
                    </table>
                ";
		if(!isset($_GET['ajax']))						
			echo $str;
	}
    /*
        // get Action Check.
        @ Paramater :
            1. $action: action in form. Default will get Action: Add, Edit, Delete
    */    
    function getActCheck( $action=''){
        if(!$action){
            $act1 = $this-> getCheckDelete();
            $act2 = $this-> getCheckPublic();
            $act3 = $this-> getCheckUnPublic();
            $result = array( $act1,    $act2,$act3    );
        }else{
            $action = strtolower( $action );
            switch( $action ){
                case 'delete_all':
                    $result = $this-> getCheckDelete();
                    break;
                case 'public_all':
                    $result = $this-> getCheckPublic();
                    break;                
                case 'unpublic_all':
                    $result = $this-> getCheckUnPublic();
                    break;
                default: $result = $this -> getCheckOther( $action );
                    break;
            }
            $result = array($result);
        }
        
        return $result;
    }
    function getCheckDelete(){
        if( $this-> checkPermission( 3 ))
        return $result = array(
                "task" => "delete_all",
                "confirm"    => "Xác nhận xóa?",
                "display" => "Xóa"
            );
        else 
            return "";
    }
    function getCheckPublic(){
        if( $this-> checkPermission( 2 ))
        return $result = array(
                "task" => "public_all",
                "confirm"    => "Xác nhận thay đổi trạng thái?",
                "display" => "Kích hoạt"
            );
        else 
            return "";
    }
    function getCheckUnPublic(){
        if( $this-> checkPermission( 2 ))
        return $result = array(
                "task" => "unpublic_all",
                "confirm"    => "Xác nhận thay đổi trạng thái?",
                "display" => "Vô hiệu"
            );
        else 
            return "";
    }
	
	/*
		// get Action .
		@ Paramater :
			1. $action: action in form. Default will get Action: Add, Edit, Delete
	*/	
	function getAct(){
        global  $oDb;
        if(($_GET["amod"] == "system_config" && $_GET["atask"] == "manage_module") || ($_GET["amod"] == "system_config" && $_GET["atask"] == "manage_task"))
        {
            $result[] = $this->getActionByID(1);
            $result[] = $this->getActionByID(2);
            $result[] = $this->getActionByID(3);
        }else
        {
            $link = "amod={$_GET['amod']}";
            if( $_GET['atask'] ) $link.= "&atask={$_GET['atask']}";
            if ($_GET['sys']) $link.="&sys={$_GET['sys']}";
            $sql = "SELECT id FROM tbl_sys_menu WHERE Link='$link'";
            $module_id = $oDb->getOne($sql);
            $sql = "SELECT roll_id FROM tbl_sys_module_roll JOIN tbl_sys_roll ON (tbl_sys_module_roll.roll_id = tbl_sys_roll.id) WHERE tbl_sys_module_roll.module_id=$module_id ORDER BY tbl_sys_roll.ordered";
            $all_roll = $oDb->getCol($sql);
            foreach($all_roll as $key => $value)
            {
                $result[] = $this->getActionByID($value);
            }
        }		
		return $result;
	}
	
	function getActionByID( $id ){
		global  $oDb;
		$sql = "select * from tbl_sys_roll where id='{$id}'";
        $roll = $oDb->getRow($sql);
		$rollId = $roll["id"];
		if( !$rollId ) return "";
		if( $this -> checkPermission( $rollId ))
			return  array(
				"task" => $roll["task"],
                "icon" => $roll["icon"],
                "tooltip" => $roll["name"],
                "confirm" => $roll['confirm']
			);
		else return "";
	}
	
	function checkPermission( $rollId ){		
		global $oDb;
		if($_GET['amod'])
		{
			$link = "amod={$_GET['amod']}";
			if( $_GET['atask'] ) $link.= "&atask={$_GET['atask']}";
			if ($_GET['sys']) $link.="&sys={$_GET['sys']}";
			$userTypeId = $_SESSION['group_id'];
			$userId = $_SESSION['userid'];
			if( $userId == 1) return  true;
			$sql = "SELECT id FROM tbl_sys_menu WHERE Link='$link'";
			$module_id = $oDb->getOne($sql);
			$sql = "SELECT RollID FROM tbl_sys_users_module WHERE ModuleID=$module_id AND UserID=$userId";
			$permission = $oDb->getCol($sql);
			
			$sql = "SELECT RollID FROM tbl_sys_groups_module WHERE ModuleID=$module_id AND GroupID=$userTypeId";
			$group_permission = $oDb->getCol($sql);
			//pre($_SESSION);
		   /*$permission = array_merge($permission,$group_permission);
			if (in_array($rollId,$permission))
				return TRUE;
			else
				return FALSE;
		}*/
		if((is_array($permission) && !empty($permission)) && (is_array($group_permission) && !empty($group_permission)))
            $permission = array_merge($permission,$group_permission);
            //pre($permission);
			if(is_array($permission) && !empty($permission))
			{
            if (in_array($rollId,$permission))
                return TRUE;
            else
                return FALSE;
			}
			else
				return TRUE;
		}
		else
        {
            return TRUE;
        } 
		
	}
	
	function showFlash( $file, $attribute = array("width" => 150, "height" => 110) ){
		$str = "<object style=\"cursor:pointer;\" classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" codebase=\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=0,0,0,0\" width=\"{$attribute['width']}px\" height=\"{$attribute['height']}px\" >
                  <param name=\"movie\" value=\"{$file}\" />
                  <param name=\"quality\" value=\"high\" />
                  <embed  style=\"cursor:pointer;\" src=\"{$file}\" quality=\"high\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" 
				  type=\"application/x-shockwave-flash\" width=\"{$attribute['width']}px\" height=\"{$attribute['height']}\" ></embed>
</object>";
		return $str;
	}
	
	function multiLevel( $table, $pkey, $parent, $sql_select = '*', $where = '', $order=''){
		$aResult = array();
		$this -> getCategoryMultiLevel($aResult, 0, 0, $table, $pkey, $parent, $sql_select, $where, $order );		
		return  $aResult;
	}
	
	function getCategoryMultiLevel( &$aRef, $parentId, $level=0, $table, $pkey, $parent, $sql_select, $where='' , $order='' ){
		global  $oDb ;
		if( $where ) $condition = " and {$where}";
		if( $order ) $condition .= " order by {$order}";
		
		if( $level == 0)
			$sql = "SELECT {$sql_select} FROM {$table} WHERE (`{$parent}` = '0' or `{$parent}` is NULL) {$condition}";
		else
			$sql = "SELECT {$sql_select} FROM {$table} WHERE `{$parent}` = '{$parentId}' {$condition}";
		//echo $sql;
		$result = $oDb -> getAll( $sql );		
		if( $result ){
			if( $level > 0)
				$aRef[count($aRef)-1]['hashchild'] = true;
			foreach ( $result as $key => $val){
				$val['level'] = $level;				
				$aRef[] = $val;				
				$this -> getCategoryMultiLevel( &$aRef, $val[$pkey], $level + 1, $table, $pkey, $parent, $sql_select, $where, $order  );
			}
		} 
	}
	function getAssocLang()
	{
		global $oDb;
		$sql = "SELECT id,name FROM lang";
		return $oDb->getAssoc($sql);
	}
	function getPrefix( $level ){
		$prefix = "&emsp;&emsp;";
		return str_repeat( $prefix, $level );
	}
	function getLangDefault()
	{
		return $this->db->getOne("SELECT id FROM lang order by isdefault desc");
	}
	
	public function crop_image($src, $name_file, $des_dir, $w=640, $h= 480, $active=true){
		
		include_once(SITE_DIR.'core/thumbnail/thumbnail.class.php');		
		$thumb=new Thumbnail($src);		
		$thumb->size($w, $h);
		$thumb->output_format= "JPG";
		
		// Get file extension
		$ext = strtoupper(end(explode(".", basename($name_file))));
		if($ext == 'SWF' || $ext == 'DOC' || $ext == 'DOCX' || $ext == 'PDF')
		move_uploaded_file($src, $des_dir."/".$name_file);
		else if($active==true && ($ext == 'JPG' || $ext == 'JPEG' || $ext == 'PNG' || $ext == 'GIF')){
			$thumb->process();
			$thumb->save($des_dir."/".$name_file);
			move_uploaded_file($src, $des_dir."/".$name_file);
		}else
			$name_file = "Please! Try again.";
		return $name_file;
	}
    
    function deleteImage($id, $field, $path){
        if($id == '')
            return;
        $sql = "SELECT $field FROM ".$this->table." WHERE id IN ($id)";
        $arr_img = $this->db->getCol($sql);
        if($arr_img && is_array($arr_img) && count($arr_img) > 0)
            foreach($arr_img as $value)
            {
                $imgpath = $path.$value;
                if(is_file($imgpath))            
                    @unlink($imgpath);
            }        
    }
}
?>