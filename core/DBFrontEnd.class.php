<?php
if (!defined('IN_VES'))
{
    die('Hacking attempt');
}
// Use for front end proccess...
class VES_FrontEnd {
	var $db;
	var $smarty;
	var $table;
	var $sql;
	var $result;
	var $limit;
	var $exchange;
	// Constructor
	function __construct ($table, $limit=5){
		global $oDb;
		global $oSmarty;
		$this->smarty = $oSmarty;
		$this->db = $oDb;
		$this->table = $table;
		$this->limit = $limit;
	}
		
	function getOne($field, $Condition = '1=1', $Order = 'ID DESC', $Limit = '1000'){
		$this->sql = "SELECT $field FROM ".$this->table." WHERE $Condition";
		$this->result = $this->db->getOne($this->sql);
		if(!$this->CheckError())
			return $this->result;
		else
			return "ERROR";
	}
	
	function getOneSQL($sql){
		$this->result = $this->db->getOne($sql);
		if(!$this->CheckError())
			return $this->result;
		else
			return "ERROR";
	}
	
	function getRow($field, $Condition = ' 1=1'){
		if($this->sql == '')
			$this->sql = "SELECT $field FROM ".$this->table." WHERE $Condition";
		else
			$this->sql = $field;
		$this->result = $this->db->getRow($this->sql);
		if(!$this->CheckError())
			return $this->result;
		else
			return "ERROR";
	}
	
	function getRowSQL($sql){
		$this->result = $this->db->getRow($sql);
		if(!$this->CheckError())
			return $this->result;
		else
			return "ERROR";
	}
	
	function getAll($field, $Condition=' 1=1 '){
		if($this->sql == '')
			$this->sql = "SELECT $field FROM ".$this->table." WHERE $Condition";
		else
			$this->sql = $field;
		
		$this->result = $this->db->getAll($this->sql);
		if(!$this->CheckError())
			return $this->result;
		else
			return "ERROR";
	}
	
	function getAllSQL($sql){
		$this->result = $this->db->getAll($sql);
		if(!$this->CheckError())
			return $this->result;
		else
			return "ERROR";
	}
	
	function getAssoc($field, $Condition){
		$this->sql = "SELECT $field FROM ".$this->table." WHERE $Condition";
		$this->result = $this->db->getAssoc($this->sql);
		if(!$this->CheckError())
			return $this->result;
		else
			return "ERROR";
	}
	
	function Paging($Limit, $field, $Condition, $PagingPath, $Order='', $custom_query = ""){
		$this->sql = "SELECT $field FROM ".$this->table;
		// Paging
		if($Condition == '')
			$Condition = "1=1";
		$page = isset($_GET['page'])?$_GET['page']:1;
		$limit = $Limit;
		$num_rows = $this->getOneSQL("SELECT count(id) from ".$this->table." WHERE $Condition");
		//pre($num_rows);
		$eu = $limit*($page-1);
		// $this->sql = "SELECT $field FROM ".$this->table." WHERE $Condition $Order LIMIT $eu, $limit";
		if($custom_flag != "")
		{
			$this->sql = "SELECT $field FROM ".$this->table." ".$custom_query." $Order LIMIT $eu, $limit";
		}
		else 
		{
			$this->sql = "SELECT $field FROM ".$this->table." WHERE $Condition $Order LIMIT $eu, $limit";
		}
		//pre($this->sql);
		$this->smarty->assign('limit',$limit);
		$this->smarty->assign('paging_path',$PagingPath);	
		if($this->table == "tbl_services_category")
			$this->smarty->assign('num_rows1', $num_rows);
		elseif($this->table == "tbl_product_item")
			$this->smarty->assign('num_rows2', $num_rows);
		elseif($this->table == "tbl_blog_item")
			$this->smarty->assign('num_rows3', $num_rows);
		elseif($this->table == "tbl_about_item")
			$this->smarty->assign('num_rows4', $num_rows);
		else
			$this->smarty->assign('num_rows', $num_rows);
		$this->getAllSQL($this->sql);
		return $this->result;
	}
	
	
	function PagingTable($Limit, $field, $Condition, $PagingPath, $Order='', $table){
		$this->sql = "SELECT $field FROM ".$table;
		// Paging
		if($Condition == '')
			$Condition = "1=1";
		$page = isset($_GET['page'])?$_GET['page']:1;
		$limit = $Limit;
		//$num_rows = $this->getOneSQL("SELECT count(id) from ".$table." WHERE $Condition");
		//pre($num_rows);
		$eu = $limit*($page-1);
		$this->sql = "SELECT $field FROM ".$table." WHERE $Condition $Order LIMIT $eu, $limit";
		//pre($this->sql);
		$this->smarty->assign('limit',$limit);
		$this->smarty->assign('paging_path',$PagingPath);	
		//$this->smarty->assign('num_rows', $num_rows);
		$this->getAllSQL($this->sql);
		return $this->result;
	}
	
	function PagingNext($Limit, $field, $Condition, $PagingPath, $Order=''){
		$this->sql = "SELECT $field FROM ".$this->table;
		// Paging
		if($Condition == '')
			$Condition = "1=1";
		$page = isset($_GET['page'])?$_GET['page']:1;
		$page += 1;
		$limit = $Limit;		
		$num_rows = $this->getOneSQL("SELECT count(id) from ".$this->table." WHERE $Condition");
		$eu = $limit*($page-1);
		$this->sql = "SELECT $field FROM ".$this->table." WHERE $Condition $Order LIMIT $eu, $limit";		
		$this->getAllSQL($this->sql);
		return $this->result;
	}
	
	// Prevent SQL Injection
	function Check($string){
		return mysql_real_escape_string($string);
	}
	// Cbeck error and show if exist
	function CheckError(){
		if(DB::isError($this->result)){
			echo '<table width="100%" border="1" bordercolor="#CCCCCC" style="border-collapse:collapse" >
				  <tr>
					<th colspan="2" scope="col">DB Error Message</th>
				  </tr>
				  <tr>
					<td width="21%">Command</td>
					<td width="79%">'.$this->sql.'</td>
				  </tr>
				  <tr>
					<td height="25">Error Message </td>
					<td>'.$this->result->getMessage().'</td>
				  </tr>
				</table>';
			return true;
		}else
			return false;
	}
    
    function get_config_vars ($var = '', $default = '') {
        global $oSmarty;
        $value = $oSmarty->get_config_vars($var);
        
        if ($value != '') {
            return (string) $value;
        } else {
            return $default;
        }
    }
}
?>
