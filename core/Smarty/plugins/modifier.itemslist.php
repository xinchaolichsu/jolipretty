<link rel="stylesheet" type="text/css" href="/lib/Smarty/templates/css/style.css">
<link rel="stylesheet" type="text/css" href="/lib/Smarty/templates/paging/style.css">
<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty lower modifier plugin
 *
 * Type:     modifier<br>
 * Name:     lower<br>
 * Purpose:  convert string to lowercase
 * @link http://smarty.php.net/manual/en/language.modifier.lower.php
 *          lower (Smarty online manual)
 * @author   Monte Ohrt <monte at ohrt dot com>
 * @param string
 * @return string
 */

/*******************************************************************************************
Credits: dezignwork.com
Desc:	 This class will will create a pagination div with all the links, page numbers/link
		 and highlighted current page. All the pagination content will be placed in a div. 
		 The style of the div can be changed.
********************************************************************************************/

// end of class

function ShowTitle($title){
	return '<div class="ves_content"><div class="ves_cat_header">'.$title.'</div><div class="ves_clear"></div><hr /><br/>';
}

function ShowItems($row, $mod){
	$str = '<div class="ves_news_content">'.
		(($row['Photo'] == '')?"":'<img src="'.$row['Photo'].'" style="border:1px solid #CCCCCC;padding:5px; margin:5px;" align="left">').
		'<div class="ves_title"><a href="/'.$mod.'/'.$row['id'].'-'._remove_marks($row['Name']).'.html">'.$row['Name'].'</a></div><br /> 
		<div class="ves_desc">'.$row['Summarise'].'</div></div><div class="ves_clear" style="clear:both"></div><div class="ves_icon_detail"><img src="/lib/Smarty/templates/images/details.gif" align="center" style="vertical-align:middle" /><a href="/'.$mod.'/'.$row['id'].'-'._remove_marks($row['Name']).'.html">Chi tiết</a></div><div class="ves_spacer"></div><div class="ves_clear"></div><div class="ves_dot_line"></div>
	<div class="ves_clear" style="clear:both"></div><div class="ves_spacer"></div>';	
	return $str;
}

function _remove_marks($string)
{
 $trans = array ('é' => 'e', '&' => '', '‘' => '', '’' => '', '“' => '', '”' => '', 'ẻ' => 'e', 'ẽ' => 'e', 'ằ' => 'a', 'ắ' => 'a', 'ọ' => 'o', 'ẽ' => 'e', 'ờ' => 'o', 'ẹ' => 'e', 'ặ' => 'a', 'ề' => 'e', 'ặ' => 'a', 'à' => 'a', 'á' => 'a', 'ả' => 'a', 'ã' => 'a', 'ạ' => 'a', 'â' => 'a', 'ấ' => 'a', 'ầ' => 'a', 'ẫ' => 'a', 'ẩ' => 'a', 'ậ' => 'a', 'ú' => 'a', 'ù' => 'u', 'ủ' => 'u', 'ũ' => 'u', 'ụ' => 'u', 'à' => 'a', 'á' => 'a', 'ô' => 'o', 'ố' => 'o', 'ồ' => 'o', 'ổ' => 'o', 'ỗ' => 'o', 'ộ' => 'o', 'ó' => 'o', 'ò' => 'o', 'ỏ' => 'o', 'õ' => 'o', 'ọ' => 'o', 'ê' => 'e', 'ế' => 'e', 'ề' => 'e', 'ể' => 'e', 'ễ' => 'e', 'ệ' => 'e', 'í' => 'i', 'ì' => 'i', 'ỉ' => 'i', 'ĩ' => 'i', 'ị' => 'i', 'ơ' => 'o', 'ớ' => 'o', 'ý' => 'y', 'ỳ' => 'y', 'ỷ' => 'y', 'ỹ' => 'y', 'ỵ' => 'y', 'ờ' => 'o', 'ở' => 'o', 'ỡ' => 'o', 'ợ' => 'o', 'ư' => 'u', 'ừ' => 'u', 'ứ' => 'u', 'ử' => 'u', 'ữ' => 'u', 'ự' => 'u', 'đ' => 'd', 'À' => 'A', 'Á' => 'A', 'Ả' => 'A', 'Ã' => 'A', 'Ạ' => 'A', 'Â' => 'A', 'Ấ' => 'A', 'À' => 'A', 'Ẫ' => 'A', 'Ẩ' => 'A', 'Ậ' => 'A', 'Ú' => 'U', 'Ù' => 'U', 'Ủ' => 'U', 'Ũ' => 'U', 'Ụ' => 'U', 'Ô' => 'O', 'Ố' => 'O', 'Ồ' => 'O', 'Ổ' => 'O', 'Ỗ' => 'O', 'Ộ' => 'O', 'Ê' => 'E', 'Ế' => 'E', 'Ề' => 'E', 'Ể' => 'E', 'Ễ' => 'E', 'Ệ' => 'E', 'Í' => 'I', 'Ì' => 'I', 'Ỉ' => 'I', 'Ĩ' => 'I', 'Ị' => 'I', 'Ơ' => 'O', 'Ớ' => 'O', 'Ờ' => 'O', 'Ở' => 'O', 'Ỡ' => 'O', 'Ợ' => 'O', 'Ư' => 'U', 'Ừ' => 'U', 'Ứ' => 'U', 'Ử' => 'U', 'Ữ' => 'U', 'Ự' => 'U', 'Đ' => 'D', 'Ý' => 'Y', 'Ỳ' => 'Y', 'Ỷ' => 'Y', 'Ỹ' => 'Y', 'Ỵ' => 'Y', 'á' => 'a', 'à' => 'a', 'ả' => 'a', 'ã' => 'a', 'ạ' => 'a', 'ă' => 'a', 'ắ' => 'a', 'ằ' => 'a', 'ẳ' => 'a', 'ẵ' => 'a', 'ặ' => 'a', 'â' => 'a', 'ấ' => 'a', 'ầ' => 'a', 'ẩ' => 'a', 'ẫ' => 'a', 'ậ' => 'a', 'ú' => 'u', 'ù' => 'u', 'ủ' => 'u', 'ũ' => 'u', 'ụ' => 'u', 'ư' => 'u', 'ứ' => 'u', 'ừ' => 'u', 'ử' => 'u', 'ữ' => 'u', 'ự' => 'u', 'í' => 'i', 'ì' => 'i', 'ỉ' => 'i', 'ĩ' => 'i', 'ị' => 'i', 'ó' => 'o', 'ò' => 'o', 'ỏ' => 'o', 'õ' => 'o', 'ọ' => 'o', 'ô' => 'o', 'ố' => 'o', 'ồ' => 'ô', 'ổ' => 'o', 'ỗ' => 'o', 'ộ' => 'o', 'ơ' => 'o', 'ớ' => 'o', 'ờ' => 'o', 'ở' => 'o', 'ỡ' => 'o', 'ợ' => 'o', 'đ' => 'd', 'Đ' => 'D', 'ý' => 'y', 'ỳ' => 'y', 'ỷ' => 'y', 'ỹ' => 'y', 'ỵ' => 'y', 'Á' => 'A', 'À' => 'A', 'Ả' => 'A', 'Ã' => 'A', 'Ạ' => 'A', 'Ă' => 'A', 'Ắ' => 'A', 'Ẳ' => 'A', 'Ẵ' => 'A', 'Ặ' => 'A', 'Â' => 'A', 'Ấ' => 'A', 'Ẩ' => 'A', 'Ẫ' => 'A', 'Ậ' => 'A', 'É' => 'E', 'È' => 'E', 'Ẻ' => 'E', 'Ẽ' => 'E', 'Ẹ' => 'E', 'Ế' => 'E', 'Ề' => 'E', 'Ể' => 'E', 'Ễ' => 'E', 'Ệ' => 'E', 'Ú' => 'U', 'Ù' => 'U', 'Ủ' => 'U', 'Ũ' => 'U', 'Ụ' => 'U', 'Ư' => 'U', 'Ứ' => 'U', 'Ừ' => 'U', 'Ử' => 'U', 'Ữ' => 'U', 'Ự' => 'U', 'Í' => 'I', 'Ì' => 'I', 'Ỉ' => 'I', 'Ĩ' => 'I', 'Ị' => 'I', 'Ó' => 'O', 'Ò' => 'O', 'Ỏ' => 'O', 'Õ' => 'O', 'Ọ' => 'O', 'Ô' => 'O', 'Ố' => 'O', 'Ổ' => 'O', 'Ỗ' => 'O', 'Ộ' => 'O', 'Ơ' => 'O', 'Ớ' => 'O', 'Ờ' => 'O', 'Ở' => 'O', 'Ỡ' => 'O', 'Ợ' => 'O', 'Ý' => 'Y', 'Ỳ' => 'Y', 'Ỷ' => 'Y', 'Ỹ' => 'Y', 'Ỵ' => 'Y','?'=>'', ' ' => '-');
	return strtr( trim($string), $trans );
}

function smarty_modifier_itemslist($title, $mod, $items, $numrows, $limit, $page=1, $path=""){
		$str = ShowTitle($title);
		if(count($items) > 0){
			// Show items
			
			foreach ($items as $k=>$val){
				$str .= ShowItems($val, $mod);
			}
			
			// Show paging module
			$objPaging = new Paging($numrows, $limit);		
			$objPaging -> set_current_page($page);
			$objPaging -> set_show_first_last();									
			if($path != ""){
				$objPaging->set_action_path($path);
			}
			$str .= "<div><center>".$objPaging->string_paging()."</center></div>";					
		}else{
			$str .= "Không tìm thấy thông tin";
		}
		$str .= "</div>";
		return $str;
	}

class Paging {
	var $num_rows;		// required vars
	var $action;
	var $show_first_last= true;
	var $parameter_name;
	
	var $link_class="paging_link_class";
	var $div_class="paging_div";
	var $current_class="paging_current_class";
	var $current_page;

	var $PAGE='Page';
	var $site_path= site_path;
	var $first_string= "First";
	var $last_string= "Last";
	var $back_string= "Back";
	var $next_string= "Next";
	var $not_found_string= "";

	function Paging($num_rows=0, $per_page=10, $action="", $parameter_name='page')
	{		
		$this->num_rows= $num_rows;
		$this->action=$action;
		$this->per_page=$per_page;
		$this->parameter_name=$parameter_name;
	}
	
	function set_not_found_string($str){
		$this->not_found_string= str;
	}
	
	function string_paging() {
		
		if(empty($this->current_page)) {
			$this->set_current_page(1);
		}
				
		
		if(!($this->num_rows>0)) {
			return $this->not_found_string;
		}
		
		$pages_required=ceil($this->num_rows/$this->per_page);
		if(!empty($this->link_class)) { $link_class=' class="'.$this->link_class.'"'; }
		if(!empty($this->div_class)) { $div_class=' class="'.$this->div_class.'"'; }
		if(!empty($this->current_class)) { $current_class=' class="'.$this->current_class.'"'; }
		$page_line="";		
		$page_line.= "<span  $div_class>";
		
		if($this->current_page%5!=0){
			$start_page= intval($this->current_page/5)*5+1;
		}else {
			$start_page= intval($this->current_page/5)*5-4;
		}
		
		$end_page=$start_page+4;
				
		
		if($end_page>$pages_required) { $end_page=$pages_required; }
		
				
		// arrow first
		// if(($this->show_first_last)&($this->current_page>5)) {
		// 	if(!empty($this->action_path)) {
		// 		$page_line.='<span class="span_a_class">&nbsp;<a href="'.str_replace("i++", "1", $this->action_path).'"><img '.$link_class.' src="/lib/paging/first.gif" title="'.$this->first_string.'" border=""></a>&nbsp;</span>&nbsp;';
		// 	} else {
		// 		$page_line.='<span class="span_a_class">&nbsp;<a href="'.$this->action."?".$this->parameter_name.'='.
		// 			 "1".'"><img '.$link_class.' src="/lib/paging/first.gif" title="'.$this->first_string.'" border="0"></a>&nbsp;</span>&nbsp;';
		// 	}
		// }
		
		// arrow back
		if($start_page>5) {
			if(!empty($this->action_path)) {
				$page_line.='<span class="span_a_class">&nbsp;<a href="'.str_replace("i++", $start_page-1, $this->action_path).'"'.$link_class.'><img '.$link_class.' src="/lib/paging/back.gif" title="'.$this->back_string.'" border="0"></a>&nbsp;</span>&nbsp;';
			} else {
				$page_line.='<span class="span_a_class">&nbsp;<a href="'.$this->action."?".$this->parameter_name.'='.
					 ($start_page-1).$link_class.'><img '.$link_class.' src="/lib/paging/back.gif" title="'.$this->back_string.'" border="0"></a>&nbsp;</span>&nbsp;';
			}
		}
		
		// pages
		for($i=$start_page; $i<=$end_page; $i++) {
			if($i>$end_page) { break; }
			if($this->current_page!=$i) {
				if(!empty($this->action_path)) {
					$page_line.='<span class="span_a_class">&nbsp;<a href="'.str_replace("i++", $i, $this->action_path).'">'.$i.'</a>&nbsp;</span>&nbsp;';
				} else {
					$page_line.='<span class="span_a_class">&nbsp;<a href="'.$this->action."?".$this->parameter_name.'='.
						 $i.'">'.$i.'</a>&nbsp;<span>';
				}
			} else {
				$page_line.='<span class="span_select_class">&nbsp;'.$i.'&nbsp;</span>&nbsp;';
			}

		} // end for
		//$page_line=substr($page_line,0,strlen($page_line)-8);
		
		// arrow next
		if($pages_required>$end_page) {
			if(!empty($this->action_path)) {
				// $page_line.='<span class="span_a_class">&nbsp;<a href="'.str_replace("i++", $end_page+1, $this->action_path).'"'.$link_class.'><img '.$link_class.' src="/lib/paging/next.gif" title="'.$this->next_string.'" border="0"></a>&nbsp;</span>';
				$page_line.='<span class="span_a_class">&nbsp;<a href="'.str_replace("i++", $end_page+1, $this->action_path).'"'.$link_class.'>Next</a>&nbsp;</span>';
			} else {
				// $page_line.='<span class="span_a_class">&nbsp;<a href="'.$this->action."?".$this->parameter_name.'='.
				// 	 ($end_page+1).$link_class.'><img '.$link_class.' src="/lib/paging/next.gif" title="'.$this->next_string.'" border="0"></a>&nbsp;</span>';
				$page_line.='<span class="span_a_class">&nbsp;<a href="'.$this->action."?".$this->parameter_name.'='.
					 ($end_page+1).$link_class.'>Next</a>&nbsp;</span>';
			}
		}

		
		// arrow last
		if(($this->show_first_last)&($pages_required>$end_page)) {
			if(!empty($this->action_path)) {
				// $page_line.='&nbsp;<span class="span_a_class">&nbsp;<a href="'.str_replace("i++", $pages_required, $this->action_path).'"'.$link_class.'><img '.$link_class.' src="/lib/paging/last.gif" title="'.$this->last_string.'" border="0"></a>&nbsp;</span>';
				$page_line.='&nbsp;<span class="span_a_class">&nbsp;<a href="'.str_replace("i++", $pages_required, $this->action_path).'"'.$link_class.'>Last</a>&nbsp;</span>';
			} else {
				// $page_line.='<span class="span_a_class">&nbsp;<a href="'.$this->action."?".$this->parameter_name.'='.
				// 	 $pages_required.'"'.$link_class.'><img '.$link_class.' src="/lib/paging/last.gif" title="'.$this->last_string.'" border="0"></a>&nbsp;</span>';
				$page_line.='<span class="span_a_class">&nbsp;<a href="'.$this->action."?".$this->parameter_name.'='.
					 $pages_required.'"'.$link_class.'>Last</a>&nbsp;</span>';
			}
		}
		
		$page_line.= '</span>';
		
		return  $page_line;
	}
	
	// mutators
	function set_action_path($path) {
		$this->action_path=$path;
	}	
	function set_link_class($class) {
		$this->link_class=$class;
	}
	function set_div_class($class) {
		$this->div_class=$class;
	}
	function set_parameter_name($name) {
		$this->parameter_name=$name;
	}
	function set_page_limit($limit) {
		$this->per_page=$limit;
	}
	function set_current_page($page) {
		$this->current_page=$page;
	}
	function set_show_first_last($b=true){
		$this->show_first_last= $b;
	}
	function set_link_string($first="First", $back="Back", $next="Next", $last="Last"){
		$this->first_string= $first;
		$this->back_string= $back;
		$this->next_string= $next;
		$this->last_string= $last;		
	}
}
?>
