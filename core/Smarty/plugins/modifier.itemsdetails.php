<link rel="stylesheet" type="text/css" href="/lib/Smarty/templates/css/style.css">
<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty lower modifier plugin
 *
 * Type:     modifier<br>
 * Name:     lower<br>
 * Purpose:  convert string to lowercase
 * @link http://smarty.php.net/manual/en/language.modifier.lower.php
 *          lower (Smarty online manual)
 * @author   Monte Ohrt <monte at ohrt dot com>
 * @param string
 * @return string
 */

/*******************************************************************************************
Credits: dezignwork.com
Desc:	 This class will will create a pagination div with all the links, page numbers/link
		 and highlighted current page. All the pagination content will be placed in a div. 
		 The style of the div can be changed.
********************************************************************************************/

// end of class

function ShowTitle($title){
	return '<div class="ves_content"><div class="ves_cat_header">'.$title.'</div><div class="ves_clear"></div><hr /><br/>';
}

function ShowItems($row){
	$str = '<div class="ves_news_content">'.
		(($row['Photo'] == '')?"":'<img src="'.$row['Photo'].'" style="border:1px solid #CCCCCC;padding:5px; margin:5px;" align="left">').'<div class="ves_title"><a href="#">'.$row['Name'].'</a></div><br /> 
		<div class="ves_desc">'.$row['Summarise'].' </div>		
		<div class="ves_clear"></div><div class="ves_spacer"></div>		
		<div class="ves_content"> '.$row['Content'].'</div></div>	
	<div class="ves_clear"></div><div class="ves_spacer"></div>
	<div class="ves_icon_detail"><img src="/lib/Smarty/templates/images/back.gif" align="center" style="vertical-align:middle" /><a href="javascript: history.go(-1)"> Quay lại</a></div>		
	<div class="ves_icon_detail"><img src="/lib/Smarty/templates/images/icon_mail.jpeg" align="center" style="vertical-align:middle" /><a href=""> Gửi tin</a></div>
	<div class="ves_icon_detail"><img src="/lib/Smarty/templates/images/icon_print.jpeg" align="center" style="vertical-align:middle" /><a href="#" onclick="window.print();"> In</a></div>
	';	
	
	return $str;
}

function smarty_modifier_itemsdetails($title, $items){
		$str = ShowTitle($title);
		
		$str .= ShowItems($items);
		
		$str .= "</div>";
		
		return $str;
	}

?>
