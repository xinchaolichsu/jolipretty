
<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty lower modifier plugin
 *
 * Type:     modifier<br>
 * Name:     lower<br>
 * Purpose:  convert string to lowercase
 * @link http://smarty.php.net/manual/en/language.modifier.lower.php
 *          lower (Smarty online manual)
 * @author   Monte Ohrt <monte at ohrt dot com>
 * @param string
 * @return string
 */

/*******************************************************************************************
Credits: dezignwork.com
Desc:	 This class will will create a pagination div with all the links, page numbers/link
		 and highlighted current page. All the pagination content will be placed in a div. 
		 The style of the div can be changed.
********************************************************************************************/

// end of class

function smarty_modifier_vncurrentdate($title){
		
		$str_search = array (
				"Mon", 
				"Tue", 
				"Wed", 
				"Thu", 
				"Fri", 
				"Sat", 
				"Sun",
				"am", 
				"pm",
				":"
			);
		$str_replace = array (
				"Thứ hai", 
				"Thứ ba", 
				"Thứ tư",
				"Thứ năm", 
				"Thứ sáu", 
				"Thứ bảy", 
				"Chủ nhật", 
				" phút, sáng", 
				" phút, chiều",
				" giờ "
			);
		$timenow  = gmdate("D, d/m/Y", time() + 7*3600);
		$timenow  = str_replace( $str_search, $str_replace, $timenow);
			
		return $timenow;
	}

?>
