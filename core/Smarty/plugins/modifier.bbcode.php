<style type="text/css">
table.type2 {
	border: none;
	background: none;
	padding: 0;
}

table.type2 th {
	background: none;
	border-top: none;
	text-align: center;
	color: #115098;
	padding: 2px 0;
}

table.type2 td {
	padding: 0;
	font-size: 1em;
}

table.type2 td.name {
	padding: 2px;
	vertical-align: middle;
}

img {
	border: 0;
}
a.button2, input.button2 {
	width: auto !important;
	padding: 1px 3px 0 3px;
	font-family: "Lucida Grande", Verdana, Helvetica, Arial, sans-serif;
	color: #000;
	font-size: 0.85em;
	background: #EFEFEF url("lib/bbcode/images/bg_button.gif") repeat-x top;
	cursor: pointer;
}

/* Alternative button */
a.button2, input.button2 {
	border: 1px solid #666666;
}

/* <a> button in the style of the form buttons */
a.button2, a.button2:link, a.button2:visited, a.button2:active {
	text-decoration: none;
	color: #000000;
	padding: 4px 8px;
}

/* Hover states */
a.button2:hover, input.button2:hover {
	border: 1px solid #BC2A4D;
	background: #EFEFEF url("lib/bbcode/images/bg_button.gif") repeat bottom;
	color: #BC2A4D;
}

/*optgroup, select {
	font-family: Verdana, Helvetica, Arial, sans-serif;
	font-size: 0.85em;
	font-weight: normal;
	font-style: normal;
	cursor: pointer;
	vertical-align: middle;
	width: auto;
	color: #000;
}*/

textarea {
	font-family: Verdana, Helvetica, Arial, sans-serif;
	font-size: 0.85em;
	width: 80%;
	padding: 2px;
	border: 1px solid #0033FF;
}

</style>

		

	<script language="javascript" src="/lib/bbcode/js/editor.js"></script>
		<script type="text/javascript">
// <![CDATA[
	var form_name = 'postform';
	var text_name = 'txtcontent';
	var load_draft = false;
	var upload = false;

	// Define the bbCode tags
	var bbcode = new Array();
	var bbtags = new Array('[b]','[/b]','[i]','[/i]','[u]','[/u]','[quote]','[/quote]','[code]','[/code]','[list]','[/list]','[list=]','[/list]','[img]','[/img]','[url]','[/url]','[flash=]', '[/flash]','[size=]','[/size]');
	var imageTag = false;

	// Helpline messages
	var help_line = {
		b: '{LA_BBCODE_B_HELP}',
		i: '{LA_BBCODE_I_HELP}',
		u: '{LA_BBCODE_U_HELP}',
		q: '{LA_BBCODE_Q_HELP}',
		c: '{LA_BBCODE_C_HELP}',
		l: '{LA_BBCODE_L_HELP}',
		o: '{LA_BBCODE_O_HELP}',
		p: '{LA_BBCODE_P_HELP}',
		w: '{LA_BBCODE_W_HELP}',
		a: '{LA_BBCODE_A_HELP}',
		s: '{LA_BBCODE_S_HELP}',
		f: '{LA_BBCODE_F_HELP}',
		e: '{LA_BBCODE_E_HELP}',
		d: '{LA_BBCODE_D_HELP}'
	}

	var panels = new Array('options-panel', 'attach-panel', 'poll-panel');
	var show_panel = 'options-panel';


// ]]>

</script>
 	 <script type="text/javascript">
				// <![CDATA[
					/**
					* Set display of page element
					* s[-1,0,1] = hide,toggle display,show
					*/
					function dE(n, s, type)
					{
						if (!type)
						{
							type = 'block';
						}
					
						var e = document.getElementById(n);
						if (!s)
						{
							s = (e.style.display == '' || e.style.display == 'block') ? -1 : 1;
						}
						e.style.display = (s == 1) ? type : 'none';
					}
					
					function change_palette()
					{
						dE('colour_palette');
						e = document.getElementById('colour_palette');
						
						if (e.style.display == 'block')
						{
							document.getElementById('bbpalette').value = 'Ẩn màu';
						}
						else
						{
							document.getElementById('bbpalette').value = 'Hiện màu';
						}
					}
		
					
				// ]]>
				</script>
<?php
	/*
 * Smarty plugin
 * ------------------------------------------------------------
 * Type:       modifier
 * Name:       bbcode2html
 * Purpose:    Converts BBCode style tags to HTML
 * Author:     Andre Rabold
 * Version:    1.4
 * Remarks:    Notice that this function does not check for
 *             correct syntax. Try not to use it with invalid
 *             BBCode because this could lead to unexpected
 *             results ;-)
 *             It seems that this function ignores manual 
 *             line breaks. IMO this can be fixed by adding 
 *             '/\n/' => "<br>" to $preg
 *
 * What's new: - Rewrote some preg expressions for more
 *               stability.
 *             - renamed CSS classes to be more generic. (Example
 *               CSS file attached.)
 *             - Support for escaped tags. Add a backslash
 *               infront of a tag if you don't want to transform
 *               it. For example: \[b]
 *
 *             Version 1.3c
 *             - Fixed a bug with <li>...</li> tags (thanks
 *               to Rob Schultz for pointing this out)
 *
 *             Version 1.3b
 *             - Added more support for phpBB2:
 *               [list]...[/list:u] unordered lists
 *               [list]...[/list:o] ordered lists
 *             
 *             Version 1.3
 *             - added support for phpBB2 like tag identifier
 *               like [b:b6a0cef7ea]This is bold[/b:b6a0cef7ea]
 *               (thanks to Rob Schultz)
 *             - added support for quotes within the quote tag
 *               so [quote="foo"]bar[/quote] does work now
 *               correctly
 *             - removed str_replace functions
 *
 *             Version 1.2
 *             - now supports CSS classes:
 *                  ng_email      (mailto links)
 *                  ng_url        (www links)
 *                  ng_quote      (quotes)
 *                  ng_quote_body (quotes)
 *                  ng_code       (source code)
 *                  ng_list       (html lists)
 *                  ng_list_item  (list items)
 *             - replaced slow ereg_replace() functions
 *             - Alterned [quote] and [code] to use CSS classes
 *               instead of HTML <blockquote />, <hr />, ... tags.
 *             - Additional BBCode tags [list] and [*] to display
 *               nice HTML lists. Example:
 *                 [list]
 *                   [*]first item
 *                   [*]second item
 *                   [*]third item
 *                 [/list]
 *               The [list] tag can have an additional parameter:
 *                 [list]   unorderer list with bullets
 *                 [list=1] ordered list 1,2,3,4,...
 *                 [list=i] ordered list i,ii,iii,iv,...
 *                 [list=I] ordered list I,II,III,IV,...
 *                 [list=a] ordered list a,b,c,d,...
 *                 [list=A] ordered list A,B,C,D,...
 *             - produces well-formed output
 *             - cleaned up the code
 * ------------------------------------------------------------
 */
function ImgDirContent($img_folder, $numPerRow = 15) //fix ngoquochoi@gmail.com
	{
		
		$ar_mili=array (
						 0 => array ( 'code' => ':)', 'smiley_url' => '13.gif', 'emotion' => 'Vui' ),
						 1 => array ( 'code' => ':(', 'smiley_url' => '18.gif', 'emotion' => 'Buồn nhỉ' ),
						 2 => array ( 'code' => ';)', 'smiley_url' => '15.gif', 'emotion' => 'Hay đáy' ),
						 3 => array ( 'code' => ':D', 'smiley_url' => '46.gif', 'emotion' => 'Duyên nhỉ' ),
						 4 => array ( 'code' => ';;)', 'smiley_url' => '12.gif', 'emotion' => 'Yêu thế' ),
						 5 => array ( 'code' => '::D', 'smiley_url' => '3.gif', 'emotion' => 'Hồn nhiên' ),
						 6 => array ( 'code' => ':x', 'smiley_url' => '7.gif', 'emotion' => 'Iu iu' ),
						 7 => array ( 'code' => ':p', 'smiley_url' => '2.gif', 'emotion' => 'Eo ôi' ),
						 
						 8 => array ( 'code' => ':-O', 'smiley_url' => '26.gif', 'emotion' => 'Không thể!' ),
						 9 => array ( 'code' => 'x-(', 'smiley_url' => '45.gif', 'emotion' => 'Đủ rồi đấy' ),
						 10 => array ( 'code' => 'X(', 'smiley_url' => '30.gif', 'emotion' => 'Cái dzì?' ),
						 11 => array ( 'code' => ':>', 'smiley_url' => '11.gif', 'emotion' => 'Hay chưa?' ), 
						 
						 12 => array ( 'code' => 'B-)', 'smiley_url' => '38.gif', 'emotion' => 'Sành điệu' ),
						 13 => array ( 'code' => ':-S', 'smiley_url' => '35.gif', 'emotion' => 'Sợ quá' ),
						 14 => array ( 'code' => ':((', 'smiley_url' => '6.gif', 'emotion' => 'Giời ơi' ), 
						 15 => array ( 'code' => ':))', 'smiley_url' => '8.gif', 'emotion' => 'hahaaa' ), 
						 16 => array ( 'code' => ':|', 'smiley_url' => '1.gif', 'emotion' => 'Không ý kiến' ), 
						 
						 17 => array ( 'code' => ':-B', 'smiley_url' => '14.gif', 'emotion' => 'Nghiêm túc nào' ), 
						 18 => array ( 'code' => '=;', 'smiley_url' => '9.gif', 'emotion' => 'Khoan đã' ),
						 19 => array ( 'code' => 'I-)', 'smiley_url' => '23.gif', 'emotion' => 'Buồn ngủ' ), 
						 20 => array ( 'code' => ':-$', 'smiley_url' => '14.gif', 'emotion' => 'Suỵt' ),
						 21 => array ( 'code' => '8-}', 'smiley_url' => '4.gif', 'emotion' => 'Ngố' ), 
						 22 => array ( 'code' => '=P~', 'smiley_url' => '20.gif', 'emotion' => 'Ngon quá' ), 
						 23 => array ( 'code' => ':-?', 'smiley_url' => '25.gif', 'emotion' => 'Xét đã' ),
						 24 => array ( 'code' => '=D>', 'smiley_url' => '28.gif', 'emotion' => 'Hoan hô' ) ,					
						 25 => array ( 'code' => ':-w', 'smiley_url' => '21.gif', 'emotion' => 'Hãy đợi đấy' ), 
						 26 => array ( 'code' => '-o<', 'smiley_url' => '19.gif', 'emotion' => 'Chúa ơi' ),
						 27 => array ( 'code' => '-X', 'smiley_url' => '10.gif', 'emotion' => 'Cẩn thận nhé' ),   
						 28 => array ( 'code' => '(tux1)', 'smiley_url' => 'tux1.gif', 'emotion' => '(tux1)' ),
						 29 => array ( 'code' => '(tux2)', 'smiley_url' => 'tux2.gif', 'emotion' => '(tux2)' ),
						 30 => array ( 'code' => 'gnu', 'smiley_url' => 'gnu.gif', 'emotion' => '(gnu)' ),
						 
						 31 => array ( 'code' => ':[', 'smiley_url' => 'msn_bat.gif', 'emotion' => ':[' ), 
						 32 => array ( 'code' => '(bah)', 'smiley_url' => 'msn_sheep.gif', 'emotion' => '(bah)' ),
						 33 => array ( 'code' => '(sn)', 'smiley_url' => 'msn_snail.gif', 'emotion' => 'sn' ), 
						 34 => array ( 'code' => '(tu)', 'smiley_url' => 'msn_turtle.gif', 'emotion' => 'tu' ),
						 35 => array ( 'code' => '(dog)', 'smiley_url' => 'msn_dog.gif', 'emotion' => 'dog' ), 
						 36 => array ( 'code' => '(b)', 'smiley_url' => 'msn_beer.gif', 'emotion' => '(b)' ), 						
						 37 => array ( 'code' => '(||)', 'smiley_url' => 'msn_bowl.gif', 'emotion' => '(||)' ),
						 38 => array ( 'code' => '(z)', 'smiley_url' => 'msn_boy.gif', 'emotion' => '(z)' ) ,					
						 39 => array ( 'code' => '(u)', 'smiley_url' => 'msn_brheart.gif', 'emotion' => '(u)' ), 						 
						 40 => array ( 'code' => '(^)', 'smiley_url' => 'msn_cake.gif', 'emotion' => '(^)' ),						 
						 41 => array ( 'code' => '(ci)', 'smiley_url' => 'msn_cigarette.gif', 'emotion' => '(ci)' ),   
						 42 => array ( 'code' => '(c)', 'smiley_url' => 'msn_coffee.gif', 'emotion' => '(c)' ),
						 43 => array ( 'code' => '(co)', 'smiley_url' => 'msn_computer.gif', 'emotion' => '(co)' ),
						 44 => array ( 'code' => '(w)', 'smiley_url' => 'msn_deadflower.gif', 'emotion' => '(w)' ),
						 
						 45 => array ( 'code' => '(y)', 'smiley_url' => 'msn_thumbup.gif', 'emotion' => '(y)' ),						 
						 46 => array ( 'code' => '(n)', 'smiley_url' => 'msn_thumbdown.gif', 'emotion' => '(n)' ),   
						 47 => array ( 'code' => '(f)', 'smiley_url' => 'msn_flower.gif', 'emotion' => '(f)' ),
						 48 => array ( 'code' => '(G)', 'smiley_url' => 'msn_gift.gif', 'emotion' => '(G)' ),
						 49 => array ( 'code' => '(i)', 'smiley_url' => 'msn_idea.gif', 'emotion' => '(i)' ),
						 
						 50 => array ( 'code' => '(st)', 'smiley_url' => 'msn_stormy.gif', 'emotion' => '(st)' ),
						 51 => array ( 'code' => '(um)', 'smiley_url' => 'msn_umbrella.gif', 'emotion' => '(um)' ),
						 52 => array ( 'code' => '(so)', 'smiley_url' => 'msn_soccer.gif', 'emotion' => '(so)' ),
						 53 => array ( 'code' => '(xx)', 'smiley_url' => 'msn_xbox.gif', 'emotion' => '(xx)' ),
						 
						 ); 
						
						foreach($ar_mili as $ar_mili)
							{
									$i ++;
										if($i % $numPerRow == 1)
											$str .= '<div>';
							
												$str .= '<a href="#" onclick="insert_text(\''.$ar_mili['code'].'\', true); return false;"><img hspace = "3" vspace="5" src = "'.$img_folder.'/'.$ar_mili['smiley_url'].'" border = 0  title="'.$ar_mili['emotion'].'"  alt="'.$ar_mili['code'].'" /></a>';
										
										if($i % $numPerRow == 0)
											$str .= '</div>';
								
										
										
							
							}			
		
		
		return $str;
	}  
function ReadImgDir($img_folder, $numPerRow = 5, /* number of images per row */ $nameOfRadio = 'name')
	{
		//use the directory class		
		
		if ($handle = opendir($img_folder)) {
					
		$imgs = dir($img_folder);
		
		 // read all files from the  directory, checks if are images and ads them to a list 
		 // (see below how to display flash banners)
		 $i = 1;
		 while ($file = $imgs->read()) {
		 	if (eregi("gif", $file) || eregi("jpg", $file) || eregi("png", $file))
			{
				if($i % $numPerRow == 1)
					$str .= '<p>';
					
				//$imglist .= "$file ";
				$str .= '<input type = "radio" value = "'.$file.'" name = "'.$nameOfRadio.'" id = "'.$nameOfRadio.'"><img hspace = "3" src = "'.$img_folder.'/'.$file.'" border = 0 />';
				
				if($i % $numPerRow == 0)
					$str .= '</p>';
				
				$i ++;

			}
			}
			closedir($handle);
		 }
		 
		 closedir($imgs->handle);		
		
		return $str;
	} 
function smarty_modifier_bbcode($message) {
		$listImgContent=ImgDirContent('/lib/bbcode/icons/emoticons');
		$output='
  <table width="100%" border="0" cellspacing="0" cellpadding="0"> 
  	<tr>
		<td>
			<div id="colour_palette" style="display: none;">
           
			  <div style="float:left">
			   <script type="text/javascript">
					colorPalette(\'h\', 15, 10);
				</script>
           		</div> 
		</td>
	</tr>       
    <tr>
		<td>
			 <input type="button" class="button2" accesskey="b" name="addbbcode0" value=" B " style="font-weight:bold; width: 30px" onclick="bbstyle(0)" title="Bold text: [b]text[/b]" />
            <input type="button" class="button2" accesskey="i" name="addbbcode2" value=" i " style="font-style:italic; width: 30px" onclick="bbstyle(2)" title="Italic text: [i]text[/i]" />
            <input type="button" class="button2" accesskey="u" name="addbbcode4" value=" u " style="text-decoration: underline; width: 30px" onclick="bbstyle(4)" title="Underline text: [u]text[/u]" />                             
            <input type="button" class="button2" accesskey="w" name="addbbcode16" value="URL" style="text-decoration: underline; width: 40px" onclick="bbstyle(16)" title="Insert URL: [url]http://url[/url] or [url=http://url]URL text[/url]" />
			  <input type="button" class="button2" accesskey="l" name="addbbcode10" value="List" style="width: 40px" onclick="bbstyle(10)" title="List: [list]text[/list]" />
			 <input type="button" class="button2" name="bbpalette" id="bbpalette" value="Hiện màu" onclick="change_palette();" title="Chọn màu: [color=red]text[/color]  Tip: you can also use color=#FF0000" />
         

		</td>
	</tr>
	<tr>	
      <td>	   
            <div style="padding-top:5px;">
              <textarea name="txtcontent" id="txtcontent" rows="10" style="width:390px;"  tabindex="3" onselect="storeCaret(this);" onclick="storeCaret(this);" onkeyup="storeCaret(this);" class="inputbox" style="border:1px solid #7f9db9"></textarea>			 
            </div>		
	  </td>
    </tr>
	<tr>
      <td>'.$listImgContent.'</td>
    </tr>
 	
  </table>	
	';
return $output;
}


?>
