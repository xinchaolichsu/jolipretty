<?php
if (!defined('IN_VES')) die('Hacking attempt');
/**
 * VietEsoft
 * ============================================================================
 * Copyright 2010.
 * Website: http://www.vietesfot.com, http://thietkewebsitetrongoi.net
 * ----------------------------------------------------------------------------
 * Phat trien boi VietEsoft
 * ============================================================================
 * Xử lý thành viên đăng nhập
*/
class customerlogin extends VES_FrontEnd
{
	var $table; 												/* Khai báo bảng dữ liệu */
	var $cust_login;											/* Mặc định là chưa đăng nhập */
	var $cust_id;												/* Customer ID */
	var $setTime;												/* Thời gian lưu thông tin đăng nhập */
	var $setCookieName;											/* Lưu thông tin đăng nhập dưới tên này */
	var $setCookieDomain;										/* Lưu domain */
	var $passwordMethod;										/* Mã hóa password theo md5 hoặc sha1, mặc định md5 */
	
	/**
	* Class Constructure
	* 
	*/
	function customerlogin()
	{
		global $oDb;
		$this->table = "tbl_customer_item";
		$this->cust_login = $_SESSION['customer_login']?true:false;
		$this->cust_id = $_SESSION['customer_id'];
		$this->setTime = 2592000; /* Lưu thông tin đăng nhập trong 1 tháng */
		$this->setCookieName = 'cusSavePass';
		$this->setCookieDomain = '';
		$this->passwordMethod = 'md5';
		
		$this->setCookieDomain = ($this->setCookieDomain == '') ? $_SERVER['HTTP_HOST'] : $this->setCookieDomain;		
		//Maybe there is a cookie?
		if ( isset($_COOKIE[$this->setCookieName]) && !$this->isCustomerId() )
		{
		  $customer = unserialize(base64_decode($_COOKIE[$this->setCookieName]));
		  $this->getLogin($customer['username'], $customer['password']);
		}
	}
	
	/**
  	* customerLogin function
  	* @param string $uname
  	* @param string $password
  	* @param bool $loadUser
  	* @return bool
	*/
	function getLogin($username, $password, $remember = false)
	{
		$username = $this->setEscape($username);
		$password = $originalPassword = $this->setEscape($password);
		switch(strtolower($this->passwordMethod)){
			case 'sha1':
				$password = "SHA1('{$password}')"; 
			break;
			case 'md5' :
				$password = "MD5('{$password}')";
			break;
		}
		$sql = "SELECT COUNT(*) FROM `{$this->table}` WHERE username = '{$username}' AND passowrd = {$password}";
		$check_cust = $oDb->getOne($sql);
		if($check_cust){
			// register session customer id
			$this->cus_id = $this->getCustomerId($username, $password);
			$_SESSION['customer_id'] = $this->cust_id;
			// register session customer login
			$this->cust_login = true;
			$_SESSION['customer_login'] = $this->cust_login;
			
			if ( $remember ){
			  $cookie = base64_encode(serialize(array('username'=>$username,'password'=>$originalPassword)));
			  setcookie($this->setCookieName, $cookie,time() + $this->setTime, '/', $this->setCookieDomain);
			}
			return true;
		} else {
			// Account không tồn tại hoặc username hoặc password không đúng
			return false;
		}
	}
	
	/**
  	* Logout function
  	* param string $redirectTo
	* @return bool
	*/
	function logout($redirectTo = '')
	{
		setcookie($this->setCookieName, '', time()-3600);
		$_SESSION['customer_login'] = false;
		unset($_SESSION['customer_id']);
		if ( $redirectTo != '' && !headers_sent()){
		   header('Location: '.$redirectTo );
		   exit();//To ensure security
		}
	}
	
	/**
  	* Produces the result of addslashes() with more safety
  	* @param string $string
  	* @return string
	*/  
	function setEscape($string)
	{
		if( get_magic_quotes_gpc() ){
            $string = stripslashes($string);
        } else {
            $string = addslashes($string);
        }
		return $string;
	}
	
	/*
	* Creates a random password. You can use it to create a password or a hash for user activation
	* param int $length
	* param string $chrs
	* return string
	*/
	function randomPassword($length=8, $chrs = '0123456789aAbBcCdDeEfFgGhHiIkKlLmMoOpPqQrRsStTuUvVwWxXyYzZ!@#$%^&*')
	{
		$password = '';
		for($i = 0; $i < $length; $i++) {
			$password .= $chrs{mt_rand(0, strlen($chrs)-1)};
		}
		return $password;
	}
	
	/**
     * Crypts a string
     *
     */
    function setCrypt($password)
    {
        switch($this->passwordMethod)
        {
            case 'md5':
				$pass_crypt = trim(md5($text_to_crypt));
			break;
            case 'sha1':
				$pass_crypt = trim(sha1($text_to_crypt));
			break;
        }
       return $pass_crypt;
    }
	
	/**
  	* Is the user an active user?
  	* @return bool
	*/
	function isActive($id)
	{
		$sql = "SELECT Active FROM {$this->table} WHERE id={$id}";
		$active = $oDb->getOne($sql);
		return $active;
	}
	
	/**
  	* Activates the user account
  	* @return bool
	*/
	function setActive()
	{
		if ( $this->isActive($this->cust_id))
			return false;
		$sql = "UPDATE {$this->table} SET Active=1 WHERE id='".$this->setEscape($this->cust_id);
		$res = $this->pearDb->query($sql);
		if (@res() == 1)
			return true;
		else
			return false;
	}
	
	/**
     * Gets logged user id if login session is true.
	 * @return bool
     */
    function isCustomerId()
    {
        if( empty($this->cust_id) )
			return false;
		else
			return true;
    }
	
	/**
	* Get customer id
  	* @param int customer id
  	* @return array
	*/  
	function getCustomerId($username, $password)
	{
		$sql = "SELECT id FROM {$this->table} WHERE username={$username} AND password={$password}";
		$customer_id = $oDb->getOne($sql);
		return $customer_id;
	}
	
	/**
  	* Get customer infomation
  	* @param int customer id
  	* @return array
	*/  
	function getCustomer($id)
	{
		$sql = "SELECT * FROM {$this->table} WHERE id={$id} AND Status=1";
		$customer = $oDb->getRow($sql);
		return $customer;
	}
}
?>
